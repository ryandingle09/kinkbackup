<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model  extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function select($sql,$param=array(),$type='assoc')
	{

	 try
		{
			$stmt = $this->db->prepare($sql);
			if(!$stmt)
			{
				print ("Could not prepare statement.\n");
				print ("errorCode: " . $this->db->errorCode () . "\n");
				print ("errorInfo: " . join (", ", $this->db->errorInfo ()) . "\n");
			}
			$stmt->execute($param);

			//$q = $this->db_office->query($sql);
				
			if($type == 'both'){
				$result = $stmt->fetch(PDO::FETCH_BOTH);
			}elseif($type == 'obj'){
				$result = $stmt->fetch(PDO::FETCH_OBJ);
			}else{
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
			}

			if(!$result)
			{
				//print ("Could not execute statements.\n");
				// print ("errorCode: " . $stmt->errorCode () . "\n");
				//print ("errorInfo: " . join (", ", $stmt->errorInfo ()) . "\n");
				$result = false;
			}
			$stmt->closeCursor();
			$stmt = null;
		}
		catch(Exception $e)
		{
				
			$e->getMessage();
				
		}

		return $result;


	}


	function select_all($sql,$param=array(),$type='assoc')
	{

	 try
		{
			$stmt = $this->db->prepare($sql);
			if(!$stmt)
			{
				print ("Could not prepare statement.\n");
				print ("errorCode: " . $this->db->errorCode () . "\n");
				print ("errorInfo: " . join (", ", $this->db->errorInfo ()) . "\n");
			}
			$stmt->execute($param);

			//$q = $this->db_office->query($sql);
			if($type == 'both'){
				$result = $stmt->fetchAll(PDO::FETCH_BOTH);
			}elseif($type == 'obj'){
				$result = $stmt->fetch(PDO::FETCH_OBJ);
			}else{
				$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			}


			if(!$result)
			{
				//print ("Could not execute statement.\n");
				//print ("errorCode: " . $stmt->errorCode () . "\n");
				//print ("errorInfo: " . join (", ", $stmt->errorInfo ()) . "\n");
				return false;
			}
			$stmt->closeCursor();
			$stmt = null;
		}
		catch(Exception $e)
		{
				
			$result=$e->getMessage();
				
		}

		return $result;


	}

	protected function  insert($sql,$param=array() )
	{
		try
		{
			$stmt = $this->db->prepare($sql);
			/*if(!$stmt)
			 {
			print ("Could not prepare statement.\n");
			print ("errorCode: " . $this->db_office->errorCode () . "\n");
			print ("errorInfo: " . join (", ", $db_office->errorInfo ()) . "\n");
			} */
			$result=$stmt->execute($param);
			if(!$result)
			{
				$err= "Could not execute statement.\n";
				$err .= "errorCode: " . $stmt->errorCode () . "\n";
				$err .= "errorInfo: " . join (", ", $stmt->errorInfo ()) . "\n";

				return $err;
				die;
			}
			$result=  $this->db->lastInsertId() ;
		}
		catch (PDOException $e)
		{
			$err = "Error!: " . $e->getMessage() . "<br/>";
			$err .= "Could not connect to server.\n";
			return $err;
			die();
		}

		return $result;

	}


	protected function  save( $sql,$param=array())
	{
		try
		{
			$stmt = $this->db->prepare($sql);
			/*if(!$stmt)
			 {
			print ("Could not prepare statement.\n");
			print ("errorCode: " . $this->db_office->errorCode () . "\n");
			print ("errorInfo: " . join (", ", $db_office->errorInfo ()) . "\n");
			} */
			$result=$stmt->execute($param);
			if(!$result)
			{
				$err= "Could not execute statement.\n";
				$err .= "errorCode: " . $stmt->errorCode () . "\n";
				$err .= "errorInfo: " . join (", ", $stmt->errorInfo ()) . "\n";

				return $err;
				die;
			}
			 
		}
		catch (PDOException $e)
		{
			$err = "Error!: " . $e->getMessage() . "<br/>";
			$err .= "Could not connect to server.\n";
			return $err;
			die();
		}

		return $result;

	}



	protected function  query( $sql,$param=array())
	{
		try
		{
			$stmt = $this->db->prepare($sql);
			if(!$stmt)
			{
				print ("Could not prepare statement.\n");
				print ("errorCode: " . $this->db_office->errorCode () . "\n");
				print ("errorInfo: " . join (", ", $db_office->errorInfo ()) . "\n");
			}
			$result=$stmt->execute($param);
			if(!$result)
			{
				$err= "Could not execute statement.\n";
				$err .= "errorCode: " . $stmt->errorCode () . "\n";
				$err .= "errorInfo: " . join (", ", $stmt->errorInfo ()) . "\n";
				
				return $err;
				die;
			}
			 
		}
		catch (PDOException $e)
		{
			$err = "Error!: " . $e->getMessage() . "<br/>";
			$err .= "Could not connect to server.\n";
			return $err;
			die();
		}

		return $result;

	}

	protected function  logs($param=array() )
	{
		try
		{
				
			$current_date_time = date('Y-m-d G:i:s');
			$userid = $this->session->userdata('userid');
			//$param = array_push($param,$current_date_time,$userid);
			$param[] =  $current_date_time;
			$param[]= $userid;
			$sql ="INSERT INTO logs (table,action,affected_id,date_submitted,submitter)
					VALUES(?,?,?,?,?)";
			$stmt = $this->db->prepare($sql);
			if(!$stmt)
			{
				print ("Could not prepare statement.\n");
				print ("errorCode: " . $this->db_office->errorCode () . "\n");
				print ("errorInfo: " . join (", ", $db_office->errorInfo ()) . "\n");
			}
			$result=$stmt->execute($param);
			if(!$result)
			{
				$err= "Could not execute statement.\n";
				$err .= "errorCode: " . $stmt->errorCode () . "\n";
				$err .= "errorInfo: " . join (", ", $stmt->errorInfo ()) . "\n";

				return $err;
				die;
			}

		}
		catch (PDOException $e)
		{
			$err = "Error!: " . $e->getMessage() . "<br/>";
			$err .= "Could not connect to server.\n";
			return $err;
			die();
		}

		return $result;

	}

}
