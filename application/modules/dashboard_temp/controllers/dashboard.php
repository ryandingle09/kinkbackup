<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	var $user_session = '';
	public function __construct()
	{
		parent::__construct();
		$this->user_session = $this->session->userdata('user_data');
		if(empty($this->user_session)){
			redirect('login');
		}
                $this->load->model('m_dashboard');
	}

	public function index()
	{
		$user_session = $this->user_session;
		// dump($this->user_session );
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];


		$scripts = 
			'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
			<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
			<script type="text/javascript" src="js/plugins/morris/raphael-min.js"></script>
			<script type="text/javascript" src="js/plugins/morris/morris.min.js"></script>
                        <script type="text/javascript" src="js/jobOrderVars.js"></script>
                        <script type="text/javascript" src="js/jOscript.js"></script>
                        <script type="text/javascript" src="js/JOtableScript.js"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					index();
				});
			</script>'; 
		$from = '01-01-'.date('Y',time());
		$to = date('m-d-Y',time());
		 
		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$this->render($folder.'/'.$page,$sidebar,$navigation,compact('scripts','from','to','nav'));
		 

	}
        
        public function MTO_cakes_uMarsh()
        {
            $data = $this->m_dashboard->get_MTO_cakes_uMarsh();
            if($data)
            {
                $date1;
                $date2;
                $time1;
                $time2;
                $jo;
                foreach ($data as $key => $row) 
                {
                    if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                        $date1 = '';
                        $time1 = '';
                    }else{
                        $date1 = date("F jS Y",strtotime($row['delivery_date']));
                        $time1 = $row['delivery_time'];
                    }
                    if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                        $date2 = '';
                        $time2 = '';
                    }else{
                        $date2 = date("F jS Y",strtotime($row['pickup_date']));
                        $time2 = $row['pickup_time'];
                    }
                    if($row['JO_no'] == ''){
                       $jo = $row['branch_code'].'-'.$row['id'];
                    }else{
                       $jo = $row['JO_no'];
                    }

                    $data[$key] = array( 
                        $row['id'],
                        $jo,
                        $row['title'],
                        $row['cake_type'],
                        $date1.''.$date2,
                        $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                        '
                            <button class="btn btn-primary">Open</button></button>
                        '
                    );
                }
            }
            else
            {
                $data=array();
            }
            echo json_encode(array('aaData'=> array_filter($data)));
            exit();
        }
        
        public function deco_cakes_uMarsh()
        {
            $data = $this->m_dashboard->get_deco_cakes_uMarsh();
            if($data)
            {
                $date1;
                $date2;
                $time1;
                $time2;
                $jo;
                foreach ($data as $key => $row) 
                {
                    if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                        $date1 = '';
                        $time1 = '';
                    }else{
                        $date1 = date("F jS Y",strtotime($row['delivery_date']));
                        $time1 = $row['delivery_time'];
                    }
                    if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                        $date2 = '';
                        $time2 = '';
                    }else{
                        $date2 = date("F jS Y",strtotime($row['pickup_date']));
                        $time2 = $row['pickup_time'];
                    }
                    if($row['JO_no'] == ''){
                       $jo = $row['branch_code'].'-'.$row['id'];
                    }else{
                       $jo = $row['JO_no'];
                    }

                    $data[$key] = array( 
                        $row['id'],
                        $jo,
                        $row['title'],
                        $row['cake_type'],
                        $date1.''.$date2,
                        $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                        '
                            <button class="btn btn-success">Done</button></button>
                        '
                    );
                }
            }
            else
            {
                $data=array();
            }
            echo json_encode(array('aaData'=> array_filter($data)));
            exit();
        }
        
        public function finishing_cakes_uMarsh()
        {
            $data = $this->m_dashboard->get_finishing_cakes_uMarsh();
            if($data)
            {
                $date1;
                $date2;
                $time1;
                $time2;
                $jo;
                foreach ($data as $key => $row) 
                {
                    if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                        $date1 = '';
                        $time1 = '';
                    }else{
                        $date1 = date("F jS Y",strtotime($row['delivery_date']));
                        $time1 = $row['delivery_time'];
                    }
                    if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                        $date2 = '';
                        $time2 = '';
                    }else{
                        $date2 = date("F jS Y",strtotime($row['pickup_date']));
                        $time2 = $row['pickup_time'];
                    }
                    if($row['JO_no'] == ''){
                       $jo = $row['branch_code'].'-'.$row['id'];
                    }else{
                       $jo = $row['JO_no'];
                    }

                    $data[$key] = array( 
                        $row['id'],
                        $jo,
                        $row['title'],
                        $row['cake_type'],
                        $date1.' '.$date2,
                        $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                        '
                            <button class="btn btn-primary">Open</button></button>
                        '
                    );
                }
            }
            else
            {
                $data=array();
            }
            echo json_encode(array('aaData'=> array_filter($data)));
            exit();
        }

        public function done_cakes_uMarsh()
        {
            $data = $this->m_dashboard->get_done_cakes_uMarsh();
            if($data)
            {
                $date1;
                $date2;
                $time1;
                $time2;
                $jo;
                foreach ($data as $key => $row) 
                {
                    if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                        $date1 = '';
                        $time1 = '';
                    }else{
                        $date1 = date("F jS Y",strtotime($row['delivery_date']));
                        $time1 = $row['delivery_time'];
                    }
                    if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                        $date2 = '';
                        $time2 = '';
                    }else{
                        $date2 = date("F jS Y",strtotime($row['pickup_date']));
                        $time2 = $row['pickup_time'];
                    }
                    if($row['JO_no'] == ''){
                       $jo = $row['branch_code'].'-'.$row['id'];
                    }else{
                       $jo = $row['JO_no'];
                    }

                    $data[$key] = array( 
                        $row['id'],
                        $jo,
                        $row['title'],
                        $row['cake_type'],
                        $date1.''.$date2,
                        $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat']
                    );
                }
            }
            else
            {
                $data=array();
            }
            echo json_encode(array('aaData'=> array_filter($data)));
            exit();
        }

}

/* End of file dashboard.php */
/* Location: ./application/modules/dashboard/controllers/dashboard.php */