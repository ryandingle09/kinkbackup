<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">Search Period From:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $from ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">Search Period To:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
	<div class="col-md-8">
    	<div class="panel panel-danger">
            <div class="panel-heading ui-draggable-handle">
                <h3 class="panel-title">Sales Report</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
                </ul>
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>Branch</th>
                            <th>Cash</th>
                            <th>Card</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Timog Makati</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Malate</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Greenhills Shaw</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Paranaque</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Direct Sales</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <div class="col-md-4 pull-right" style="margin-top: 20px">
                    <table class="table table-responsive table-bordered">
                        <tbody>
                            <tr>
                                <td>Total Cash payment</td>
                                <td>xxx</td>
                            </tr><tr>
                                <td>Total Total Card Payment</td>
                                <td>xxx</td>
                            </tr><tr>
                                <td>Total Sales</td>
                                <td>xxx</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-body">
            	<h1 class="text-center"></span> Percentage Share</h1>
                <code style="background: rgb(0, 0, 255);">&nbsp</code> - Makati<br><br>
                <code style="background: rgb(0, 128, 0);">&nbsp</code> - Greenhills Shaw<br><br>
                <code style="background: rgb(255, 0, 0);">&nbsp</code> - Malate<br><br>
                <code style="background: rgb(255, 192, 203);">&nbsp</code> - Paranaque<br><br>
                <code style="background: rgb(238, 130, 238);">&nbsp</code> - Timog<br>
                <div id="morris-donut-example" style="height: 500px;"></div>
            </div>
        </div>
    </div>
</div>