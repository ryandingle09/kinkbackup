<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">From:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $from ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">To:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="panel panel-danger">

    <div class="panel-heading">                                
        <h3 class="panel-title">Transactions - MTO</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
        </ul>                                
    </div>

    <div class="panel-body">

        <div class="panel panel-default tabs">                            
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Pickup / Delivery</a></li>
                <li><a href="#tab-third" role="tab" data-toggle="tab">Pickup at Other Branches</a></li>
            </ul>                            
            <div class="panel-body tab-content">

                <div class="tab-pane active" id="tab-first">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <!--<th>Pickup Branch</th>-->
                                <th>JO#</th>
                                <th>Item Category</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Claim Date</th>
                                <th>Claim Time</th>
                                <th>Total Amount</th>
                                <th>Balance Amount</th>
                                <th>Mode of Payment</th>
                                <th>Amount Paid</th>
                                <th>Claim Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>MKT-4536</td>
                                <td>Marshmallow Cake</td>
                                <td>xxx</td>
                                <td>xx</td>
                                <td>xxx</td>
                                <td>xxx</td>
                                <td>xxx</td>
                                <td>3000</td>
                                <td>Cash</td>
                                <td>500</td>
                                <td><button type="button" class="btn btn-success active">Claimed</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane" id="tab-third">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <!--<th>Pickup Branch</th>-->
                                <th>Pick-up Branch</th>
                                <th>JO#</th>
                                <th>Item Category</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Claim Date</th>
                                <th>Claim Time</th>
                                <th>Total Amount</th>
                                <th>Balance Amount</th>
                                <th>Mode of Payment</th>
                                <th>Amount Paid</th>
                                <th>Claim Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            	<td></td>
                                <td>TMG-4536</td>
                                <td>Marshmallow Cake</td>
                                <td>xxx</td>
                                <td>xx</td>
                                <td>xxx</td>
                                <td>xxx</td>
                                <td>xxx</td>
                                <td>3000</td>
                                <td>Cash</td>
                                <td>500</td>
                                <td><button type="button" class="btn btn-success active">Claimed</button></td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <!--
                    <div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
                        <div class="mb-container">
                            <div class="mb-middle">
                                <div class="mb-title"><span class="fa fa-times"></span> Are you sure?</div>
                                <div class="mb-content">
                                    <p>Are you sure you want to Claim?</p>
                                </div>
                                <div class="mb-footer">
                                    <button class="btn btn-success btn-lg">Yes</button>
                                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>
</div>
    <!--
    <div class="panel panel-danger">
        <div class="panel-heading">                                
            <h3 class="panel-title">Product-Transactions</h3>                            
        </div>
        <div class="panel-body">
        	<div class="col-md-3">
            	<div class="form-group" style="padding-top: 10px;padding-bottom: 20px">	
                	<label><b>Select Product Category</b></label>
                	<select class="form-control">
                    	<option>---</option>
                        <option>Available Cake</option>
                        <option>Cookies</option>
                        <option>Chocolate</option>
                        <option>Novelty</option>
                    </select>
                </div>
            </div>
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>JO#</th>
                        <th>Claim Date</th>
                        <th>Item Category</th>
                        <th>Title</th>
                        <th>Total Amount</th>
                        <th>Outstanding Balance</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
    --> 

<div class="panel panel-danger panel-toggled">
    <div class="panel-heading">                                
        <h3 class="panel-title">Job Order with Balance</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
            <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
        </ul>                                
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>JO#</th>
                    <th>Claim Date</th>
                    <th>Item Category</th>
                    <th>Title</th>
                    <th>Total Amount</th>
                    <th>Outstanding Balance</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
</div> 

<div class="panel panel-success panel-toggled">
    <div class="panel-heading">                                
        <h3 class="panel-title">Daily Sales Report</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
        </ul> 
	</div>
    <div class="panel-body">
        <div class="panel panel-danger">
            <div class="panel-heading">            

                <h3 class="panel-title">MTO</h3>

            </div>

            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>JO#</th>
                            <th>Item Category</th>
                            <th>Title</th>
                            <th>Total Amount</th>
                            <th>Mode of Payment</th>
                            <th>Amount Paid</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Sub Total</th>
                            <th>xxx</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div> 
        
        <div class="panel panel-danger">
            <div class="panel-heading">                                
                <h3 class="panel-title">Available Items</h3>                   
            </div>

            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Item</th>
                            <th>Product Name</th>
                            <th>Unit Price</th>
                            <th>Quantity</th>
                            <th>Mode of Payment</th>
                            <th>Amount Paid</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Sub Total</th>
                            <th>xxx</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div> 
    
        <div class="col-md-3 pull-right">
            <table class="table table-responsive table-bordered">
                <thead>
                    <th>Mode of Payment</th>
                    <th>Amount</th>
                </thead>
                <tbody>
                    <tr>
                        <td>Cash</td>
                        <td>500</td>
                    </tr>
                    <tr>
                        <td>Credit Card</td>
                        <td>1000</td>
                    </tr>
                    <tr>
                        <td>Total Sales Today</td>
                        <td>1500</td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
<!--
<div class="panel panel-danger panel-toggled">
    <div class="panel-heading">                                
        <h3 class="panel-title">Daily Sales Report Product</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
            <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>                                
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>Item</th>
                    <th>Product Name</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Mode of Payment</th>
                    <th>Amount Paid</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Available Cake</td>
                    <td>Dick Embrace-Choco</td>
                    <td>1,000</td>
                    <td>5</td>
                    <td>Credit Card</td>
                    <td>5,000</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
</div> 
<div class="panel2">
    <div class="panel-body">
    
    </div>
</div>
-->
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</div>
<!--
<div class="panel panel-danger">
    <div class="panel-heading">                                
        <h3 class="panel-title">Transactions - MTO</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
        </ul>                                
    </div>
    <div class="panel-body">
        <div class="panel panel-danger">  
            <div class="panel-heading"> 
                <h3 class="panel-title"> Received from Other Branches / Pick-up at Other Branches</h3>                             
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>Ordered Branch/Pick-up Branch</th>
                            <th>JO#</th>
                            <th>Item Category</th>
                            <th>Title</th>
                            <th>Order Set</th>
                            <th>Claim Type</th>
                            <th>Claim Location</th>
                            <th>Claim Date</th>
                            <th>Claim Time</th>
                            <th>Total Amount</th>
                            <th>Balance Amount</th>
                            <th>Mode of Payment</th>
                            <th>Amount Paid</th>
                            <th>Claim Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>xxx</th>
                            <th>MKT-4537</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th><button class="btn btn-info">Claimed</button></th>
                        </tr>
                        <tr>
                            <th>xxx</th>
                            <th>MKT-4537</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th><button class="btn btn-info">Claimed</button></th>
                        </tr>
                        <tr>
                            <th>xxx</th>
                            <th>MKT-4537</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th>test</th>
                            <th><button class="btn btn-info">Claimed</button></th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> 

<div class="panel panel-danger panel-toggled">
    <div class="panel-heading">                                
        <h3 class="panel-title">Job Order with Balance</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
        </ul>                                
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>JO#</th>
                    <th>Claim Date</th>
                    <th>Item Category</th>
                    <th>Title</th>
                    <th>Total Amount</th>
                    <th>Outstanding Balance</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
</div>
--> 