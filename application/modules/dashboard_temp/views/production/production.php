<!-- START TABS -->                                
<div class="panel panel-default tabs">                            
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Product Category</a></li>
        <li><a href="#tab-second" role="tab" data-toggle="tab">Item Category</a></li>
        <li><a href="#tab-third" role="tab" data-toggle="tab">Order Set</a></li>
        <li><a href="#tab-fourth" role="tab" data-toggle="tab">Inventory Items</a></li>
        <li><a href="#tab-fourth1" role="tab" data-toggle="tab">Cookies</a></li>
        <li><a href="#tab-fourth2" role="tab" data-toggle="tab">Chocolates</a></li>
        <li><a href="#tab-fourth3" role="tab" data-toggle="tab">Cake Flavors</a></li>
        <li><a href="#tab-fourth4" role="tab" data-toggle="tab">Sizes</a></li>
    </ul>                            
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab-first">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Just For Kids</th>
                                <th>All Occasion</th>
                                <th>Adult Cakes</th>
                                <th>Wedding Cakes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>200</td>
                                <td>53</td>
                                <td>54</td>
                                <td>55</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-second">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Marshmallow Icing Cake</th>
                                <th>Fondant Cake</th>
                                <th>MTO Carousel</th>
                                <th>MTO Cookies</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>200</td>
                                <td>53</td>
                                <td>54</td>
                                <td>55</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Available Cakes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>200</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth1">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Character Cookies</th>
                                <th>Adult Cookies</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>100</td>
                                <td>150</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth2">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Dick Choco</th>
                                <th>Choco Pops</th>
                                <th>Etc</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>25</td>
                                <td>35</td>
                                <td>34</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth3">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Chocomoist</th>
                                <th>Butter</th>
                                <th>Mocha</th>
                                <th>Vanilla</th>
                                <th>Choco Pound</th>
                                <th>Butter Pound</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>200</td>
                                <td>300</td>
                                <td>100</td>
                                <td>400</td>
                                <td>200</td>
                                <td>150</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth4">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Round -9" Dia
</th>
                                <th>Round - 12" Dia
</th>
                                <th>Round - 14" Dia
</th>
                                <th>Round - 18" Dia
</th>
                                <th>Round - 24" Dia 
</th>
                                <th>Regular 
</th>
                                <th>Jumbo
</th>
                                <th>Long Base
</th>
                                <th>Giant
</th>
                                <th>Etc.
</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>200</td>
                                <td>300</td>
                                <td>100</td>
                                <td>400</td>
                                <td>200</td>
                                <td>150</td>
                                <td>151</td>
                                <td>75</td>
                                <td>152</td>
                                <td>34</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-third">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Single Layer</th>
                                <th>2 Layer</th>
                                <th>3 Layer</th>
                                <th>Multi-Layer</th>
                                <th>Cupcakes</th>
                                <th>Babycakes</th>
                                <th>Character Cookies</th>
                                <th>Caricature Cookies</th>
                                <th>Adult Cookies</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>200</td>
                                <td>300</td>
                                <td>100</td>
                                <td>400</td>
                                <td>200</td>
                                <td>150</td>
                                <td>151</td>
                                <td></td>
                                <td>152</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
    </div>
</div>                                                   
<!-- END TABS --> 
<!--
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</div>-->