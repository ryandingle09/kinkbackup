<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">Search Period From:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $from ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">Search Period To:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12">
    	<div class="panel panel-danger">
            <div class="panel-heading ui-draggable-handle">
                <h3 class="panel-title">Sales Report</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
                </ul>
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>Branch</th>
                            <th>Cash</th>
                            <th>Card</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Timog Makati</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Malate</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Greenhills Shaw</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Paranaque</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Direct Sales</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <div class="col-md-4 pull-right" style="margin-top: 40px">
                    <table class="table table-responsive table-bordered">
                        <tbody>
                            <tr>
                                <td>Total Cash Payment</td>
                                <td>xxx</td>
                            </tr><tr>
                                <td>Total Card Payment</td>
                                <td>xx</td>
                            </tr><tr>
                                <td>Total Sales</td>
                                <td>xxx</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-body">
            	<h1 class="text-center"></span> Percentage Share</h1>
                <div id="morris-donut-example" style="height: 500px;"></div>
            </div>
        </div>
    </div>
    -->
    
<div class="panel panel-danger panel-toggled target">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Returned Items</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Branch</th>
                    <th>Item Category</th>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div class="col-md-4 pull-right" style="margin-top: 40px">
            <table class="table table-responsive table-bordered">
                <tbody>
                    <tr>
                        <td>Total Cash Payment</td>
                        <td>xx</td>
                    </tr>
            </table>
        </div>
    </div>
</div>
<!--
<div class="panel panel-danger panel-toggled target">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Sales</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>JO#</th>
                    <th>Item Category</th>
                    <th>Title</th>
                    <th>Order Set</th>
                    <th>Size</th>
                    <th>Prize</th>
                    <th>Amount Paid</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div class="col-md-4 pull-right" style="margin-top: 40px">
            <table class="table table-responsive table-bordered">
                <tbody>
                    <tr>
                        <td>Total Cash Payment</td>
                        <td>xx</td>
                    </tr>
            </table>
        </div>
    </div>
</div>

<div class="panel panel-danger panel-toggled target">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Production</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>JO#</th>
                    <th>Item Category</th>
                    <th>Title</th>
                    <th>Order Set</th>
                    <th>Size</th>
                    <th>Job Rendered</th>
                    <th>Recomendation</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Decor</td>
                    <td><input type="text" class="form-control" /></td>
                </tr>
            </tbody>
        </table>
        <div class="col-md-4 pull-right" style="margin-top: 40px">
            <table class="table table-responsive table-bordered">
                <tbody>
                    <tr>
                        <td>Total Cash Payment</td>
                        <td>xx</td>
                    </tr>
            </table>
        </div>
    </div>
</div>
-->