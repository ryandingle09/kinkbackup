<div id="navigation">
<a href="<?= base_url('dashboard/job_order') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'jobOrderController' ? 'active' : null ?>">

    <div class="widget widget-success widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-truck"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Job Order</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('dashboard/job_order_tracker') ?>">
<div class="col-md2-3  <?= $this->uri->rsegment(1) == 'jobOrderTrackerController' ? 'active' : null ?>">

    <div class="widget widget-danger widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-bullseye"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Job Order Tracker</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('dashboard/available_items') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'availableItemsController' ? 'active' : null ?>">

    <div class="widget widget-info widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-check"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Available Items</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('dashboard/inventory') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'inventoryController' ? 'active' : null ?>">

    <div class="widget widget-primary widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-calendar"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Inventory</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('dashboard/calendar') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'calendarController' ? 'active' : null ?>">

    <div class="widget widget-warning widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-calendar"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Calendar</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
</div>