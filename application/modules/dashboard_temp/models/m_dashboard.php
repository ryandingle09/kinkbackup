<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_dashboard extends MY_Model {
    
    public function get_MTO_cakes_uMarsh()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope IS NULL
        ';
        return $this->select_all($sql);
    }
    public function get_deco_cakes_uMarsh()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "decorating"
        ';
        return $this->select_all($sql);
    }
    public function get_finishing_cakes_uMarsh()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "finishing"
        ';
        return $this->select_all($sql);
    }
    
    public function get_done_cakes_uMarsh()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "done"
        ';
        return $this->select_all($sql);
    }

}

/* End of file m_dashboard.php */
/* Location: ./application/modules/dashboard/models/m_dashboard.php */