<div class="col-md-4">
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong><span class="fa fa-pencil"></span> Note!</strong><br> 
        Row having color <code style="background: #eee;">&nbsp;&nbsp;&nbsp;&nbsp;</code> are the priority orders
    </div>
</div>
<div class="panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab7" data-toggle="tab">Baking</a></li>
        <li><a href="#tab8" data-toggle="tab">Decorating Stage</a></li>
        <li><a href="#tab9" data-toggle="tab">Finishing Stage</a></li>
        <li><a href="#tab10" data-toggle="tab">Done</a></li>
        <!--
        <li><a href="#tab11" data-toggle="tab">MTO - Ready for Dispatch</a></li>
        <li><a href="#tab12" data-toggle="tab">MTO - Dispatch</a></li>
        -->
    </ul>
    <div class="panel-body tab-content">

        <div class="tab-pane active" id="tab7">

            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;MTO Cakes</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internalbakingmtocakes">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item Category</th>
                                <th>JO#</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Layer</th>
                                <th>Flavor</th>
                                <th>Size</th>
                                <th>Cake Upgrade</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td><button type="button" class="btn btn-default">Select</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;MTO Cookies</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internalbakingmtocookies">
                        <thead>
                            <tr>
                                <th>No.</th> 
                                <th>JO#</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Quantity</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                 <td><button type="button" class="btn btn-default">Select</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;Available Cakes</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internalbakingavailablecakes">
                        <thead>
                            <tr>
                                <th>No.</th> 
                                <th>RO#</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td><button type="button" class="btn btn-default">Select</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;Available Cookies</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internalbakingavailablecookies">
                        <thead>
                            <tr>
                                <th>No.</th> 
                                <th>RO#</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                 <td><button type="button" class="btn btn-default">Select</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;Selected Items</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>MTO Cakes</tr>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Item Category</th>
                                <th>JO#</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Layer</th>
                                <th>Flavor</th>
                                <th>Size</th>
                                <th>Double Cake</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td><button type="button" class="btn btn-default">Select</button></td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>MTO Cookies</tr>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>No.</th> 
                                <th>JO#</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Quantity</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <table class="table table-bordered">
                    	<thead>
                            <tr>
                                <th>Available Cakes</tr>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>No.</th> 
                                <th>RO#</th>
                                <th>Quantity</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <table class="table table-bordered">
                   		<thead>
                            <tr>
                                <th>Available Cookies</tr>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>No.</th> 
                                <th>RO#</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="panel panel-danger panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;Baking</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="col-md-2 pull-left">
                        <table class="table table-responsive table-bordered">
                            <tbody>
                                <tr>
                                    <td colspan="2">Baking Division</td>
                                </tr>
                                <tr>
                                    <td>Baking Order #</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-2 pull-right">
                        <table class="table table-responsive table-bordered">
                            <tbody>
                                <tr>
                                    <td>Data Of Issue</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Date Of Completion </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-responsive table-bordered">
                            <tbody>
                                <tr>
                                    <td>Flavors</td>
                                    <td>9" Diameter</td>
                                    <td>9"x13"</td>
                                    <td>12" Diameter</td>
                                    <td>12"x 16"</td>
                                    <td>14" Diameter</td>
                                </tr>
                                <tr>
                                    <td>Mocha</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Butter</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Vanilla</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Choco Moist</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-4">
                        <h5>MTO Cookies</h5>
                            <table class="table table-responsive table-bordered">
                                <tbody>
                                    <tr>
                                        <td>JO#</td>
                                        <td>Order Set</td>
                                        <td>Quantity</td>
                                    </tr>
                                    <tr>
                                        <td>xxx</td>
                                        <td>xxx</td>
                                        <td>xxx</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <h5>Available Cakes</h5>
                            <table class="table table-responsive table-bordered" id="internalbakingavailablecookies">
                                <tbody>
                                    <tr>
                                        <td>RQ#</td>
                                        <td>Order Set</td>
                                    </tr>
                                    <tr>
                                        <td>TMG-1938</td>
                                        <td>5</td>
                                    </tr>
                                    <tr>
                                        <td>MKT-8273</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td>PRQ-8735</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <h5>Available Cookies</h5>
                            <table class="table table-responsive table-bordered" id="internalbakingavailablecookies">
                                <tbody>
                                    <tr>
                                        <td>JO No.</td>
                                        <td>Order Set</td>
                                    </tr>
                                    <tr>
                                        <td>TMG-1938</td>
                                        <td>5</td>
                                    </tr>
                                    <tr>
                                        <td>MKT-8273</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td>PRQ-8735</td>
                                        <td>10</td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right"><span class="fa fa-print"></span> Print</button>
                    </div>
                </div>
            </div>

        </div>


        <div class="tab-pane" id="tab8">
            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;MTO Cakes</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internaldecomtocakes">
                        <thead>
                            <tr>
                            	<th>No.</th>
                                <th>JO#</th>
                                <th>Item Category</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Served By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    Username
                                </td>
                            </tr>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    <button class="btn btn-info">Open</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="panel panel-danger panel-toggled target">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;MTO Cookies</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internaldecomtocookies">
                        <thead>
                            <tr>
                            	<th>No.</th>
                                <th>JO#</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Quantity</th>
                                <th>Balance</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Quantity Produced</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;Available Cakes</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internaldecoavailablecakes">
                        <thead>
                            <tr>
                            	<th>No.</th>
                                <th>RQ#</th>
                                <th>Product Name</th>
                                <th>Flavor</th>
                                <th>Served By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    Username
                                </td>
                            </tr>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    Username
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;Available Cookies</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internaldecoavailablecookies">
                        <thead>
                            <tr>
                            	<th>No.</th>
                                <th>RQ#</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Served By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    Username
                                </td>
                            </tr>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    Username
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div class="tab-pane" id="tab9">
            
            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;MTO</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internalfinishingmtocakes">
                        <thead>
                            <tr>
                                <th>JO#</th>
                                <th>Item Category</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Served By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    Username
                                </td>
                            </tr>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    <button class="btn btn-info">Open</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;Available Cakes</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internalfinishingmtocookies">
                        <thead>
                            <tr>	
                            	<th>No.</th>
                                <th>RQ#</th>
                                <th>Product Name</th>
                                <th>Flavor</th>
                                <th>Served By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    Username
                                </td>
                            </tr>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    Username
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="tab-pane" id="tab10">

            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;MTO Cakes</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internaldonemtocakes">
                        <thead>
                            <tr>
                            	<th>No.</th>
                                <th>JO#</th>
                                <th>Item Category</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Decorator</th>
                                <th>Artist</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                 <td><button type="button" class="btn btn-primary">Checked</button></td>
                            </tr>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                 <td><button type="button" class="btn btn-primary">Checked</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            <div class="panel panel-danger panel-toggled target">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;MTO Cookies</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internaldonemtocookies">
                        <thead>
                            <tr>
                            	<th>No.</th>
                                <th>JO#</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Quantity</th>
                                <th>Balance</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Quantity Produced</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td><button type="button" class="btn btn-primary">Checked</button></td>
                            </tr>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td><button type="button" class="btn btn-primary">Checked</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;Available Cakes</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internaldoneavailablecakes">
                        <thead>
                            <tr>
                            	<th>No.</th>
                                <th>RQ#</th>
                                <th>Product Name</th>
                                <th>Flavor</th>
                                <th>Decoratot</th>
                                <th>Artist</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>
                                    Checked
                                </td>
                            </tr>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td><button type="button" class="btn btn-primary">Checked</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-danger target panel-toggled">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;Available Cookies</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable" id="internaldoneavailablecookies">
                        <thead>
                            <tr>
                            	<th>No.</th>
                                <th>RQ#</th>
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Flavor</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                 <td><button type="button" class="btn btn-primary">Checked</button></td>
                            </tr>
                            <tr>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                 <td><button type="button" class="btn btn-primary">Checked</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

		<!--
        <div class="tab-pane" id="tab11">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>JO#</th>
                        <th>Item Category</th>
                        <th>Title</th>
                        <th>Claim Type</th>
                        <th>Location</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td><button class="btn btn-info">Select</button></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td><button class="btn btn-info">Select</button></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td><button class="btn btn-info">Select</button></td>
                    </tr>
                </tbody>
            </table>
        </div>   

        <div class="tab-pane" id="tab12">
            <div class="panel panel-danger target">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">&nbsp;MTO</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="row" style="margin-bottom: 20px;margin-top: 20px">
                        <div class="col-md-4 pull-left">
                            <h5>Delivery Order No :&nbsp;&nbsp;&nbsp;<b style="border-bottom: 1px solid #ccc">0002</b>&nbsp;&nbsp;&nbsp;Driver :&nbsp;&nbsp;<b style="border-bottom: 1px solid #ccc">_________________</b></h5>
                        </div>
                        <div class="col-md-1 pull-right">
                            <h5>Date :&nbsp;&nbsp;&nbsp;<b style="border: 1px solid #ccc">12/12/09</b></h5>
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>JO#</th>
                                <th>Item Category</th>
                                <th>Title</th>
                                <th>Claim Type</th>
                                <th>Location</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Rec. By</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>test</td>
                                <td>test</td>
                                <td>test</td>
                                <td>test</td>
                                <td>test</td>
                                <td>test</td>
                                <td>test</td>
                                <td>test</td>
                                <td>test</td>
                                <td>test</td>
                            </tr>
                        </tbody>
                    </table>
                    <button class="btn btn-primary pull-right"><span class="fa fa-print"></span> Print Delivery Order Form</button>
                </div>
            </div>
        </div>
        -->
    </div>
</div> 
<!-- simple sample css and js print style for specific content
<style type="text/css">
	@media print {
	  body * {
		visibility: hidden;
	  }
	  #dispatched-record .rm-on-print{
		display: none; 
	  }
	  #dispatched-record, #dispatched-record * {
		visibility: visible;
	  }
	  #dispatched-record {
		position: absolute;
		left: -50;
		top: -50;
		width: 100%;
	  }
	}
</style>
<script type="text/javascript">
/*$(document).ready(function(){
	$(".print-me").click(function(){
		$(".rm-on-print").remove();
	});
});*/
function printDiv(divName) {
	 var printContents = document.getElementById(divName).innerHTML;
	 var originalContents = document.body.innerHTML;
	 document.body.innerHTML = printContents;

	 window.print();

	 document.body.innerHTML = originalContents;
}
</script>
-->
<!--
<div class="panel panel-info">
	<div class="panel-heading">
    	<h3 class="panel-title">Ready For Dispatch/Deliver</h3>
        <button class="btn btn-primary pull-right print-me" onblur="printDiv('dispatched-record')">Print Now</button>
    </div>
    <div class="panel-body" id="dispatched-record">
    	<div class="row">
        	<div class="col-md-3 pull-left">
            	<table class="table table-responsive table-bordered">
                	<thead>
                    	<th>Delivery Order #</th>
                        <th>Driver Name</th>
                    </thead>
                    <tbody>
                    	<td>0001</td>
                        <td>Fill UP</td>
                    </tbody>
                </table>
            </div>
            <div class="col-md-3 pull-right">
            	<table class="table table-responsive table-bordered">
                	<thead>
                    	<th>Date</td>
                    	<td>28-Feb-15</td>
                    </thead>
                </table>
            </div>
            <div class="col-md-12">
            	<table class="table table-responsive table-bordered">
                	<thead>
                    	<th>JO#</th>
                        <th>Item Category</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Claim Type</th>
                        <th>Claim Location	</th>
                        <th>Target Time</th>
                        <th>Received in good order by:	</th>
                        <th class="rm-on-print">Action</th>
                    </thead>
                    <tbody>
                    	<td>MKT-4537</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>Delivery</td>
                        <td>Caloocan</td>
                        <td></td>
                        <td></td>
                        <td class="rm-on-print">
                        	<button class="btn btn-cancel">Cancel</button>
                        </td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
-->