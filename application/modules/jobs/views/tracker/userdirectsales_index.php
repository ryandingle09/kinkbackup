<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab8" data-toggle="tab">Job Request</a></li>
        <li><a href="#tab9" data-toggle="tab">On Progress</a></li>
        <li><a href="#tab10" data-toggle="tab">Done</a></li>
    </ul>
    
    <div class="panel-body tab-content">
    
        <div class="tab-pane active" id="tab8">
        
            <div class="panel panel-danger">
                <div class="panel-body">
                    <h3 class="push-down-20">Job Request</h3>
                    <table class="table datatable" id="StoreSalesjobrequestOnTracker">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>JO#</th>
                                <th>Sales</th>
                                <th>Item</th>
                                <th>title</th>
                                <th>Order Set</th>
                                <th>Claim Type</th>
                                <th>Claim Location</th>
                                <th>Claim Date</th>
                                <th>Claim Time</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            
        </div>
        
        <div class="tab-pane" id="tab9">
        
            <div class="panel panel-danger">
                <div class="panel-body">
                    <h3 class="push-down-20">On Progress</h3>
                    <table class="table datatable" id="StoreSalesonProgessOnTracker">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>JO#</th>
                                <th>Sales</th>
                                <th>Item</th>
                                <th>title</th>
                                <th>Order Set</th>
                                <th>Claim Type</th>
                                <th>Claim Location</th>
                                <th>Claim Date</th>
                                <th>Claim Time</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        
        <div class="tab-pane" id="tab10">
        
            <div class="panel panel-danger">
                <div class="panel-body">
                    <h3 class="push-down-20">Done</h3>
                    <table class="table datatable" id="StoreSalesonDoneTracker">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>JO#</th>
                                <th>Sales</th>
                                <th>Item</th>
                                <th>title</th>
                                <th>Order Set</th>
                                <th>Claim Type</th>
                                <th>Claim Location</th>
                                <th>Claim Date</th>
                                <th>Claim Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            
        </div>
        
	</div>
</div>