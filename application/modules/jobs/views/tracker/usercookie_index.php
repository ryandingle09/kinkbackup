<div class="clearfix"></div>
<div class="col-md-4">
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong><span class="fa fa-pencil"></span> Note!</strong><br> 
        Row having color <code style="background: #eee;">&nbsp;&nbsp;&nbsp;&nbsp;</code> are the priority orders
    </div>
</div>
<div class="panel panel-danger">
    <div class="panel-heading">                                
        <h3 class="panel-title">MTO Cookies</h3>                           
    </div>
    <div class="panel-body">
        <table class="table datatable" id="trackercookies">
            <thead>
                <tr>
                    <th>No</th>
                    <th>JO#</th>
                    <th>Title</th>
                    <th>Order Set</th>
                    <th>Quantity</th>
                    <th>Target Date</th>
                    <th>Target Time</th>
                    <th>Action</th>
                    <th>Quantity Produce</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th><button class="btn btn-info">Start</button>&nbsp;<button class="btn btn-success">Done</button></th>
                    <th><input type="text" class="form-control" /></th>
                    <th>test</th>
                </tr>
                <tr style="background: #eee;">
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th><button class="btn btn-info">Start</button>&nbsp;<button class="btn btn-success">Done</button></th>
                    <th><input type="text" class="form-control" /></th>
                    <th>test</th>
                </tr>
            </tbody>
        </table>
    </div>
</div> 