<?php
//temporary test get datas
$addons = $this->m_jobs->get_addons();
$branches = $this->m_jobs->get_branches();
$cake_design = $this->m_jobs->get_cake_design();
$cake_type = $this->m_jobs->get_cake_type();
$discount = $this->m_jobs->get_discount();
$flavor = $this->m_jobs->get_flavors();
$icing_type = $this->m_jobs->get_icing_type();
$location = $this->m_jobs->get_locations();
$size = $this->m_jobs->get_sizes();
$occassion = $this->m_jobs->get_occassions();
$catalogue_categories = $this->m_jobs->get_catalogue_categories();
$cake_upgrade = $this->m_jobs->get_cake_upgrade();
$collection = $this->m_jobs->get_collection();
?>
<?=form_open_multipart('jobs/ajax_function/insert','class="add-item" data-identity="catalogue" id="wizard-validation" data-url="'.current_url().'"');?>
<input type="text" name="identity" value="catalogue" style="display: none" />
<input type="hidden" name="<?=$this->security->get_csrf_token_name(); ?>" value="<?=$this->security->get_csrf_hash(); ?>" />
<div id="message"></div>
<div class="row grid-fluid">
    <?php error_reporting(0);
     ?>
    <a href="<?=base_url();?>jobs/order"><button type="button" class="btn btn-lg btn-default">Customized Designs</button></a>
    <a href="<?=base_url();?>jobs/order/catalogue"><button type="button" class="btn btn-lg btn-success">Catalogue Designs</button></a>
    <div class="tab-pane catalogue-content" id="tab9">
	<div class="col-md-12">
            <div class="wizard wizard1 show-submit wizard-validation">                                
                <ul>
                    <li>
                        <a href="#step-5">
                            <span class="stepNumber">1</span>
                            <span class="stepDesc">Customer Details<br /><small>&nbsp;</small></span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-6">
                            <span class="stepNumber">2</span>
                            <span class="stepDesc">Occasion Details<br /><small>&nbsp;</small></span>
                        </a>
                    </li> 
                    <li>
                        <a href="#step-11">
                            <span class="stepNumber">3</span>
                            <span class="stepDesc">Product Selection<br /><small>&nbsp;</small></span>
                        </a>
                    </li> 
                    <li>
                        <a href="#step-7">
                            <span class="stepNumber">3</span>
                            <span class="stepDesc">Order Details<br /><small>&nbsp;</small></span>
                        </a>
                    </li> 
                    <li>
                        <a href="#step-8">
                            <span class="stepNumber">4</span>
                            <span class="stepDesc">Collection Details<br /><small>&nbsp;</small></span>
                        </a>
                    </li>  
                    <li>
                        <a href="#step-9">
                            <span class="stepNumber">5</span>
                            <span class="stepDesc">Discounts<br /><small>&nbsp;</small></span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-10">
                            <span class="stepNumber">6</span>
                            <span class="stepDesc">Summary<br /><small>&nbsp;</small></span>
                        </a>
                    </li> 
                </ul>

                <div id="step-5">   

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>
                                <span class="fa fa-user"></span> Customer Details For New Customers &nbsp;
                                <button type="button" class="btn btn-info open-modal" data-modal="#myModalexu" data-identity="get_customers_from_list"><span class="fa fa-user"></span> Click Here For Existing Customers</button>
                            </h3>
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <input type="text" name="firstname" class="form-control firstname live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Last Name</label>
                                <input type="text" name="lastname" class="form-control lastname live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Contact Number</label>
                                <input type="text" name="contact" class="form-control contact live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email Address</label>
                                <input type="email" name="email" class="form-control email live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input type="text" name="address" class="form-control address live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Location</label>
                                <input type="text" name="location" placeholder="ex. Quezon City" class="form-control location live-change" data-identity="des">
                            </div>
                        </div>                              
                    </div>                                    

                </div>
                <div id="step-6">

                    <div class="panel panel-default">
                        <div class="panel-body" style="height: 216px">
                        <h3><span class="fa fa-star"></span> Occasion Details</h3>
                            <div class="form-group col-md-6">
                                <label>Occasion</label>
                                <select name="occasion" class="form-control occasion get-item-dropdown" data-identity="celebrant">
                                    <option value="">Select Occassion</option>
                                    <?php foreach($occassion as $row){?>
                                    <option value="<?=$row['occassion_type'];?>"><?=$row['occassion_type'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="form-group col-md-6 oc">
                                <label>Occasion Date</label>
                                <input type="text" name="occasion_date" class="form-control datepicker occasion_date">                                            
                            </div>
                        </div>                              
                    </div>                                                                  

                </div> 

                <div id="step-11">
                    
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3><span class="fa fa-cart"></span> Order Details</h3>
                            <div class="form-group col-md-6">
                                <label>Catalogue Design</label>
                                <select name="category" class="form-control category get-item-dropdown" data-identity="catalogue_products">
                                    <option value="" selected="selected">Select</option>
                                    <?php foreach($catalogue_categories as $row){?>
                                    <option value="<?=$row['id'];?>"><?=$row['category_name'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="col-md-12" style="height: 575px; overflow: auto">
                                <table class="table table-responsive table-bordered catalogue_design_table" style="display: none;">
                                </table>
                                <div class="catalogue_design_table_false" style="display: none"></div>
                                <div class="onclass"></div>
                                <input type="text" name="p_count" id="all_p_count" style="display: none">
                            </div>
                        </div>                              
                    </div>
                    
                </div>
                
                <div id="step-7">
                    
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6">
                                        <label>Specification (Minor Changes if any)</label>
                                        <textarea name="specification" class="form-control"></textarea>
                                    </div>
                                     <div class="form-group col-md-6">
                                         <label>Cake message</label>
                                         <textarea name="cake_message" class="form-control"></textarea>
                                     </div>
                                </div>
                                <div class="col-md-12 selected_p" style="padding-top: 20px">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Name</th>
                                                        <th>Icing Type</th>
                                                        <th>Size</th>
                                                        <th>Flavor</th>
                                                        <th>Size</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="p_info">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                              
                    </div>
                    
                </div>


            <div id="step-8">

                <div class="panel panel-default">
                    <div class="panel-body" style="height: 531px">
                    <h3><span class="fa fa-shopping-cart"></span> Collection Details</h3>
                    <div class="form-group">
                        <label>Claim Type</label>
                        <select name="claim_type" class="form-control claim_type specialselect pickup_branch get-item-dropdown" data-identity="collection_type">
                            <option value="">Select Claim Type</option>
                            <?php foreach($collection as $row){;?>
                            <option value="<?=$row['collection_type'];?>"><?=$row['collection_type'];?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="dp"></div> 
                </div>                                                 
            </div>                                                                        

        </div>  

        <div id="step-9">                                                                                                         
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Select Discount Type</label>
                                <select name="discount_type" class="form-control specialselect discount_type get-item-dropdown" data-identity="discount">
                                    <option value="">None</option>
                                    <?php foreach($discount as $row){?>
                                    <option value="<?=$row['name'];?>"><?=$row['description'];?></option>
                                    <?php }?>
                                </select>
                            </div>

                            <div id="co" class="panel-body hide special">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>C/O by :</label>
                                        <input name="co_by" type="text" class="form-control co_by"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Discount %</label>
                                        <input name="discount" type="text" class="form-control discount live-change" data-identity="co_discount" />
                                    </div>
                                </div>
                            </div>

                            <div id="promo" class="panel-body hide special">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Promo No.</label>
                                        <input name="promo_no" type="text" class="form-control promo_no live-change" data-identity="promo" />
                                    </div>
                                    <div class="form-group">
                                        <label>Details</label>
                                        <input type="text" name="promo_detail" value="Freee 12 pcs cookies" disabled="disabled"  class="form-control promo_detail" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>   
        
        <div id="step-10">                                                                                                         
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row col-md-12">
                        <div class="col-md-4 col-md-offset-2">
                            <table class="table table-bordered">
                                <!--<thead>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td>Amount</td>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>-->
                                <thead>
                                    <tr>
                                        <th colspan="3">Order Summary</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Title Code: </td>
                                        <td colspan="2" class="s_code"></td>
                                        <td class="s_amount" style="text-align: right"></td>
                                    </tr>
                                    <tr>
                                        <td>Icing Type: </td>
                                        <td colspan="2" class="s_icing"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Size: </td>
                                        <td colspan="2" class="s_size"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Flavor: </td>
                                        <td colspan="2" class="s_flavor"></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <!--<tr>
                                        <th>Priority Charge: </th>
                                        <th colspan="3" style="text-align: right" class="p_charge"></th>
                                    </tr>-->
                                    <tr class="del_charge" style="display: none">
                                        <th colspan="3">Delivery Charge: </th>
                                        <th style="text-align: right" class="d_charge"></th>
                                    </tr>
                                    <tr>
                                        <th colspan="3">Sub Total: </th>
                                        <th style="text-align: right" class="sub_total_amount"></th>
                                    </tr>
                                    <tr class="dis_charge" style="display: none">
                                        <th colspan="4">Less: Discount</th>
                                    </tr>
                                </thead>
                                <tbody class="dis_class" style="display: none">
                                </tbody>
                                <thead>
                                    <tr>
                                        <th colspan="3">Total Amount: </th>
                                        <th style="text-align: right" class="order_total_amount"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="4">Payment: </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="2">Payment Type:</td>
                                        <td colspan="2">
                                            <select class="form-control payment_type" name="payment_type">
                                                <option selected="selected" value="">Select</option>
                                                <option value="Full Payment">Full Payment</option>
                                                <option value="Down Payment">Down Payment</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Mode of Payment:</td>
                                        <td colspan="2">
                                            <select class="form-control payment_mode" name="payment_mode">
                                                <option selected="selected" value="">select</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Card">Card</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Amount to be paid:</td>
                                        <td><input name="amount_to_be_paid" type="text" class="form-control amount_to_paid amount_to_be_paid live-change"  data-identity="payment_catalogue"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Amount Recieved:</td>
                                        <td><input name="amount_recieved" type="text" placeholder="Put 0 if Card or Exact Amount Paid by Customer" class="form-control amount_recieved live-change" data-identity="change"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Change:</td>
                                        <td class="show_change"><!--<input name="amount_change" type="text" class="form-control amount_change">--></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Balance:</td>
                                        <td class="show_balance"><!--<input name="amount_balance" type="text" class="form-control amount_balance">--></td>
                                    </tr>
                                </tbody>
                            </table>
                            <!--
                            <div clas="form-group">
                                <h4 style="padding: 20px;border: 1px solid #ccc">Total Amount : <b class="pull-right show_total">0.00</b></h4>
                            </div>
                            <div clas="form-group">
                                <label>Amount to be paid</label>
                                <input type="text" class="form-control live-change amount_to_paid" name="amount_to_paid" data-identity="payment">
                            </div>
                            <div clas="form-group">
                                <label>Payment Type</label>
                                <select class="form-control select payment_type" name="payment_type">
                                    <option selected="selected" value="">Select</option>
                                    <option value="Full">Full</option>
                                    <option value="Down Payment">Down Payment</option>
                                </select>
                            </div>
                            <div clas="form-group">
                                <label>Mode of payment</label>
                                <select class="form-control select payment_mode" name="payment_mode">
                                    <option selected="selected" value="">select</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Card">Card</option>
                                </select>
                            </div>
                            <div clas="form-group">
                                <h4 style="padding: 20px;border: 1px solid #ccc;margin-top: 15px">Balance : <b class="pull-right show_balance">0.00</b></h4>
                            </div>
                             <div clas="form-group">
                                <h4 style="padding: 20px;border: 1px solid #ccc;margin-top: 15px">Change : <b class="pull-right show_change">0.00</b></h4>
                            </div>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
        </div>
        </div>
    </div>
</div> 

<div class="selected-product" style="display:none">
    <div class="form-group data">
        <input name="product_id" type="text" class="product_id" placeholder="product_id">
        <input name="product_code" type="text" class="product_code" placeholder="product_code">
        <input name="product_name" type="text" class="product_name" placeholder="product_name">
        <input name="product_icing" type="text" class="product_icing" placeholder="product_icing">
        <input name="product_size" type="text" class="product_size" placeholder="product_size">
        <input name="product_flavor" type="text" class="product_flavor" placeholder="product_flavor">
        <input name="product_price" type="text" class="product_price" placeholder="product_price">
        <input name="cake_quantity" type="text" class="cake_quantity" placeholder="cake_quantity">
        <input name="icing_price" type="text" class="icing_price" placeholder="icing_price">
        <input name="flavor_price" type="text" class="flavor_price" placeholder="flavor_price">
        <input name="size_price" type="text" class="size_price" placeholder="size_price">
    </div>
</div><br>
<div class="real-datas" style="display:none">
    <br>
    Priority Charge : <input name="priority_charge" class="priority_charge" type="int" value="0" placeholder="show priority_charge if has priority_charge"><br>
    Delivery Charge : <input name="delivery_charge" class="delivery_charge" type="int" value="0" placeholder="show delivery charge if delivery"><br>
    Discount :<input name="discount_value" class="discount_value" type="int" value="0" placeholder="show discount if has discount"> <br>
    Product Price :<input name="total_amount" class="total_amount" type="int" value="0" placeholder="show total amount or purchase products"> <br>
    Amount Paid : <input name="amount_paid" class="amount_paid" type="int" value="0" placeholder="show amount to be paid"> <br>
    Amount Change : <input name="amount_change" class="amount_change" type="int" value="0" placeholder="show if customer has change amount"> <br>
    Amount balance : <input name="amount_balance" class="amount_balance" type="int" value="0" placeholder="show if customer has balance amount"> <br>
    sub_total : <input name="sub_total" class="sub_total" type="int"  value="0" placeholder="Sub Total"> <br>
    Total Price : <input name="total_price_amount" class="total_price_amount" type="int"  value="0" placeholder="total price"> <br>
    Priority :<input name="priority" type="text" class="priority" value="" placeholder="If  has Priority Charge based on item cake">
</div>
<!--
<div class="col-md-3 push-up-30">
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr>
                        <th width="100">Item</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th width="200"></th>
                    </tr>
                </thead>
                <tbody>                                            
                    <tr>
                        <td>J.O#mel-0003</td>
                        <td>1</td>
                        <td>6,100.00</td>
                        <td>
                            <button type="button" class="btn btn-primary col-sm-6">Edit</button>
                            <button type="button" class="btn btn-danger col-sm-6">Delete</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>                              
    </div>  
</div>
-->
<div class="modal modal-data" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Details</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
        </div>
    </div>
</div>

<div class="modal modal-data" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Prices</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           </div>
        </div>
    </div>
</div>
        
<div class="modal print-data" id="modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="exampleModalLabel">Order Summary</h4>
            </div>
            <div class="modal-body">
                <div class="container_fluid">
                     <div class="col-md-12" style="text-align: left">';
                         
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="text-center">Kinkcakes</h1>
                                <b><p>0979700<br>Makati City</p></b>
                                <b><p><?=date('Y / m / d');?></p></b>
                            </div>
                        </div>
                         <div style="text-align: left">
                             <table style="text-align: left" class="table table-responsive">
                                <thead>
                                    <th colspan="2">Order Summary </th>
                                    <th>Amount</th>
                                </thead>
                                <tbody class="order_summarys">
                                </tbody>
                                <tbody class="order_summarys2">
                                </tbody>
                                <tbody class="cake_upgrade">
                                </tbody>
                                <thead class="charges">
                                </thead>
                                <thead class="subtotals">
                                </head>
                                <thead class="discounts_name">
                                </thead>
                                <tbody class="discounts">
                                </tbody>
                                <thead class="tmt">
                                </thead>
                                <thead class="ors">
                                </thead>
                            </table>
                         </div>
                        <div class="available_items" style="text-align: left"></div>
                        <div class="jo_datas" style="text-align: left"></div>
                   </div>
                </div>
            </div>
            <div style="padding: 20px">
                <button type="button" class="btn btn-default save-print" data-stat="0" data-url="<?=current_url();?>">Save and Close <span class="fa fa-save"></span></button>
                <button type="button" class="btn btn-info print-res">Print As Customer Reciept <span class="fa fa-print"></span></button>
           </div>
        </div>
    </div>
</div>


<div class="modal print-data" id="myModalexu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 65%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Find Existing user</h4>
            </div>
            <div class="modal-body">
                <div class="container_fluid">
                    <input type="text" value="" class="selected_customer" name="selected_customer" style="display:  none">
                    <input type="text" value="" class="must_1" name="must_1" style="display:  none">
                    <div class="result">
                        <table class="table datatable" id="customer_select">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Firstname</th>
                                    <th>lastname</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Location</th>
                                    <th>Contact No.</th>
                                    <th>Buy Count</th>
                                    <th>Customer Since</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="div-me-lol">
                <button type="button" class="btn btn-primary close-modal" data-identity="get_customers_from_list" data-modal="myModalexu" data-dismiss="modal" style="margin-top: 50px;margin-bottom: 20px;margin-left: 20px">Close</button>
           </div>
        </div>
    </div>
</div>
        
<!-- BLUEIMP GALLERY -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides" style="margin-top: 50px"></div>
    <h3 class="title"></h3>
    <!--<a class="prev">‹</a>
    <a class="next">›</a>-->
    <a class="close">×</a>
    <!--<a class="play-pause"></a>
    <ol class="indicator"></ol>-->
</div>     
<!-- END BLUEIMP GALLERY -->


<?=form_close();?>