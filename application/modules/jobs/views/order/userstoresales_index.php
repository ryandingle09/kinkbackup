<?php
//temporary test get datas
$addons = $this->m_jobs->get_addons();
$branches = $this->m_jobs->get_branches();
$cake_design = $this->m_jobs->get_cake_design();
$cake_type = $this->m_jobs->get_cake_type();
$discount = $this->m_jobs->get_discount();
$flavor = $this->m_jobs->get_flavors();
$icing_type = $this->m_jobs->get_icing_type();
$location = $this->m_jobs->get_locations();
$size = $this->m_jobs->get_sizes();
$occassion = $this->m_jobs->get_occassions();
$catalogue_categories = $this->m_jobs->get_catalogue_categories();
$cake_upgrade = $this->m_jobs->get_cake_upgrade();
$collection = $this->m_jobs->get_collection();
?>
<?=form_open_multipart('jobs/ajax_function/insert','class="add-item" data-identity="customized" id="wizard-validation" data-url="'.current_url().'"');?>
<input type="text" name="identity" value="customized" style="display: none" />
<input type="hidden" name="<?=$this->security->get_csrf_token_name(); ?>" value="<?=$this->security->get_csrf_hash(); ?>" />
<div class="row">
    <div class="col-md-12" id="message"></div>
    <a href="<?=base_url();?>jobs/order"><button type="button" class="btn btn-lg btn-success">Customized Designs</button></a>
    <a href="<?=base_url();?>jobs/order/catalogue"><button type="button" class="btn btn-lg btn-default">Catalogue Designs</button></a>
    
    <div class="tab-pane active customized-content" id="tab8">
        <div class="col-md-12">
            <div class="wizard wizard1 show-submit wizard-validation">                                
                <ul>
                    <li>
                        <a href="#step-5">
                            <span class="stepNumber">1</span>
                            <span class="stepDesc">Customer Details<br /><small>&nbsp;</small></span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-6">
                            <span class="stepNumber">2</span>
                            <span class="stepDesc">Occasion Details<br /><small>&nbsp;</small></span>
                        </a>
                    </li>
                    <li>
                        <a href="#step-7">
                            <span class="stepNumber">3</span>
                            <span class="stepDesc">Order Details<br /><small>&nbsp;</small></span>
                        </a>
                    </li>  
                    <li>
                        <a href="#step-8">
                            <span class="stepNumber">4</span>
                            <span class="stepDesc">Collection Details<br /><small>&nbsp;</small></span>
                        </a>
                    </li>  
                    <li>
                        <a href="#step-9">
                            <span class="stepNumber">5</span>
                            <span class="stepDesc">Discounts<br /><small>&nbsp;</small></span>
                        </a>
                    </li> 
                    <li>
                        <a href="#step-10">
                            <span class="stepNumber">6</span>
                            <span class="stepDesc">Summary<br /><small>&nbsp;</small></span>
                        </a>
                    </li> 
                </ul>

                <div id="step-5">   

                    <div class="panel panel-default">
                        <div class="panel-body">
                             <h3>
                                <span class="fa fa-user"></span> Customer Details For New Customers &nbsp;
                                <button type="button" class="btn btn-info open-modal" data-modal="#myModalexu" data-identity="get_customers_from_list"><span class="fa fa-user"></span> Click Here For Existing Customers</button>
                            </h3>
                            <div class="form-group col-md-6">
                                <label>First Name</label>
                                <input type="text" name="firstname" class="form-control firstname live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Last Name</label>
                                <input type="text" name="lastname" class="form-control lastname live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Contact Number</label>
                                <input type="text" name="contact" class="form-control contact live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Email Address</label>
                                <input type="email" name="email" class="form-control email live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input type="text" name="address" class="form-control address live-change" data-identity="des">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Location</label>
                                <input type="text" name="location" placeholder="ex. Quezon City" class="form-control location live-change" data-identity="des">
                            </div>
                        </div>                              
                    </div>                                    

                </div>
                <div id="step-6">

                    <div class="panel panel-default">
                        <div class="panel-body" style="height: 216px">
                        <h3><span class="fa fa-star"></span> Occasion Details</h3>
                            <div class="form-group col-md-6">
                                <label>Occasion</label>
                                <select name="occasion" class="form-control occasion get-item-dropdown" data-identity="celebrant">
                                    <option value="">Select Occassion</option>
                                    <?php foreach($occassion as $row){?>
                                    <option value="<?=$row['occassion_type'];?>"><?=$row['occassion_type'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="form-group col-md-6 oc">
                                <label>Occasion Date</label>
                                <input type="text" name="occasion_date" class="form-control datepicker occasion_date">                                            
                            </div>
                        </div>                              
                    </div>                                                                  

                </div> 
                <div id="step-7" style="height: 600px;overflow: auto">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3><span class="fa fa-shopping-cart"></span> Order Details</h3>
                            <div class="hmm"></div>
                            <div class="col-md-4 col-md-offset-4">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category" class="form-control category get-item-dropdown" data-oc="true" data-push="cake" data-input=".cake_category">
                                        <option value="">Select</option>
                                        <?php foreach($catalogue_categories as $row){?>
                                        <option value="<?=$row['category_name']?>"><?=$row['category_name'];?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Title</label>
                                    <input name="title" type="text" class="form-control title live-change" data-push="cake" data-input=".cake_title" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label>Order Set</label>
                                    <select name="order_set" class="form-control order_set get-item-dropdown" data-price=".cake_order_set_price" data-get="g_order_set" data-push="cake" data-input=".cake_order_set" data-identity="order_set">
                                        <option value="">Select</option>
                                        <?php foreach($cake_type as $row){?>
                                        <option value="<?=$row['name'];?>"><?=$row['name'];?></option>
                                        <?php }?>
                                    </select>
                                </div>
                                <br>
                                <div class="ct"></div><br>
                                <div class="ds"></div><br>
                                <div class="sc"></div><br>
                                <div class="fc"></div><br>
                                <div class="adn"></div><br>
                                <div class="cu"></div><br>
                            </div>
                        </div>                              
                    </div> 
                    <div class="panel panel-defualt target">
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-4">
                                <br>
                                <br>
                                <br>
                                <div class="col-md-12" style="marginleft: 0px;padding-left: 0px">
                                    <div class="form-horizontal" style="marginleft: 0px;padding-left: 0px">
                                        <label class="col-lg-3" style="marginleft: 0px;padding-left: 0px">Difficulty</label>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <input type="text" name="difficulty" class="form-control difficulty" placeholder="Amount">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="col-md-4 col-md-offset-4">     
                            <br>
                            <div class="col-md-12" style="padding-bottom: 20px">
                                <div class="form-group">
                                    <label>Specification</label><br>
                                    <textarea name="specification" class="form-control specification live-change" data-identity="compute_new_total" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Upload Image - Customer Specs (If Any)</label>
                                <input type="file" class="fileinput btn-primary" name="userfile" class="userfile" id="filename" title="Browse file"/>
                            </div>
                            <br>
                            <div class="form-group">
                                <label>Cake Message</label>
                                <textarea name="cake_message" class="form-control cake_message" rows="5"></textarea>
                            </div>
                        </div>
                        </div>
                    </div>   
                </div>

            <div id="step-8">

                <div class="panel panel-default">
                    <div class="panel-body" style="height: 531px">
			<h3><span class="fa fa-shopping-cart"></span> Collection Details</h3>
                            <div class="form-group">
                                <label>Claim Type</label>
                                <select name="claim_type" class="form-control claim_type pickup_branch get-item-dropdown" data-identity="collection_type">
                                    <option value="">Select Claim Type</option>
                                    <?php foreach($collection as $row){;?>
                                    <option value="<?=$row['collection_type'];?>"><?=$row['collection_type'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <div class="dp"></div>
                    	</div>                                                 
                    </div>                                                                        

		</div>  
                
							
                <div id="step-9">                                                                                                         
                    <div class="panel panel-default">
			<div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Select Discount Type</label>
                                        <select name="discount_type" class="form-control specialselect discount_type get-item-dropdown" data-identity="discount">
                                            <option value="">None</option>
                                            <?php foreach($discount as $row){?>
                                            <option value="<?=$row['name'];?>"><?=$row['description'];?></option>
                                            <?php }?>
                                        </select>
                                    </div>

                                    <div id="co" class="panel-body hide special">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>C/O by :</label>
                                                <input name="co_by" type="text" class="form-control co_by"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Discount %</label>
                                                <input name="discount" type="text" class="form-control discount live-change" data-identity="co_discount" />
                                            </div>
                                        </div>
                                    </div>

                                    <div id="promo" class="panel-body hide special">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Promo No.</label>
                                                <input name="promo_no" type="text" class="form-control promo_no live-change" data-identity="promo" />
                                            </div>
                                            <div class="form-group">
                                                <label>Details</label>
                                                <input type="text" name="promo_detail" value="Freee 12 pcs cookies" disabled="disabled"  class="form-control promo_detail" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div>
               	</div>
                
                  
                
                <div id="step-10">                                                                                                         
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row col-md-12">
                                <div class="col-md-4 col-md-offset-2">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th colspan="3">Order Summary</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Item: </td>
                                                <td colspan="2" class="s_item"></td>
                                                <td class="s_amount" style="text-align: right"></td>
                                            </tr>
                                            <tr>
                                                <td>Category: </td>
                                                <td colspan="2" class="s_category"></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Title: </td>
                                                <td colspan="2" class="s_title"></td>
                                                <td></td>
                                            </tr>
                                            <tr class="ssorderset">
                                                <td>order Set: </td>
                                                <td colspan="2" class="s_order_set"></td>
                                                <td></td>
                                            </tr>
                                            <tr class="ssdesign">
                                                <td>Design: </td>
                                                <td colspan="2" class="s_design"></td>
                                                <td></td>
                                            </tr>
                                            <tr class="sssize">
                                                <td>Size: </td>
                                                <td colspan="2" class="s_size"></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                        <thead class="ssadn">
                                            <tr>
                                                <td>Addons: </td>
                                                <td colspan="2">Quantity</td>
                                                <td></td>
                                            </tr>
                                        </thead>
                                        <tbody class="addons">
                                        </tbody>
                                        <tbody class="s_cake_upgrades">
                                            <tr>
                                                <td>Cake Upgrade</td>
                                                <td colspan="2" class="s_upgrade">Test Double Cake</td>
                                                <td style="text-align: right" class="s_upgrade_price">600.00</td>
                                            </tr>
                                        </tbody>
                                        <thead>
                                            <tr class="del_charge" style="display: none">
                                                <th colspan="3">Delivery Charge: </th>
                                                <th style="text-align: right" class="d_charge"></th>
                                            </tr>
                                            <tr>
                                                <th colspan="3">Sub Total: </th>
                                                <th style="text-align: right" class="sub_total_amount"></th>
                                            </tr>
                                            <tr class="dis_charge" style="display: none">
                                                <th colspan="4">Less: Discount</th>
                                            </tr>
                                        </thead>
                                        <tbody class="dis_class" style="display: none">
                                        </tbody>
                                        <thead>
                                            <tr>
                                                <th colspan="3">Total Amount: </th>
                                                <th style="text-align: right" class="order_total_amount"></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th colspan="4">Payment: </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="2">Payment Type:</td>
                                                <td colspan="2">
                                                    <select class="form-control payment_type" name="payment_type">
                                                        <option selected="selected" value="">Select</option>
                                                        <option value="Full Payment">Full Payment</option>
                                                        <option value="Down Payment">Down Payment</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Mode of Payment:</td>
                                                <td colspan="2">
                                                    <select class="form-control payment_mode" name="payment_mode">
                                                        <option selected="selected" value="">select</option>
                                                        <option value="Cash">Cash</option>
                                                        <option value="Card">Card</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">Amount to be paid:</td>
                                                <td><input name="amount_to_be_paid" type="text" class="form-control amount_to_paid amount_to_be_paid live-change"  data-identity="payment_customized"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">Amount Recieved:</td>
                                                <td><input name="amount_recieved" type="text" class="form-control amount_recieved live-change" data-identity="change"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">Change:</td>
                                                <td class="show_change"><!--<input name="amount_change" type="text" class="form-control amount_change">--></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">Balance:</td>
                                                <td class="show_balance"><!--<input name="amount_balance" type="text" class="form-control amount_balance">--></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                    <div style="display: none">
                        <input name="cake_item" type="text" class="cake_item" placeholder="cake_item">
                        <input name="cake_order_set" type="text" class="cake_order_set" placeholder="cake_order_set">
                        <input name="cake_design" type="text" class="cake_design" placeholder="cake_design">
                        <input name="cake_size" type="text" class="cake_size" placeholder="cake_size">
                        <input name="cake_flavor" type="text" class="cake_flavor" placeholder="cake_flavor">
                        <input name="cake_category" type="text" class="cake_category" placeholder="cake_category">
                        <input name="cake_title" type="text" class="cake_title" placeholder="cake_title">
                        <br>
                        <input name="cake_item_price" type="text" class="cake_item_price" placeholder="cake_item_price">
                        <input name="cake_order_set_price" type="text" class="cake_order_set_price" placeholder="cake_order_set_price">
                        <input name="cake_design_price" type="text" class="cake_design_price" placeholder="cake_design_price">
                        <input name="cake_size_price" type="text" class="cake_size_price" placeholder="cake_size_price">
                        <input name="cake_flavor_price" type="text" class="cake_flavor_price" placeholder="cake_flavor_price">
                        <br>
                        Addons total prices : <input name="total_addons_price" type="int" value="0" class="total_addons_price" placeholder="total_addons_price"><br>
                        Cake upgrade Total Price : <input name="total_upgrade_price" type="int" value="0" class="total_upgrade_price" placeholder="total_upgrade_price"><br>
                        Priority Charge : <input name="priority_charge" class="priority_charge" type="int" value="0" placeholder="show priority_charge if has priority_charge"><br>
                        Delivery Charge : <input name="delivery_charge" class="delivery_charge" type="int" value="0" placeholder="show delivery charge if delivery"><br>
                        Quantity : <input name="cake_quantity" class="cake_quantity" type="int" value="1" placeholder="show how many products customer buy"><br>
                        Discount :<input name="discount_value" class="discount_value" type="int" value="0" placeholder="show discount if has discount"><br>
                        total product purchase :<input name="total_amount" class="total_amount" type="int" value="0" placeholder="show total amount or purchase products"><br>
                        Amount Paid : <input name="amount_paid" class="amount_paid" type="int" value="0" placeholder="show amount to be paid"><br>
                        Amount Change : <input name="amount_change" class="amount_change" type="int" value="0" placeholder="show if customer has change amount"><br>
                        Amount balance : <input name="amount_balance" class="amount_balance" type="int" value="0" placeholder="show if customer has balance amount"><br>
                        sub_total : <input name="sub_total" class="sub_total" type="int"  value="0" placeholder="Sub Total"><br>
                        Total Price : <input name="total_price_amount" class="total_price_amount" type="int"  value="0" placeholder="total price"><br>
                        <div class="adn_data">
                        </div>
                    </div>
                </div>

            </div>
        </div>
	 
</div>
        
<div class="modal fade modal-data" id="modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Details</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
        
<div class="modal print-data" id="modal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="exampleModalLabel">Order Summary</h4>
            </div>
            <div class="modal-body">
                <div class="container_fluid">
                     <div class="col-md-12" style="text-align: left">';
                         
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="text-center">Kinkcakes</h1>
                                <b><p>0979700<br>Makati City</p></b>
                                <b><p><?=date('Y / m / d');?></p></b>
                            </div>
                        </div>
                         <div style="text-align: left">
                             <table style="text-align: left" class="table table-responsive">
                                <thead>
                                    <th colspan="2">Order Summary </th>
                                    <th>Amount</th>
                                </thead>
                                <tbody class="order_summarys">
                                </tbody>
                                <tbody class="order_summarys2">
                                </tbody>
                                <thead>
                                    <tr>
                                        <th>Addons</th>
                                        <th colspan="2">Quantity</th>
                                    </tr>
                                </thead>
                                <tbody class="addons">
                                </tbody>
                                <thead class="cake_upgrade">
                                </thead>
                                <thead class="charges">
                                </thead>
                                <thead class="subtotals">
                                </head>
                                <thead class="discounts_name">
                                </thead>
                                <tbody class="discounts">
                                </tbody>
                                <thead class="tmt">
                                </head>
                                <thead class="ors">
                                </thead>
                            </table>
                         </div>
                        <div class="available_items" style="text-align: left"></div>
                        <div class="jo_datas" style="text-align: left"></div>
                   </div>
                </div>
            </div>
            <div style="padding: 20px">
                <button type="button" class="btn btn-default save-print" data-stat="0" data-url="<?=current_url();?>">Save and Close <span class="fa fa-save"></span></button>
                <button type="button" class="btn btn-info print-res">Print As Customer Reciept <span class="fa fa-print"></span></button>
           </div>
        </div>
    </div>
</div>
        
<div class="modal print-data" id="myModalexu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="width: 65%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Find Existing user</h4>
            </div>
            <div class="modal-body">
                <div class="container_fluid">
                    <input type="text" value="" class="selected_customer" name="selected_customer" style="display:  none">
                    <input type="text" value="" class="must_1" name="must_1" style="display:  none">
                    <div class="result">
                        <table class="table datatable" id="customer_select">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Firstname</th>
                                    <th>lastname</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Location</th>
                                    <th>Contact No.</th>
                                    <th>Buy Count</th>
                                    <th>Customer Since</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="div-me-lol">
                <button type="button" class="btn btn-primary close-modal" data-identity="get_customers_from_list" data-modal="myModalexu" data-dismiss="modal" style="margin-top: 50px;margin-bottom: 20px;margin-left: 20px">Save and Close <span class="fa fa-save"></span></button>
           </div>
        </div>
    </div>
</div>
        
<!--        
<div class="modal print-data" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Payment</h4>
            </div>
            <div class="modal-body">
                <div class="container_fluid">
                    <div class="row">
                        <div>
                            <div clas="form-group">
                                <h4 style="padding: 20px;border: 1px solid #ccc">Total Amount : <b class="pull-right show_total">0.00</b></h4>
                            </div>
                            <div clas="form-group">
                                <label>Amount to be paid</label>
                                <input type="text" class="form-control live-change amount_to_paid" name="amount_to_paid" data-identity="payment">
                            </div>
                            <div clas="form-group">
                                <label>Payment Type</label>
                                <select class="form-control select payment_type" name="payment_type">
                                    <option selected="selected" value="">Select</option>
                                    <option value="Full">Full</option>
                                    <option value="Down Payment">Down Payment</option>
                                </select>
                            </div>
                            <div clas="form-group">
                                <label>Mode of payment</label>
                                <select class="form-control select payment_mode" name="payment_mode">
                                    <option selected="selected" value="">select</option>
                                    <option value="Cash">Cash</option>
                                    <option value="Card">Card</option>
                                </select>
                            </div>
                            <div clas="form-group">
                                <h4 style="padding: 20px;border: 1px solid #ccc;margin-top: 15px">Balance : <b class="pull-right show_balance">0.00</b></h4>
                            </div>
                             <div clas="form-group">
                                <h4 style="padding: 20px;border: 1px solid #ccc;margin-top: 15px">Change : <b class="pull-right show_change">0.00</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="div-me-lol">
                <button type="button" class="btn btn-primary showRes" data-id="" style="margin-top: 50px;margin-bottom: 20px;margin-left: 20px"> Submit Order <span class="fa fa-check"></span></button>
           </div>
        </div>
    </div>
</div>-->

<?=form_close();?>
