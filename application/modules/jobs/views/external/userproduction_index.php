<div class="col-md-4">
    <!--<div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong><span class="fa fa-pencil"></span> Note!</strong><br> 
        Row having color <code style="background: #eee;">&nbsp;&nbsp;&nbsp;&nbsp;</code> are the priority orders
    </div>-->
</div>
<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab8" data-toggle="tab">Job Request</a></li>
        <li><a href="#tab9" data-toggle="tab">On Progress</a></li>
        <!--<li><a href="#tab10" data-toggle="tab">Done</a></li>-->
        <li><a href="#tab11" data-toggle="tab">Dispatched</a></li>
    </ul>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab8">
            <table class="table datatable" id="jobExternalProductionJobRequest">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>JO#</th>
                        <th>Sales</th>
                        <th>Item Category</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Claim Type</th>
                        <th>Claim Location</th>
                        <th>Claim Date</th>
                        <th>Claim Time</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                        <th>Print</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="tab-pane" id="tab9">
            <table class="table datatable" id="jobExternalProductionOnProgress">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>JO#</th>
                        <th>Sales</th>
                        <th>Item Category</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Claim Type</th>
                        <th>Claim Location</th>
                        <th>Claim Date</th>
                        <th>Claim Time</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Print</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!--
        <div class="tab-pane" id="tab10">

            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Sales</th>
                        <th>JO#</th>
                        <th>Item Category</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Claim Type</th>
                        <th>Claim Location</th>
                        <th>Claim Date</th>
                        <th>Claim Time</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                        <th>Print</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>xxx</td>
                        <td>MKT-4573</td>
                        <td>Marshmallow Cake</td>
                        <td>xxx </td>
                        <td></td>
                        <td></td>
                        <td>Delivery</td>
                        <td>Caloocan</td>
                        <td>25-Dec-14</td>
                        <td>1:00PM</td>
                        <td>12nn</td>
                        <td><button type="button" class="btn btn-danger">Start</button></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>xxx</td>
                        <td>MKT-4573</td>
                        <td>Marshmallow Cake</td>
                        <td>xxx </td>
                        <td></td>
                        <td></td>
                        <td>Delivery</td>
                        <td>Caloocan</td>
                        <td>25-Dec-14</td>
                        <td>1:00PM</td>
                        <td>12nn</td>
                        <td><button type="button" class="btn btn-danger">Start</button></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>xxx</td>
                        <td>MKT-4573</td>
                        <td>Marshmallow Cake</td>
                        <td>xxx </td>
                        <td></td>
                        <td></td>
                        <td>Delivery</td>
                        <td>Caloocan</td>
                        <td>25-Dec-14</td>
                        <td>1:00PM</td>
                        <td>12nn</td>
                        <td><button type="button" class="btn btn-danger">Start</button></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        -->

        <div class="tab-pane" id="tab11">
            <legend><h3>MTO</h3></legend>
            <table class="table datatable" id="mtodispatched">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>JO#</th>
                        <th>Item Category</th>
                        <th>Title</th>
                        <th>Claim Type</th>
                        <th>Location</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td>xxx</td>
                        <td><button class="btn btn-default">Select</button></td>
                        <td><button class="btn btn-info">Recieved</button></td>
                    </tr>
                </tbody>
            </table>
            <legend style="padding-top: 40px"><h3>MTO - Dispatched</h3></legend>
            <h3>MTO Cakes</h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>JO#</th>
                        <th>Item Category</th>
                        <th>Title</th>
                        <th>Claim Type</th>
                        <th>Location / Address</th>
                        <th>Claim Type</th>
                        <th>Claim Time</th>
                        <th>Recieved By</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                    </tr>
                </tbody>
            </table>
            
            <h3>MTO Cookies</h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>JO#</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Ordered Quantity</th>
                        <th>Delivered Quantity</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                    </tr>
                </tbody>
            </table>
            <button class="btn btn-primary pull-right"><span class="fa fa-print"></span> Print Delivery Order Form</button>
            
        </div>
			<!--
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Sales</th>
                        <th>JO#</th>
                        <th>Item Category</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Claim Type</th>
                        <th>Claim Location</th>
                        <th>Claim Date</th>
                        <th>Claim Time</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                        <th>Print</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>xxx</td>
                        <td>MKT-4573</td>
                        <td>Marshmallow Cake</td>
                        <td>xxx </td>
                        <td></td>
                        <td></td>
                        <td>Delivery</td>
                        <td>Caloocan</td>
                        <td>25-Dec-14</td>
                        <td>1:00PM</td>
                        <td>12nn</td>
                        <td><button type="button" class="btn btn-danger">Start</button></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            -->
        </div>    
    </div>
</div> 