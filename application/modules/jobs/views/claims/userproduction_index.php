<div class="panel panel-danger">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-calendar"></span> Other Store's Order</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">	
	<table class="table datatable" id="mtoClaimatCommisary">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>Order Branch</th>
                    <th>JO#</th>
                    <th>Item</th>
                    <th>Title</th>
                    <th>Order Set</th>
                    <th>Claim Type</th>
                    <th>Claim Location</th>
                    <th>Claim Date</th>
                    <th>Claim Time</th>
                    <th>Total Amount</th>
                    <th>Balance Amount</th>
                    <th>Amount Paid</th>
                    <th>Claim Status</th>
                </tr>
            </thead>
	</table>
    </div>
</div>