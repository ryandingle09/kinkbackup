<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_jobs extends MY_Model {
    //get seperated prices and data for custom ordering **/
    public function g_size($id)
    {
       $sql = 
        '
            SELECT * FROM sizes WHERE id = '.$id.'
        ';
        return $this->select_all($sql);
    }
    
    public function g_flavor($a)
    {
       $sql = 
        '
            SELECT * FROM flavors WHERE id = '.$a.'
        ';
        return $this->select_all($sql);
    }
    
    public function g_design($id)
    {
       $sql = 
        '
            SELECT * FROM cake_design WHERE id = "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    
    public function g_icing_type($id)
    {
       $sql = 
        '
            SELECT * FROM icing_types WHERE id = "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    
    public function g_order_set($id)
    {
       $sql = 
        '
            SELECT * FROM cake_type WHERE name = "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    //end ---- */
    /**** ---- GET MTO ONCHANGE DATAS ON CUSTOMIZED CAKE ORDERING --**/
    public function get_addon($id)
    {
       $sql = 
        '
            SELECT * FROM add_ons WHERE icing_id = "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    
    public function get_cake_upgrades($id)
    {
       $sql = 
        '
            SELECT * FROM cake_upgrade WHERE deny_job_type_id != "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    
    public function get_size($id)
    {
       $sql = 
        '
            SELECT * FROM sizes WHERE deny_job_type_id != "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    
    public function get_flavor($a,$b)
    {
       $sql = 
        '
            SELECT * FROM flavors WHERE icing_id = '.$a.' and cake_type_id = '.$b.'
        ';
        return $this->select_all($sql);
    }
    
    public function get_design($id)
    {
       $sql = 
        '
            SELECT * FROM cake_design WHERE cake_type = "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    
    public function get_icing_types($id)
    {
       $sql = 
        '
            SELECT * FROM icing_types WHERE cake_type = "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    
    public function get_priority_charge($id)
    {
       $sql = 
        '
            SELECT * FROM icing_types WHERE id = "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    public function get_priority_charge2($id)
    {
       $sql = 
        '
            SELECT * FROM icing_types WHERE type = "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    
    public function get_cake_type_id($id)
    {
       $sql = 
        '
            SELECT * FROM cake_type WHERE name = "'.$id.'"
        ';
        return $this->select_all($sql);
    }
    /*** ----- END ---***/
    
    public function getExsistCustomerData($id)
    {
        $sql = 
        '
            SELECT * FROM customers WHERE id = "'.$id.'"
         ';
        return $this->select_all($sql);
    }
    
    /** ----- JOBS Internal functions ---***/
    //--------baking
    public function get_internal_mto_tables_classfication_cakes()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "baking" and icing_type = "Fondant Cake" or icing_type = "Marshmallow Icing Cake"
        ';
        return $this->select_all($sql);
    }
    public function get_internal_mto_tables_classfication_cookie()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "baking"  and icing_type = "MTO Cookie" or icing_type = "MTO Carousel"
        ';
        return $this->select_all($sql);
    }
    //---------decorating
    public function get_internal_deco_tables_classfication_cakes()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "decorating" and icing_type = "Fondant Cake" or icing_type = "Marshmallow Icing Cake"
        ';
        return $this->select_all($sql);
    }
    public function get_internal_deco_tables_classfication_cookie()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "decorating"  and icing_type = "MTO Cookie" or icing_type = "MTO Carousel"
        ';
        return $this->select_all($sql);
    }
    //---------finishing
    public function get_internal_finishing_tables_classfication_cakes()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "finishing" and icing_type = "Fondant Cake" or icing_type = "Marshmallow Icing Cake"
        ';
        return $this->select_all($sql);
    }
    public function get_internal_finishing_tables_classfication_cookie()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "finishing" and icing_type = "MTO Cookie" or icing_type = "MTO Carousel"
        ';
        return $this->select_all($sql);
    }
    /** ----- END Internal functions ---***/
    //---------done
    public function get_internal_done_tables_classfication_cakes()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "done" and icing_type = "Fondant Cake" or icing_type = "Marshmallow Icing Cake"
        ';
        return $this->select_all($sql);
    }
    public function get_internal_done_tables_classfication_cookie()
    {
        $sql =
        '
            SELECT * FROM orders WHERE stage_job_scope = "done" and icing_type = "MTO Cookie" or icing_type = "MTO Carousel"
        ';
        return $this->select_all($sql);
    }
    /** ----- END Internal functions ---***/
    /** ----- JOBS External functions ---***/
    public function get_mtodispatched()
    {
        $sql = '
        SELECT
        orders.id,
        orders.branch_code,
        orders.claim_type,
        orders.title,
        orders.JO_no,
        orders.icing_type,
        orders.cake_type,
        orders.delivery_date,
        orders.delivery_time,
        orders.delivery_time_stat,
        orders.delivery_location,
        orders.pickup_time,
        orders.pickup_date,
        orders.pickup_time_stat,
        orders.claim_status,
        orders.pickup_branch,
        orders.stage_job_scope,
        orders.target_date,
        employees.first_name,
        employees.last_name
        FROM orders
        JOIN employees ON user_id = orders.submitter
        WHERE orders.delivery_dispatch IS NOT NULL
        ';
        return $this->select_all($sql);
    }
    public function get_jobExternalProductionJobRequest()
    {
        $sql = '
        SELECT
        orders.id,
        orders.branch_code,
        orders.claim_type,
        orders.title,
        orders.JO_no,
        orders.icing_type,
        orders.cake_type,
        orders.delivery_date,
        orders.delivery_time,
        orders.delivery_time_stat,
        orders.delivery_location,
        orders.pickup_time,
        orders.pickup_date,
        orders.pickup_time_stat,
        orders.claim_status,
        orders.pickup_branch,
        orders.stage_job_scope,
        orders.target_date,
        employees.first_name,
        employees.last_name
        FROM orders
        JOIN employees ON user_id = orders.submitter
        WHERE orders.stage_job_scope IS NULL
        ORDER BY ABS(now() - target_date) DESC, ABS(now() - delivery_date) DESC, ABS(now() - pickup_date) DESC
        ';
        return $this->select_all($sql);
    }
    
    public function get_jobExternalProductionOnProgress()
    {
        $sql = '
        SELECT
        orders.id,
        orders.branch_code,
        orders.claim_type,
        orders.title,
        orders.JO_no,
        orders.icing_type,
        orders.cake_type,
        orders.delivery_date,
        orders.delivery_time,
        orders.delivery_time_stat,
        orders.delivery_location,
        orders.pickup_time,
        orders.pickup_date,
        orders.pickup_time_stat,
        orders.claim_status,
        orders.pickup_branch,
        orders.stage_job_scope,
        employees.first_name,
        employees.last_name
        FROM orders
        JOIN employees ON user_id = orders.submitter
        WHERE orders.stage_job_scope != "done" and orders.stage_job_scope IS NOT NULL 
        ';
        return $this->select_all($sql);
    }
    public function get_jobExternalProductionDispatched()
    {
        $sql = 
        '
        SELECT
        orders.id,
        orders.branch_code,
        orders.claim_type,
        orders.title,
        orders.JO_no,
        orders.icing_type,
        orders.cake_type,
        orders.delivery_date,
        orders.delivery_time,
        orders.delivery_time_stat,
        orders.delivery_location,
        orders.pickup_time,
        orders.pickup_date,
        orders.pickup_time_stat,
        orders.claim_status,
        orders.pickup_branch,
        orders.stage_job_scope,
        employees.first_name,
        employees.last_name
        FROM orders
        JOIN employees ON user_id = orders.submitter
        WHERE orders.stage_job_scope = "done" 
        ';
        return $this->select_all($sql);
    }
    
     /** ----- END ---***/
    
    /** ----- JOBS Claim functions ---***/
    public function get_mtoClaimatCommisary()
    {
        $sql = '
        SELECT
        orders.id,
        orders.branch_code,
        orders.branch_name,
        orders.claim_type,
        orders.title,
        orders.JO_no,
        orders.icing_type,
        orders.cake_type,
        orders.delivery_date,
        orders.delivery_time,
        orders.delivery_time_stat,
        orders.delivery_location,
        orders.pickup_time,
        orders.pickup_date,
        orders.pickup_time_stat,
        orders.claim_status,
        orders.stage_job_scope,
        orders.total_price_amount,
        orders.balance,
        orders.amount_paid,
        orders.claim_status,
        orders.pickup_branch
        FROM orders
        ';
        return $this->select_all($sql);
    }
    /** ---- END -- **** /
    
    /** JOB TRACKER FUNC -KISTA *** ---*/
    public function get_fondant_mto_decorating()
    {
        $sql = '
            SELECT * FROM orders WHERE stage_job_scope = "decorating" and icing_type = "Fondant Cake"
        ';
        return $this->select_all($sql);
    }
    public function get_fondant_mto_finishing()
    {
        $sql = '
            SELECT * FROM orders WHERE stage_job_scope = "finishing" and icing_type = "Fondant Cake"
        ';
        return $this->select_all($sql);
    }
    public function get_fondant_mto_done()
    {
        $sql = '
            SELECT * FROM orders WHERE stage_job_scope = "done" and icing_type = "Fondant Cake"
        ';
        return $this->select_all($sql);
    }
    public function get_fondant_mto_cakes()
    {
        $sql = '
            SELECT * FROM orders WHERE stage_job_scope IS NULL and icing_type = "Fondant Cake"
        ';
        return $this->select_all($sql);
    }
    public function get_trackercookies()
    {
        $sql = '
            SELECT * FROM orders WHERE stage_job_scope IS NULL and icing_type = "MTO Cookie" or icing_type = "MTO Carousel"
        ';
        return $this->select_all($sql);
    }
    public function get_StoreSalesjobrequestOnTracker()
    {
        $sql = '
        SELECT
        orders.id,
        orders.branch_code,
        orders.claim_type,
        orders.title,
        orders.JO_no,
        orders.icing_type,
        orders.cake_type,
        orders.delivery_date,
        orders.delivery_time,
        orders.delivery_time_stat,
        orders.delivery_location,
        orders.pickup_time,
        orders.pickup_date,
        orders.pickup_time_stat,
        orders.claim_status,
        orders.stage_job_scope,
        orders.pickup_branch,
        employees.first_name,
        employees.last_name
        FROM orders
        JOIN employees ON user_id = orders.submitter
        ';
        return $this->select_all($sql);
    }
    
    public function get_StoreSalesonProgessOnTracker()
    {
        $sql = '
        SELECT
        orders.id,
        orders.branch_code,
        orders.claim_type,
        orders.title,
        orders.JO_no,
        orders.icing_type,
        orders.cake_type,
        orders.delivery_date,
        orders.delivery_time,
        orders.delivery_time_stat,
        orders.delivery_location,
        orders.pickup_time,
        orders.pickup_date,
        orders.pickup_time_stat,
        orders.claim_status,
        orders.pickup_branch,
        orders.stage_job_scope,
        employees.first_name,
        employees.last_name
        FROM orders
        JOIN employees ON user_id = orders.submitter
        WHERE orders.stage_job_scope = "decorating" or orders.stage_job_scope = "baking" or orders.stage_job_scope = "finishing" 
        ';
        return $this->select_all($sql);
    }
    
    public function get_StoreSalesonDoneTracker()
    {
        $sql = '
        SELECT
        orders.id,
        orders.branch_code,
        orders.claim_type,
        orders.title,
        orders.JO_no,
        orders.icing_type,
        orders.cake_type,
        orders.delivery_date,
        orders.delivery_time,
        orders.delivery_time_stat,
        orders.delivery_location,
        orders.pickup_time,
        orders.pickup_date,
        orders.pickup_time_stat,
        orders.claim_status,
        orders.pickup_branch,
        orders.stage_job_scope,
        employees.first_name,
        employees.last_name
        FROM orders
        JOIN employees ON user_id = orders.submitter
        WHERE orders.stage_job_scope = "done"
        ';
        return $this->select_all($sql);
    }
     
    /*** ----- END OF JOB TRACKER FUNC ---- ***/
    
    /** ---------- JOBS ORDER FUNC ------**/
    public function update_branch_code_orders($order_id,$code,$name)
    {
        $jo = ''.$code.'-'.$order_id.'';
        $param = array($jo,$code,$name);
        $sql =
            'UPDATE orders SET JO_no = ?, branch_code = ?, branch_name = ? WHERE id = "'.$order_id.'"'
        ;
        return $this->save($sql,$param);
    }
    public function get_branch_code($user)
    {
        $sql =
        '
        SELECT
        users.user_id,
        users.branch_id,
        branches.branch_id,
        branches.branch_code,
        branches.branch_name
        FROM users
        JOIN branches ON branches.branch_id = users.branch_id 
        WHERE users.user_id = "'.$user.'"
        '
        ;
        return $this->select_all($sql);
    }
    public function get_addons_order_data($order_id)
    {
       $param = array($order_id);
       $sql = '
        SELECT 
        addons_item.id as a_id,
        addons_item.order_id,
        addons_item.addons,
        addons_item.quantity,
        add_ons.id as adn_id,
        add_ons.price as a_price,
        add_ons.name as a_name
        FROM addons_item
        JOIN add_ons ON add_ons.id = addons_item.addons 
        WHERE addons_item.order_id = ?
       ';
       return $this->select_all($sql,$param);
    }
    
    public function get_order_data2($order_id)
    {
       $param = array($order_id);
       $sql = '
        SELECT * FROM orders WHERE id = ?
        ';
        return $this->select_all($sql,$param);
    }
    /*
    public function updaterec($order_id)
    {
        $total_product_price = $this->input->security->xss_clean($_POST['total_amount']);
        $total_price_amount = $this->input->security->xss_clean($_POST['total_price_amount']);
        $amount_paid = $this->input->security->xss_clean($_POST['amount_paid']);
        $amount_change = $this->input->security->xss_clean($_POST['amount_change']);
        $balance = $this->input->security->xss_clean($_POST['amount_balance']);
        $payment_type = $this->input->security->xss_clean($_POST['payment_type']);
        $mode_of_payment = $this->input->security->xss_clean($_POST['payment_mode']);
        $sub_total = $this->input->security->xss_clean($_POST['sub_total']);
        
        $param = array(
            $total_product_price,
            $total_price_amount,
            $amount_paid,
            $amount_change,
            $balance,
            $payment_type,
            $mode_of_payment,
            $sub_total
        );
        $sql = '
            UPDATE orders SET total_product_price = ?, total_price_amount = ?, amount_paid = ?, amount_change = ?, balance = ?, payment_type = ?, mode_of_payment = ?, sub_total = ? 
            WHERE id = "'.$order_id.'"
        ';
        return $this->save($sql,$param);
    }
    */
    public function get_reciept_order_data($order_id)
    {
       //$param = array($order_id);
       $sql = '
           SELECT * FROM orders WHERE id = "'.$order_id.'" LIMIT 1
        ';
       return $this->select_all($sql);
    }
    
    public function get_reciept_order_summary($order_id)
    {
       $param = array($order_id);
       $sql = '
        SELECT 
        products.product_id,
        products.title_code, 
        ordered_products.product_id,
        ordered_products.job_order_id,
        ordered_products.product_price,
        ordered_products.product_icing,
        ordered_products.product_size,
        ordered_products.product_flavor,
        orders.total_product_price
        FROM ordered_products
        JOIN products ON products.product_id = ordered_products.product_id 
        JOIN orders ON orders.id  = ordered_products.job_order_id
        WHERE ordered_products.job_order_id = ?
        ';
       return $this->select_all($sql,$param);
    }
    
    public function get_customer()
    {
        $sql = "SELECT * FROM customers WHERE buy_count != '0'";
        return $this->select_all($sql);
    }
    
    public function get_catalogue_products($id)
    {
        $sql = ' SELECT * FROM catalogue_products WHERE category_id = ? ';
        $param = array($id );
        return $this->select_all($sql,$param);
    }
    
    public function get_products($id)
    {
        $sql = '
        SELECT 
        p.product_id,
        p.product_name,
        p.product_description,
        p.product_image,
        p.title_code,
        p.stockss,
        p.category_id,
        p.cost,
        p.price,
        p.flavor as p_flavor,
        p.size as p_size,
        p.creator,
        p.icing_type,
        f.cake_type_id,
        f.id as f_id,
        f.name as f_name,
        f.description as f_description,
        f.icing_id,
        f.order,
        f.price as f_price,
        s.id as s_id,
        s.name as s_name,
        s.description as s_description,
        s.shape,
        s.creator as s_creator,
        s.price as s_price,
        c.category_name as c_name,
        c.category_description as c_description,
        c.creator as c_creator,
        i.type as i_type,
        i.id as i_id,
        i.price as i_price

        FROM products p 
        JOIN flavors f 
        ON f.id = p.flavor 
        JOIN sizes s 
        ON s.id = p.size 
        JOIN catalogue_categories c ON c.id = p.category_id  
        JOIN icing_types i ON i.id = p.icing_type 
        WHERE p.category_id = ? 
        ';
        $param = array($id);
        return $this->select_all($sql,$param);
    }
    /*
    public function add_ordered_products($order_id, $item, $price, $size, $flavor, $icing)
    {
        $c_param = array($order_id, $item, $price, $size, $flavor, $icing);
        $sql_c = 'INSERT into ordered_products(job_order_id,product_id,product_price,product_size,product_flavor,product_icing) values (?,?,?,?,?,?)';
        return $this->insert($sql_c,$c_param);
    }
    */
    public function add_customer($identity)
    {
        if($identity == 'customized')
        {
            if($this->input->post('must_1') == '')
            {
                $fullname = $this->input->post('firstname').' '.$this->input->post('lastname');
                //do the dew dew
                $c_param = array(
                    $this->input->post('firstname'),
                    $this->input->post('lastname'),
                    $fullname,
                    $this->input->post('contact'),
                    $this->input->post('email'),
                    $this->input->post('address'),
                    $this->input->post('location'),
                    1
                );
                $sql_c = '
                    INSERT into customers(
                        firstname,
                        lastname,
                        fullname,
                        contact,
                        email,
                        address,
                        location,
                        buy_count
                    )
                    values (?,?,?,?,?,?,?,?)
                ';
                return $this->insert($sql_c,$c_param);
            }
            else
            {
                //do the dew dew
                $c_param = array(
                    $this->input->post('selected_customer'),
                );
                $sql_c = 'UPDATE customers SET buy_count = (buy_count + 1) where id = ?';
                return $this->save($sql_c,$c_param);
            }
        }
        elseif($identity == 'catalogue')
        {
            if($this->input->post('must_1') == '')
            {
                $fullname = $this->input->post('firstname').' '.$this->input->post('lastname');
                //do the dew dew
                $c_param = array(
                    $this->input->post('firstname'),
                    $this->input->post('lastname'),
                    $fullname,
                    $this->input->post('contact'),
                    $this->input->post('email'),
                    $this->input->post('address'),
                    $this->input->post('location'),
                    1
                );
                $sql_c = '
                    INSERT into customers(
                        firstname,
                        lastname,
                        fullname,
                        contact,
                        email,
                        address,
                        location,
                        buy_count
                    )
                    values (?,?,?,?,?,?,?,?)
                ';
                return $this->insert($sql_c,$c_param);
            }
            else
            {
                //do the dew dew
                $c_param = array(
                    $this->input->post('selected_customer'),
                );
                $sql_c = 'UPDATE customers SET buy_count = (buy_count + 1) where id = ?';
                return $this->save($sql_c,$c_param);
            }
        }
        else return false;
    }

    public function add_order($id,$identity,$user)
    {
        if($identity == 'customized')
        {
            //do the dew
            if($this->input->post('must_1') == '1') $ua_id = $this->input->post('selected_customer');
            else $ua_id = $id;
            $param = array(
                $this->input->post('cake_title'),
                1,//order_type
                $ua_id,
                //$this->input->post('product_id'),
                //$this->input->post('product_code'),
                //$this->input->post('product_name'),
                $this->input->post('category'),
                $this->input->post('occasion'),
                $this->input->post('celebrant'),
                $this->input->post('occasion_date'),
                $this->input->post('cake_item'),
                $this->input->post('cake_order_set'),//cake type as catalogue
                $this->input->post('cake_order_set'),//order set as catalogue
                $this->input->post('cake_design'),
                $this->input->post('cake_upgrade'),
                $this->input->post('specification'),
                $this->input->post('cake_message'),
                $this->input->post('cake_size'),
                $this->input->post('cake_flavor'),
                $this->input->post('cake_quantity'),
                $this->input->post('promo_no'),
                $this->input->post('discount_value'),
                $this->input->post('discount_type'),
                $this->input->post('co_by'),
                $this->input->post('promo_no'),
                $this->input->post('pickup_branch'),
                $this->input->post('pickup_date'),
                $this->input->post('pickup_time'),
                $this->input->post('pickup_time_stat'),
                $this->input->post('claim_type'),
                $this->input->post('delivery_charge'),
                $this->input->post('priority_charge'),
                $this->input->post('payment_mode'),
                date('Y-m-d H:i:s'),
                $this->input->post('difficulty'),
                $this->input->post('sub_total'),
                $this->input->post('total_amount'),
                $this->input->post('total_price_amount'),
                $this->input->post('amount_to_be_paid'),
                $this->input->post('total_price_amount'),
                $this->input->post('amount_recieved'),
                $this->input->post('amount_balance'),
                $this->input->post('amount_change'),
                $this->input->post('payment_type'),
                $user,
                date('Y-m-d H:i:s'),
                $user,
                date('Y-m-d H:i:s')
            );
            $sql = '
                INSERT INTO orders(
                    title,
                    order_type,
                    customer_id,
                    catalogue,
                    occasion,
                    celebrant,
                    occasion_date,
                    icing_type,
                    cake_type,
                    order_set,
                    design,
                    cake_upgrade,
                    specification,
                    cake_message,
                    size,
                    flavor,
                    qty,
                    promo_code,
                    discount_rate,
                    discount_type,
                    discount_co,
                    discount_promo_code,
                    pickup_branch,
                    pickup_date,
                    pickup_time,
                    pickup_time_stat,
                    claim_type,
                    delivery_charge,
                    priority_charge,
                    mode_of_payment,
                    bill_issued_at,
                    difficulty,
                    sub_total,
                    total_product_price,
                    total_price_amount,
                    amount_paid,
                    amount_total,
                    amount_recieve,
                    balance,
                    amount_change,
                    payment_type,
                    creator,
                    date_created,
                    submitter,
                    date_submitted
                )  
                values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
            ';
            return $this->insert($sql,$param);
        }
        elseif($identity == 'catalogue')
        {
            //do the dew
            if($this->input->post('must_1') == '1') $ua_id = $this->input->post('selected_customer');
            else $ua_id = $id;
            $param = array(
                $this->input->post('product_name'),
                2,//order_type as catalogue 2
                $ua_id,
                $this->input->post('product_id'),
                $this->input->post('product_code'),
                $this->input->post('product_name'),
                $this->input->post('category'),
                $this->input->post('occasion'),
                $this->input->post('celebrant'),
                $this->input->post('occasion_date'),
                $this->input->post('product_icing'),
                'catalogue',//cake type as catalogue
                'catalogue',//order set as catalogue
                $this->input->post('specification'),
                $this->input->post('cake_message'),
                $this->input->post('product_size'),
                $this->input->post('product_flavor'),
                $this->input->post('cake_quantity'),
                $this->input->post('promo_no'),
                $this->input->post('discount_value'),
                $this->input->post('discount_type'),
                $this->input->post('co_by'),
                $this->input->post('promo_no'),
                $this->input->post('pickup_branch'),
                $this->input->post('pickup_date'),
                $this->input->post('pickup_time'),
                $this->input->post('pickup_time_stat'),
                $this->input->post('claim_type'),
                $this->input->post('delivery_charge'),
                $this->input->post('priority_charge'),
                $this->input->post('payment_mode'),
                date('Y-m-d H:i:s'),
                $this->input->post('sub_total'),
                $this->input->post('product_price'),
                $this->input->post('total_price_amount'),
                $this->input->post('amount_to_be_paid'),
                $this->input->post('total_price_amount'),
                $this->input->post('amount_recieved'),
                $this->input->post('amount_balance'),
                $this->input->post('amount_change'),
                $this->input->post('payment_type'),
                $user,
                date('Y-m-d H:i:s'),
                $user,
                date('Y-m-d H:i:s')
            );
            $sql = '
                INSERT INTO orders(
                    title,
                    order_type,
                    customer_id,
                    product_id,
                    product_code,
                    product_name,
                    catalogue,
                    occasion,
                    celebrant,
                    occasion_date,
                    icing_type,
                    cake_type,
                    order_set,
                    specification,
                    cake_message,
                    size,
                    flavor,
                    qty,
                    promo_code,
                    discount_rate,
                    discount_type,
                    discount_co,
                    discount_promo_code,
                    pickup_branch,
                    pickup_date,
                    pickup_time,
                    pickup_time_stat,
                    claim_type,
                    delivery_charge,
                    priority_charge,
                    mode_of_payment,
                    bill_issued_at,
                    sub_total,
                    total_product_price,
                    total_price_amount,
                    amount_paid,
                    amount_total,
                    amount_recieve,
                    balance,
                    amount_change,
                    payment_type,
                    creator,
                    date_created,
                    submitter,
                    date_submitted
                )  
                values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
                ';
            return $this->insert($sql,$param);
        }
        else return false;

    }
    
    public function add_collection_item($id,$identity)
    {
        if($this->input->post('claim_type') == 'delivery')
        {
            $param2 = array(
                $this->input->post('delivery_date').' '.date("h:i:s", time()),
                $this->input->post('delivery_date'),
                ''.$this->input->post('delivery_complete_address').','.$this->input->post('delivery_location').'',
                $this->input->post('delivery_time'),
                $this->input->post('delivery_time_stat'),
                $this->input->post('delivery_complete_address'),
                $this->input->post('delivery_location'),
                $this->input->post('delivery_contact_person'),
                $this->input->post('delivery_contact_no')
            );
            $sql2 = 'UPDATE orders SET target_date = ?, delivery_date = ?, delivery_to_address = ? ,delivery_time = ?, delivery_time_stat = ?, delivery_complete_address = ?,delivery_location = ?,delivery_contact_person = ?, delivery_contact_no = ? WHERE id = "'.$id.'"';
            return $this->save($sql2,$param2);
        }
        elseif($this->input->post('claim_type') == 'pickup')
        {

            $param2 = array(
                $this->input->post('pickup_date').' '.date("h:i:s", time()),
                $this->input->post('pickup_date'),
                $this->input->post('pickup_time'),
                $this->input->post('pickup_time_stat'),
                $this->input->post('pickup_branch')
            );
            $sql2 = 'UPDATE orders SET target_date = ?, pickup_date = ?, pickup_time = ?,pickup_time_stat = ?, pickup_branch = ? WHERE id = "'.$id.'"';
            return $this->save($sql2,$param2);
        }
        else return false;
    }

    /*
    public function add_discount_item($id,$identity)
    {
        if($this->input->post('discount_type') == 'senior-citizen')
        {
            $param = array(
                $this->input->post('discount_type'),
                $this->input->post('discount_value')
            );
            $sql = 'UPDATE orders SET discount_type = ?,discount_rate = ? WHERE id = "'.$id.'"';
            return $this->save($sql,$param);
        }
        if($this->input->post('discount_type') == 'co')
        {
            $param = array(
                $this->input->post('discount_type'),
                $this->input->post('co_by'),
                $this->input->post('discount_value')
            );
            $sql = 'UPDATE orders SET discount_type = ?,discount_co = ?,discount_rate = ? WHERE id = "'.$id.'"';
            return $this->save($sql,$param);
        }
        if($this->input->post('discount_type') == 'promo')
        {
            $param = array(
                $this->input->post('discount_type'),
                $this->input->post('promo_no'),
                $this->input->post('discount_value')
            );
            $sql = 'UPDATE orders SET discount_type = ?,discount_promo_code = ?,discount_rate = ? WHERE id = "'.$id.'"';
            return $this->save($sql,$param);
        }else return false;
    }
    */

    public function add_addons_item($id,$identity)
    {
        //for colection_item order table
        //for addons_item order table
        if($identity == 'customized')
        {
            foreach($this->input->post('addons') as $addons)
            {
                $param = array(
                    $id,
                    $addons,
                    $this->input->post('quantity_'.$addons.''),
                );
                $sql = '
                    INSERT INTO addons_item(order_id,addons,quantity) 
                    values (?,?,?)
                    ';
                $this->insert($sql,$param);
            }	
        }
        else return false;
    }

    public function get_cake_upgrade()
    {
        $sql = "SELECT * FROM cake_upgrade";
        return $this->select_all($sql);
    }

    public function get_catalogue_categories()
    {
        $sql = "SELECT * FROM catalogue_categories";
        return $this->select_all($sql);
    }

    public function get_addons()
    {
        $sql = "SELECT * FROM add_ons";
        return $this->select_all($sql);
    }

    public function get_discount()
    {
        $sql = "SELECT * FROM discount";
        return $this->select_all($sql);
    }

    public function get_branches()
    {
        $sql = "SELECT * FROM branches";
        return $this->select_all($sql);
    }

    public function get_flavors()
    {
        $sql = "SELECT * FROM flavors";
        return $this->select_all($sql);
    }

    public function get_icing_type()
    {
        $sql = "SELECT * FROM icing_types";
        return $this->select_all($sql);
    }

    public function get_cake_type()
    {
        $sql = "SELECT * FROM cake_type";
        return $this->select_all($sql);
    }

    public function get_cake_design()
    {
        $sql = "SELECT * FROM cake_design";
        return $this->select_all($sql);
    }

    public function get_sizes()
    {
        $sql = "SELECT * FROM sizes";
        return $this->select_all($sql);
    }

    public function get_locations()
    {
        $sql = "SELECT * FROM locations";
        return $this->select_all($sql);
    }

    public function get_occassions()
    {
        $sql = "SELECT * FROM occassions";
        return $this->select_all($sql);
    }
    
    public function get_collection()
    {
        $sql = "SELECT * FROM collection_type";
        return $this->select_all($sql);
    }
    
    public function get_collection_charge($id)
    {
        $param = array($id);
        $sql = "SELECT * FROM locations WHERE  location_name = ?";
        return $this->select_all($sql,$param);
    }
    
    public function get_discount_charge($id)
    {
        $param = array($id);
        $sql = "SELECT * FROM discount WHERE name = ?";
        return $this->select_all($sql,$param);
    }
    /*** ----- END OF JOB ORDER FUNC- KISTA ---- ***/
    

}

/* End of file m_jobs.php */
/* Location: ./application/modules/jobs/models/m_jobs.php */