<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_function extends MX_Controller {
	function __construct(){
            parent::__construct();
            $this->load->library(array('form_validation'));
            $this->load->model('m_jobs');
            //$this->load->config('upload');
	}
	public function insert()
	{
            if($this->input->post('identity') == 'customized')
            {
                //set validations
                /*$this->form_validation->set_rules('item','Order Title','required|trim|xss_clean');
                $this->form_validation->set_rules('category','Order Category','required|trim|xss_clean');
                $this->form_validation->set_rules('title','Order Title','required|trim|xss_clean');
                $this->form_validation->set_rules('order_set','Order Set','required|trim|xss_clean');
                $this->form_validation->set_rules('design','Order Design','required|trim|xss_clean');
                $this->form_validation->set_rules('size','Order Size','required|trim|xss_clean');
                $this->form_validation->set_rules('flavor','Order Flavor','required|trim|xss_clean');
                

                if($this->input->post('addons'))
                {
                    foreach($this->input->post('addons') as $addons)
                    {
                        $this->form_validation->set_rules('quantity_'.$addons.'','Addons Quantity','required|trim|xss_clean|numeric');
                    }	
                }

                if($this->input->post('discount_type'))
                {
                    if($this->input->post('discount_type') == 'co')
                    {
                        $this->form_validation->set_rules('co_by','C/O By','required|trim|xss_clean');
                        $this->form_validation->set_rules('discount','Discount','required|trim|xss_clean');
                    }
                    elseif($this->input->post('discount_type') == 'promo')
                    {
                        $this->form_validation->set_rules('promo_no','Promo Ref. No.','required|trim|xss_clean');
                    }
                }
                //verify validations
                if($this->form_validation->run() == false)
                {
                    $data = array(
                        'status' => 0,
                        'message' => validation_errors()
                    );
                    echo json_encode($data);
                    //echo validation_errors();
                }
                else
                {*/
                    /*$random = rand(000000000,999999999);
                    $config['upload_path'] = '/home/kinkcake/assets/uploads';//base_url().'assets/uploads/';
                    $config['allowed_types'] = 'jpg|png|gif';
                    $config['file_name'] = 'IMG_'.$random.'';
                    $config['max_size']	= 0;
                    $config['max_width']  = 0;
                    $config['max_height']  = 0;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    //check file input nigga
                    $file = $this->upload->data();
                    $file_name = $file['file_name'];
                    if(!$this->upload->do_upload())
                    {
                        //if fail then shet shet shet
                        echo $this->upload->display_errors();
                        //echo 'do the trick';
                        $data = array(
                            'status' => 0,
                            'message' => $this->upload->display_errors()
                        );
                        echo json_encode($data);
                    }
                    else
                    {*/
                        //then do the pranks
                        $user = $this->session->userdata('user_id');
                        $id = $this->m_jobs->add_customer($this->input->post('identity'));
                        $order_id = $this->m_jobs->add_order($id, $this->input->post('identity'),$user);
                        $this->m_jobs->add_collection_item($order_id, $this->input->post('identity'));
                        //$this->m_jobs->add_discount_item($order_id, $this->input->post('identity'));
                        if($this->input->post('addons'))
                        {
                            $this->m_jobs->add_addons_item($order_id, $this->input->post('identity'));
                        }
                        
                        $code = $this->m_jobs->get_branch_code($user);
                        foreach($code as $b)
                        {
                            $this->m_jobs->update_branch_code_orders($order_id,$b['branch_code'],$b['branch_name']);
                        }
                        $data = array(
                            'status' => 1,
                            'order_id' => $order_id,
                        );
                        echo json_encode($data);
                    //}
                //}
            }
            //FOR CATALOGUE
            elseif($this->input->post('identity') == 'catalogue')
            {
                $this->form_validation->set_rules('discount_type','Discount Type','required|trim|xss_clean');
                if($this->input->post('discount_type') == 'co')
                {
                    $this->form_validation->set_rules('co_by','C/O By','required|trim|xss_clean');
                    $this->form_validation->set_rules('discount','Discount','required|trim|xss_clean');
                }
                elseif($this->input->post('discount_type') == 'promo')
                {
                    $this->form_validation->set_rules('promo_no','Promo Ref. No.','required|trim|xss_clean');
                }
                //money validation
                $this->form_validation->set_rules('amount_to_be_paid','Amount to be paid','required|trim|xss_clean|is_numeric');
                $this->form_validation->set_rules('payment_type','Type of Payment','required|trim|xss_clean');
                $this->form_validation->set_rules('payment_mode','Mode of Payment','required|trim|xss_clean');
                
                if($this->form_validation->run() == false)
                {
                    $data = array(
                        'status' => 0,
                        'message' => validation_errors()
                    );
                    echo json_encode($data);
                }
                else
                {
                    if($this->input->post('cake_quantity') === '0'){ echo '<p>The Product Selection is required to select.</p>'; }
                    else
                    {
                        $user       = $this->session->userdata('user_id');
                        $id         = $this->m_jobs->add_customer($this->input->post('identity'));
                        $order_id   = $this->m_jobs->add_order($id, $this->input->post('identity'),$user);
                        $this->m_jobs->add_collection_item($order_id, $this->input->post('identity'));
                        $code = $this->m_jobs->get_branch_code($user);
                        foreach($code as $b)
                        {
                            $this->m_jobs->update_branch_code_orders($order_id,$b['branch_code'],$b['branch_name']);
                        }
                        $data = array(
                            'status' => 1,
                            'order_id' => $order_id,
                            'date' => date("Y / m / d")
                        );
                        echo json_encode($data);
                    }
                }
            }
            elseif($this->uri->segment(4) == 'setpay')
            {
                $data =  $this->m_jobs->updaterec($this->uri->segment(5));
                echo json_encode($data);
            }
            else
            {
                echo 'No action required!';
            }
	} 
	
	public function get()
	{
            if($this->uri->segment(4) == 'cake_id')
            {
                $data = $this->m_jobs->get_cake_type_id($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'locations')
            {
                $data = $this->m_jobs->get_locations();
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'branches')
            {
                $data = $this->m_jobs->get_branches();
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'cake_upgrade')
            {
                $data = $this->m_jobs->get_cake_upgrades($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'addon')
            {
                $data = $this->m_jobs->get_addon($this->uri->segment(5));
                echo json_encode($data);
            }
            
            //for get prices in cusomized order
            elseif($this->uri->segment(4) == 'g_size')
            {
                $data = $this->m_jobs->g_size($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'g_flavor')
            {
                $data = $this->m_jobs->g_flavor($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'g_icing_type')
            {
                $data = $this->m_jobs->g_icing_type($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'g_order_set')
            {
                $data = $this->m_jobs->g_order_set($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'g_design')
            {
                $data = $this->m_jobs->g_design($this->uri->segment(5));
                echo json_encode($data);
            }
            //end prices
            
            elseif($this->uri->segment(4) == 'size')
            {
                $data = $this->m_jobs->get_size($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'flavor')
            {
                $data = $this->m_jobs->get_flavor($this->uri->segment(5),$this->uri->segment(6));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'icing_type')
            {
                $data = $this->m_jobs->get_icing_types($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'design')
            {
                $data = $this->m_jobs->get_design($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'catalogue_products')
            {
                $data = $this->m_jobs->get_catalogue_products($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'getExsistCustomerData')
            {
                $data = $this->m_jobs->getExsistCustomerData($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'catalogue_price')
            {
                $data = $this->m_jobs->get_products($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'collection_charge')
            {
                $data = $this->m_jobs->get_collection_charge($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'priority_charge')
            {
                $data = $this->m_jobs->get_priority_charge($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'priority_charge2')
            {
                $data = $this->m_jobs->get_priority_charge2($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'discount')
            {
                $data = $this->m_jobs->get_discount_charge($this->uri->segment(5));
                echo json_encode($data);
            }
            elseif($this->uri->segment(4) == 'rec')
            {
                /*if($this->uri->segment(6) == 'order_summary')
                {
                    $data = $this->m_jobs->get_reciept_order_summary($this->uri->segment(5));
                    echo json_encode($data);
                }*/
                if($this->uri->segment(6) == 'order_summary2')
                {
                    $data = $this->m_jobs->get_order_data2($this->uri->segment(5));
                    echo json_encode($data);
                }
                elseif($this->uri->segment(6) == 'order_data')
                {
                    $data = $this->m_jobs->get_reciept_order_data($this->uri->segment(5));
                    echo json_encode($data);
                }
                elseif($this->uri->segment(6) == 'getlastAc')
                {
                    $data = $this->m_jobs->get_reciept_order_data($this->uri->segment(5));
                    echo json_encode($data);
                }
                elseif($this->uri->segment(6) == 'get_addons')
                {
                    $data = $this->m_jobs->get_addons_order_data($this->uri->segment(5));
                    echo json_encode($data);
                }
                else
                {
                    echo 'A mistake from you nigga!';
                }
            }
            //tables
            //internal baking
            elseif($this->uri->segment(4) == 'internalbakingmto')
            {
                if($this->uri->segment(5) == 'cake')
                {
                    $data = $this->m_jobs->get_internal_mto_tables_classfication_cakes();
                    if($data)
                    {
                        $date1;
                        $date2;
                        $time1;
                        $time2;
                        $jo;
                        foreach ($data as $key => $row) 
                        {
                            if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                                $date1 = '';
                                $time1 = '';
                            }else{
                                $date1 = date("F jS Y",strtotime($row['delivery_date']));
                                $time1 = $row['delivery_time'];
                            }
                            if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                                $date2 = '';
                                $time2 = '';
                            }else{
                                $date2 = date("F jS Y",strtotime($row['pickup_date']));
                                $time2 = $row['pickup_time'];
                            }
                            if($row['JO_no'] == ''){
                               $jo = $row['branch_code'].'-'.$row['id'];
                            }else{
                               $jo = $row['JO_no'];
                            }

                            $data[$key] = array( 
                                $row['id'],
                                $row['catalogue'],
                                $jo,
                                $row['title'],
                                $row['order_set'],
                                $row['design'],
                                $row['flavor'],
                                $row['size'],
                                $row['cake_upgrade'],
                                $date1.''.$date2,
                                $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                                '
                                    <button type="button" class="btn btn-default">Select</button>
                                '
                            );
                        }
                    }
                    else
                    {
                        $data=array();
                    }
                    echo json_encode(array('aaData'=> array_filter($data)));
                    exit();
                }
                elseif($this->uri->segment(5) == 'cookie')
                {
                    $data = $this->m_jobs->get_internal_mto_tables_classfication_cookie();
                    if($data)
                    {
                        $date1;
                        $date2;
                        $time1;
                        $time2;
                        $jo;
                        foreach ($data as $key => $row) 
                        {
                            if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                                $date1 = '';
                                $time1 = '';
                            }else{
                                $date1 = date("F jS Y",strtotime($row['delivery_date']));
                                $time1 = $row['delivery_time'];
                            }
                            if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                                $date2 = '';
                                $time2 = '';
                            }else{
                                $date2 = date("F jS Y",strtotime($row['pickup_date']));
                                $time2 = $row['pickup_time'];
                            }
                            if($row['JO_no'] == ''){
                               $jo = $row['branch_code'].'-'.$row['id'];
                            }else{
                               $jo = $row['JO_no'];
                            }

                            $data[$key] = array( 
                                $row['id'],
                                $jo,
                                $row['title'],
                                $row['order_set'],
                                $row['qty'],
                                $date1.''.$date2,
                                $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                                '
                                    <button type="button" class="btn btn-default">Select</button>
                                '
                            );
                        }
                    }
                    else
                    {
                        $data=array();
                    }
                    echo json_encode(array('aaData'=> array_filter($data)));
                    exit();
                }
                else
                {
                    show_404();
                }
            }
            //end
            //internal decorating
            elseif($this->uri->segment(4) == 'internaldecomto')
            {
                if($this->uri->segment(5) == 'cake')
                {
                    $data = $this->m_jobs->get_internal_deco_tables_classfication_cakes();
                    if($data)
                    {
                        $date1;
                        $date2;
                        $time1;
                        $time2;
                        $jo;
                        foreach ($data as $key => $row) 
                        {
                            if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                                $date1 = '';
                                $time1 = '';
                            }else{
                                $date1 = date("F jS Y",strtotime($row['delivery_date']));
                                $time1 = $row['delivery_time'];
                            }
                            if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                                $date2 = '';
                                $time2 = '';
                            }else{
                                $date2 = date("F jS Y",strtotime($row['pickup_date']));
                                $time2 = $row['pickup_time'];
                            }
                            if($row['JO_no'] == ''){
                               $jo = $row['branch_code'].'-'.$row['id'];
                            }else{
                               $jo = $row['JO_no'];
                            }
                            if($row['stage_serve_by'] == '')
                            {
                               $servant = '<button class="btn btn-info">Open</button>';
                            }
                            else
                            {
                                $servant = $row['stage_serve_by'];
                            }
                            $data[$key] = array( 
                                $row['id'],
                                $jo,
                                $row['catalogue'],
                                $row['title'],
                                $row['order_set'],
                                $date1.''.$date2,
                                $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                                $servant
                            );
                        }
                    }
                    else
                    {
                        $data=array();
                    }
                    echo json_encode(array('aaData'=> array_filter($data)));
                    exit();
                }
                elseif($this->uri->segment(5) == 'cookie')
                {
                    $data = $this->m_jobs->get_internal_deco_tables_classfication_cookie();
                    if($data)
                    {
                        $date1;
                        $date2;
                        $time1;
                        $time2;
                        $jo;
                        foreach ($data as $key => $row) 
                        {
                            if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                                $date1 = '';
                                $time1 = '';
                            }else{
                                $date1 = date("F jS Y",strtotime($row['delivery_date']));
                                $time1 = $row['delivery_time'];
                            }
                            if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                                $date2 = '';
                                $time2 = '';
                            }else{
                                $date2 = date("F jS Y",strtotime($row['pickup_date']));
                                $time2 = $row['pickup_time'];
                            }
                            if($row['JO_no'] == ''){
                               $jo = $row['branch_code'].'-'.$row['id'];
                            }else{
                               $jo = $row['JO_no'];
                            }

                            $data[$key] = array( 
                                $row['id'],
                                $jo,
                                $row['title'],
                                $row['order_set'],
                                $row['qty'],
                                $row['balance'],
                                $date1.''.$date2,
                                $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                                $row['qty'],
                                $date1.''.$date2
                            );
                        }
                    }
                    else
                    {
                        $data=array();
                    }
                    echo json_encode(array('aaData'=> array_filter($data)));
                    exit();
                }
                else
                {
                    show_404();
                }
            }
            //end
            //internal finishing
            elseif($this->uri->segment(4) == 'internalfinishingmto')
            {
                if($this->uri->segment(5) == 'cake')
                {
                    $data = $this->m_jobs->get_internal_finishing_tables_classfication_cakes();
                    if($data)
                    {
                        $date1;
                        $date2;
                        $time1;
                        $time2;
                        $jo;
                        foreach ($data as $key => $row) 
                        {
                            if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                                $date1 = '';
                                $time1 = '';
                            }else{
                                $date1 = date("F jS Y",strtotime($row['delivery_date']));
                                $time1 = $row['delivery_time'];
                            }
                            if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                                $date2 = '';
                                $time2 = '';
                            }else{
                                $date2 = date("F jS Y",strtotime($row['pickup_date']));
                                $time2 = $row['pickup_time'];
                            }
                            if($row['JO_no'] == ''){
                               $jo = $row['branch_code'].'-'.$row['id'];
                            }else{
                               $jo = $row['JO_no'];
                            }
                            if($row['stage_serve_by'] == '')
                            {
                               $servant = '<button class="btn btn-info">Open</button>';
                            }
                            else
                            {
                                $servant = $row['stage_serve_by'];
                            }
                            $data[$key] = array( 
                                $row['id'],
                                $jo,
                                $row['catalogue'],
                                $row['title'],
                                $row['order_set'],
                                $date1.''.$date2,
                                $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                                $servant
                            );
                        }
                    }
                    else
                    {
                        $data=array();
                    }
                    echo json_encode(array('aaData'=> array_filter($data)));
                    exit();
                }
                elseif($this->uri->segment(5) == 'cookie')
                {
                    $data = $this->m_jobs->get_internal_finishing_tables_classfication_cookie();
                    if($data)
                    {
                        $date1;
                        $date2;
                        $time1;
                        $time2;
                        $jo;
                        foreach ($data as $key => $row) 
                        {
                            if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                                $date1 = '';
                                $time1 = '';
                            }else{
                                $date1 = date("F jS Y",strtotime($row['delivery_date']));
                                $time1 = $row['delivery_time'];
                            }
                            if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                                $date2 = '';
                                $time2 = '';
                            }else{
                                $date2 = date("F jS Y",strtotime($row['pickup_date']));
                                $time2 = $row['pickup_time'];
                            }
                            if($row['JO_no'] == ''){
                               $jo = $row['branch_code'].'-'.$row['id'];
                            }else{
                               $jo = $row['JO_no'];
                            }

                            $data[$key] = array( 
                                $row['id'],
                                $jo,
                                $row['title'],
                                $row['order_set'],
                                $row['qty'],
                                $row['balance'],
                                $date1.''.$date2,
                                $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                                $row['qty'],
                                $date1.''.$date2
                            );
                        }
                    }
                    else
                    {
                        $data=array();
                    }
                    echo json_encode(array('aaData'=> array_filter($data)));
                    exit();
                }
                else
                {
                    show_404();
                }
            }
            //end
            //internal Done
            elseif($this->uri->segment(4) == 'internaldonemto')
            {
                if($this->uri->segment(5) == 'cake')
                {
                    $data = $this->m_jobs->get_internal_done_tables_classfication_cakes();
                    if($data)
                    {
                        $date1;
                        $date2;
                        $time1;
                        $time2;
                        $jo;
                        foreach ($data as $key => $row) 
                        {
                            if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                                $date1 = '';
                                $time1 = '';
                            }else{
                                $date1 = date("F jS Y",strtotime($row['delivery_date']));
                                $time1 = $row['delivery_time'];
                            }
                            if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                                $date2 = '';
                                $time2 = '';
                            }else{
                                $date2 = date("F jS Y",strtotime($row['pickup_date']));
                                $time2 = $row['pickup_time'];
                            }
                            if($row['JO_no'] == ''){
                               $jo = $row['branch_code'].'-'.$row['id'];
                            }else{
                               $jo = $row['JO_no'];
                            }
                            if($row['stage_serve_by'] == '')
                            {
                               $servant = '<button class="btn btn-info">Open</button>';
                            }
                            else
                            {
                                $servant = $row['stage_serve_by'];
                            }
                            $data[$key] = array( 
                                $row['id'],
                                $jo,
                                $row['catalogue'],
                                $row['title'],
                                $row['order_set'],
                                $date1.''.$date2,
                                $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                                'Not Function Yet',
                                'Not Function Yet',
                                '<button type="button" class="btn btn-primary">Checked</button>',
                            );
                        }
                    }
                    else
                    {
                        $data=array();
                    }
                    echo json_encode(array('aaData'=> array_filter($data)));
                    exit();
                }
                elseif($this->uri->segment(5) == 'cookie')
                {
                    $data = $this->m_jobs->get_internal_done_tables_classfication_cookie();
                    if($data)
                    {
                        $date1;
                        $date2;
                        $time1;
                        $time2;
                        $jo;
                        foreach ($data as $key => $row) 
                        {
                            if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                                $date1 = '';
                                $time1 = '';
                            }else{
                                $date1 = date("F jS Y",strtotime($row['delivery_date']));
                                $time1 = $row['delivery_time'];
                            }
                            if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                                $date2 = '';
                                $time2 = '';
                            }else{
                                $date2 = date("F jS Y",strtotime($row['pickup_date']));
                                $time2 = $row['pickup_time'];
                            }
                            if($row['JO_no'] == ''){
                               $jo = $row['branch_code'].'-'.$row['id'];
                            }else{
                               $jo = $row['JO_no'];
                            }

                            $data[$key] = array( 
                                $row['id'],
                                $jo,
                                $row['title'],
                                $row['order_set'],
                                $row['qty'],
                                $row['balance'],
                                $date1.''.$date2,
                                $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                                $row['qty'],
                                $date1.''.$date2,
                                '<button type="button" class="btn btn-primary">Checked</button>'
                            );
                        }
                    }
                    else
                    {
                        $data=array();
                    }
                    echo json_encode(array('aaData'=> array_filter($data)));
                    exit();
                }
                else
                {
                    show_404();
                }
            }
            //end
            //TRACKER FUNCTIONS
            elseif($this->uri->segment(4) == 'get_fondant_mto_cakes')
            {
                $data = $this->m_jobs->get_fondant_mto_cakes();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        if($row['stage_serve_by'] == '')
                        {
                           $servant = '<button class="btn btn-info">Open</button>';
                        }
                        else
                        {
                            $servant = $row['stage_serve_by'];
                        }
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['title'],
                            $row['order_set'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '
                            <button class="btn btn-primary">Open</button></button>
                            '
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'get_fondant_mto_decorating')
            {
                $data = $this->m_jobs->get_fondant_mto_decorating();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        if($row['stage_serve_by'] == '')
                        {
                           $servant = '<button class="btn btn-info">Open</button>';
                        }
                        else
                        {
                            $servant = $row['stage_serve_by'];
                        }
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['title'],
                            $row['order_set'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '
                            <button class="btn btn-success">Done</button></button>
                            '
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'get_fondant_mto_finishing')
            {
                $data = $this->m_jobs->get_fondant_mto_finishing();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        if($row['stage_serve_by'] == '')
                        {
                           $servant = '<button class="btn btn-info">Open</button>';
                        }
                        else
                        {
                            $servant = $row['stage_serve_by'];
                        }
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['title'],
                            $row['order_set'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '
                            <button class="btn btn-primary">Open</button></button>
                            '
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'get_fondant_mto_done')
            {
                $data = $this->m_jobs->get_fondant_mto_done();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        if($row['stage_serve_by'] == '')
                        {
                           $servant = '<button class="btn btn-info">Open</button>';
                        }
                        else
                        {
                            $servant = $row['stage_serve_by'];
                        }
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['title'],
                            $row['order_set'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat']
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'trackercookies')
            {
                $data = $this->m_jobs->get_trackercookies();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }

                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['title'],
                            $row['order_set'],
                            $row['qty']. ' pcs',
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '
                                <button class="btn btn-info">Start</button>&nbsp;<button class="btn btn-success">Done</button>
                            ',
                            '
                                <input type="text" class="form-control" />
                            ',
                            $date1.''.$date2
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'StoreSalesjobrequestOnTracker')
            {
                $data = $this->m_jobs->get_StoreSalesjobrequestOnTracker();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['first_name'].' '.$row['last_name'],
                            $row['icing_type'],
                            $row['title'],
                            $row['cake_type'],
                            $row['claim_type'],
                            $row['delivery_location'].''.$row['pickup_branch'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat']
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'StoreSalesonProgessOnTracker')
            {
                $data = $this->m_jobs->get_StoreSalesonProgessOnTracker();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['first_name'].' '.$row['last_name'],
                            $row['icing_type'],
                            $row['title'],
                            $row['cake_type'],
                            $row['claim_type'],
                            $row['delivery_location'].''.$row['pickup_branch'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat']
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'jobExternalProductionJobRequest')
            {
                $data = $this->m_jobs->get_jobExternalProductionJobRequest();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['first_name'].' '.$row['last_name'],
                            $row['icing_type'],
                            $row['title'],
                            $row['cake_type'],
                            $row['claim_type'],
                            $row['delivery_location'].''.$row['pickup_branch'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '<button type="button" class="btn btn-info"><span class="fa fa-arrow-right"></span> Start</button>',
                            '<button type="button" class="btn btn-default"><span class="fa fa-print"></span> Print this data</button>'
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'jobExternalProductionOnProgress')
            {
                $data = $this->m_jobs->get_jobExternalProductionOnProgress();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['first_name'].' '.$row['last_name'],
                            $row['icing_type'],
                            $row['title'],
                            $row['cake_type'],
                            $row['claim_type'],
                            $row['delivery_location'].''.$row['pickup_branch'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '<button type="button" class="btn btn-default"><span class="fa fa-print"></span> Print this data</button>'
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'StoreSalesonDoneTracker')
            {
                $data = $this->m_jobs->get_StoreSalesonDoneTracker();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['first_name'].' '.$row['last_name'],
                            $row['icing_type'],
                            $row['title'],
                            $row['cake_type'],
                            $row['claim_type'],
                            $row['delivery_location'].''.$row['pickup_branch'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '<button type="button" class="btn btn-info active">Received</button>'
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'mtoClaimatCommisary')
            {
                $data = $this->m_jobs->get_mtoClaimatCommisary();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        if($row['claim_status'] == ''){
                           $c_status = '<button type="button" class="btn btn-default">Not Yet Claim</button>'; 
                        }else{
                           $c_status = '<button type="button" class="btn btn-success"><span class="fa fa-check></span> Claimed</button>'; 
                        }
                        
                        $data[$key] = array( 
                            $row['id'],
                            $row['branch_name'],
                            $jo,
                            $row['icing_type'],
                            $row['title'],
                            $row['cake_type'],
                            $row['claim_type'],
                            $row['delivery_location'].''.$row['pickup_branch'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            $row['total_price_amount'],
                            $row['balance'],
                            $row['amount_paid'],
                            $c_status
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'mtodispatched')
            {
                $data = $this->m_jobs->get_mtodispatched();
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['icing_type'],
                            $row['title'],
                            $row['claim_type'],
                            $row['delivery_location'].''.$row['pickup_branch'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '
                            <button class="btn btn-default">Select</button>
                            ',
                            '
                            <button class="btn btn-info">Recieved</button>
                            ',
                            
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(4) == 'existing-user')
            {
                $data = $this->m_jobs->get_customer();
                if($data)
                {
                    foreach ($data as $key => $row) 
                    {
                        $data[$key] = array( 
                            $row['id'],
                            $row['firstname'],
                            $row['lastname'],
                            $row['email'],
                            $row['address'],
                            $row['location'],
                            $row['contact'],
                            $row['buy_count'],
                            $row['created_on'],
                            '<button type="button" class="btn btn-info action_select ifselected_'.$row['id'].'" data-id="'.$row['id'].'" data-identity="select-customer"><span class="fa fa-check"></span> Select</button></td>'
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            //end tables
            else show_404();
	} 
	
	public function update()
	{
		
	} 
	
	public function delete()
	{
	
	} 
        
        public function custom($command)
        {
           if($command == 'reciept_catalogue')
           {
              echo json_encode($this->m_jobs->get_reciept_data($this->uri->segment(4)));
           }
           else show_404();
        }
}

?>