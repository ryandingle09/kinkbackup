<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends MX_Controller {

	var $user_session = '';
	public function __construct()
	{
		parent::__construct();
		$this->user_session = $this->session->userdata('user_data');
		if(empty($this->user_session)){
			redirect('login');
		}
		$module_name	=  get_class($this);
		if(!in_array($module_name, $this->user_session['allowedModules'])){
			redirect($this->user_session['default_home']);
		}

		$this->load->helper('security');
		$this->load->model('m_jobs');
	}

	public function index()
	{
		//check here if user has valid access on each action
		redirect($this->user_session['default_home']);
	}
	
	public function order()
	{
		 $user_session = $this->user_session;
		  $catalog =  $this->uri->segment(3);
		 
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
		<script type="text/javascript" src="js/plugins/smartwizard/jquery.smartWizard-2.0.min.js"></script>
                <script type="text/javascript" src="js/plugins/jquery-validation/jquery.validate.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
                <script type="text/javascript" src="js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
		<script type="text/javascript" src="js/jobOrderVars.js"></script>
		<script type="text/javascript" src="js/jOscript.js"></script>
                <script type="text/javascript" src="js/JOtableScript.js"></script>
		<script>
                    $(document).ready(function(){
                            jo();
                    });
		</script>'; 
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		
		if($catalog ==''){
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));	
		}else{
			$this->render($action.'/'.$class.'_catalog',$sidebar,$navigation,compact('scripts', 'nav'));
		}
		
	}

	public function tracker()
	{
		 $user_session = $this->user_session;
		  
		 
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
		<script type="text/javascript" src="js/plugins/smartwizard/jquery.smartWizard-2.0.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
		<script type="text/javascript" src="js/jobOrderVars.js"></script>
		<script type="text/javascript" src="js/jOscript.js"></script>
                <script type="text/javascript" src="js/JOtableScript.js"></script>
		<script>
			$(document).ready(function(){
				jo();
			});
		</script>'; 
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		 
		$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));	
		 
		
	}

	public function internal()
	{
		 $user_session = $this->user_session;
		  
		 
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
		<script type="text/javascript" src="js/plugins/smartwizard/jquery.smartWizard-2.0.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
		<script type="text/javascript" src="js/jobOrderVars.js"></script>
		<script type="text/javascript" src="js/jOscript.js"></script>
                <script type="text/javascript" src="js/JOtableScript.js"></script>
		 '; 
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		 
		
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));		
		}
		
	}

	public function external()
	{
		 $user_session = $this->user_session;
		  
		 
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
		<script type="text/javascript" src="js/plugins/smartwizard/jquery.smartWizard-2.0.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
		<script type="text/javascript" src="js/jobOrderVars.js"></script>
		<script type="text/javascript" src="js/jOscript.js"></script>
                <script type="text/javascript" src="js/JOtableScript.js"></script>
		'; 
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		 
			
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));	
		}
		
	}

	public function claims()
	{
		 $user_session = $this->user_session;
		  
		 
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/jobOrderVars.js"></script>
		<script type="text/javascript" src="js/jOscript.js"></script>
                <script type="text/javascript" src="js/JOtableScript.js"></script>
		'; 
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		 
		
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));		
		} 
		
	}



}

/* End of file jobs.php */
/* Location: ./application/modules/jobs/controllers/jobs.php */