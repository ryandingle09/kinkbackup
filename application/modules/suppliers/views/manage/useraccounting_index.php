
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-6">
        <div id="step-1" class="content" style="display: block;"> 
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-plus"></span> Add new Supplier</h3>
                    <div class="form-group col-md-6">
                        <label>Supplier</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Contact person</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Address</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Phone ( Primary )</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Phone ( Alternative )</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Fax</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Email</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <button class="btn btn-primary">Save Supplier</button>
                    </div>
                </div>                              
            </div>        
        </div>
    </div>
    <div class="col-md-6">
        <div id="step-2" class="content" style="display: block;"> 
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-plus"></span> Add new Item</h3>
                    <div class="form-group col-md-6">
                        <label>Item</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Unit Cost</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Unit of Measure</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Item Description</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Payment Terms</label>
                        <select class="form-control">
                            <option>select --</option>
                            <option>COD</option>
                            <option>15 Days</option>
                            <option>30 Days</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Claim Type</label>
                        <select class="form-control">
                            <option>select --</option>
                            <option>Delivery</option>
                            <option>Pick-up</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <button class="btn btn-primary">Save Item</button>
                    </div>
                </div>                              
            </div>        
        </div>
    </div>
    <div class="col-md-3">
        <div id="step-3" class="content" style="display: block;"> 
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3><span class="fa fa-plus"></span> Add Payment Terms</h3>
                    <div class="form-group">
                        <input type="email" class="form-control">
                    </div><br>
                    <div class="form-group">
                        <button class="btn btn-primary">Save Term</button>
                    </div>
                </div>                              
            </div>        
        </div>
    </div>
</div>
<div class="panel panel-default tabs">                            
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#tab-third" role="tab" data-toggle="tab">Supplier</a></li>
    </ul>                            
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab-third">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Supplier</th>
                        <th>Product Name</th>
                        <th>Contact Person</th>
                        <th>Address</th>
                        <th>Phone No.</th>
                        <th>Fax</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for($a=1;$a<=200;$a++) {?>
                    <tr>
                        <th><?php echo $a ;?></th>
                        <th>Test</th>
                        <th>Test</th>
                        <th>xxx</th>
                        <th><?php echo $a ;?></th>
                        <th>xxx</th>
                        <th>Test</th>
                        <th>Test</th>
                        <th>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">View</a></li>
                                    <li><a href="#">Edit</a></li>
                                    <li><a href="#">Delete</a></li>
                                </ul>
                            </div>
                        </th>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
                <div class="mb-container">
                    <div class="mb-middle">
                        <div class="mb-title"><span class="fa fa-times"></span> Are you sure?</div>
                        <div class="mb-content">
                            <p>Are you sure you want to Claim?</p>
                        </div>
                        <div class="mb-footer">
                            <button class="btn btn-success btn-lg">Yes</button>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>