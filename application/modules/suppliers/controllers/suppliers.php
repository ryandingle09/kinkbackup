<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suppliers extends MX_Controller {


	var $user_session = '';
	var $module_name ='';
	public function __construct()
	{
		parent::__construct();
		$this->user_session = $this->session->userdata('user_data');
		if(empty($this->user_session)){
			redirect('login');
		}
		$this->module_name	=  get_class($this);
		if(!in_array($this->module_name, $this->user_session['allowedModules'])){
			redirect($this->user_session['default_home']);
		}


	}

	public function index()
	{
		//check here if user has valid access on each action
		redirect($this->user_session['default_home']);
	}

	public function manage()
	{
		$user_session = $this->user_session;
		  
		 
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
		<script type="text/javascript" src="js/plugins/smartwizard/jquery.smartWizard-2.0.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
		 '; 
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		 
		
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			//redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));		
		}
	}


}

/* End of file supplier.php */
/* Location: ./application/modules/supplier/controllers/supplier.php */