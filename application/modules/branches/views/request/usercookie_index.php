<div class="panel panel-danger">
    <div class="panel-heading">                                
        <h3 class="panel-title">Available Cookies</h3>                           
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>RO#</th>
                    <th>Order Date</th>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Notes</th>
                    <th>Action</th>
                    <th>Quantity Produce</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th><button class="btn btn-info">Start</button>&nbsp;<button class="btn btn-success">Done</button></th>
                    <th><input type="text" class="form-control" /></th>
                    <th>test</th>
                </tr>
                <tr style="background: #eee;">
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th><button class="btn btn-info">Start</button>&nbsp;<button class="btn btn-success">Done</button></th>
                    <th><input type="text" class="form-control" /></th>
                    <th>test</th>
                </tr>
            </tbody>
        </table>
    </div>
</div> 