<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab8" data-toggle="tab">Requests</a></li>
        <li><a href="#tab9" data-toggle="tab">Issued</a></li>
    </ul>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab8">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Request No.</th>
                        <th>Request Date</th>
                        <th>Branch</th>
                        <th>Item Category</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Notes</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>GH-001</td>
                        <td>xxx</td>
                        <td>KC Timog</td>
                        <td>Cookie</td>
                        <td>Adult Cookies</td>
                        <td>10</td>
                        <td>pc</td>
                        <td> -- </td>
                        <td>
                            <!-- Single button -->
                            <div class="btn-group">
                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Action <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Process</a></li>
                                <li><a href="#" data-toggle="modal" data-target=".modal-lg-issued">Edit Request</a></li>
                              </ul>
                            </div>
                            <!-- if on processing delivering 
                            <button type="button" class="btn btn-warning">Process</button>
                            if delivred
                            <button type="button" class="btn btn-success">Delivered</button>
                            -->
                        </td>
                    </tr>
                    <tr>
                        <td>TMG-001</td>
                        <td>xxx</td>
                        <td>KC Timog</td>
                        <td>Chocolate</td>
                        <td>Dick Choco</td>
                        <td>10</td>
                        <td>pc</td>
                        <td> -- </td>
                        <td><button type="button" class="btn btn-success">Delivered</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="tab-pane" id="tab9">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Branch</th>
                        <th>Item Category</th>
                        <th>Product Name</th>
                        <th>Qty</th>
                        <th>Unit</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td>KC Timog
                        </td>
                        <td>Available Cake
                        </td>
                        <td>Dick Man Lying  
                        </td>
                        <td>10</td>
                        <td>pc</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>KC Timog
                        </td>
                        <td>Cookie
                        </td>
                        <td>Adult Cookies
                        </td>
                        <td>36</td>
                        <td>pc</td>
                    </tr>
                </tbody>
            </table>
        </div>   
    </div>
</div> 

<!-- MODAL FOR Action Edit Issued Request --->
<div class="modal fade modal-lg-issued" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	<div class="panel">
        	
        </div>
    </div>
  </div>
</div>