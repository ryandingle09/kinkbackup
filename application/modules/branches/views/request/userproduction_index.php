<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab8" data-toggle="tab">Item Requests</a></li>
        <li><a href="#tab9" data-toggle="tab">Issuance</a></li>
        <li><a href="#tab10" data-toggle="tab">Dispatch</a></li>
    </ul>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab8">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>RQ#</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Item Category</th>
                        <th>Product Name</th>
                        <th>Qty</th>
                        <th>Unit</th>
                        <th>Notes</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td><input type="text" class="form-control" /></td>
                        <td><input type="text" class="form-control" /></td>
                        <td><button class="btn btn-info">Process</button></td>
                    </tr>
                </tbody>
            </table>
            <button class="btn btn-primary pull-right" style="margin-top: 30px"><span class="fa fa-print"></span> Print</button>
        </div>
        
        <div class="tab-pane" id="tab9">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>RQ#</th>
                        <th>Order Date</th>
                        <th>Time</th>
                        <th>Item Category</th>
                        <th>Product Name</th>
                        <th>Flavor</th>
                        <th>Qty</th>
                        <th>Unit</th>
                        <th>Feedback</th>
                        <th>Action</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td><input type="text" class="form-control" /></td>
                        <td><button class="btn btn-info">Dispatch</button></td>
                        <td> -- </td>
                    </tr>
                    <tr>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td>xx</td>
                        <td><input type="text" class="form-control" /></td>
                        <td><button class="btn btn-info">Dispatch</button></td>
                        <td><button class="btn btn-success">Recieved</button></td>
                    </tr>
                </tbody>
            </table>
        </div>   
    
        <div class="tab-pane" id="tab10">
            <legend><h3>BRANCH REQUEST ITEMS</h3></legend>
            <div class="row" style="margin-bottom: 20px;margin-top: 20px">
                <div class="col-md-4 pull-left">
                    <h5>Delivery Order No :&nbsp;&nbsp;&nbsp;<b style="border-bottom: 1px solid #ccc">0002</b>&nbsp;&nbsp;&nbsp;Driver :&nbsp&nbsp<b style="border-bottom: 1px solid #ccc">_________________</b></h5>
                </div>
                <div class="col-md-1 pull-right">
                    <h5>Date :&nbsp;&nbsp;&nbsp;<b style="border: 1px solid #ccc">12/12/09</b></h5>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>RQ#</th>
                        <th>Item Category</th>
                        <th>Product Name</th>
                        <th>Flavor</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Location</th>
                        <th>Notes</th>
                        <th>Recieved By</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody>
                <?php for($a=1;$a<=10;$a++){ ?>
                    <tr>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
            <button class="btn btn-primary pull-right"><span class="fa fa-print"></span> Print Delivery Order Form</button>
        </div>

    </div>
</div> 