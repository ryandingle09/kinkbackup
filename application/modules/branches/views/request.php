<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab8" data-toggle="tab">Available Cakes</a></li>
        <li><a href="#tab9" data-toggle="tab">Decorating Stage</a></li>
        <li><a href="#tab10" data-toggle="tab">Finishing Stage</a></li>
        <li><a href="#tab11" data-toggle="tab">Done</a></li>
    </ul>
    <div class="panel-body tab-content">

        <div class="tab-pane active" id="tab8">
        <!--
             <div class="col-md-4" style="margin-bottom: 50px">
                <form class="form-horizontal">
                    <fieldset>
                        <legend><h3>Enter Job</h3></legend>
                        <div class="col-lg-12">
                            <label class="col-lg-4 form-group">RQ#</label>
                            <div class="col-md-6">
                                <select class="form-control">
                                    <option value="">---</option>
                                    <option value="">MKT-103</option>
                                    <option value="">TMG-234</option>
                                    <option value="">GH-345</option>
                                </select>
                            </div>   
                        </div>
                        <div class="col-lg-12">
                            <label class="col-lg-4 form-group">Product Name</label>
                            <div class="col-md-5">
                                <select class="form-control">
                                    <option value="">---</option>
                                    <option value="">xxx</option>
                                    <option value="">xxx</option>
                                    <option value="">xxx</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <label class="col-lg-4 form-group">Flavor</label>
                            <div class="col-md-5">
                                <select class="form-control">
                                    <option value="">---</option>
                                    <option value="">Mocha</option>
                                    <option value="">Vanila</option>
                                    <option value="">Choco Moist</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
      -->

            <table class="table datatable">
                <thead>
                    <tr>
                        <th>RQ#</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Notes</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td><button type="button" class="btn btn-info">Open</button></td>
                    </tr>
                </tbody>
            </table>
        </div>  
        
        <div class="tab-pane" id="tab9">
        	<table class="table datatable">
                <thead>
                    <tr>
                        <th>RQ#</th>
                        <th>Product Name</th>
                        <th>Flavor</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Notes</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>test</td>
                        <td>test</td>
                        <td>
                        <select class="form-control">
                            <option value="">---</option>
                            <option value="">Mocha</option>
                            <option value="">Vanila</option>
                            <option value="">Choco Moist</option>
                        </select>
                        </td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td><button type="button" class="btn btn-info">Done</button></td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <div class="tab-pane" id="tab10">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>RQ#</th>
                        <th>Product Name</th>
                        <th>Flavor</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Notes</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>test</td>
                        <td>test</td>
                        <td>Vanilla</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td><button type="button" class="btn btn-info">Open</button></td>
                    </tr>
                </tbody>
            </table>
            
            <legend style="padding-top: 50px"><h3>Selected Job | Item</h3></legend>
            <table class="table">
                <thead>
                    <tr>
                        <th>RQ#</th>
                        <th>Product Name</th>
                        <th>Flavor</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Notes</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>test</td>
                        <td>test</td>
                        <td>Vanilla</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                        <td><button type="button" class="btn btn-info">Done</button></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="tab11">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>RQ#</th>
                        <th>Product Name</th>
                        <th>Flavor</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>test</td>
                        <td>test</td>
                        <td>test</td>
                    </tr>
                </tbody>
            </table>
        </div>  
    </div>
</div> 