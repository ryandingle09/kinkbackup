<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Branches extends MX_Controller {

	var $user_session = '';
	public function __construct()
	{
		parent::__construct();
		$this->user_session = $this->session->userdata('user_data');
		if(empty($this->user_session)){
			redirect('login');
		}
		$module_name	=  get_class($this);
		if(!in_array($module_name, $this->user_session['allowedModules'])){
			redirect($this->user_session['default_home']);
		}


	}
	
	public function request()
	{
		 $user_session = $this->user_session;
		  
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>'; 
		    $page = $user_session['page'];
		  $folder = $page;
		   $class = $user_session['class'];
		 $action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));
	}

}

/* End of file branches.php */
/* Location: ./application/modules/branches/controllers/branches.php */