<div class="tab-pane active" id="tab9">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h3 class="panel-title"><span class="fa fa-mail-"></span> Available Items</h3>
		</div>
		<div class="panel-body"> 
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-md-3 col-xs-12 control-label">Category</label>
				<div class="col-md-6 col-xs-12">                                                                                            
					<select class="form-control select">
						<option>Available Cakes</option>
						<option>Option 2</option>
						<option>Option 3</option>
						<option>Option 4</option>
						<option>Option 5</option>
					</select>
					<!-- <span class="help-block">Select box example</span> -->
				</div>
			</div>
            <!--
            <div class="form-group">
				<label class="col-md-3 col-xs-12 control-label">Mode of Payment</label>
				<div class="col-md-6 col-xs-12">                                                                                            
					<select class="form-control select">
						<option value="">Cash</option>
						<option value="">Card</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-xs-12 control-label">Amount</label>
				<div class="col-md-6 col-xs-12">
					<input type="text" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-xs-12 control-label">Change</label>
				<div class="col-md-6 col-xs-12">
					<input type="text" class="form-control">
				</div>
			</div>
            <div class="form-group">
            	<label class="col-md-3 col-xs-12 control-label"></label>
				<div class="col-md-6 col-xs-12">
					<button type="button" class="btn btn-primary">Add Item</button>
                </div>
			</div>
            -->
		</form>
		</div>
        <div class="tab-pane active" id="tab9">
            <div class="panel panel-danger">
                <div class="panel-body"> 
                	<table class="table datatable">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Stock</th>
                                <th>Unit</th>
                                <th>Quantity to Purchase</th>
                                <th>Total Price</th>
                               	<th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            	<th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><input type="text" class="" /></th>
                                <th></th>
                                <th><button class="btn btn-info">Select</button></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
		<!--<div class="panel-footer">
			<button type="button" class="btn btn-primary pull-right">Submit</button>
		</div>--->
	</div>
    <div class="row">
    	<div class="col-md-8 pull-right">	
        	<h3>Selected Items</h3>
			<table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Quantityt</th>
                        <th>Mode Of Payment</th>
                        <th>Amount</th>
                        <th>Change</th>
                        <th>Action</th>
                </thead>
                <tbody>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>
                        	<div class="btn-group">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Select <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Cash</a></li>
                                <li><a href="#">Card</a></li>
                              </ul>
                            </div>
                            <input type="text" placeholder="If Promo Ref#" />
                        </th>
                        <th><input type="text" class="" /></th>
                        <th></th>
                        <th><button class="btn btn-danger">Remove</button></th>
                    </tr>
                </tbody>
            </table>        	
        </div>
    </div>
</div>
</div>