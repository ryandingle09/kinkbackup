<div class="panel panel-danger">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-calendar"></span> Inventory</h3>
        <ul class="panel-controls">
            <li><a href="#"  data-target="#modal_large" data-toggle="modal" title="Return"><span class="fa fa-reply"></span></a></li>
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
<!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
</ul>
</div>
<div class="panel-body">
    <select class="form-control select" data-style="btn-primary">
        <option>Select Item</option>
        <option>Available Cake</option>
        <option>Cookies</option>
        <option>Chocolate</option>
    </select>
    <table class="table datatable">
        <thead>
            <tr>
                <th>Image</th>
                <th>Item Category</th>
                <th>Product Name</th>
                <th>Stock on Hand</th>
                <th>Unit</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td id="img">
                    <td>Available Cake</td>
                    <td>Marshmallow Cake</td>
                    <td>12</td>
                    <td>packs</td>
                </tr>
                <tr>
                    <td id="img">
                        <td>Available Cake</td>
                        <td>Marshmallow Cake</td>
                        <td>12</td>
                        <td>packs</td>
                    </tr>
                    <tr>
                        <td id="img">
                            <td>Available Cake</td>
                            <td>Marshmallow Cake</td>
                            <td>12</td>
                            <td>packs</td>
                        </tr>
                        <tr>
                            <td id="img">
                                <td>Available Cake</td>
                                <td>Marshmallow Cake</td>
                                <td>12</td>
                                <td>packs</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div style="display: none;" class="modal" id="modal_large" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="largeModalHead">Return to Commisory</h4>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span class="fa fa-mail-reply"></span> Item Request</h3>
                                </div>
                                <div class="panel-body"> 
                                    <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Category</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control">
                                                <option>Available Cakes</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                            <!-- <span class="help-block">Select box example</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Product</label>
                                        <div class="col-md-6 col-xs-12">                                                                                            
                                            <select class="form-control">
                                                <option>Option 1</option>
                                                <option>Option 2</option>
                                                <option>Option 3</option>
                                                <option>Option 4</option>
                                                <option>Option 5</option>
                                            </select>
                                            <!-- <span class="help-block">Select box example</span> -->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Quantity</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <input class="form-control" type="text">
                                            <!-- <span class="help-block">This is sample of text field</span> -->
                                        </div>
                                    </div>

                                    <div class="form-group">                                        
                                        <label class="col-md-3 col-xs-12 control-label">Date Required</label>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input class="form-control datepicker" value="<?= date('Y-m-d') ?>" type="text">                                            
                                            </div>
                                            <!-- <span class="help-block">Click on input field to get datepicker</span> -->
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Note</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <textarea class="form-control" rows="5"></textarea>
                                            <!-- <span class="help-block">Default textarea field</span> -->
                                        </div>
                                    </div>

                                    </form>

                                </div>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary pull-right">Submit</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
                        </div>
                    </div>
                </div>
            </div>