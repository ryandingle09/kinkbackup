<div class="panel panel-danger">
	<div class="panel-heading">
    	<h3 class="widget-title"><span class="fa fa-reply"></span> Return Items</h3>
    </div>
    <div class="panel-body">
    	<table class="table datatable">
        	<thead>
            	<tr>
                	<th>Date</th>
                    <th>Branch</th>
                    <th>Item Category</th>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                </tr>
                <tr>
                	<th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                </tr>
            </tbody>
        </table>
        <div class="col-md-4 pull-right" style="margin-top: 20px">
            <table class="table table-responsive table-bordered">
                <tbody>
                    <tr>
                        <td>Total</td>
                        <td>xxx</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>