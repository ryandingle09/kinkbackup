<div class="panel panel-danger">
	<div class="panel-heading ui-draggable-handle">
		<h3 class="panel-title">&nbsp;Return Items</h3>
		<ul class="panel-controls">
			<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
<!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
	<li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
</ul>
</div>
<div class="panel-body">	
	<table class="table datatable">
		<thead>
			<tr>
				<th>Date</th>
				<th>Branch</th>
				<th>Item Category</th>
				<th>Product Name</th>
				<th>Qty</th>
				<th>Unit</th>
				<th>Notes</th>
				<th>Validate</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>25-Dec-14</td>
				<td>KC Timog
</td>
				<td>Available Cake
</td>
				<td>Dick Man Lying-Choco
</td>
				<td>1</td>
				<td>pc</td>
				<td>Out	
</td>
				<td><button type="button" class="btn btn-info">Recieved</button></td>
			</tr>
			<tr>
				<td>25-Dec-14</td>
				<td>KC Timog
</td>
				<td>Cookie
</td>
				<td>Adult Cookies
</td>
				<td>36
</td>
				<td>pc
</td>
				<td></td>
				<td><button type="button" class="btn btn-info">Recieved</button></td>
			</tr>
		</tbody>
	</table>
</div>
</div>