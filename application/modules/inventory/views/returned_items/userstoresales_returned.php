<!--
<div class="tab-pane active" id="tab9">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<h3 class="panel-title"><span class="fa fa-mail-reply"></span> Return Item</h3>
		</div>
		<div class="panel-body"> 
		<form class="form-horizontal">
			<div class="form-group">
				<label class="col-md-3 col-xs-12 control-label">Category</label>
				<div class="col-md-6 col-xs-12">                                                                                            
					<select class="form-control select">
						<option>Available Cakes</option>
						<option>Option 2</option>
						<option>Option 3</option>
						<option>Option 4</option>
						<option>Option 5</option>
					</select>
					<!-- <span class="help-block">Select box example</span> -->
				<!---</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-xs-12 control-label">Product</label>
				<div class="col-md-6 col-xs-12">                                                                                            
					<select class="form-control select">
						<option>Option 1</option>
						<option>Option 2</option>
						<option>Option 3</option>
						<option>Option 4</option>
						<option>Option 5</option>
					</select>
					<!-- <span class="help-block">Select box example</span> -->
				<!--</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-xs-12 control-label">Quantity</label>
				<div class="col-md-6 col-xs-12">                                            
					<input class="form-control" type="text">
					<!-- <span class="help-block">This is sample of text field</span> -->
				<!--</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 col-xs-12 control-label">Note</label>
				<div class="col-md-6 col-xs-12">                                            
					<textarea class="form-control" rows="5"></textarea>
					<!-- <span class="help-block">Default textarea field</span> -->
				<!--</div>
			</div>

			</form>

		</div>
		<div class="panel-footer">
			<button type="button" class="btn btn-primary pull-right">Submit</button>
		</div>
	</div>
</div>
-->
<div class="panel panel-danger">
	<div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-reply"></span> Return Items</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
        </ul>
    </div>
	<div class="panel-body">
		<table class="table datatable">
		    <thead>
		        <tr>
		            <th>Date</th>
		            <th>Item</th>
		            <th>Product Name</th>
		            <th>Quantity</th>
		            <th>Unit</th>
		            <th>Notes</th>
		        </tr>
		    </thead>
		    <tbody>
		        <tr>
		        	<td>01/01/2014</td>
		        	<td>Available Cake</td>
		        	<td>Dick Man Lying</td>
		        	<td>1</td>
		        	<td>pc</td>
		        	<td>Out</td>
		        </tr>
		        <tr>
		        	<td>01/01/2014</td>
		        	<td>Cookie
</td>
		        	<td>Adult Cookies</td>
		        	<td>36
</td>
		        	<td>pc
</td>
		        	<td></td>
		        </tr>
		        <tr>
		        	<td>01/03/2014</td>
		        	<td>Chocolate</td>
		        	<td>Dick Choco
</td>
		        	<td>12
</td>
		        	<td>pack</td>
		        	<td></td>
		        </tr>
		    </tbody>
		</table>
	</div>
</div>