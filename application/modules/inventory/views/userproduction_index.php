<div class="panel panel-danger">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-calendar"></span> Raw Materials</h3>
       	<ul class="panel-controls">
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
    	</ul>
   	</div>
    <div class="panel-body">    
        <div class="row">
            <div class="col-md-4" style="margin-top: 30px;margin-bottom: 30px">
                <div class="form-horizontal">
                    <label class="col-lg-2">Item : </label>
                    <div class="col-md-10" style="margin-bottom: 10px;">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="eg. Sugar">
                        </div>
                    </div>
                    <label class="col-lg-2">Quantity : </label>
                    <div class="col-md-10" style="margin-bottom: 10px;">
                        <div class="form-group">
                            <select class="form-control">
                            <option value="">----</option>
                            <?php for($a=1;$a<=20;$a++) {?>
                                <option value=""><?=$a;?></option>
                            <?php }?>
                            </select>
                        </div>
                    </div>
                    <label class="col-lg-2"></label>
                    <div class="col-lg-10" style="margin-bottom: 10px;">
                        <div class="form-group">
                            <button class="btn btn-info">Add (new Delivery)</button>
                            <button class="btn btn-danger">Less (Used)</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 pull-left" style="margin-top: 30px;margin-bottom: 30px">
                <div class="form-horizontal">
                    <label class="col-lg-2">Unit : </label>
                    <div class="col-md-7" style="margin-bottom: 10px;">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="eg. Sack">
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Buffer</th>
                </tr>
            </thead>
            <tbody>
    
            </tbody>
        </table>
    </div>
</div>

<div class="panel panel-danger panel-toggled target">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-calendar"></span> Available Items</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
    	</ul>
    </div>
    <div class="panel-body">    
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Item Category</th>
                    <th>Product Name</th>
                    <th>Selling Price</th>
                    <th>Stock on hand</th>
                    <!--<th>Add new Supply</th>-->
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<!--
<div class="panel panel-danger panel-toggled target">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-calendar"></span> Food Items</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
<!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> 
</ul>
</div>
<div class="panel-body">
    <div class="row">
        <div class="col-md-4" style="margin-top: 30px;margin-bottom: 30px">
            <div class="form-horizontal">
                <label class="col-lg-2">Item : </label>
                <div class="col-md-10" style="margin-bottom: 10px;">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="eg. Sugar">
                    </div>
                </div>
                <label class="col-lg-2">Quantity : </label>
                <div class="col-md-10" style="margin-bottom: 10px;">
                    <div class="form-group">
                        <select class="form-control">
                        <option value="">----</option>
                        <?php for($a=1;$a<=20;$a++) {?>
                            <option value=""><?=$a;?></option>
                        <?php }?>
                        </select>
                    </div>
                </div>
                <label class="col-lg-2"></label>
                <div class="col-lg-10" style="margin-bottom: 10px;">
                    <div class="form-group">
                        <button class="btn btn-info">Add (new Delivery)</button>
                        <button class="btn btn-danger">Less (Used)</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 pull-left" style="margin-top: 30px;margin-bottom: 30px">
            <div class="form-horizontal">
                <label class="col-lg-2">Unit : </label>
                <div class="col-md-7" style="margin-bottom: 10px;">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="eg. Sack">
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <table class="table datatable">
        <thead>
            <tr>
                <th>Image</th>
                <th>Item Category</th>
                <th>Product Name</th>
                <th>Qty</th>
                <th>Unit</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</div>
<div class="panel panel-danger panel-toggled target">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-list"></span> Inventory List</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
<!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> 
</ul>
</div>
<div class="panel-body">    
    <table class="table datatable">
        <thead>
            <tr>
                <th>Image</th>
                <th>Product Name</th>
                <th>Selling Price</th>
                <th>Stock on hand</th>
                <th>Add new Supply</th>
            </tr>
        </thead>
        <tbody>
        	<tr>
        		<th><input type="file" class="form-control" /></th>
                <th>xxx</th>
                <th>xxx</th>
                <th>xxx</th>
                <th><button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Edit</button></th>
            </tr> 
        </tbody>
    </table>
</div>
</div>
-->

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-lg">
        <div class="modal-content">
        	<div class="panel panel-danger">
            	<div class="panel-footer">
                	<h3 class="panel-title">Add New Supply</h3>
              	</div>
            	<table class="table table-bordered">
                    <tr>
                        <td>Remaining Stock</td>
                        <td>25 pcs</td>
                    </tr>
                    <tr>
                        <td>Add Quantity</td>
                        <td><input type="text" class="col-md-4 form-control pull-right" /></td>
                    </tr>
                    <tr>
                        <td>Stock On Hand</td>
                        <td>125pcs</td>
                    </tr>
                </table>
                <div class="panel-footer">
                	<button type="button" class="btn btn-primary pull-right">Add</button>&nbsp;&nbsp;<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
              	</div>
            </div>
        </div>
  	</div>
</div>


