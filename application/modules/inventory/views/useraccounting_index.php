<div class="clearfix"></div>

<div class="panel2">
    <div class="panel-body">
        <div class="panel panel-danger target">
            <div class="panel-heading">                                
                <h3 class="panel-title">Item List</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
            </ul>                                
        </div>
        <div class="panel-body">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Supplier</th>
                        <th>Item</th>
                        <th>Description</th>
                        <th>Cost</th>
                        <th>Per Unit</th>
                        <th>Tresh Hold</th>
                        <th>Remaining Stock</th>
                    </tr>
                </thead>
                <tbody>
                <?php for($a=1;$a<=100;$a++){?>
                    <tr>
                        <th>Penco</th>
                        <th>Powdered Sugar</th>
                        <th></th>
                        <th>2500</th>
                        <th>sack</th>
                        <th>12</th>
                        <th>10</th>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
        <!--
            <div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
                <div class="mb-container">
                    <div class="mb-middle">
                        <div class="mb-title"><span class="fa fa-times"></span> Are you sure?</div>
                        <div class="mb-content">
                            <p>Are you sure you want to Claim?</p>
                        </div>
                        <div class="mb-footer">
                            <button class="btn btn-success btn-lg">Yes</button>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        -->
</div>

<div class="panel panel-danger panel-toggled target">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-calendar"></span> Raw Materials</h3>
       	<ul class="panel-controls">
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
    	</ul>
   	</div>
    <div class="panel-body">  
      
        <table class="table datatable">
            <thead>
                <tr>
                    <td>No.</td>
                    <th>Item</th>
                    <th>Unit</th>
                    <th>Buffer</th>
                    <th>Remaining Stock</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        
    </div>
    
</div>

<div class="panel panel-danger panel-toggled target">
    <div class="panel-heading">                                
        <h3 class="panel-title">
            Available Items Inventory
        </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
        </ul>                                
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Item Category</th>
                    <th>Product Name</th>
                    <th>Selling Price</th>
                    <th>Quantity</th>
                    <th>Stock On Hand</th>
                    <th>Add new Supply</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>xxx</th>
                    <th>xxx</th>
                    <th>xxx</th>
                    <th>xxx</th>
                    <th>xxx</th>
                    <th>xxx</th>
                    <th><button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Edit</button></th>
                </tr>
            </tbody>
        </table>
        
        <div class="panel panel-danger panel-toggled target" style="margin-top: 50px">
            <div class="panel-heading">                                
                <h3 class="panel-title">
                    Item Request
                </h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
            	<div class="row">
                
                    <div class="col-md-4" style="margin-top: 30px;margin-bottom: 30px">
                    
                        <div class="form-horizontal">
                            <label class="col-lg-2">RQ# :</label>
                            <div class="col-md-10" style="margin-bottom: 10px;">
                                <div class="form-group">
                                	 <b>ACC349875</b>
                                </div>
                           	</div>
                           <label class="col-lg-2">Item Category</label>
                            <div class="col-md-10" style="margin-bottom: 10px;">
                                <div class="form-group">
                                    <select class="form-control">
                                    <option value="">----</option>
                                        <option value="">xxx</option>
                                        <option value="">xxx</option>
                                    </select>
                                </div>
                            </div>
                            <label class="col-lg-2">Product Name</label>
                            <div class="col-md-10" style="margin-bottom: 10px;">
                                <div class="form-group">
                                    <select class="form-control">
                                    <option value="">----</option>
                                        <option value="">xxx</option>
                                        <option value="">xxx</option>
                                    </select>
                                </div>
                            </div>
                            <label class="col-lg-2">Quantity : </label>
                            <div class="col-md-10" style="margin-bottom: 10px;">
                                <div class="form-group">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Date Created</th>
                            <th>Item Category</th>
                            <th>Product Name</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Action</th>
                            <th>Validate</th>
                            <th>Feedback</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th><button class="btn btn-primary">Edit</button><button class="btn btn-primary">Cancel</button></th>
                            <th><button class="btn btn-primary">Recieved</button></th>
                            <th>xxx</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Add New Supply</h4>
			</div>
			<div class="modal-body">
				<table class="table table-responsive table-bordered">
			        <tbody>
			            <tr>
			                <td>Stock On hand :</td>
			                <td>25 pcs</td>
			            </tr><tr>
			                <td>Add Quantity :</td>
			                <td><input type="text" value="100pcs" class="form-control"></td>
			            </tr><tr>
			                <td>Total :</td>
			                <td>125pcs</td>
			            </tr>
			        </tbody>
			    </table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>
