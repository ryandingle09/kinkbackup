<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">Branch:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" class="form-control" />
            </div>
        </div>
        <label class="col-md-2 control-label">Year:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="panel panel-default">
    <div class="panel-body">
    	<div class="row">
        
        	<div class="col-md-12">
                <legend><h1 class="text-center">Total Sales</h1></legend>
                <div class="row">
                    <div class="col-md-2">
                    	<table class="table table-bordered table-responsive">
                        	<tbody>
                            	<tr>
                                	<td>January</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>February</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>March</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>April</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>May</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>June</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>July</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>August</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>September</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>October</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>November</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>December</td>
                                    <td>xxx</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-10">
                		<div id="morris-bar-ts" style="height: 300px;"></div>
                    </div>
            	</div>
            </div>
            
			
            <div class="col-md-12">
                <legend><h1 class="text-center">DAS</h1></legend>
                <div class="row">
                    <div class="col-md-2">
                    	<table class="table table-bordered table-responsive">
                        	<tbody>
                            	<tr>
                                	<td>January</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>February</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>March</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>April</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>May</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>June</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>July</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>August</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>September</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>October</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>November</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>December</td>
                                    <td>xxx</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-10">
                		<div id="morris-bar-das" style="height: 300px;"></div>
                    </div>
            	</div>
            </div>

			<div class="col-md-12">
                <legend><h1 class="text-center">Total Branch Sales</h1></legend>
                <div class="row">
                    <div class="col-md-2">
                    	<table class="table table-bordered table-responsive">
                        	<tbody>
                            	<tr>
                                	<td>Timog</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>Makati</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>Malate</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>Greenhills</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>Shaw</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>Paranaque</td>
                                    <td>xxx</td>
                                </tr>
                                <tr>
                                	<td>Direct Sales</td>
                                    <td>xxx</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-10">
                		<div id="morris-bar-branch" style="height: 300px;"></div>
                    </div>
            	</div>
            </div>
            
        </div>
    </div>
</div>
