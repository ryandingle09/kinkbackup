<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">From:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $from ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">To:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-danger">
    <div class="panel-heading">
        <div class="panel-title">
            <h3 class="widget-title">Sales</h3>
        </div>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
            <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
        </ul>  
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>JO#</th>
                    <th>Date of Order</th>
                    <th>Item Category</th>
                    <th>Title</th>
                    <th>Order Set</th>
                    <th>Claim Date</th>
                    <th>Price</th>
                    <th>FP / DP</th>
                    <th>PB</th>
                    <th>Outstanding</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>xx</th>
                    <th>MKT-4536</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>25-Dec-14</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <th>xx</th>
                    <th>TMG-2345</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>25-Dec-14</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <th>xx</th>
                    <th>GH-5686</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>xx</th>
                    <th>25-Dec-14</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </tbody>
        </table>
        <div class="col-md-4 pull-right" style="margin-top: 20px">
            <table class="table table-responsive table-bordered">
                <tbody>
                    <tr>
                        <td>Total</td>
                        <td>xxx</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>