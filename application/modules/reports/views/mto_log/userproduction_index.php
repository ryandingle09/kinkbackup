<div class="panel panel-success">
	<div class="panel-heading">
    	<h3 class="panel-title">Job Order</h3>
        <button class="btn btn-primary pull-right">Print List</button>
    </div>
    <div class="panel-body">
    	<table class="table datatable">
    		<thead>
    			<tr>
    				<th>Date of Production</th>
    				<th>Image</th>
    				<th>JO#</th>
    				<th>Item Category</th>
    				<th>Title</th>
    				<th>Order Set</th>
    				<th>Size</th>
    				<th>Decorator</th>
                    <th>Artist</th>
    			</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td>xxx</td>
    				<td>xxx</td>
    				<td>xxx</td>
    				<td>xxx</td>
    				<td>xxx</td>
    				<td>xxx</td>
    				<td>xxx</td>
    				<td>xxx</td>
                    <td>xxx</td>
    			</tr>
    		</tbody>
    	</table>
    </div>
</div>
</div>
