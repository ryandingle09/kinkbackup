<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">From:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $from ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">To:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-danger">
    <div class="panel-heading">                                
        <h3 class="panel-title">MTO Cookies</h3>                           
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Completion Date</th>
                    <th>JO#</th>
                    <th>Title</th>
                    <th>Order Set</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                </tr>
            </tbody>
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th>test</th>
                </tr>
            </thead>
        </table>
    </div>
</div> 
<div class="panel panel-danger">
    <div class="panel-heading">                                
        <h3 class="panel-title">Available Cookies</h3>                           
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Completion  Date</th>
                    <th>RQ#</th>
                    <th>Product Name</th>
                    <th>Notes</th>
                    <th>Quantity</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                </tr>
            </tbody>
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Total</th>
                    <th>test</th>
                </tr>
            </thead>
        </table>
    </div>
</div> 