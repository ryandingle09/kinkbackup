<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">From:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $from ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">To:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>

<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">Division</label>
        <div class="col-md-4">
        	<input type="text" class="form-control" />
        </div>
        
        <label class="col-md-2 control-label">Name</label>
        <div class="col-md-4">
             <input type="text" class="form-control" />
        </div>
    </div>
</div>
<div class="panel panel-danger">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Sales</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>JO#</th>
                    <th>Item Category</th>
                    <th>Title</th>
                    <th>Order Set</th>
                    <th>Size</th>
                    <th>Prize</th>
                    <th>Amount Paid</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <div class="col-md-4 pull-right" style="margin-top: 40px">
            <table class="table table-responsive table-bordered">
                <tbody>
                    <tr>
                        <td>Total Cash Payment</td>
                        <td>xx</td>
                    </tr>
            </table>
        </div>
    </div>
</div>

<div class="panel panel-danger panel-toggled target">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title">Production</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Date</th>
                    <th>JO#</th>
                    <th>Item Category</th>
                    <th>Title</th>
                    <th>Order Set</th>
                    <th>Size</th>
                    <th>Job Rendered</th>
                    <th>Recomendation</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Decor</td>
                    <td><input type="text" class="form-control" /></td>
                </tr>
            </tbody>
        </table>
        <div class="col-md-4 pull-right" style="margin-top: 40px">
            <table class="table table-responsive table-bordered">
                <tbody>
                    <tr>
                        <td>Total Cash Payment</td>
                        <td>xx</td>
                    </tr>
            </table>
        </div>
    </div>
</div>