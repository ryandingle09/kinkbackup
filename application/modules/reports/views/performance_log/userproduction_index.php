<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">Search Period From:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $from ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">Search Period To:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-2 control-label">Search Division:</label>
        <div class="col-md-4">
            <input type="text" class="form-control"/>
            </div>
        <label class="col-md-2 control-label">Search Name:</label>
        <div class="col-md-4">
            <input type="text" class="form-control" />
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-sm-6">
    <div class="panel panel-danger">
        <div class="panel-body">
            <div class="col-sm-6">
            <label class="col-md-5 control-label">Name:</label>
            
            </div>
            <div class="col-sm-6">
            <label class="col-md-5 control-label">Division:</label>
            
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="panel panel-danger">
	<div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-cutlery"></span> MTO Cakes</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
        </ul>
    </div>
	<div class="panel-body">
		<table class="table datatable">
		    <thead>
		        <tr>
                    <th>No.</th>
                    <th>Stage</th>
                    <th>Prodction Date</th>
                    <th>Image</th>
                    <th>JO#</th>
                    <th>Title</th>
                    <th>Order Set</th>
		            <th>Size</th>
		        </tr>
		    </thead>
		    <tbody>
                <tr>
                    <td>1</td>
                    <td>Deco/Finishing</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Deco/Finishing</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Deco/Finishing</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
		    </tbody>
		</table>
	</div>
</div>
<div class="panel panel-danger">
    <div class="panel-heading ui-draggable-handle">
        <h3 class="panel-title"><span class="fa fa-cutlery"></span> Available Cakes</h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
        </ul>
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Stage</th>
                    <th>Production Date</th>
                    <th>Branch</th>
                    <th>Product Name</th>
                    <th>Qty</th>
                    <th>Notes</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Deco/Finishing</td>
                    <td></td>
                    <td>KC Timog</td>
                    <td>Dick Man Lying</td>
                    <td>10</td>
                    <td></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Deco</td>
                    <td></td>
                    <td>KC Timog</td>
                    <td>Bood Lace</td>
                    <td>36</td>
                    <td></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Finishing</td>
                    <td></td>
                    <td>KC Timog</td>
                    <td>Dick Man Lying</td>
                    <td>100</td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>