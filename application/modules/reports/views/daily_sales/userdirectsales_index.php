<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">From:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $from ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">To:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-danger">
    <div class="panel-heading">                                
        <h3 class="panel-title">MTO</h3>                           
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>Date</th>
                    <th>JO#</th>
                    <th>Item Category</th>
                    <th>Title</th>
                    <th>Total Amount</th>
                    <th>Payment Type</th>
                    <th>Mode of Payment</th>
                    <th>Amount Paid</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                </tr>
            </tbody>
        </table>
    </div>
</div> 
<div class="col-md-3 pull-right">
    <table class="table table-bordered">
        <tr>
            <th>Mode of Payment</th>
            <th>Amount</th>
        </tr>
        <tr>
            <th>Cash</th>
            <th>500</th>
        </tr>
        <tr>
            <th>Credit Card</th>
            <th>1000</th>
        </tr>
        <tr>
            <th>Total Sales</th>
            <th>1500</th>
        </tr>
    </table>    
</div>
