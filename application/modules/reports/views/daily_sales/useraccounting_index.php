
<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">Branch:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" class="form-control"/>
            </div>
        </div>
        <label class="col-md-2 control-label">Date:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="<?= $to ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="panel2">
    <div class="panel-body">
        <div class="panel panel-danger target">
            <div class="panel-heading">                                
                <h3 class="panel-title">
                    MTO
                </h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>JO#</th>
                            <th>Item</th>
                            <th>Title</th>
                            <th>Total Amount</th>
                            <th>Status</th>
                            <th>Mode of Payment</th>
                            <th>Amout Paid</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($a=1;$a<=200;$a++) {?>
                        <tr>
                            <th><?php echo $a ;?></th>
                            <th>MKT-4537</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>Fully Paid</th>
                            <th>xxx</th>
                            <th>xxx</th>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
                    <div class="mb-container">
                        <div class="mb-middle">
                            <div class="mb-title"><span class="fa fa-times"></span> Are you sure?</div>
                            <div class="mb-content">
                                <p>Are you sure you want to Claim?</p>
                            </div>
                            <div class="mb-footer">
                                <button class="btn btn-success btn-lg">Yes</button>
                                <button class="btn btn-default btn-lg mb-control-close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel2">
    <div class="panel-body">
        <div class="panel panel-danger target panel-toggled">
            <div class="panel-heading">                                
                <h3 class="panel-title">
                    Available Items
                </h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Item</th>
                            <th>Product Name</th>
                            <th>Unit Price</th>
                            <th>Quantity</th>
                            <th>Total Amount</th>
                            <th>Mode of Payment</th>
                            <th>Amount Paid</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($a=1;$a<=200;$a++) {?>
                        <tr>
                            <th><?php echo $a ;?></th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                            <th>xxx</th>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div class="message-box message-box-danger animated fadeIn" id="message-box-danger">
                    <div class="mb-container">
                        <div class="mb-middle">
                            <div class="mb-title"><span class="fa fa-times"></span> Are you sure?</div>
                            <div class="mb-content">
                                <p>Are you sure you want to Claim?</p>
                            </div>
                            <div class="mb-footer">
                                <button class="btn btn-success btn-lg">Yes</button>
                                <button class="btn btn-default btn-lg mb-control-close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-4 pull-right" style="margin-top: 20px">
    <table class="table table-responsive table-bordered">
        <tbody>
            <tr>
                <td>Total Cash payment</td>
                <td>xxx</td>
            </tr><tr>
                <td>Total Total Card Payment</td>
                <td>xxx</td>
            </tr><tr>
                <td>Total Sales</td>
                <td>xxx</td>
            </tr>
        </tbody>
    </table>
</div>