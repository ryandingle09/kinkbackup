<button class="btn btn-warning pull-right">Print records</button>
<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab8" data-toggle="tab">Raw Materials</a></li>
        <li><a href="#tab9" data-toggle="tab">Food Items</a></li>
        <li><a href="#tab10" data-toggle="tab">Returned Item</a></li>
    </ul>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab8">
            <h3 class="push-down-20">Additional Stocks</h3>
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
            <h3 class="push-down-20">Usage</h3>
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
        </div>    
        <div class="tab-pane" id="tab9">
        	<h3 class="push-down-20">Additional Stocks</h3>
            <table class="table datatable">
               <thead>
                    <tr>
                        <th>Date</th>
                        <th>Item Category</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
            <h3 class="push-down-20">Dispatched</h3>
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Branch</th>
                        <th>Item Category</th>
                        <th>Product Name</th>
                        <th>Qty</th>
                        <th>Unit</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
        </div> 
        <div class="tab-pane" id="tab10">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Branch</th>
                        <th>Item Category</th>
                        <th>Product Name</th>
                        <th>Qty</th>
                        <th>Unit</th>
                        <th>Notes</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tbody>
            </table>
        </div> 
    </div>
</div> 