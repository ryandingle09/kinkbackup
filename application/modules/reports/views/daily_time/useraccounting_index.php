<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab8" data-toggle="tab">Daily Time Record ( DTR )</a></li>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab8">
            <table class="table datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Branch</th>
                        <th>User Name</th>
                        <th>Employee Name</th>
                        <th>Time In</th>
                        <th>Time Out</th>
                        <th>Rendered Hours</th>
                        <th>Minute Late</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for($a = 1;$a<=20;$a++) {?>
                    <tr>
                        <th>12/12/12</th>
                        <th>Branch name</th>
                        <th>Hello</th>
                        <th>9:30 AM</th>
                        <th>9: 30 PM</th>
                        <th>10 Hours</th>
                        <th>8</th>
                        <th>0</th>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>  
    </div>
</div>