<div id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20">
    <div class="form-group">
        <label class="col-md-2 control-label">From:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">To:</label>
        <div class="col-md-4">
            <div class="input-group">
                <input type="text" id="dp-3" class="form-control" value="" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="panel2">
    <div class="panel-body">
        <div class="panel panel-danger target">
            <div class="panel-heading">                                
                <h3 class="panel-title">Productivity Report</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                    <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
                </ul>                                
            </div>
            <div class="panel-body">
                <div class="">                                
                    <h3 class="panel-title">Products Sold</h3> <br /><br /> <br />                          
                </div>
                <div class="panel panel-danger target">
                    <div class="panel-body">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Product Category</th>
                                    <th>Jan</th>
                                    <th>Feb</th>
                                    <th>Mar</th>
                                    <th>Apr</th>
                                    <th>May</th>
                                    <th>June</th>
                                    <th>Jul</th>
                                    <th>Aug</th>
                                    <th>Sept</th>
                                    <th>Oct</th>
                                    <th>Nov</th>
                                    <th>Dec</th>
                                    <th>Monthly Average</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Just For Kids</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>All Occasions</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Adult Cakes</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Wedding Cakes</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
            	</div>
        	</div>
            
            <div class="panel-body">
                <div class="panel panel-danger target">
                    <div class="panel-body">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Item Category</th>
                                    <th>Jan</th>
                                    <th>Feb</th>
                                    <th>Mar</th>
                                    <th>Apr</th>
                                    <th>May</th>
                                    <th>June</th>
                                    <th>Jul</th>
                                    <th>Aug</th>
                                    <th>Sept</th>
                                    <th>Oct</th>
                                    <th>Nov</th>
                                    <th>Dec</th>
                                    <th>Monthly Average</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Marshmallow Icing Cake</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Fondant Cake</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>MTO Carousel</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>MTO Cookies</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
            	</div>
        	</div>
            
             <div class="panel-body">
                <div class="panel panel-danger target">
                    <div class="panel-body">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Order Set</th>
                                    <th>Jan</th>
                                    <th>Feb</th>
                                    <th>Mar</th>
                                    <th>Apr</th>
                                    <th>May</th>
                                    <th>June</th>
                                    <th>Jul</th>
                                    <th>Aug</th>
                                    <th>Sept</th>
                                    <th>Oct</th>
                                    <th>Nov</th>
                                    <th>Dec</th>
                                    <th>Monthly Average</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Single Layer</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>2 Layer</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>3 Layer</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Multi-Layer</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Cupcakes</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Babycakes</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Character Cookies</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Caricature Cookies</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Adult Cookies</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
            	</div>
        	</div>
            
             <div class="panel-body">
                <div class="panel panel-danger target">
                    <div class="panel-body">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Cake Flavors</th>
                                    <th>Jan</th>
                                    <th>Feb</th>
                                    <th>Mar</th>
                                    <th>Apr</th>
                                    <th>May</th>
                                    <th>June</th>
                                    <th>Jul</th>
                                    <th>Aug</th>
                                    <th>Sept</th>
                                    <th>Oct</th>
                                    <th>Nov</th>
                                    <th>Dec</th>
                                    <th>Monthly Average</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Vanilla</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Mocha</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Butter</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Chocolate</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Butter Pound</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Choco Pound</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
            	</div>
        	</div>
            
             <div class="panel-body">
                <div class="panel panel-danger target">
                    <div class="panel-body">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Cake Sizes</th>
                                    <th>Jan</th>
                                    <th>Feb</th>
                                    <th>Mar</th>
                                    <th>Apr</th>
                                    <th>May</th>
                                    <th>June</th>
                                    <th>Jul</th>
                                    <th>Aug</th>
                                    <th>Sept</th>
                                    <th>Oct</th>
                                    <th>Nov</th>
                                    <th>Dec</th>
                                    <th>Monthly Average</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Round -9"Dia</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Round - 12" Dia</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Round - 14" Dia</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Round - 18" Dia</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Round - 24" Dia</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Regular</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Regular</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Jumbo</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Long Base</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
            	</div>
        	</div>
            
             <div class="panel-body">
                <div class="panel panel-danger target">
                    <div class="panel-body">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Inventory Items</th>
                                    <th>Jan</th>
                                    <th>Feb</th>
                                    <th>Mar</th>
                                    <th>Apr</th>
                                    <th>May</th>
                                    <th>June</th>
                                    <th>Jul</th>
                                    <th>Aug</th>
                                    <th>Sept</th>
                                    <th>Oct</th>
                                    <th>Nov</th>
                                    <th>Dec</th>
                                    <th>Monthly Average</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Available Cakes</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Cookies</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Character Cookies</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Adult Cookies</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Chocolates</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>xxx</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>xxx</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Etc..</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Chocolates</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>xxx</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                                <tr>
                                    <th>Etc..</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                    <th>test</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
            	</div>
        	</div>
            
        </div>
    </div> 
</div>

<div class="panel2">
    <div class="panel-body">
        <div class="panel panel-danger target">
            <div class="panel-heading">                                
                <h3 class="panel-title">Returned Items</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
            <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
        </ul>                                
    </div>
    <div class="panel-body">
        <table class="table datatable">
            <thead>
                <tr>
                    <th>Out Cakes</th>
                    <th>Jan</th>
                    <th>Feb</th>
                    <th>Mar</th>
                    <th>Apr</th>
                    <th>May</th>
                    <th>June</th>
                    <th>Jul</th>
                    <th>Aug</th>
                    <th>Sept</th>
                    <th>Oct</th>
                    <th>Nov</th>
                    <th>Dec</th>
                    <th>Monthly Average</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Timog Branch</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                </tr>
                <tr>
                    <th>Makati Branch</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                </tr>
                <tr>
                    <th>Malate Branch</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                </tr>
                <tr>
                    <th>Greenhills Branch</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                </tr>
                <tr>
                    <th>Shaw Branch</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                </tr>
                <tr>
                    <th>Total</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                    <th>test</th>
                </tr>
            </tbody>
        </table>
    </div>
</div> 