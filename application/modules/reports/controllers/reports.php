<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends MX_Controller {

	var $user_session = '';
	var $module_name ='';
	public function __construct()
	{
		parent::__construct();
		$this->user_session = $this->session->userdata('user_data');
		if(empty($this->user_session)){
			redirect('login');
		}
		$this->module_name	=  get_class($this);
		if(!in_array($this->module_name, $this->user_session['allowedModules'])){
			redirect($this->user_session['default_home']);
		}


	}

	public function index()
	{
		//check here if user has valid access on each action
		redirect($this->user_session['default_home']);
	}
	
	public function mto_log()
	{
		 $user_session = $this->user_session;
		  
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>'; 
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));	
		}
	}


	public function performance_log()
	{
		$user_session = $this->user_session;
		$scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script>
			$(document).ready(function(){
				pl();
			});
		</script>';
		$from = '01-01-'.date('Y',time());
		$to = date('m-d-Y',time());

		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			
			  redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts','from','to', 'nav'));
		}
	}

	public function history_log()
	{
		 $user_session = $this->user_session;
		  
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>'; 
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));	
		}
		
	}

	public function daily_sales()
	{
		 $user_session = $this->user_session;
		  
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>'; 
		$from = '01-01-'.date('Y',time());
		$to = date('m-d-Y',time());

		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts','from','to', 'nav'));	
		}
		
	}

	public function daily_time()
	{
		 $user_session = $this->user_session;
		  
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>'; 
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));	
		}
		
	}

	public function branch_sales()
	{
		 $user_session = $this->user_session;
		  
		$scripts = 
        '<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/morris/raphael-min.js"></script>
		<script type="text/javascript" src="js/plugins/morris/morris.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript">
		 	$(document).ready(function(){
						index();
				});
			var morrisCharts = function() {
				Morris.Bar({
						element: "morris-bar-ts",
						data: [
							{ y: "January", a: 100, b: 90 },
							{ y: "February", a: 75,  b: 65 },
							{ y: "March", a: 50,  b: 40 },
							{ y: "April", a: 75,  b: 65 },
							{ y: "May", a: 50,  b: 40 },
							{ y: "June", a: 75,  b: 65 },
							{ y: "July", a: 100, b: 90 },
							{ y: "August", a: 75,  b: 65 },
							{ y: "September", a: 50,  b: 40 },
							{ y: "October", a: 75,  b: 65 },
							{ y: "November", a: 50,  b: 40 },
							{ y: "December", a: 75,  b: 65 },
						],
						xkey: "y",
						ykeys: ["a"],
						labels: ["Series A"],
						barColors: ["#3fbae4"]
					});
					
				Morris.Bar({
						element: "morris-bar-das",
						data: [
							{ y: "January", a: 100, b: 90 },
							{ y: "February", a: 75,  b: 65 },
							{ y: "March", a: 50,  b: 40 },
							{ y: "April", a: 75,  b: 65 },
							{ y: "May", a: 50,  b: 40 },
							{ y: "June", a: 75,  b: 65 },
							{ y: "July", a: 100, b: 90 },
							{ y: "August", a: 75,  b: 65 },
							{ y: "September", a: 50,  b: 40 },
							{ y: "October", a: 75,  b: 65 },
							{ y: "November", a: 50,  b: 40 },
							{ y: "December", a: 75,  b: 65 },
						],
						xkey: "y",
						ykeys: ["a"],
						labels: ["Series A"],
						barColors: ["#3fbae4"]
					});
					
				Morris.Bar({
						element: "morris-bar-branch",
						data: [
							{ y: "January", a: 100, b: 90 },
							{ y: "February", a: 75,  b: 65 },
							{ y: "March", a: 50,  b: 40 },
							{ y: "April", a: 75,  b: 65 },
							{ y: "May", a: 50,  b: 40 },
							{ y: "June", a: 75,  b: 65 },
							{ y: "July", a: 100, b: 90 },
							{ y: "August", a: 75,  b: 65 },
							{ y: "September", a: 50,  b: 40 },
							{ y: "October", a: 75,  b: 65 },
							{ y: "November", a: 50,  b: 40 },
							{ y: "December", a: 75,  b: 65 },
						],
						xkey: "y",
						ykeys: ["a"],
						labels: ["Series A"],
						barColors: ["#3fbae4"]
					});
			}();
		</script>';

		$from = '01-01-'.date('Y',time());
		$to = date('m-d-Y',time());

		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'from','to','nav'));	
		}
		
	}

	public function productivity_statistics()
	{
		 $user_session = $this->user_session;
		  
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>'; 
		$from = '01-01-'.date('Y',time());
		$to = date('m-d-Y',time());

		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts','from','to', 'nav'));	
		}
		
	}

	public function sales_tracker()
	{
		 $user_session = $this->user_session;
		  
		 $scripts =
		'<script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>'; 
		$from = '01-01-'.date('Y',time());
		$to = date('m-d-Y',time());

		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$action = $this->router->fetch_method();

		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_index.php';
		 
		if(!file_exists($view_path)){
			redirect($this->user_session['default_home']);
		}else{
			$this->render($action.'/'.$class.'_index',$sidebar,$navigation,compact('scripts','from','to', 'nav'));	
		}
		
	}

}

/* End of file reports.php */
/* Location: ./application/modules/report/controllers/reports.php */