<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends MX_Controller {

	var $user_session = '';
	public function __construct()
	{
		parent::__construct();
		$this->user_session = $this->session->userdata('user_data');
		if(empty($this->user_session)){
			redirect('login');
		}
	}

	public function index()
	{
		  $user_session = $this->user_session;
		  //dump($this->user_session );
		  $page = $user_session['page'];
		  $folder = $page;
		    $class = $user_session['class'];

		  
		  $scripts =
		"<script type=\"text/javascript\" src=\"js/plugins/moment.min.js\"></script>
		<script type=\"text/javascript\" src=\"js/plugins/fullcalendar/fullcalendar.min.js\"></script>
		<script>
		    $(document).ready(function(){
		        $('#calendars').fullCalendar({
		            // defaultDate: '2014-11-10',
		            events: [
		                {
		                    start: '2015-01-10',
		                    end: '2015-01-10',
		                    title: 'Delivery',
		                    className: 'bg-danger'
		                },
		                {
		                    start: '2015-01-15',
		                    end: '2015-01-15',
		                    title: 'Delivery',
		                    className: 'bg-danger'
		                },
		                {
		                    start: '2015-01-25',
		                    end: '2015-01-25',
		                    title: 'Delivery',
		                    className: 'bg-danger'
		                },
		                {
		                    start: '2015-01-10',
		                    end: '2015-01-10',
		                    title: 'Delivery',
		                    className: 'bg-danger'
		                },
		                {
		                    start: '2015-01-13',
		                    end: '2015-01-15',
		                    title: 'Delivery',
		                    className: 'bg-danger'
		                },
		                {
		                    start: '2015-01-10',
		                    end: '2015-01-10',
		                    title: 'Delivery',
		                    className: 'bg-danger'
		                },
		            ]
		        });
		    })
		</script>"; 
		$from = '01-01-'.date('Y',time());
		$to = date('m-d-Y',time());
		$module =  $this->uri->segment(1);
		$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
		$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
		$this->render($module.'/'.$class.'_index',$sidebar,$navigation,compact('scripts', 'nav'));	
		 

	}
}

/* End of file calendar.php */
/* Location: ./application/modules/calendar/controllers/calendar.php */