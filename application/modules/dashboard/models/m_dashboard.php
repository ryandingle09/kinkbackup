<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Model M_dashboard
 *
 * 
 * @author Joshua Orozco <borozky@me.com>
 */
class M_dashboard extends MY_Model {




	/**
	 * Found rows from 'SELECT FOUND_ROWS()'. Only use if $_POST['search'] has value
	 * @var integer
	 */
	private $found_rows = 0;

	/**
	 * Number of rows without the LIMIT clause
	 * @var integer
	 */
	private $num_rows = 0;

	/**
	 * SQL for getting number of rows without the LIMIT clause
	 * @var string
	 */
	private $sql_num_rows = "";

	/**
	 * Getter of $this->found_rows
	 * @return int 
	 */
	public function get_found_rows()
	{
		return $this->found_rows;
	}

	/**
	 * Getter of $this->num_rows
	 * @return int
	 */
	public function get_num_rows()
	{
		return $this->num_rows;
	}

	/**
	 * Do a simple query using $this->select();
	 * @param  string  	$sql            	Your SQL. Required
	 * @param  boolean 	$num_found_exec 	This will get the number of desired results without LIMIT clause 
	 *                                   	and the number of results found
	 *                                   	Defaults to TRUE
	 * @return array                  		
	 */
	public function simple_query( $sql, $num_found_exec = TRUE )
	{
		$results = $this->select( $sql );

		if( $num_found_exec === TRUE )
		{
			$found_rows = $this->select( "SELECT FOUND_ROWS() as `found_rows`" );
			$this->found_rows = intval($found_rows['found_rows']);

			if( $this->sql_num_rows )
			{
				$num_rows = $this->select_all( $this->sql_num_rows );
				$this->num_rows = intval($num_rows[0]['num_rows']);
			}
		}
		
		return $results;
	}

	/**
	 * Do a normal query using $this->select_all();
	 * @param  string  	$sql            	Your SQL. Required
	 * @param  boolean 	$num_found_exec 	This will get the number of desired results without LIMIT clause
	 *                                   	and the number of results found
	 *                                   	Defaults to TRUE
	 * @return array                  		
	 */
	public function query_all( $sql, $num_found_exec = TRUE )
	{
		$results = $this->select_all( $sql );

		if( $num_found_exec === TRUE )
		{
			$found_rows = $this->select( "SELECT FOUND_ROWS() as `found_rows`" );
			$this->found_rows = $found_rows['found_rows'];

			if( $this->sql_num_rows )
			{
				$num_rows = $this->select_all( $this->sql_num_rows );
				$this->num_rows = intval($num_rows[0]['num_rows']);
			}
		}
		
		
		return $results;
	}


	/**
	 * Datatable query processor. 
	 * 
	 * @param  array 	$filter 	$_POST
	 * @param  string 	$table  	Required
	 * @param  string 	$pk     	Primary key is required. Use primary key of $table
	 * @param  array  	$w_ar   	WHERE CLAUSE in array format. Examples:
	 *                          	
	 * $w_ar[] = " YOUR SIMPLE CONDITION ",
	 * // enclose multiple conditions with () when using OR clause
	 * $w_ar[] = " ( <first condition> OR <another condition> )"
	 * $w_ar[] = " ( 	<first condition> 
	 *                  OR <another condition>
	 *                  OR <another condition> 
	 *             )"
	 *                          	
	 * @param  array  	$join   	additional joins
	 * @param  array  	$sum    	sum of values
	 * @return array  
	 * @access protected
	 * @author Joshua Orozco
	 */
	protected function _dt_query( $filter, $table, $pk, $columns, $w_ar = array(), $join = array() )
	{
		if ( !$filter || !$table || !$pk || !$columns ) return;

		$select = array();
		$where = "";
		$o = array(
				'column' => '',
				'dir' => ''
			);
		$lim = array();

		foreach( $columns as $k => $v )
		{
			$select[ $v['dt'] ] = $v['db'];
		}

		if( $w_ar )
		{
			$where = "WHERE ";
			$where .= implode("\nAND ", $w_ar)."\n";
		}

		// ordering. Only one column is supported
		$idx = 0;
		if( isset($filter['counter']) && $filter['counter'] == TRUE )
		{
			$idx = 1;
		}
		$o['column'] = $columns[ $filter['order'][0]['column'] - $idx ]['db'];
		$o['dir'] =  $filter['order'][0]['dir'];
		$order_by = "ORDER BY {$o['column']} {$o['dir']}";

		// limits
		$lim['start'] = intval($filter['start']);
		$lim['length'] = intval($filter['length']);
		$limit = "LIMIT {$lim['start']}, {$lim['length']}";

		// combine
		$extra = "{$order_by}\n{$limit}";

		$sql = $this->_generate_sql( $select, $table, $join, $where, "", $extra, $pk );
		$data = $this->query_all( $sql );

		// formatter
		$i = $filter['start'];
		if( $data )
		{
			foreach( $data as $k => $v )
			{
				foreach( $columns as $x => $y )
				{
					if( isset($y['formatter']) )
					{
						$data[$k][$x] = $y['formatter']( $data[$k][$x] );
					}
				}
				if( isset($filter['counter']) && $filter['counter'] == TRUE )
				{
					array_unshift( $data[$k], (++$i) );
				}
			}
		}

		return array(
			'sql'				=> $sql,
			'draw' 				=> $filter['draw'],
			'recordsTotal' 		=> intval( $this->num_rows ),
			'recordsFiltered' 	=> intval( $this->num_rows ),
			'data'				=> $data,
			
			);
	}


	/**
	 * Default LEFT JOINs
	 * @var array
	 */
	private $left_joins = array(
		'catalogue_categories' => array(
				'id' => 'orders.`category`'
			),
		'product_categories' => array(
				'category_id' => 'kink_products.`product_category_id`'
			),
		'order_settings' => array(
				'order_setting_id' => 'orders.`order_set`'
			),
		'products' => array(
				'product_id' => 'orders.`product_id`'
			),
		'kink_products' => array(
				'product_id' => 'orders.`product_id`'
			),
		'flavors' => array(
				'id' => 'orders.`flavor`',
			),
		'branches' => array(
				'branch_code' => 'orders.`branch_code`',
			),
		'sizes' => array(
				'id' => 'orders.`size`'
			),
		'icing_types' => array(
				'id' => 'orders.`icing_type`'
			),
		'employees' => array(
				'employee_id' => 'orders.`employee_id`'
			),
		'users' => array(
				'user_id' => 'employees.`user_id`'
			),
		);

	/**
	 * Generate sql based on columns needed
	 * 
	 * @param  array $select   	SELECT array. Required
	 * @param  mixed $from     FROM table. Required
	 * @param  string $where    WHERE clause
	 * @param  mixed $group_by GROUP BY column name
	 * @param  string $extra    extra
	 * @return string           SQL string
	 * @access protected
	 */	
	protected function _generate_sql( $select, $from, $additional_join = array(), $where = "", $group_by = "", $extra = "", $pk = 'id' , $debug = FALSE )
	{
		$left_joins = $this->left_joins;
		$sql = array(
			'select' => array(),
			'from' => "",
			'left_join' => "",
			'where' => "{$where}",
			'group_by' => "",
			'extra' => "{$extra}",
			);
		$use_col = array();
		$use_join = array();

		//var_dump($select);
		foreach ( $select as $key => $value ) 
		{
			if( ! is_array($value) )
			{
				$sql['select'][] = $value.' AS `'.$key.'`'; 
			} 
			else 
			{
				// utilise $this->left_joins 
				foreach( $value as $k => $v )
				{
					if( $v == 'name' || $v == 'id' )
					{
						$sql['select'][] = $key.'.`'.$v.'` AS '.$key.'_'.$v;
					} 
					else
					{
						$sql['select'][] = $key.'.`'.$v.'`';
					}
				}
			}

			if( array_key_exists( $key, $left_joins ) )
			{
				$use_join[$key] = $left_joins[$key];
			}
		}
		//var_dump($sql);
		//
		
		// your custom JOIN clause
		if( ! empty( $additional_join ) ) 
		{
			if( is_array($additional_join) )
			{
				foreach( $additional_join as $k => $v )
				{
					$use_join[$k] = $v;
				}
			} 
			else 
			{
				$sql['left_join'] .= $additional_join;
			}
		}

		$sql['select'] = implode( ",\n\t", $sql['select']);

		if( ! empty( $from ) )
		{
			$sql['from'] = "FROM `";
			if( is_array( $from ) )
			{
				$sql['from'] .= implode( "`,`", $from )."`\n";
			} 
			else 
			{
				$sql['from'] .= $from."`\n";
			}
		}

		$use_direction = "LEFT ";		// by default
		foreach( $use_join as $table => $on )
		{

			if( array_key_exists('direction', $on) )
			{
				$use_direction = $on['direction'];
				unset( $on['direction'] );
			}

			$sql['left_join'] .= "{$use_direction}JOIN `$table`\n";

			foreach( $on as $k => $v )
			{

				$sql['left_join'] .= "ON {$table}.`{$k}` = {$v}\n";
			}

		}

		if( ! empty( $group_by ) )
		{
			$sql['group_by'] = "GROUP BY ";
			if( is_array( $group_by ) )
			{
				$sql['group_by'] .= implode(', ', $group_by);
			} 
			else 
			{
				$sql['group_by'] .= $group_by;
			}
		}

		//sql for getting number of rows based on default primary ID
		if( $pk && is_string($pk) )
		{
			$this->sql_num_rows = "SELECT COUNT(`{$from}`.`{$pk}`) AS `num_rows`\nFROM `{$from}`\n{$sql['left_join']}{$where}";
		}
		

		$res_sql = "SELECT \n\t".
							$sql['select']."\n".
					$sql['from'].
					$sql['left_join'].
					$sql['where'].
					$sql['group_by'].
					$sql['extra'];

		if( $debug === TRUE )
		{
			echo "<pre>{$res_sql}</pre>";
		}

		return 	$res_sql;
	}

	/**
	 * Select data according to given columns (SELECT <your column> FROM <table>)
	 * 
	 * @param  string 	$table   	Required
	 * @param  array  	$columns  	Optional. Columns must exists on the table given
	 * @return array           		SQL Result
	 */
	public function get_table_cols_data( $table, $columns = array() )
	{
		$sql = "SELECT ";
		if ( is_array( $columns ) )
		{
			$sql .= join( ',' , $columns );
		}
		else
		{
			$sql .= "*";
		}
		$sql .= " FROM $table";

		return $this->query_all( $sql );
	}

	/***************************************************************************************************************/

	

	public function dashboard_storesales( $filter, $from, $to )
	{
		if( !$filter ) return;

		$w_ar = array();
		$join = array();
		$columns = array();

		$mto = ( isset($filter['mto']) && !empty($filter['mto']) ) ? $filter['mto'] : 'both' ;
		$claim_type = ( isset($filter['claim_type']) && !empty($filter['claim_type']) ) ? $filter['claim_type'] : 'both' ;

		if ( isset( $filter['category'] ) ) :
			switch ( $filter['category'] ) :

				case 'transactions':
						$join['order_settings'] = array( 'order_setting_id' => 'orders.`order_set`' );	
						$join['branches'] = array( 'branch_name' => 'orders.`branch_code`' );	
						$join['icing_types'] = array( 'type' => 'orders.`icing_type`' );		

						if( $claim_type == 'pickup' )
						{
							$columns = array(
									array( 'db' => 'orders.`pickup_at_branch_code`', 		'dt' => 0 ),
									array( 'db' => 'orders.`JO_no`', 						'dt' => 1 ),
									array( 'db' => 'icing_types.`type`', 					'dt' => 2 ),
									array( 'db' => 'orders.`title`', 						'dt' => 3 ),
									array( 'db' => 'order_settings.`order_setting_name`', 	'dt' => 4 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 5 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 6 ),
									array( 'db' => 'orders.`amount_total`',					'dt' => 7 ),
									array( 'db' => 'orders.`balance`',					 	'dt' => 8 ),
									array( 'db' => 'orders.`mode_of_payment`',				'dt' => 9 ),
									array( 'db' => 'orders.`amount_paid`',					'dt' => 10 ),
									array( 'db' => 'orders.`claim_status`',					'dt' => 11 ),	
								);

							$join['branches'] = array( 'branch_name' => 'orders.`pickup_at_branch_code`' );

						} else if ( $claim_type == 'delivery' ) {

							$columns = array(
									array( 'db' => 'orders.`branch_code`', 					'dt' => 0 ),
									array( 'db' => 'orders.`JO_no`', 						'dt' => 1 ),
									array( 'db' => 'icing_types.`type`', 					'dt' => 2 ),
									array( 'db' => 'orders.`title`', 						'dt' => 3 ),
									array( 'db' => 'order_settings.`order_setting_name`', 	'dt' => 4 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 5 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 6 ),
									array( 'db' => 'orders.`amount_total`',					'dt' => 7 ),
									array( 'db' => 'orders.`balance`',					 	'dt' => 8 ),
									array( 'db' => 'orders.`mode_of_payment`',				'dt' => 9 ),
									array( 'db' => 'orders.`amount_paid`',					'dt' => 10 ),
									array( 'db' => 'orders.`claim_status`',					'dt' => 11 ),	
								);

						} else {
							$columns = array(
									array( 'db' => 'orders.`JO_no`', 						'dt' => 0 ),
									array( 'db' => 'icing_types.`type`', 					'dt' => 1 ),
									array( 'db' => 'orders.`title`', 						'dt' => 2 ),
									array( 'db' => 'order_settings.`order_setting_name`', 	'dt' => 3 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 4 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 5 ),
									array( 'db' => 'orders.`amount_total`',					'dt' => 6 ),
									array( 'db' => 'orders.`balance`',					 	'dt' => 7 ),
									array( 'db' => 'orders.`mode_of_payment`',				'dt' => 8 ),
									array( 'db' => 'orders.`amount_paid`',					'dt' => 9 ),
									array( 'db' => 'orders.`claim_status`',					'dt' => 10 ),	
								);
						}
					break;
				case 'balance':
						$columns = array(
								array( 'db' => 'orders.`JO_no`', 						'dt' => 0 ),
								array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 1 ),
								array( 'db' => 'product_categories.`category_name`', 	'dt' => 2 ),
								array( 'db' => 'orders.`title`', 						'dt' => 3 ),
								array( 'db' => 'orders.`amount_total`', 				'dt' => 4 ),
								array( 'db' => 'orders.`balance`', 						'dt' => 5 ),
							);

						$join['kink_products'] = array( 'product_id' => 'orders.`product_id`' );
						$join['product_categories'] = array( 'category_name' => 'kink_products.`product_category_id`' );
						$w_ar[] = '(orders.`balance` > 0 OR orders.`amount_total` > orders.`amount_paid`)';
					break;
				case 'dsr':
					$join['kink_products'] = array( 'product_id' => 'orders.`product_id`' );

					// refers to CURRENT date
					$from = date( "Y-m-d 00:00:00.00", strtotime( date("Y-m-d") ) );
					$to = date( "Y-m-d 23:59:59.99", strtotime( date("Y-m-d") ) );

					if( $mto == 'mto' )
					{
						$columns = array(
								array( 'db' => 'orders.`JO_no`', 						'dt' => 0 ),
								array( 'db' => 'orders.`order_notes`', 					'dt' => 1 ),
								array( 'db' => 'product_categories.`category_name`', 	'dt' => 2 ),
								array( 'db' => 'orders.`title`', 						'dt' => 3 ),
								array( 'db' => 'orders.`amount_total`', 				'dt' => 4 ),
								array( 'db' => 'orders.`payment_type`', 				'dt' => 5 ),
								array( 'db' => 'orders.`mode_of_payment`', 				'dt' => 6 ),
								array( 'db' => 'orders.`amount_paid`', 					'dt' => 7 ),
							);
						
						$join['product_categories'] = array( 'category_name' => 'kink_products.`product_category_id`' );
					}
					else if ( $mto == 'available' )
					{
						$columns = array(
								array( 'db' => 'orders.`title`', 						'dt' => 0 ),
								array( 'db' => 'kink_products.`product_name`', 			'dt' => 1 ),
								array( 'db' => 'orders.`order_notes`', 					'dt' => 2 ),
								array( 'db' => 'kink_products.`selling_price`', 		'dt' => 3 ),
								array( 'db' => 'orders.`qty`', 							'dt' => 4 ),
								array( 'db' => 'orders.`mode_of_payment`', 				'dt' => 5 ),
								array( 'db' => 'orders.`amount_paid`', 					'dt' => 6 ),
							);
					}
					break;

			endswitch;
		endif;


		if( $mto == 'mto'  )
		{
			$w_ar[] = '(orders.`product_id` = FALSE OR orders.`product_id` IS NULL)';
		} else if ( $mto == 'available' ) {
			$w_ar[] = '(orders.`product_id` != FALSE)';
		}

		if( $claim_type == 'pickup' )
		{
			$w_ar[] = '(orders.`claim_type` = \'pickup\' OR orders.`claim_type` = 1)';
		} else if ( $claim_type == 'delivery' )
		{
			$w_ar[] = '(orders.`claim_type` = \'delivery\' OR orders.`claim_type` = 2)';
		}

		if( $from ) {
			$from = (string) $from;
			if ( $to ) {
				$w_ar[] = "(orders.`date_created` >= '{$from}' AND orders.`date_created` <= '{$to}')";
			} else {
				$w_ar[] = "(orders.`date_created` >= '{$from}')";
			}
		}

		return $this->_dt_query( $filter, 'orders', 'id', $columns, $w_ar, $join );
	}

	public function dashboard_directsales( $filter, $from, $to )
	{
		if( !$filter ) return;

		$w_ar = array();
		$join = array();
		$columns = array();

		$mto = ( isset($filter['mto']) && !empty($filter['mto']) ) ? $filter['mto'] : 'both' ;
		$claim_type = ( isset($filter['claim_type']) && !empty($filter['claim_type']) ) ? $filter['claim_type'] : 'both' ;

		if ( isset( $filter['category'] ) ) :
			switch ( $filter['category'] ) :

				case 'transactions':
						$join['order_settings'] = array( 'order_setting_id' => 'orders.`order_set`' );	
						$join['branches'] = array( 'branch_name' => 'orders.`branch_code`' );	
						$join['icing_types'] = array( 'type' => 'orders.`icing_type`' );		

						if( $claim_type == 'pickup' )
						{
							$columns = array(
									array( 'db' => 'orders.`pickup_at_branch_code`', 		'dt' => 0 ),
									array( 'db' => 'orders.`JO_no`', 						'dt' => 1 ),
									array( 'db' => 'icing_types.`type`', 					'dt' => 2 ),
									array( 'db' => 'orders.`title`', 						'dt' => 3 ),
									array( 'db' => 'order_settings.`order_setting_name`', 	'dt' => 4 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 5 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 6 ),
									array( 'db' => 'orders.`amount_total`',					'dt' => 7 ),
									array( 'db' => 'orders.`balance`',					 	'dt' => 8 ),
									array( 'db' => 'orders.`mode_of_payment`',				'dt' => 9 ),
									array( 'db' => 'orders.`amount_paid`',					'dt' => 10 ),
									array( 'db' => 'orders.`claim_status`',					'dt' => 11 ),	
								);

							$join['branches'] = array( 'branch_name' => 'orders.`pickup_at_branch_code`' );

						} else {
							$columns = array(

									array( 'db' => 'orders.`JO_no`', 						'dt' => 0 ),
									array( 'db' => 'icing_types.`type`', 					'dt' => 1 ),
									array( 'db' => 'orders.`title`', 						'dt' => 2 ),
									array( 'db' => 'order_settings.`order_setting_name`', 	'dt' => 3 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 4 ),
									array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 5 ),
									array( 'db' => 'orders.`amount_total`',					'dt' => 6 ),
									array( 'db' => 'orders.`balance`',					 	'dt' => 7 ),
									array( 'db' => 'orders.`mode_of_payment`',				'dt' => 8 ),
									array( 'db' => 'orders.`amount_paid`',					'dt' => 9 ),
									array( 'db' => 'orders.`claim_status`',					'dt' => 10 ),	
								);
						}
					break;
				case 'balance':
						$columns = array(
								array( 'db' => 'orders.`JO_no`', 						'dt' => 0 ),
								array( 'db' => 'orders.`delivery_arrival`', 			'dt' => 1 ),
								array( 'db' => 'product_categories.`category_name`', 	'dt' => 2 ),
								array( 'db' => 'orders.`title`', 						'dt' => 3 ),
								array( 'db' => 'orders.`amount_total`', 				'dt' => 4 ),
								array( 'db' => 'orders.`balance`', 						'dt' => 5 ),
							);

						$join['kink_products'] = array( 'product_id' => 'orders.`product_id`' );
						$join['product_categories'] = array( 'category_name' => 'kink_products.`product_category_id`' );
						$w_ar[] = '(orders.`balance` > 0 OR orders.`amount_total` > orders.`amount_paid`)';
					break;
				case 'dsr':
					$join['kink_products'] = array( 'product_id' => 'orders.`product_id`' );
					$from = date( "Y-m-d 00:00:00.00", strtotime( date("Y-m-d") ) );
					$to = date( "Y-m-d 23:59:59.99", strtotime( date("Y-m-d") ) );

					if( $mto == 'mto' )
					{
						$columns = array(
								array( 'db' => 'orders.`JO_no`', 						'dt' => 0 ),
								array( 'db' => 'orders.`order_notes`', 					'dt' => 1 ),
								array( 'db' => 'product_categories.`category_name`', 	'dt' => 2 ),
								array( 'db' => 'orders.`title`', 						'dt' => 3 ),
								array( 'db' => 'orders.`amount_total`', 				'dt' => 4 ),
								array( 'db' => 'orders.`payment_type`', 				'dt' => 5 ),
								array( 'db' => 'orders.`mode_of_payment`', 				'dt' => 6 ),
								array( 'db' => 'orders.`amount_paid`', 					'dt' => 7 ),
							);
						
						$join['product_categories'] = array( 'category_name' => 'kink_products.`product_category_id`' );
					}
					else if ( $mto == 'available' )
					{
						$columns = array(
								array( 'db' => 'orders.`title`', 						'dt' => 0 ),
								array( 'db' => 'kink_products.`product_name`', 			'dt' => 1 ),
								array( 'db' => 'orders.`order_notes`', 					'dt' => 2 ),
								array( 'db' => 'kink_products.`selling_price`', 		'dt' => 3 ),
								array( 'db' => 'orders.`qty`', 							'dt' => 4 ),
								array( 'db' => 'orders.`mode_of_payment`', 				'dt' => 5 ),
								array( 'db' => 'orders.`amount_paid`', 					'dt' => 6 ),
							);
					}
					break;

			endswitch;
		endif;

		if( $mto == 'mto'  )
		{
			$w_ar[] = '(orders.`product_id` = FALSE OR orders.`product_id` IS NULL)';
		} else if ( $mto == 'available' ) {
			$w_ar[] = '(orders.`product_id` != FALSE)';
		}

		if( $claim_type == 'pickup' )
		{
			$w_ar[] = '(orders.`claim_type` = \'pickup\' OR orders.`claim_type` = 1)';
		} else if ( $claim_type == 'delivery' )
		{
			$w_ar[] = '(orders.`claim_type` = \'delivery\' OR orders.`claim_type` = 2)';
		}

		if( $from ) {
			$from = (string) $from;
			if ( $to ) {
				$w_ar[] = "(orders.`date_created` >= '{$from}' AND orders.`date_created` <= '{$to}')";
			} else {
				$w_ar[] = "(orders.`date_created` >= '{$from}')";
			}
		}

		return $this->_dt_query( $filter, 'orders', 'id', $columns, $w_ar, $join );
	}

	public function dashboard_accounting( $filter, $from = NULL, $to = NULL )
	{
		if( $from ) {
			$from = (string) $from;
			if ( $to ) {
				$w_ar[] = "(orders.`date_created` >= '{$from}' AND orders.`date_created` <= '{$to}')";
			} else {
				$w_ar[] = "(orders.`date_created` >= '{$from}')";
			}
		}

		$where = "";
		if( $w_ar ){
			$where = "WHERE ";
			$where .= implode("\nAND ", $w_ar);
		}

		//echo $where;

		$sql = "SELECT
					branches.branch_name,
					SUM(CASE 
							WHEN orders.mode_of_payment = 'cash' 
							THEN orders.amount_paid
							ELSE 0
						END) AS `cash`,
					SUM(CASE 
							WHEN orders.mode_of_payment = 'card' 
							THEN orders.amount_paid
							ELSE 0
						END) AS `card`,
					SUM(orders.amount_paid) AS `total`
				FROM orders
				RIGHT JOIN branches
				ON branches.branch_code = orders.branch_code
				{$where}
				GROUP BY branches.branch_name";

		return $this->query_all( $sql, FALSE );

	}

	public function dashboard_manager( )
	{
		$sql = "SELECT
					branches.branch_name,
					SUM(CASE 
							WHEN orders.mode_of_payment = 'cash' 
							THEN orders.amount_paid
							ELSE 0
						END) AS `cash`,
					SUM(CASE 
							WHEN orders.mode_of_payment = 'card' 
							THEN orders.amount_paid
							ELSE 0
						END) AS `card`,
					SUM(orders.amount_paid) AS `total`
				FROM orders
				RIGHT JOIN branches
				ON branches.branch_code = orders.branch_code
				GROUP BY branches.branch_name";

		return $this->query_all( $sql, FALSE );
	}
	public function dashboard_production()
	{
		$res = array();
		$sql['product_categories'] = 	"SELECT
											product_categories.category_name AS `type`,
											COUNT(orders.id) AS `count`
										FROM product_categories
										LEFT JOIN kink_products
										ON product_categories.category_id = kink_products.product_category_id
										LEFT JOIN orders
										ON kink_products.product_id = orders.product_id
										GROUP BY product_categories.category_id";

		$sql['item_categories'] =		"SELECT
											icing_types.type AS `type`,
											COUNT(orders.id) AS `count`
										FROM icing_types
										LEFT JOIN orders
										ON icing_types.id = orders.icing_type
										GROUP BY icing_types.`type`";

		$sql['order_set'] = 			"SELECT
											order_settings.order_setting_name AS `type`,
											COUNT(orders.id) AS `count`
										FROM order_settings
										LEFT JOIN orders
										ON order_settings.order_setting_id = orders.order_set
										GROUP BY order_settings.order_setting_id";

		$sql['cookies'] = 				"SELECT
											order_settings.order_setting_name AS `type`,
											COUNT(orders.id) AS `count`
										FROM order_settings
										LEFT JOIN orders
										ON order_settings.order_setting_id = orders.order_set
										WHERE order_settings.order_setting_name LIKE '%cookie%'
										GROUP BY order_settings.order_setting_id";

		$sql['cake_flavors'] = 			"SELECT
											flavors.name AS `type`,
											COUNT(orders.id) AS `count`
										FROM flavors
										LEFT JOIN orders
										ON flavors.id = orders.flavor
										GROUP BY flavors.id";

		$sql['sizes'] = 				"SELECT
											sizes.name AS `type`,
											COUNT(orders.id) AS `count`
										FROM sizes
										LEFT JOIN orders
										ON sizes.id = orders.size
										GROUP BY sizes.id";


		foreach( $sql as $k => $v ){
			$res[$k] = $this->query_all( $v, FALSE );
		}

		return $res;
	}

	// fondant and cookie does NOT have a dashboard

	public function dashboard_marshmallow($identity)
	{
            if($identity == 'mto_cakes')
            {
                $sql = '
                    SELECT * FROM orders WHERE stage_job_scope IS NULL and icing_type = "Marshmallow Icing Cake"
                ';
                return $this->select_all($sql);
            }
            elseif($identity == 'decorating')
            {
               $sql = '
                    SELECT * FROM orders WHERE stage_job_scope = "decorating" and icing_type = "Marshmallow Icing Cake"
                ';
                return $this->query_all($sql);
            }
            elseif($identity == 'finishing')
            {
               $sql = '
                    SELECT * FROM orders WHERE stage_job_scope = "finishing" and icing_type = "Marshmallow Icing Cake"
                ';
                return $this->select_all($sql);
            }
            elseif($identity == 'done')
            {
                $sql = '
                    SELECT * FROM orders WHERE stage_job_scope = "done" and icing_type = "Marshmallow Icing Cake"
                ';
                return $this->select_all($sql);
            }
            else return false;
	} 

	public function dashboard_admin( $filter )
	{

	}


	/***************************************************************************************************************/
	



/* End of file m_reports.php */
/* Location: ./application/modules/report/models/m_reports.php */

}

/* End of file m_dashboard.php */
/* Location: ./application/modules/dashboard/models/m_dashboard.php */