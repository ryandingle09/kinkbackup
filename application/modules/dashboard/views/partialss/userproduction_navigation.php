<div id="navigation">
<a href="<?= base_url('up/dashboard/jo_external_sales') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'jobOrderExternalSalesController' ? 'active' : null ?>">

    <div class="widget widget-success widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-truck"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">JO Tracker External Sales</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('up/dashboard/jo_progress_internal_report') ?>">
<div class="col-md2-3  <?= $this->uri->rsegment(1) == 'jobOrderProgressInternalReportController' ? 'active' : null ?>">

    <div class="widget widget-danger widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-bullseye"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">JO Tracker Internal Production</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('up/dashboard/mto_claim_at_commissary') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'mtoClaimAtCommissaryController' ? 'active' : null ?>">

    <div class="widget widget-warning widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-calendar"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">MTO Claim at Commissary</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('up/dashboard/up/branch_request') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'branchRequestController' ? 'active' : null ?>">

    <div class="widget widget-info widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-envelope"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Branch Request</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('up/dashboard/up/inventory') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'inventoryController' ? 'active' : null ?>">

    <div class="widget widget-primary widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-calendar"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Inventory</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
</div>