<form id="date" class="col-md-5 col-md-offset-7 form-horizontal push-down-20" action="" method="post">
    <div class="form-group">
        <label class="col-md-2 control-label">Search Period From:</label>
        <div class="col-md-3">
            <div class="input-group">
                <input name="from" type="text" id="dp-3" class="form-control" value="<?php echo ( isset($_POST['from']) && !empty($_POST['from'])) ? $_POST['from'] : $from ; ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <label class="col-md-2 control-label">Search Period To:</label>
        <div class="col-md-3">
            <div class="input-group">
                <input name="to" type="text" id="dp-3" class="form-control" value="<?php echo ( isset($_POST['to']) && !empty($_POST['to'])) ? $_POST['to'] : $to ; ?>" data-date-format="mm-dd-yyyy" data-date-viewmode="years"/>
                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <div class="col-md-1 pull-left">
            <input type="submit" class="btn btn-sm" value="Go" name="from-to">
        </div>
    </div>
</form>
<div class="clearfix"></div>
<div class="row">
	<div class="col-md-8">
    	<div class="panel panel-danger">
            <div class="panel-heading ui-draggable-handle">
                <h3 class="panel-title">Sales Report</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <!-- <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li> -->
                </ul>
            </div>
            <div class="panel-body">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>Branch</th>
                            <th>Cash</th>
                            <th>Card</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $total['cash'] = 0;
                        $total['card'] = 0;
                        if ( $dashboard[$employee_class] ) : 
                                foreach ( $dashboard[$employee_class] as $k => $v ) :
                        ?>
                                <tr>
                                    <td><?php echo $v[ 'branch_name' ]; ?></td>
                                    <td>₱ <?php echo number_format($v[ 'cash' ], 2, '.', ''); ?></td>
                                    <td>₱ <?php echo number_format($v[ 'card' ], 2, '.', ''); ?></td>
                                    <td>₱ <?php echo number_format( ( $v[ 'total' ] !== null ) ? $v['total'] : 0, 2, '.', ''); ?></td>
                                </tr>
                        <?php 
                                $total['cash'] += $v['cash'];
                                $total['card'] += $v['card'];
                            endforeach;
                        endif; 
                        ?>
                    </tbody>
                </table>
                <div class="col-md-4 pull-right" style="margin-top: 20px">
                    <table class="table table-responsive table-bordered">
                        <tbody>
                            <tr>
                                <td>Total Cash payment</td>
                                <td>₱ <?php echo  number_format($total[ 'cash' ], 2, '.', ''); ?></td>
                            </tr><tr>
                                <td>Total Card Payment</td>
                                <td>₱ <?php echo  number_format($total[ 'card' ], 2, '.', ''); ?></td>
                            </tr><tr>
                                <td>Total Sales</td>
                                <td>₱ <?php echo number_format($total[ 'cash' ] + $total[ 'card' ], 2, '.', ''); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-body">
            	<h1 class="text-center"></span> Percentage Share</h1>
                <div id="morris-donut-example" style="height: 500px;"></div>
            </div>
        </div>
    </div>
</div>