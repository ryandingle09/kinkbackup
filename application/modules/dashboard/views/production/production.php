<!-- START TABS -->                                
<div class="panel panel-default tabs">                            
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#tab-first" role="tab" data-toggle="tab">Product Category</a></li>
        <li><a href="#tab-second" role="tab" data-toggle="tab">Item Category</a></li>
        <li><a href="#tab-third" role="tab" data-toggle="tab">Order Set</a></li>
        <li><a href="#tab-fourth" role="tab" data-toggle="tab">Inventory Items</a></li>
        <li><a href="#tab-fourth1" role="tab" data-toggle="tab">Cookies</a></li>
        <li><a href="#tab-fourth2" role="tab" data-toggle="tab">Chocolates</a></li>
        <li><a href="#tab-fourth3" role="tab" data-toggle="tab">Cake Flavors</a></li>
        <li><a href="#tab-fourth4" role="tab" data-toggle="tab">Sizes</a></li>
    </ul>                            
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab-first">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <?php
                            if ( $dashboard[$employee_class]['product_categories'] ) :
                                echo "<thead><tr>";
                                foreach ( $dashboard[$employee_class]['product_categories'] as $k => $v ) {
                                    echo "<th>{$v['type']}</th>";
                                }

                                echo "</tr></thead><tbody><tr>";

                                foreach ( $dashboard[$employee_class]['product_categories'] as $k => $v ) {
                                    echo "<td>{$v['count']}</td>";
                                }

                                echo "</tr><tbody>";
                            endif;
                        ?>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-second">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <?php
                            if ( $dashboard[$employee_class]['item_categories'] ) :
                                echo "<thead><tr>";
                                foreach ( $dashboard[$employee_class]['item_categories'] as $k => $v ) {
                                    echo "<th>{$v['type']}</th>";
                                }

                                echo "</tr></thead><tbody><tr>";

                                foreach ( $dashboard[$employee_class]['item_categories'] as $k => $v ) {
                                    echo "<td>{$v['count']}</td>";
                                }

                                echo "</tr><tbody>";
                            endif;
                        ?>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Available Cakes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>200</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth1">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <?php
                            if ( $dashboard[$employee_class]['cookies'] ) :
                                echo "<thead><tr>";
                                foreach ( $dashboard[$employee_class]['cookies'] as $k => $v ) {
                                    echo "<th>{$v['type']}</th>";
                                }

                                echo "</tr></thead><tbody><tr>";

                                foreach ( $dashboard[$employee_class]['cookies'] as $k => $v ) {
                                    echo "<td>{$v['count']}</td>";
                                }

                                echo "</tr><tbody>";
                            endif;
                        ?>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth2">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Dick Choco</th>
                                <th>Choco Pops</th>
                                <th>Etc</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>25</td>
                                <td>35</td>
                                <td>34</td>
                            </tr>
                        </tbody>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth3">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <?php
                            if ( $dashboard[$employee_class]['cake_flavors'] ) :
                                echo "<thead><tr>";
                                foreach ( $dashboard[$employee_class]['cake_flavors'] as $k => $v ) {
                                    echo "<th>{$v['type']}</th>";
                                }

                                echo "</tr></thead><tbody><tr>";

                                foreach ( $dashboard[$employee_class]['cake_flavors'] as $k => $v ) {
                                    echo "<td>{$v['count']}</td>";
                                }

                                echo "</tr><tbody>";
                            endif;
                        ?>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-fourth4">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <?php
                            if ( $dashboard[$employee_class]['sizes'] ) :
                                echo "<thead><tr>";
                                foreach ( $dashboard[$employee_class]['sizes'] as $k => $v ) {
                                    echo "<th>{$v['type']}</th>";
                                }

                                echo "</tr></thead><tbody><tr>";

                                foreach ( $dashboard[$employee_class]['sizes'] as $k => $v ) {
                                    echo "<td>{$v['count']}</td>";
                                }

                                echo "</tr><tbody>";
                            endif;
                        ?>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
        <div class="tab-pane" id="tab-third">
            <!-- START BORDERED TABLE SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body panel-body-table">                                
                    <table class="table table-bordered">
                        <?php
                            if ( $dashboard[$employee_class]['order_set'] ) :
                                echo "<thead><tr>";
                                foreach ( $dashboard[$employee_class]['order_set'] as $k => $v ) {
                                    echo "<th>{$v['type']}</th>";
                                }

                                echo "</tr></thead><tbody><tr>";

                                foreach ( $dashboard[$employee_class]['order_set'] as $k => $v ) {
                                    echo "<td>{$v['count']}</td>";
                                }

                                echo "</tr><tbody>";
                            endif;
                        ?>
                    </table>                                
                </div>
            </div>
            <!-- END BORDERED TABLE SAMPLE -->
        </div>
    </div>
</div>                                                   
<!-- END TABS --> 
<!--
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
    <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</div>-->