<div class="panel panel-default tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab8" data-toggle="tab">MTO Cakes</a></li>
        <li><a href="#tab9" data-toggle="tab">Deco Stage</a></li>
        <li><a href="#tab10" data-toggle="tab">Finishing Stage</a></li>
        <li><a href="#tab11" data-toggle="tab">Done</a></li>
    </ul>
    <div class="panel-body tab-content">
        <div class="tab-pane active" id="tab8">
            <table class="table datatable" id="mto_cakes">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>JO#</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td><button class="btn btn-primary">Open</button></td>
                    </tr>
                    <tr>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td><button class="btn btn-primary">Open</button></td>
                    </tr>
                    <tr>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td><button class="btn btn-primary">Open</button></td>
                    </tr>
            </table>
        </div>  
        <div class="tab-pane" id="tab9">
            <table class="table datatable" id="decorating">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>JO#</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                </thead>
                <tbody>
                    <tr>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td><button class="btn btn-success">Done</button></td>
                    </tr>
                </tbody>
            </table>
        </div> 
        <div class="tab-pane" id="tab10">
            <table class="table datatable" id="finishing">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>JO#</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td><button class="btn btn-primary">Open</button></td>
                    </tr>
                    <tr>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td><button class="btn btn-primary">Open</button></button></td>
                    </tr>
                    <tr>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td>Test</td>
                       <td><button class="btn btn-primary">Open</button></button></td>
                    </tr>
                </tbody>
            </table>

            <div class="panel panel-danger" style="margin-top: 50px">  
                <div class="panel-body">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>JO#</th>
                                <th>Title</th>
                                <th>Order Set</th>
                                <th>Target Date</th>
                                <th>Target Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                               <td>Test</td>
                               <td>Test</td>
                               <td>Test</td>
                               <td>Test</td>
                               <td>Test</td>
                               <td>Test</td>
                               <td><button class="btn btn-success">Done</button></button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 
        <div class="tab-pane" id="tab11">
            <table class="table datatable" id="done">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>JO#</th>
                        <th>Title</th>
                        <th>Order Set</th>
                        <th>Target Date</th>
                        <th>Target Time</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1
                        </td>
                        <td>MKT-4537
                        </td>
                        <td>xxx
                        </td>
                        <td></td>
                        <td>25-Dec-14
                        </td>
                        <td>1:00PM
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>TMG-2346
                        </td>
                        <td>xxx
                        </td>
                        <td></td>
                        <td>25-Dec-14
                        </td>
                        <td>2:00PM
                        </td>
                    </tr>
                    <tr>
                        <td>3
                        </td>
                        <td>GH-5687
                        </td>
                        <td>xxx
                        </td>
                        <td></td>
                        <td>27-Dec-14
                        </td>
                        <td>10:00AM
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>  
    </div>
</div> 