<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller {
	var $user_session = '';
	public function __construct()
	{
		parent::__construct();
		$this->user_session = $this->session->userdata('user_data');
		if(empty($this->user_session)){
			redirect('login');
		}

		$this->load->model( 'm_dashboard' );
	}

	public function index()
	{


		$user_session = $this->user_session;
		// dump($this->user_session );
		$page = $user_session['page'];
		$folder = $page;
		$class = $user_session['class'];
		$employee_class = str_replace('user', '', $class);
		$from = '01-01-'.date('Y',time());
		$to = date('m-d-Y',time());

		if( isset($_POST['from']) && $_POST['from'] ) {
			$from = date( "Y-m-d 00:00:00.00", strtotime(str_replace('-', '/', $_POST['from'])) );
		} else {
			$from = date( "Y-m-d 00:00:00.00", strtotime(str_replace('-', '/', $from ) ));
		}

		if(  isset($_POST['to']) && $_POST['to'] ){
			$to = date( "Y-m-d 23:59:59.99", strtotime(str_replace('-', '/', $_POST['to'])) );
		} else {
			$to = date( "Y-m-d 23:59:59.99", strtotime(str_replace('-', '/', $to) ));
		}

		$options = array();



		switch( $employee_class ){
			case 'storesales': 
				$options = array(
								array( 'mto' => 'mto',		'claim_type' => 'both',		'order_index' => 0, 	'counter' => false,		'category' => 'transactions' ),
								array( 'mto' => 'mto', 		'claim_type' => 'pickup',	'order_index' => 5, 	'counter' => false,		'category' => 'transactions' ),
								array( 'mto' => 'mto', 		'claim_type' => 'delivery',	'order_index' => 5, 	'counter' => false,		'category' => 'transactions' ),
								array( 'mto' => 'both', 	'claim_type' => 'both',		'order_index' => 2,		'counter' => true, 		'category' => 'balance'),
								array( 'mto' => 'mto', 		'claim_type' => 'both',		'order_index' => 1,		'counter' => true, 		'category' => 'dsr' ),
								array( 'mto' => 'available','claim_type' => 'both',		'order_index' => 1,		'counter' => true,		'category' => 'dsr' ),
							);
				break;
			case 'directsales' :
				$options = array(
								array( 'mto' => 'mto',		'claim_type' => 'both',		'order_index' => 0, 	'counter' => false,		'category' => 'transactions' ),
								array( 'mto' => 'mto', 		'claim_type' => 'pickup',	'order_index' => 5, 	'counter' => false,		'category' => 'transactions' ),
								//array( 'mto' => 'mto',        'claim_type' => 'delivery',	'order_index' => 5, 	'counter' => false,		'category' => 'transactions' ),
								array( 'mto' => 'both', 	'claim_type' => 'both',		'order_index' => 2,		'counter' => true, 		'category' => 'balance'),
								array( 'mto' => 'mto', 		'claim_type' => 'both',		'order_index' => 1,		'counter' => true, 		'category' => 'dsr' ),
								array( 'mto' => 'available','claim_type' => 'both',		'order_index' => 1,		'counter' => true,		'category' => 'dsr' ),
							);
				break;
		}

		if( isset($_POST['draw']) )
		{
			if ( method_exists( $this->m_dashboard, "dashboard_{$employee_class}") ) {
				echo json_encode( call_user_func( array($this->m_dashboard, "dashboard_{$employee_class}" ), $_POST, $from, $to ) , JSON_PRETTY_PRINT);
			}
		} 
		else 
		{
			$dashboard[$employee_class] = call_user_func( array($this->m_dashboard, "dashboard_{$employee_class}" ), $_POST, $from, $to);

			$dt = "";
			foreach( $options as $k => $v )
			{
				$counter = "";
				$category = "";
				$mto = "";
				$claim_type = "";

				if( isset($v['claim_type']) ){
					if( $v['claim_type'] ){
						$claim_type = "d.claim_type = '".$v['claim_type']."';";
					}
				}	

				if( isset($v['counter'])){
					if( $v['counter'] == true ){
						$counter = "d.counter = 1;";
					} else {
						$counter = "d.counter = 0;";
					}
					
				}

				if( isset($v['counter']) ){
					$mto = "d.mto = '". $v['mto'] ."';";
				}

				if( isset($v['category']) ){
					$category = "d.category = '". $v['category'] ."';";
				}

				$dt .= "
					$( '#{$employee_class}-dashboard-{$k}' ).dataTable({
						serverSide: true,
						processing: true,
						filter: false,
						ajax: {
							url: '',
							type: 'POST',
							data: function( d ){
								d.class  		= '{$employee_class}';
								{$claim_type}
								{$mto}
								{$counter}
								{$category}
								d.from 			= $('input[name=from]').val();
								d.to 			= $('input[name=to]').val();
							}
						},
						order: [[". $v['order_index'] .", 'DESC']]
					});
				\n";
			}


			// for morris donut chart
			$total = 0;
			$share = array();
			$morris = "";			
			if( $employee_class == 'accounting')
			{
				foreach( $dashboard[$employee_class] as $k => $v )
				{
					$total += $v['total'];
				}

				foreach ($dashboard[$employee_class] as $key => $value)
				{
					$share[] = array(
							'label'	=> $value['branch_name'],
							'value' => (float) number_format( round( ($value['total']/$total) * 100, 2), 2, '.', '')
						);
				}

				$morris = "	var morrisCharts = function() {
								Morris.Donut({
									element: 'morris-donut-example',
									data: ".json_encode( $share, JSON_PRETTY_PRINT ).",
									colors: ['red', 'green', 'blue', 'yellow', 'purple', 'orange', 'cyan', 'magenta', 'brown', 'pink']
								});
							
							}();";
			}


						
			$scripts = 
				"<script type='text/javascript' src='js/plugins/datatables/jquery.dataTables.min.js'></script>
				<script type='text/javascript' src='js/plugins/bootstrap/bootstrap-datepicker.js'></script>
				<script type='text/javascript' src='js/plugins/morris/raphael-min.js'></script>
				<script type='text/javascript' src='js/plugins/morris/morris.min.js'></script>
                                <script type='text/javascript' src='js/tableScript.js'></script>
				<script type='text/javascript'>
					$(document).ready(function(){
						index();
							{$dt}
					});
					{$morris}
				</script>"; 

			$from = '01-01-'.date('Y',time());
			$to = date('m-d-Y',time());
			$sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
			$navigation = $this->load->view('partials/'.$class.'_navigation','',true);
			$this->render($folder.'/'.$page,$sidebar,$navigation,compact('scripts','from','to','nav', 'employee_class', 'dashboard'));			
		}



		 

	}
        
        public function get()
        {
            if($this->uri->segment(3) == 'mto_cakes')
            {
                $data = $this->m_dashboard->dashboard_marshmallow($this->uri->segment(3));
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['title'],
                            $row['order_set'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '
                            <button class="btn btn-primary">Open</button></button>
                            '
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(3) == 'decorating')
            {
                $data = $this->m_dashboard->dashboard_marshmallow($this->uri->segment(3));
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['title'],
                            $row['order_set'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '
                            <button class="btn btn-success"><span class="fa fa-check"><span> Done</button></button>
                            '
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(3) == 'finishing')
            {
                $data = $this->m_dashboard->dashboard_marshmallow($this->uri->segment(3));
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['title'],
                            $row['order_set'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat'],
                            '
                            <button class="btn btn-primary">Open</button></button>
                            '
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }
            elseif($this->uri->segment(3) == 'done')
            {
                $data = $this->m_dashboard->dashboard_marshmallow($this->uri->segment(3));
                if($data)
                {
                    $date1;
                    $date2;
                    $time1;
                    $time2;
                    $jo;
                    foreach ($data as $key => $row) 
                    {
                        if($row['delivery_date'] == '0000-00-00' || $row['delivery_time'] == '' || $row['delivery_time'] == '0'){
                            $date1 = '';
                            $time1 = '';
                        }else{
                            $date1 = date("F jS Y",strtotime($row['delivery_date']));
                            $time1 = $row['delivery_time'];
                        }
                        if($row['pickup_date'] == '0000-00-00' || $row['pickup_time'] == '' || $row['pickup_time'] == '0'){
                            $date2 = '';
                            $time2 = '';
                        }else{
                            $date2 = date("F jS Y",strtotime($row['pickup_date']));
                            $time2 = $row['pickup_time'];
                        }
                        if($row['JO_no'] == ''){
                           $jo = $row['branch_code'].'-'.$row['id'];
                        }else{
                           $jo = $row['JO_no'];
                        }
                        $data[$key] = array( 
                            $row['id'],
                            $jo,
                            $row['title'],
                            $row['order_set'],
                            $date1.''.$date2,
                            $time1.' '.$row['delivery_time_stat'].' '.$time2.' '.$row['pickup_time_stat']
                        );
                    }
                }
                else
                {
                    $data=array();
                }
                echo json_encode(array('aaData'=> array_filter($data)));
                exit();
            }else show_404();
        }

	 

}

/* End of file dashboard.php */
/* Location: ./application/modules/dashboard/controllers/dashboard.php */