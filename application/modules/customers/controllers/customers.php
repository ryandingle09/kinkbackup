<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends MX_Controller {

    var $user_session = '';
    public function __construct()
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('user_data');
        if(empty($this->user_session)){
            redirect('login');
        }
        $module_name	=  get_class($this);
        if(!in_array($module_name, $this->user_session['allowedModules'])){
           //redirect($this->user_session['default_home']);
        }
        $this->load->model('m_customers');
    }
	
    public function lists()
    {
        $user_session = $this->user_session;
        $scripts =
        '
        <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="js/jobOrderVars.js"></script>
        <script type="text/javascript" src="js/cVars.js"></script>
        <script type="text/javascript" src="js/cScript.js"></script>
        ';
        $from = '01-01-'.date('Y',time());
        $to = date('m-d-Y',time());
        $page = $user_session['page'];
        $folder = $page;
        $class = $user_session['class'];
        $action = $this->router->fetch_method();
        $sidebar = $this->load->view('partials/'.$class.'_sidebar','',true);
        $navigation = $this->load->view('partials/'.$class.'_navigation','',true);
        $view_path=APPPATH.'modules/'.strtolower(get_class($this)).'/views/'.$action.'/'.$class.'_lists.php';
        if(!file_exists($view_path)){
            redirect($this->user_session['default_home']);
        }else{
            $this->render($action.'/'.$class.'_lists',$sidebar,$navigation,compact('scripts','from','to', 'nav'));	
        }
    }
    
    public function get_customers()
    {
        $data = $this->m_customers->get_customers();
        if($data)
        {
            foreach ($data as $key => $row) 
            {
                $fullname = ucwords($row['firstname'].' '.$row['lastname']);
                $data[$key] = array( 
                    $row['id'],
                    $fullname,
                    $row['contact'],
                    $row['email'],
                    $row['address'],
                    $row['location'],
                    '<button type="button" data-id="'.$row['id'].'" data-fullname="'.$fullname.'"data-contact="'.$row['contact'].'" data-email="'.$row['email'].'" class="btn btn-success get_item" data-target="#modal_large" data-toggle="modal"><span class="fa fa-exchange"></span> View</button>'
                );
            }
        }
        else
        {
            $data=array();
        }
        echo json_encode(array('aaData'=> array_filter($data)));
        exit();
    }
    
    public function get_customer_data()
    {
        $data = $this->m_customers->get_customer_data($this->uri->segment(3));
        if($data)
        {
            foreach($data as $key => $row) 
            {
                $data[$key] = array( 
                    $row['id'],
                    date("F jS Y H:i:s",strtotime($row['date_created'])),
                    $row['branch_name'],
                    $row['JO_no'],
                    $row['icing_type'],
                    $row['title'],
                    $row['cake_type'],
                    $row['total_price_amount'],
                    $row['occasion'],        
                    date("F jS Y",strtotime($row['occasion_date']))
                );
            }
        }
        else
        {
            $data=array();
        }
        echo json_encode(array('aaData'=> array_filter($data)));
        exit();
    }

}

/* End of file customers.php */
/* Location: ./application/modules/customers/controllers/customers.php */