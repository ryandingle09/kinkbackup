<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_customers extends MY_Model {
    
    public function get_customers()
    {
        $sql = "SELECT * FROM customers WHERE buy_count != '0'";
        return $this->select_all($sql);
    }
    
    public function get_customer_data($id)
    {  
        $sql = '
        SELECT 
        orders.id,
        orders.date_created,
        orders.JO_no,
        orders.cake_type,
        orders.icing_type,
        orders.title,
        orders.total_price_amount,
        orders.occasion,
        orders.occasion_date,
        orders.branch_code,
        orders.delivery_time_stat,
        orders.pickup_time_stat,
        branches.branch_code,
        branches.branch_name
        FROM orders 
        JOIN branches ON branches.branch_code = orders.branch_code
        WHERE orders.customer_id = '.$id.'
        ';
        return $this->select_all($sql);
    }

}

/* End of file m_customers.php */
/* Location: ./application/modules/customers/models/m_customers.php */