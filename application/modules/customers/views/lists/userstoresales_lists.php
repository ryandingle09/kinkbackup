<div class="panel panel-danger">
    <div class="panel-heading">                                
        <h3 class="panel-title"><span class="fa fa-group"></span> Customer List</h3>                           
    </div>
    <div class="panel-body">
        <table class="table datatable" id="customers">
            <thead>
                <tr>
                    <th>Customer ID</th>
                    <th>Full Name</th>
                    <th>Contact No.</th>
                    <th>Email Address</th>
                    <th>Address</th>
                    <th>Location</th>
                    <th>Transaction History</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Carl Louis Manuel</td>
                    <td>09292683437</td>
                    <td>carlxaeron09@gmail.com</td>
                    <td>Adress</td>
                    <td><button type="button" class="btn btn-success" data-target="#modal_large" data-toggle="modal">View</button></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div style="display: none;" class="modal" id="modal_large" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 75%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="largeModalHead"><span class="fa fa-exchange"></span> Customer Transaction History</h4>
            </div>
            <div class="modal-body">
                <input type="text" name="c_id" class="c_id" style="display: none">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Contact No.</th>
                            <th>Email Address</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="c_fname"></td>
                            <td class="c_contact"></td>
                            <td class="c_email"></td>
                        </tr>
                   </tbody>
		</table>
                <div class="clearfix"></div>
                <hr/>
                <table class="table datatable" id="get_customer_data">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Date of Order</th>
                            <th>Order Branch</th>
                            <th>JO#</th>
                            <th>Item Category</th>
                            <th>Title</th>
                            <th>Order Set</th>
                            <th>Total Amount</th>
                            <th>Occasion</th>
                            <!--<th>Celebrant Name</th>-->
                            <th>Occasion Date</th>
                        </tr>
                    </thead>
                </table>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                        
            </div>
        </div>
    </div>
</div>