<?php

class Login extends MX_Controller {
	
	 

	public function __construct()
	{
		parent::__construct();
		 $this->load->model('M_auth');
		$this->load->model('M_modules');
		$this->load->helper(array('form', 'url','date','dump'));
		$this->load->library('form_validation');
		 
	}

	public function index()
	{
		 
		if($this->session->userdata('logged_in')){
			$default_home = $this->session->userdata('default_home');
			redirect($default_home, 'refresh');
		}

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|min_length[3]|max_length[20]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
		
		if($this->form_validation->run($this) === FALSE){


			$this->load->view('/login');


		}else{

			//$uri = $this->input->post('redirect');
			//if($uri){
				$default_home = $this->session->userdata('default_home');
				
				//if($this->session->userdata('group_id') != 7)
				//{
					//redirect('/'.$uri);
				//}
				//else
				//{	
					//redirect($default_home);
				//}
				
			//}else{
				
				redirect($default_home);
			//}
		}

		//$data['head_view'] = 'header';
		//$data['main_view']  = 'login';
        
        
       // $data['head_title'] =  get_class($this);
        //$data['foot_view']  = 'footer';
		//$this->load->view('/template', $data);
		//$this->template_loader($data,'login' ); 
	}


	function check_database($password){
		$username 	= $this->input->post('username');
		$result 	= $this->M_auth->logon($username);
		$ip 		= $this->input->ip_address();
		 
		//die;

		if($result != false){
			$password_correct = $result['password'];
				
			$params = array(8, FALSE);
			$this->load->library('passwordhash',$params);
			$hasher = new PasswordHash($params);
			$check =  $hasher->CheckPassword($password, $password_correct);
				
			if($check){
				$group_detail = $this->M_auth->user_group($result['user_id']);
				if($group_detail){
					$branches = $this->M_auth->user_branches($result['user_id']);
					$branch_id = $this->M_auth->user_branches($result['user_id']);
					$allowedModules = $this->M_auth->allowedModules($group_detail['group_id']);
					$user_data = array(
							'username' => $result['username'],
							'fullname'=> $result['first_name'].' '.$result['last_name'],
							'user_id'=> $result['user_id'],
							'logged_in'=>true,
							'group_id'=>$group_detail['group_id'],
							'default_home'=>$group_detail['default_home'],
							'group_name'=>$group_detail['group_name'],
							'branches'=>$branches,
							'branch_id'=>$branch_id,
							'page'=>$group_detail['page'],
							'class'=>$group_detail['class'],
							'allowedModules' => $allowedModules



						);
					$this->session->set_userdata('username', $result['username']);
					$this->session->set_userdata('fullname', $result['first_name'].' '.$result['last_name']);
					$this->session->set_userdata('user_id', $result['user_id']);
					$this->session->set_userdata('logged_in',true);
					
					$this->session->set_userdata('group_id',$group_detail['group_id']);
					$this->session->set_userdata('default_home',$group_detail['default_home']);
					$this->session->set_userdata('group_name',$group_detail['group_name']);
					$this->session->set_userdata('class',$group_detail['class']);
					
					
					$this->session->set_userdata('branches',$branches);
					
					$this->session->set_userdata('branch_id',$branch_id);
					 

					$this->session->set_userdata('user_data',$user_data);
					/*permissions*/
					//$this->load->model('M_permissions');
					
					//$role_id = $this->session->userdata('group_id');
					//$permissions = $this->M_permissions->getGroupPermissions($role_id);
					
					//$this->session->set_userdata('group_permissions',$permissions);
					//$this->getCounters();
				}else{
					$reason = 'Invalid Access';
					//$this->M_auth->logIPAddress($ip,$username,'****',$reason);
					$this->form_validation->set_message('check_database',$reason);
					return FALSE;
				}
				
				
				//$this->M_auth->logIPAddress($ip,$username,'****','Valid Access');
				return TRUE;
			}else{
				$reason = 'Invalid Username/Password';
				//$this->M_auth->logIPAddress($ip,$username,'****',$reason);
				$this->form_validation->set_message('check_database',$reason);
				return FALSE;
			}
		}else{
			$reason = 'Invalid Authentication';
			//$this->M_auth->logIPAddress($ip,$username,'****',$reason);
			$this->form_validation->set_message('check_database', $reason);
			return FALSE;
		}
	}
	
	function validate_credentials()
	{		
		
		 
		$this->load->model('membership_model');
		$query = $this->membership_model->validate();
		
		if($query) // if the user's credentials validated...
		{
			$data = array(
				'username' => $this->input->post('username'),
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			redirect('site/members_area');
		}
		else // incorrect username or password
		{
			//$this->index();
			redirect('/login');
		}
	}
	
	
	function validate()
	{		
		
		 $results= modules::run('projecttracka/login');
		   // var_dump($results);
		   $group = $this->session->userdata('user_prefix');
		   
		 if($results['status'] == true)
		 {
		 	 //echo 'ok';
			 //die;
		 	//redirect('/main');
			 $status = false;
			 $group = $this->session->userdata('user_prefix');
			
			//$result1 = $this->User->getCompanyDetailsForInvoice($group);
			//print_r($result1);
			  $status = true;
			
			$result = array("status"=>$status );
			$json =  json_encode($result);
			echo $json;
			
		 
		 }else{
		 	$status = false;
			$reason = $results['reasons'];
			$result = array("status"=>$status,'splash'=>$reason);
			$json =  json_encode($result);
			echo $json;
		 	//redirect('/login');
		 }
		 
		
		
		/*$this->load->model('membership_model');
		$query = $this->membership_model->validate();
		
		if($query) // if the user's credentials validated...
		{
			$data = array(
				'username' => $this->input->post('username'),
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			redirect('site/members_area');
		}
		else // incorrect username or password
		{
			//$this->index();
			redirect('/login');
		}*/
	}
	
		
	
	function signup()
	{
		$data['main_content'] = 'signup_form';
		$this->load->view('includes/template', $data);
	}
	
	function create_member()
	{
		$this->load->library('form_validation');
		
		// field name, error message, validation rules
		$this->form_validation->set_rules('first_name', 'Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('email_address', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');
		
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('signup_form');
		}
		
		else
		{			
			$this->load->model('membership_model');
			
			if($query = $this->membership_model->create_user())
			{
				$data['main_content'] = 'signup_successful';
				$this->load->view('includes/template', $data);
			}
			else
			{
				$this->load->view('signup_form');			
			}
		}
		
	}
	
	function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('is_logged_in');
		$this->session->sess_destroy();
		//$this->index();
		redirect('/login');
	}	
	
	function is_logged_in()
	{
		//$is_logged_in = $this->session->userdata('is_logged_in');
		$is_logged_in = $this->session->userdata('loggedin');
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			echo 'You don\'t have permission to access this page. <a href="/../login?redirect='.uri_string().'">Login</a>';	
			die();		
			//$this->load->view('login_form');
		} 		
	}
	
	function cp()
	{
		if( $this->session->userdata('username') )
		{
			// load the model for this controller
			$this->load->model('membership_model');
			// Get User Details from Database
			$user = $this->membership_model->get_member_details();
			if( !$user )
			{
				// No user found
				return false;
			}
			else
			{
				// display our widget
				$this->load->view('user_widget', $user);
			}			
		}
		else
		{
			// There is no session so we return nothing
			return false;
		}
	}
	
	
	function get_notifications()
	{
	
		print_r($_POST);
	
	
	
	
	}
}