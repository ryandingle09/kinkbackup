<?php

class Auth_model extends MY_Model {

	/*function validate()
	{
		$this->db->where('username', $this->input->post('username'));
		$this->db->where('password', md5($this->input->post('password')));
		$query = $this->db->get('users');
		
		if($query->num_rows == 1)
		{
			return true;
		}
	}*/
	
	function validate()
	{
		$sql ="SELECT count(*) cntr FROM users WHERE username=? AND password=?";
		$param = array($this->input->post('username'),md5($this->input->post('password')));
		
		$result = $this->select($sql,$param,'both');
		
		if($result['cntr'])
		{
			return 1;
		}else{
			return 0;
		}
		 
	}
	
	
	
	
	function logon($username )
	{
	 
	
		//$username='ronald.tuibeo@huxxer.com';
		//$password='$2a$08$VmbO.OFwLiizotQS4doYkO15aRsURtHmbzPOO0/nLXlSOksEyz50.';
	 $sql = "SELECT * FROM users WHERE username= ?  ";
		$param = array($username );
		$result =$this->select($sql,$param);	
	
		 
		return $result;
	
	}
	
	
	public function accessLevel($group_id)
	{
	
	 
	 	$sql = "SELECT id,name FROM acl_groups WHERE id=?";
		$param = array($group_id);
		$result = $this->select($sql,$param);
		
		return $result;
		
	
	
	}
	
	
	public function getUserPrefix($group_id)
	{
		$sql = "SELECT prefix FROM lib_groups WHERE id =?";
		
		$param = array($group_id);
		
		$result = $this->select($sql,$param);
		
		return $result;
	
	
	
	
	}
	
	
	public function getDefaultPrefix()
	{
		$sql = "SELECT prefix FROM lib_groups WHERE `default`=?";
		
		$param = array(1);
		
		$result = $this->select($sql,$param);
		
		return $result;
	
	
	
	
	}
	
	
	public function getValidIPAddresses($ip)
	{
		$sql = "SELECT count(*) cntr FROM lib_ip_address WHERE `ip_address`=?";
		
		$param = array($ip);
		
		$result = $this->select($sql,$param);
		
		if($result->cntr > 0){
			return true;
		}else{
		
			return false;
		}
	
	
	
	}
	
	
	public function logIPAddress($ip,$username,$password,$type)
	{
		$current_time = date('Y-m-d G:i:s');
		$sql = "INSERT INTO ip_logs (ip_address,log_time,username,`password`,type) VALUES(?,?,?,?,?)";
		
		$param = array($ip,$current_time,$username,$password,$type);
		
		$result = $this->query($sql,$param);
		
		return $result;
	
	
	
	}
	
	
	public function logValidUserOnIPAddress($ip,$username)
	{
		$current_time = date('Y-m-d G:i:s');
		$sql = "UPDATE lib_ip_address SET username=?, last_login_time=? WHERE ip_address=?";
		
		$param = array($username,$current_time,$ip);
		
		$result = $this->query($sql,$param);
		
		return $result;
	
	
	
	}
}
