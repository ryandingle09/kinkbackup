<style>
body{background-color: #eee}
.form-signin
{
max-width: 330px;
padding: 15px;
margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
margin-bottom: 10px;
}
.form-signin .checkbox
{
font-weight: normal;
}
.form-signin .form-control
{
position: relative;
font-size: 16px;
height: auto;
padding: 10px;
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
}
.form-signin .form-control:focus
{
z-index: 2;
}
.form-signin input[type="text"]
{
margin-bottom: -1px;
border-bottom-left-radius: 0;
border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
margin-bottom: 10px;
border-top-left-radius: 0;
border-top-right-radius: 0;
}
.account-wall
{
margin-top: 20px;
padding: 40px 0px 20px 0px;
background-color: #f7f7f7;
-moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
-webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
color: #555;
font-size: 18px;
font-weight: 400;
display: block;
}
.profile-img
{
width: 100px;
height: 100px;
margin: 0 auto 10px;
display: block;
-moz-border-radius: 30%;
-webkit-border-radius: 30%;
border-radius: 30%;
}
.need-help
{
margin-top: 10px;
}
.new-account
{
display: block;
margin-top: 10px;
}
.container{margin: 110px auto;}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-6 col-md-4 col-md-offset-4">
			<h1 class="text-center login-title">Sign in to  Quotr</h1>
			<div class="account-wall">
				<img class="profile-img" src="assets/images/logo.png"
				alt="">
				<form class="form-signin" id="login_form" onsubmit="return false">
					<input type="text" name="username" id="username" class="form-control" placeholder="Email" required autofocus>
					<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
					<button class="btn btn-lg btn-primary btn-block" id="login">
					Sign in</button>
					<label class="checkbox pull-left">
					<input type="checkbox" value="remember-me">
					Remember me
					</label>
					<a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
				</form>
			</div>
			<a href="#" class="text-center new-account">Create an account </a>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){

	$("#login").click(function(){
	
		 
		  
		 var uname = $("#username").val();
		  var pass = $("#password").val();
		 dataString	= 'username='+uname+'&password='+pass;
		 $.ajax({
			type: "POST",
			url: "login/validate",
			data: dataString,
			dataType: 'json',
			success: function(json){
				if(json.status == true){
					//success to save
					 // $("#conditions").html(json.splash);
					 //  $('#myModalPreview').modal();
					//window.location='/admin/m2libordertypes';
					window.location='<?=($this->input->get('redirect'))?$this->input->get('redirect'):"/main";?>';
				}else{
					//fail to save
					$("#err_msg").html(json.splash);
					 
				}
			}
		}); 
		 
		 
	
	});
	
	$("#reject").click(function(){
		window.location='/login/logout';
	
	});
	$("#accept").click(function(){
		window.location='<?=($this->input->get('redirect'))?$this->input->get('redirect'):"/main";?>';
	
	});


});

</script>