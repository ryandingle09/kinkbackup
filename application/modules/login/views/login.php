<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title><?= SITE_TITLE ?> - LOGIN</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->

        <base href="<?php echo base_url() ?>assets/">
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <link rel="stylesheet" type="text/css" id="theme" href="css/main.css"/>
        <!-- EOF CSS INCLUDE -->                                     
    </head>
    <body>
        
        <div class="login-container lightmode <?= $this->uri->rsegment(1).' '.$this->uri->rsegment(2) ?>">
            <div class="login-box animated fadeInDown">
            <div class="alert alert-info" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <br><br>
                <div class="row">
                    <div class="col-md-6">
                    	<strong>User Store Sales</strong><br/>
                        username: Sales-Mitch<br/>
                        password: pass<br/><br/> 
                    </div>
                    <div class="col-md-6">
                    	<strong>User Production</strong><br/>
                        username: Prod-Paul<br/>
                        password: pass<br/><br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    	<strong>User Marshmallow</strong><br/>
                        username: Marsh-Cel<br/>
                        password: pass<br/><br/>
                    </div>
                    <div class="col-md-6">
                    	<strong>User Fondant</strong><br/>
                        username: Fondant-Jhay<br/>
                        password: pass<br/><br>
                    </div>
                </div>
                <!--
                <div class="row">
                    <div class="col-md-6">
                    	<strong>User Baker</strong><br/>
                        username: 5<br/>
                        password: (blank)<br/><br/>
                    </div>
                    <div class="col-md-6">
                    	<strong>User Hr</strong><br/>
                        username: 6<br/>
                        password: (blank)<br/><br/>
                    </div>
                </div>
                -->
                <div class="row">
                    <div class="col-md-6">
                    	<strong>User Admin</strong><br/>
                        username: Acc-Jun<br/>
                        password: pass<br/><br/>
                    </div>
                    <div class="col-md-6">
                    	<strong>User Manager</strong><br/>
                        username: Mgr-Lyn<br/>
                        password: pass<br/><br/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    	<strong>User Direct Sales</strong><br/>
                        username: Direct_rj<br/>
                        password: pass<br/><br/>
                    </div>
                    <div class="col-md-6">
                    	<strong>User Cookie</strong><br/>
                        username: cookie<br/>
                        password: pass<br/><br/>
                    </div>
                </div>
                <!--
                <div class="row">
                    <div class="col-md-12">
                    	<strong>User Direct Sales</strong><br/>
                        username: 11<br/>
                        password: (blank)<br/><br/>
                    </div>
                </div>
                -->
            </div>
                <h1 id="title"><?= SITE_TITLE ?></h1>
                <div class="login-body">
                    <div class="login-title"><strong>Log In</strong> to your account</div>
                    <form action="<?= base_url('login') ?>" class="form-horizontal" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input name='username' type="text" class="form-control" placeholder="Username"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input name='password' type="password" class="form-control" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Log In</button>
                        </div>
                    </div>
                    <!-- <div class="login-or">OR</div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <button class="btn btn-info btn-block btn-twitter"><span class="fa fa-twitter"></span> Twitter</button>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-info btn-block btn-facebook"><span class="fa fa-facebook"></span> Facebook</button>
                        </div>
                        <div class="col-md-4">                            
                            <button class="btn btn-info btn-block btn-google"><span class="fa fa-google-plus"></span> Google</button>
                        </div>
                    </div>
                    <div class="login-subtitle">
                        Don't have an account yet? <a href="#">Create an account</a>
                    </div> -->
                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 clear">
                                <input type="hidden" name="redirect" value="<?=$this->input->get('redirect');?>">
                                <span style="color: red;"><?php echo validation_errors(); ?> </span>                            
                                 
                            </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; <?= date('Y',time()) ?> <?= SITE_TITLE ?>
                    </div>
                    <div class="pull-right">
                        <!-- <a href="#">About</a> |
                        <a href="#">Privacy</a> |
                        <a href="#">Contact Us</a> -->
                    </div>
                </div>
            </div>
            
        </div>
        
    </body>
</html>