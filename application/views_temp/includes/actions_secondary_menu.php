<?
$menus='';
$ci = & get_instance();

$module 	= $ci->uri->segment(1,0);
$controller = $ci->uri->segment(2,0);
$action		= $ci->uri->segment(3,0);

$controller = $ci->router->fetch_class();
$action		= $ci->router->fetch_method();
//print_r($secondary_menu);
if($secondary_menu){
	foreach($secondary_menu as $key=>$smenu)
	{
		$active =($smenu->action_name==$action && $action) ? 'active':'';
		$menus .= '<li class="'.$active.'"><a href="/'.$module.'/'.$controller.'/'.$smenu->action_name.'">'.$smenu->display_name.'</a></li><li class="divider"></li>';	
	}
	
	echo $menus;
}else{
	echo 'Please grant permissions first!';
}