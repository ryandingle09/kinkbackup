<li class="xn-logo">
    <a href="<?= base_url('dashboard') ?>"><?= SITE_TITLE ?></a>
    <a href="#" class="x-navigation-control"></a>
</li>
<li class="xn-profile">
    <a href="#" class="profile-mini">
        <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
    </a>
    <div class="profile">
        <div class="profile-image">
            <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
        </div>
        <div class="profile-data">
            <div class="profile-data-name">John Doe</div>
            <div class="profile-data-title"><?= $this->session->userdata('userTypeName') ?></div>
        </div>
        <div class="profile-controls">
            <a href="<?= base_url('dashboard') ?>" class="profile-control-left"><span class="fa fa-info"></span></a>
            <a href="<?= base_url('dashboard') ?>" class="profile-control-right"><span class="fa fa-envelope"></span></a>
        </div>
    </div>                                                                        
</li>