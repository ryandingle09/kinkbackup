<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            <a href="<?= base_url('dashboard') ?>"><?php $this->config->item('cakes')['title'] ?></a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name">John Doe</div>
                    <div class="profile-data-title">Web Developer/Designer</div>
                </div>
                <div class="profile-controls">
                    <a href="<?= base_url('dashboard') ?>" class="profile-control-left"><span class="fa fa-info"></span></a>
                    <a href="<?= base_url('dashboard') ?>" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                </div>
            </div>                                                                        
        </li>
        <li class="xn-title">Navigation</li>
        <li class="<?= $this->uri->rsegment(1) == 'cakes' ? 'active' : null ?>">
            <a href="<?= base_url('dashboard') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li>   
        <li class="<?= $this->uri->rsegment(1) == 'returnItemsController' ? 'active' : null ?>">
            <a href="<?= base_url('return_items') ?>"><span class="fa fa-reply"></span> <span class="xn-text">Return Items</span></a>                        
        </li>  
        <li class="<?= $this->uri->rsegment(1) == 'performanceLogController' ? 'active' : null ?>">
            <a href="<?= base_url('performance_log') ?>"><span class="fa fa-trophy"></span> <span class="xn-text">Performance Log</span></a>                        
        </li>                
        <li class="<?= $this->uri->rsegment(1) == 'customerListController' ? 'active' : null ?>">
            <a href="<?= base_url('customer_list') ?>"><span class="fa fa-users"></span> <span class="xn-text">Customer List</span></a>                        
        </li> 
        
    </ul>
    <!-- END X-NAVIGATION -->
</div>