<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <?= modules::run('cakes/partialsControllerMain/profile') ?>
        <li class="<?= $this->uri->rsegment(1) == 'cakes' ? 'active' : null ?>">
            <a href="<?= base_url('dashboard') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li>   
        <li class="<?= $this->uri->rsegment(1) == 'userController' ? 'active' : null ?>">
            <a href="<?= base_url('admin/dashboard/user') ?>"><span class="fa fa-users"></span> <span class="xn-text">Users</span></a>                        
        </li>             
        <li class="<?= $this->uri->rsegment(1) == 'occasionController' ? 'active' : null ?>">
            <a href="<?= base_url('admin/dashboard/occasion') ?>"><span class="fa fa-sun-o"></span> <span class="xn-text">Occasion</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'dailyTimeRecordController' ? 'active' : null ?>">
            <a href="<?= base_url('admin/dashboard/dailyTimeRecord') ?>"><span class="fa fa-clock-o"></span> <span class="xn-text">Daily Time Record</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'dailySalesReportController' ? 'active' : null ?>">
            <a href="<?= base_url('admin/dashboard/dailySalesReport') ?>"><span class="fa fa-clock-o"></span> <span class="xn-text">Daily Sales Report</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'supplierController' ? 'active' : null ?>">
            <a href="<?= base_url('admin/dashboard/supplier') ?>"><span class="fa fa-calendar"></span> <span class="xn-text">Supplier</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'itemInventoryController' ? 'active' : null ?>">
            <a href="<?= base_url('admin/dashboard/itemInventory') ?>"><span class="fa fa-database"></span> <span class="xn-text">Item Inventory</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'performanceLogController' ? 'active' : null ?>">
            <a href="<?= base_url('admin/dashboard/performanceLog') ?>"><span class="fa fa-trophy"></span> <span class="xn-text">Perofmance Log</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'historyLogController' ? 'active' : null ?>">
            <a href="<?= base_url('admin/dashboard/historyLog') ?>"><span class="fa fa-globe"></span> <span class="xn-text">History Log</span></a>                        
        </li> 
    </ul>
    <!-- END X-NAVIGATION -->
</div>