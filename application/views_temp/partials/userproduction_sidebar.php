<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
       <?= $this->load->view('partials/profile'); ?>
        <!--<li class="xn-title">Navigation</li>-->
        <li class="<?= $this->uri->rsegment(1) == 'cakes' ? 'active' : null ?>">
            <a href="<?= base_url('dashboard') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li>
        <li class="<?= $this->uri->rsegment(2) == 'returned_items' ? 'active' : null ?>">
            <a href="<?= base_url('inventory/returned_items') ?>"><span class="fa fa-reply"></span> <span class="xn-text">Returned Items</span></a>                        
        </li>  
        <li class="<?= $this->uri->rsegment(2) == 'mto_log' ? 'active' : null ?>">
            <a href="<?= base_url('reports/mto_log') ?>"><span class="fa fa-calendar"></span> <span class="xn-text">MTO Log</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(2) == 'performance_log' ? 'active' : null ?>">
            <a href="<?= base_url('reports/performance_log') ?>"><span class="fa fa-trophy"></span> <span class="xn-text">Performance Log</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(2) == 'history_log' ? 'active' : null ?>">
            <a href="<?= base_url('reports/history_log') ?>"><span class="fa fa-globe"></span> <span class="xn-text">History Log</span></a>                        
        </li>    
    </ul>
    <!-- END X-NAVIGATION -->
</div>