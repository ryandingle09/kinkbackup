<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <?= modules::run('cakes/partialsControllerMain/profile') ?>
        <!--<li class="xn-title">Navigation</li>
        <li class="<?= $this->uri->rsegment(1) == 'cakes' ? 'active' : null ?>">
            <a href="<?= base_url('dashboard') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'returnItemsController' ? 'active' : null ?>">
            <a href="<?= base_url('hr/dashboard/return_items') ?>"><span class="fa fa-reply"></span> <span class="xn-text">Return Items</span></a>                        
        </li>  
        --->
        <li class="<?= $this->uri->rsegment(1) == 'performanceLogController' ? 'active' : null ?>">
            <a href="<?= base_url('hr/dashboard/performance_log') ?>"><span class="fa fa-trophy"></span> <span class="xn-text">Performance Log</span></a>                        
        </li>
        <!--                
        <li class="<?= $this->uri->rsegment(1) == 'customerListController' ? 'active' : null ?>">
            <a href="<?= base_url('hr/dashboard/customer_list') ?>"><span class="fa fa-users"></span> <span class="xn-text">Customer List</span></a>                        
        </li> 
        -->
    </ul>
    <!-- END X-NAVIGATION -->
</div>