<div id="navigation">
<!--
<a href="<?= base_url('dashboard') ?>">
<div class="col-md-3 <?= $this->uri->rsegment(1) == 'cakes' ? 'active' : null ?>">

    <div class="widget widget-primary widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-desktop"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Dashboard</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
-->
<a href="<?= base_url('dashboard') ?>">
<div class="col-md-3 <?= $this->uri->rsegment(1) == 'dashboard' ? 'active' : null ?>">

    <div class="widget widget-success widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-bullseye"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Dashboard /  &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;   JO Tracker</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('branches/request') ?>">
<div class="col-md-3  <?= $this->uri->rsegment(2) == 'request' ? 'active' : null ?>">

    <div class="widget widget-danger widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-envelope"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Branch Request</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('reports/performance_log') ?>">
<div class="col-md-3 <?= $this->uri->rsegment(2) == 'performance_log' ? 'active' : null ?>">

    <div class="widget widget-warning widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-calendar"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Performance Log</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
</div>
<div class="clearfix"></div>