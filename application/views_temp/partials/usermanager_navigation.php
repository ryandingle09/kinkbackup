<div id="navigation">
	<!---
    <a href="<?= base_url('mgr/dashboard/performance_log') ?>">
    <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'performanceLogController' ? 'active' : null ?>">
        <div class="widget widget-success widget-item-icon">
            <div class="widget-item-right">
                <span class="fa fa-trophy"></span>
            </div>                             
            <div class="widget-data-left">
                <div class="widget-int num-count"></div>
                <div class="widget-title">Performance Log</div>
                <div class="widget-subtitle"></div>
            </div>                                     
        </div>
    </div>
    </a>
    --->
    
    <!--
    <a href="<?= base_url('mgr/dashboard/history_log') ?>">
    <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'historyLogController' ? 'active' : null ?>">
        <div class="widget widget-info widget-item-icon">
            <div class="widget-item-right">
                <span class="fa fa-recycle"></span>
            </div>                             
            <div class="widget-data-left">
                <div class="widget-int num-count"></div>
                <div class="widget-title">History Log</div>
                <div class="widget-subtitle"></div>
            </div>                                     
        </div>
    </div>
	</a>
    -->
    <a href="<?= base_url('reports/branch_sales') ?>">
    <div class="col-md2-3 <?= $this->uri->rsegment(2) == 'branch_sales' ? 'active' : null ?>">
        <div class="widget widget-primary widget-item-icon">
            <div class="widget-item-right">
                <span class="fa fa-home"></span>
            </div>                             
            <div class="widget-data-left">
                <div class="widget-int num-count"></div>
                <div class="widget-title">Branch Sales Performance</div>
                <div class="widget-subtitle"></div>
            </div>                                     
        </div>
    </div>
    <a href="<?= base_url('reports/daily_sales') ?>">
    <div class="col-md2-3 <?= $this->uri->rsegment(2) == 'daily_sales' ? 'active' : null ?>">
        <div class="widget widget-danger widget-item-icon">
            <div class="widget-item-right">
                <span class="fa fa-clock-o"></span>
            </div>                             
            <div class="widget-data-left">
                <div class="widget-int num-count"></div>
                <div class="widget-title">Daily Sales Report</div>
                <div class="widget-subtitle"></div>
            </div>                                     
        </div>
    </div>
	</a>
    <a href="<?= base_url('reports/productivity_statistics') ?>">
    <div class="col-md2-3 <?= $this->uri->rsegment(2) == 'productivity_statistics' ? 'active' : null ?>">
        <div class="widget widget-warning widget-item-icon">
            <div class="widget-item-right">
                <span class="fa fa-calendar"></span>
            </div>                             
            <div class="widget-data-left">
                <div class="widget-int num-count"></div>
                <div class="widget-title">Productivity Statistics</div>
                <div class="widget-subtitle"></div>
            </div>                                     
        </div>
    </div>
	</a>
     <a href="<?= base_url('reports/sales_tracker') ?>">
    <div class="col-md2-3 <?= $this->uri->rsegment(2) == 'sales_tracker' ? 'active' : null ?>">
        <div class="widget widget-info widget-item-icon">
            <div class="widget-item-right">
                <span class="fa fa-bullseye"></span>
            </div>                             
            <div class="widget-data-left">
                <div class="widget-int num-count"></div>
                <div class="widget-title">Sales Tracker</div>
                <div class="widget-subtitle"></div>
            </div>                                     
        </div>
    </div>
    </a>
</div>