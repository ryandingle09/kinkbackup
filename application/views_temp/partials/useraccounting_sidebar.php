<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <?= $this->load->view('partials/profile'); ?> 
        <li class="<?= $this->uri->rsegment(1) == 'dashboard' ? 'active' : null ?>">
            <a href="<?= base_url('dashboard') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
        </li>
        <li class="<?= $this->uri->rsegment(1) == 'suppliers' ? 'active' : null ?>">
            <a href="<?= base_url('suppliers/manage') ?>"><span class="fa fa-calendar"></span> <span class="xn-text">Supplier</span></a>
        </li>
        <li class="<?= $this->uri->rsegment(2) == 'returned_items' ? 'active' : null ?>">
            <a href="<?= base_url('inventory/returned_items') ?>"><span class="fa fa-reply"></span> <span class="xn-text">Return Items</span></a>                        
        </li>
        <!--
        <li class="<?= $this->uri->rsegment(1) == 'availableItemsInventoryController' ? 'active' : null ?>">
            <a href="<?= base_url('acc/dashboard/availableItemsInventory') ?>"><span class="fa fa-database"></span> <span class="xn-text">Available Items Inventory</span></a>                        
        </li>-->
        <li class="<?= $this->uri->rsegment(2) == 'performance_log' ? 'active' : null ?>">
            <a href="<?= base_url('reports/performance_log') ?>"><span class="fa fa-trophy"></span> <span class="xn-text">Performance Log</span></a>                        
        </li>
    </ul>
    <!-- END X-NAVIGATION -->
</div>