<div id="navigation">
<!--
<a href="<?= base_url('dashboard') ?>">
<div class="col-md-3 <?= $this->uri->rsegment(1) == 'cakes' ? 'active' : null ?>">

    <div class="widget widget-primary widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-desktop"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Dashboard</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
-->
<a href="<?= base_url('jobs/tracker') ?>">
<div class="col-md-3 <?= $this->uri->rsegment(2) == 'tracker' ? 'active' : null ?>">

    <div class="widget widget-success widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-desktop"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">JO Tracker</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<!--
<a href="<?= base_url('ua/dashboard/available_cakes') ?>">
<div class="col-md-3  <?= $this->uri->rsegment(1) == 'availableCakesController' ? 'active' : null ?>">

    <div class="widget widget-danger widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-bullseye"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Available Cakes</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
-->
<a href="<?= base_url('reports/performance_log') ?>">
    <div class="col-md-3 <?= $this->uri->rsegment(2) == 'performance_log' ? 'active' : null ?>">

        <div class="widget widget-warning widget-item-icon">
            <div class="widget-item-right">
                <span class="fa fa-trophy"></span>
            </div>                             
            <div class="widget-data-left">
                <div class="widget-int num-count"></div>
                <div class="widget-title">Performance Log</div>
                <div class="widget-subtitle"></div>
            </div>                                     
        </div>
    </div>
</a>
</div>
<div class="clearfix"></div>