<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <?= $this->load->view('partials/profile'); ?>   
        <li class="xn-title">Navigation</li>
        <li class="<?= $this->uri->rsegment(1) == 'cakes' ? 'active' : null ?>">
            <a href="<?= base_url('dashboard') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li>
        <li class="<?= $this->uri->rsegment(2) == 'performance_log' ? 'active' : null ?>">
            <a href="<?= base_url('reports/performance_log') ?>"><span class="fa fa-trophy"></span> <span class="xn-text">Performance Log</span></a>                        
        </li> 
         <li class="<?= $this->uri->rsegment(2) == 'lists' ? 'active' : null ?>">
            <a href="<?= base_url('customers/lists') ?>"><span class="fa fa-users"></span> <span class="xn-text">Customer List</span></a>                        
        </li> 
        <!--<li class="<?= $this->uri->rsegment(1) == 'branch_sales' ? 'active' : null ?>">
            <a href="<?= base_url('mgr/dashboard/bsp') ?>"><span class="fa fa-home"></span> <span class="xn-text">Returned Items</span></a>                        
        </li>  
        <li class="<?= $this->uri->rsegment(1) == 'performanceLogController' ? 'active' : null ?>">
            <a href="<?= base_url('mgr/dashboard/performance_log') ?>"><span class="fa fa-trophy"></span> <span class="xn-text">Performance Log</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'historyLogController' ? 'active' : null ?>">
            <a href="<?= base_url('mgr/dashboard/history_log') ?>"><span class="fa fa-globe"></span> <span class="xn-text">History Log</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'historyLogController' ? 'active' : null ?>">
            <a href="<?= base_url('mgr/dashboard/history_log') ?>"><span class="fa fa-globe"></span> <span class="xn-text">History Log</span></a>                        
        </li>  
        <li class="<?= $this->uri->rsegment(1) == 'dailySalesReportController' ? 'active' : null ?>">
            <a href="<?= base_url('mgr/dashboard/dsr') ?>"><span class="fa fa-clock-o"></span> <span class="xn-text">Daily Sales Report</span></a>                        
        </li>     
        <li class="<?= $this->uri->rsegment(1) == 'productivityStaticticsController' ? 'active' : null ?>">
            <a href="<?= base_url('mgr/dashboard/ps') ?>"><span class="fa fa-stats"></span> <span class="xn-text">Product Statistics</span></a>                        
        </li>-->    
    </ul>
    <!-- END X-NAVIGATION -->
</div>