<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once APPPATH."/third_party/uploadhandler.php"; 
 
class Upload extends UploadHandler { 
    public function __construct() { 
        parent::__construct(); 
		$CI =& get_instance();
        $CI->load->database();     
        $CI->load->helper('url');
        $CI->load->library('session'); 
        $CI->config->item('base_url'); 
    } 
	
	
	 public function post_database($table,$field,$path,$id='') {
        if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
            return $this->delete();
        }
        $upload = isset($_FILES[$this->options['param_name']]) ?
            $_FILES[$this->options['param_name']] : null;
        $info = array();
        if ($upload && is_array($upload['tmp_name'])) {
            // param_name is an array identifier like "files[]",
            // $_FILES is a multi-dimensional array:
            foreach ($upload['tmp_name'] as $index => $value) {
                $info[] = $this->handle_file_upload(
                    $upload['tmp_name'][$index],
                    isset($_SERVER['HTTP_X_FILE_NAME']) ?
                        $_SERVER['HTTP_X_FILE_NAME'] : $upload['name'][$index],
                    isset($_SERVER['HTTP_X_FILE_SIZE']) ?
                        $_SERVER['HTTP_X_FILE_SIZE'] : $upload['size'][$index],
                    isset($_SERVER['HTTP_X_FILE_TYPE']) ?
                        $_SERVER['HTTP_X_FILE_TYPE'] : $upload['type'][$index],
                    $upload['error'][$index],
                    $index
                );
            }
        } elseif ($upload || isset($_SERVER['HTTP_X_FILE_NAME'])) {
            // param_name is a single object identifier like "file",
            // $_FILES is a one-dimensional array:
            $info[] = $this->handle_file_upload(
                isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
                isset($_SERVER['HTTP_X_FILE_NAME']) ?
                    $_SERVER['HTTP_X_FILE_NAME'] : (isset($upload['name']) ?
                        $upload['name'] : null),
                isset($_SERVER['HTTP_X_FILE_SIZE']) ?
                    $_SERVER['HTTP_X_FILE_SIZE'] : (isset($upload['size']) ?
                        $upload['size'] : null),
                isset($_SERVER['HTTP_X_FILE_TYPE']) ?
                    $_SERVER['HTTP_X_FILE_TYPE'] : (isset($upload['type']) ?
                        $upload['type'] : null),
                isset($upload['error']) ? $upload['error'] : null
            );
        }
		
		print_r($info);
		$err = isset($info[0]->error)?$info[0]->error:'';
		 
		//save to temp_service_holder table
		if($err == '')
		{
			$filename = $info[0]->name;
			 
			if($id == '')
			{
				$result = $this->insertToDatabase($table,$field,$filename,$path);
			}
			else
			{
				$result = $this->updateDatabase($table,$field,$filename,$id,$path);
			}
		
		}
		 
        header('Vary: Accept');
        $json = json_encode($info);
        $redirect = isset($_REQUEST['redirect']) ?
            stripslashes($_REQUEST['redirect']) : null;
        if ($redirect) {
            header('Location: '.sprintf($redirect, rawurlencode($json)));
            return;
        }
        if (isset($_SERVER['HTTP_ACCEPT']) &&
            (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)) {
            header('Content-type: application/json');
        } else {
            header('Content-type: text/plain');
        }
        echo $json;
    }
	
	
	function insertToDatabase($table,$field,$filename,$path='')
	{
	
	
		if($path == '')
		{
			$sql = "INSERT INTO ".$table." (".$field.") VALUES('".$filename."')";
		}
		else
		{
		
			$sql = "INSERT INTO ".$table." (".$field.",path) VALUES('".$filename."','".$path."')";
		}
		return $rs = mysql_query($sql);
		
	
	}
	
	function updateDatabase($table,$field,$filename,$id,$path='')
	{
		 $ci = & get_instance();
		 $ci->load->database();   
		 
		//$CI->db->get('database_table');
		if($path == '')
		{
			$sql = "UPDATE ".$table." SET `".$field."`=".$field.";".$filename." WHERE id =?";
		}
		else
		{
			echo $sql = "UPDATE ".$table." SET path= concat(path, ';','".$path."'), `".$field."`= concat(".$field.",';','".$filename ."') WHERE id =?";
		}
		$param = array($id);
		echo $id;
		//return $rs = mysql_query($sql);
		
		$result= $ci->db->query($sql,$param);
		return $result;
	}
	
	
}