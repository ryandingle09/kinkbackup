<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <?= modules::run('cakes/partialsControllerMain/profile') ?>
        <!--<li class="xn-title">Navigation</li>-->
        <li class="<?= $this->uri->rsegment(1) == 'cakes' ? 'active' : null ?>">
            <a href="<?= base_url('dashboard') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li>
        <li class="<?= $this->uri->rsegment(1) == 'returnedItemsController' ? 'active' : null ?>">
            <a href="<?= base_url('up/returned_items') ?>"><span class="fa fa-reply"></span> <span class="xn-text">Returned Items</span></a>                        
        </li>  
        <li class="<?= $this->uri->rsegment(1) == 'mtoLogController' ? 'active' : null ?>">
            <a href="<?= base_url('up/mtoLog') ?>"><span class="fa fa-calendar"></span> <span class="xn-text">MTO Log</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'performanceLogController' ? 'active' : null ?>">
            <a href="<?= base_url('up/performance_log') ?>"><span class="fa fa-trophy"></span> <span class="xn-text">Performance Log</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(1) == 'historyLogController' ? 'active' : null ?>">
            <a href="<?= base_url('up/history_log') ?>"><span class="fa fa-globe"></span> <span class="xn-text">History Log</span></a>                        
        </li>    
    </ul>
    <!-- END X-NAVIGATION -->
</div>