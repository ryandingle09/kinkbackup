<div id="navigation">
<a href="<?= base_url('jobs/external') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(2) == 'external' ? 'active' : null ?>">

    <div class="widget widget-success widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-truck"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">JO Tracker External Sales</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('jobs/internal') ?>">
<div class="col-md2-3  <?= $this->uri->rsegment(2) == 'internal' ? 'active' : null ?>">

    <div class="widget widget-danger widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-bullseye"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">JO Tracker Internal Production</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('jobs/claims') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(2) == 'claims' ? 'active' : null ?>">

    <div class="widget widget-warning widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-calendar"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">MTO Claim at Commissary</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('branches/request') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(2) == 'request' ? 'active' : null ?>">

    <div class="widget widget-info widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-envelope"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Branch Request</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('inventory') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'inventory' ? 'active' : null ?>">

    <div class="widget widget-primary widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-calendar"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Inventory</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
</div>