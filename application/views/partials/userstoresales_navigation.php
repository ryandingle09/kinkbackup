<div id="navigation">
<a href="<?= base_url('jobs/order') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(2) == 'order' ? 'active' : null ?>">

    <div class="widget widget-success widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-truck"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Job Order</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('jobs/tracker') ?>">
<div class="col-md2-3  <?= $this->uri->rsegment(2) == 'tracker' ? 'active' : null ?>">

    <div class="widget widget-danger widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-bullseye"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Job Order Tracker</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('inventory/available_items') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(2) == 'available_items' ? 'active' : null ?>">

    <div class="widget widget-info widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-check"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Available Items</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('inventory') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'inventory' && $this->uri->rsegment(2)=='index' ? 'active' : null ?>">

    <div class="widget widget-primary widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-calendar"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Inventory</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
<a href="<?= base_url('calendar') ?>">
<div class="col-md2-3 <?= $this->uri->rsegment(1) == 'calendar' ? 'active' : null ?>">

    <div class="widget widget-warning widget-item-icon">
        <div class="widget-item-right">
            <span class="fa fa-calendar"></span>
        </div>                             
        <div class="widget-data-left">
            <div class="widget-int num-count"></div>
            <div class="widget-title">Calendar</div>
            <div class="widget-subtitle"></div>
        </div>                                     
    </div>

</div>
</a>
</div>