<div id="navigation">
    <a href="<?= base_url('hr/dashboard/daily_time_record') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'dailyTimeRecordController' ? 'active' : null ?>">
            <div class="widget widget-success widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-clock-o"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Daily Time Record</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
	</a>
    <!--
    <a href="<?= base_url('hr/dashboard/inventory') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'inventoryController' ? 'active' : null ?>">
            <div class="widget widget-primary widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-calendar"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Inventory</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
    	</div>
    </a>
    <a href="<?= base_url('hr/dashboard/branch_request') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'branchRequestController' ? 'active' : null ?>">
            <div class="widget widget-info widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-envelope"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Branch Request</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    -->
    <a href="<?= base_url('hr/dashboard/historylog') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'historyLogController' ? 'active' : null ?>">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-recycle"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">History Log</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
	</a>
</div>
<div class="clearfix"></div>