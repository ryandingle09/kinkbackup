<?
 
$num = cal_days_in_month(CAL_GREGORIAN, $selected_month, $selected_year);
 // echo '<pre>';
 // print_r($mytimesheets);
$tasks[0] ='No Particular Task';
$table = ' <center><h3><span id="mnth">'.date('F Y',mktime(0,0,0,$selected_month,1,$selected_year)).'</span></h3></center>';
$projects[0]='';
$table .='<table>
			<thead>
				<tr>
					<th>Date<i class="icon-calendar"></i></th>
					<th>Clock-In<i class="icon-time"></i></th>
					<th>Clock-Out<i class="icon-time"></i></th>
					<th>Project<i class="icon-folder-close"></i></th>
					<th>Task<i class="icon-tasks"></i></th>
					<th>Accomplishment<i class="icon-pencil"></i></th>
					<th>Comments</th>
					<th>Priority</th>
					<th>Duration</th>
					<th>Status</th>
					
					<th>Edit</th>
				</tr>
			</thead>
			<tbody>';
$total_duration=array();
for($i =1; $i<= $num;$i++){ 
	if($i < 10) $i = '0'.$i;
	
  	if(isset($mytimesheets[$i])){
  		foreach($mytimesheets as $days => $val){
			$j=0;
			if($days === $i){
				foreach($val as $data){
					$out = $data->end_time == '0000-00-00 00:00:00'?'':$data->end_time;
				
					$table .= '<tr>';
					
					if($j==0){
							$table .= '		<td>'.$i.'</td>';
							$j++;
					}else{
							$table .= '<td>&nbsp;</td>';
					}
					if($data->priority == '1'){
						$priority = 'High';
					}elseif($data->priority == '2'){
						$priority = 'Medium';
					}else{
						$priority = 'Low';
					}		
					$table .= '	<td><span id="start_'.$i.'">'.$data->start_time.'</span></td>
											<td><span id="out_'.$i.'">'.$out.'</span></td>
											<td><span id="proj_'.$i.'">'.$projects[$data->project_id].'</span></td>
											<td><span id="task_'.$i.'">'.$tasks[$data->task_id].'</span></td>
											<td><div   id="editable_'.$data->id.'" style="width: 479px; height: auto;">'.$data->work_done.'</div></td>
											<td><span id="comment_'.$i.'">'.$data->comments.'</span></td>
											<td class="'.$priority.'"><span id="priority_'.$i.'" class="'.$priority.'">'.$priority.'</span></td>
											<td>'.$data->duration.'</td>
											<td>'.$taskstatuses[$data->status].'</td>';
						
					if($out == '' || $data->work_done == 'break'){
						$table .= '<td> </td>';
					}else{
					
						$table .= '<td><span id="icon_'.$data->id.'"><a href="#" class="edit_task" id="edit_'.$data->id.'"><i class="icon-edit"></a></span></td>';
					}
											
						$table .= '			</tr>';
					if($data->duration != ''){
						$total_duration[] = $data->duration;
					}				
				}
			}		
		}
	
	 
  
  
  
  
  
  
  	//print_r($mytimesheets[$i]);
 	}else{
 		//echo $i.'<br>';
				$table .= '<tr>
										<td>'.$i.'</td>
										<td><span id="start_'.$i.'">&nbsp;</span> </td>
										<td><span id="out_'.$i.'">&nbsp; </span></td>
										<td><span id="proj_'.$i.'">&nbsp;</span> </td>
										<td><span id="task_'.$i.'">&nbsp; </span></td>
										<td>&nbsp; </td>
										<td>&nbsp; </td>
										<td>&nbsp; </td>
										<td>&nbsp; </td>
								</tr>';
 	}
 }

$hours=0;
$mins =0;
if(!empty($total_duration)){
	
	foreach($total_duration as $td){
		
		$times = explode(':',$td);
		$hours += $times[0];
		$mins += $times[1];
		
	
	}
	if($mins >= 60){
		$hours += intval($mins / 60);
		$mins = $mins % 60;
	}
	
}
	$hours = $hours < 10?'0'.$hours:$hours;
	$mins = $mins < 10?'0'.$mins:$mins;
$table .= '<tr>
										<td></td>
										<td><span ><b>Total</b></span> </td>
										<td><span  >&nbsp; </span></td>
										<td><span  >&nbsp;</span> </td>
										<td><span  >&nbsp; </span></td>
										<td>&nbsp; </td>
										<td><b>'.$hours.':'.$mins.':00</b> </td>
										<td>&nbsp; </td>
								</tr>';
echo $table;
 
/*
$projects[0]='';
$table .='<table><thead><tr><th>Date</th><th>Clock-In</th><th>Clock-Out</th><th>Project</th><th>Task</th><th>Accomplishment</th><th>Duration</th><th>Status</th></tr></thead><tbody>';
for($i =1; $i<= $num;$i++){ //$i is days in a month
	if($i < 10) $i = '0'.$i;
	 if(!empty($mytimesheets)){
		$k=0;
		foreach($mytimesheets as $days => $val){
			
			if(!empty($val)){
			
				if($days === $i){
					
					$j=0;
					
					foreach($val as $data){
					
						$out = $data->end_time == '0000-00-00 00:00:00'?'':$data->end_time;
						$table .= '<tr>';
						if($j==0){
							$table .= '		<td>'.$i.'</td>';
							$j++;
						}else{
							$table .= '<td>&nbsp;</td>';
						}
						$table .= '				<td><span id="start_'.$i.'">'.$data->start_time.'</span></td>
										<td><span id="out_'.$i.'">'.$out.'</span></td>
										<td><span id="proj_'.$i.'">'.$projects[$data->project_id].'</span></td>
										<td><span id="task_'.$i.'">'.$tasks[$data->task_id].'</span></td>
										<td>'.$data->work_done.'</td>
										<td>'.$data->duration.'</td>
										<td>'.$taskstatuses[$data->status].'</td>
								</tr>';
								
					}
					//break;
				}else{
				
						if($k==0){
						
						/* $table .= '<tr>
										<td>'.$i.'s</td>
										<td><span id="start_'.$i.'">&nbsp;</span> </td>
										<td><span id="out_'.$i.'">&nbsp; </span></td>
										<td><span id="proj_'.$i.'">&nbsp;</span> </td>
										<td><span id="task_'.$i.'">&nbsp; </span></td>
										<td>&nbsp; </td>
										<td>&nbsp; </td>
										<td>&nbsp; </td>
								</tr>';
								
								$k++;
								 
								 
						}
				}
			}
		
		}   //end foreach
	
	 }else{
		//means no timesheet record
		$table .= '<tr>
										<td>'.$i.'</td>
										<td><span id="start_'.$i.'">&nbsp;</span> </td>
										<td><span id="out_'.$i.'">&nbsp; </span></td>
										<td><span id="proj_'.$i.'">&nbsp;</span> </td>
										<td><span id="task_'.$i.'">&nbsp; </span></td>
										<td>&nbsp; </td>
										<td>&nbsp; </td>
										<td>&nbsp; </td>
								</tr>';
	 }
	  

}

echo $table;*/