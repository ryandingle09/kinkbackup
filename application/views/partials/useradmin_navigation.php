<div id="navigation">
    <a href="<?= base_url('admin/dashboard/branch') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'branchController' ? 'active' : null ?>">
            <div class="widget widget-success widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-building"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Branch</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    <a href="<?= base_url('admin/dashboard/da') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'deliveryAresController' ? 'active' : null ?>">
            <div class="widget widget-primary widget-item-icon">
                <div class="widget-item-right">
                    <span class="glyphicon glyphicon-road"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Delivery Areas</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    <a href="<?= base_url('admin/dashboard/catc') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'catalogueCakeController' ? 'active' : null ?>">
            <div class="widget widget-info widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-book"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Catalogue Cake</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    <a href="<?= base_url('admin/dashboard/cake_settings') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'cakeSettingsController' ? 'active' : null ?>">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-wrench"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Cake Settings</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    <a href="<?= base_url('admin/dashboard/inventory') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'inventoryController' ? 'active' : null ?>">
            <div class="widget widget-danger widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-database"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Inventory</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
</div>