<div id="navigation">
    <a href="<?= base_url('reports/daily_sales') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(2) == 'daily_sales' ? 'active' : null ?>">
            <div class="widget widget-success widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-clock-o"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Daily Sales Report</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    <a href="<?= base_url('reports/daily_time') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(2) == 'daily_time' ? 'active' : null ?>">
            <div class="widget widget-info widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-clock-o"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Daily Time Record</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    <!--
    <a href="<?= base_url('acc/dashboard/supplier') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'supplierController' ? 'active' : null ?>">
            <div class="widget widget-primary widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-calendar"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Supplier</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    <a href="<?= base_url('acc/dashboard/po') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'purchaseOrderController' ? 'active' : null ?>">
            <div class="widget widget-info widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-shopping-cart"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Purchase Order</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    <a href="<?= base_url('acc/dashboard/historylog') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'historyLogController' ? 'active' : null ?>">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-recycle"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">History Log</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    -->
    <a href="<?= base_url('inventory') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'inventory' && $this->uri->rsegment(2)=='index' ? 'active' : null ?>">
            <div class="widget widget-danger widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-database"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Item Inventory</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>
    <!--
    <a href="<?= base_url('acc/dashboard/performanceLog') ?>">
        <div class="col-md2-3 <?= $this->uri->rsegment(1) == 'performanceLogController' ? 'active' : null ?>">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-right">
                    <span class="fa fa-trophy"></span>
                </div>                             
                <div class="widget-data-left">
                    <div class="widget-int num-count"></div>
                    <div class="widget-title">Performance Log</div>
                    <div class="widget-subtitle"></div>
                </div>                                     
            </div>
        </div>
    </a>-->
</div>