<div class="page-sidebar">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <?= $this->load->view('partials/profile'); ?>
       	<!--<li class="xn-title">Navigation</li>-->
        <li class="<?= $this->uri->rsegment(1) == 'dashboard' ? 'active' : null ?>">
            <a href="<?= base_url('dashboard') ?>"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
        </li>   
        <li class="<?= $this->uri->rsegment(2) == 'request' ? 'active' : null ?>">
            <a href="<?= base_url('branches/request') ?>"><span class="fa fa-envelope"></span> <span class="xn-text">Branch Request</span></a>                        
        </li> 
        <li class="<?= $this->uri->rsegment(2) == 'returned_items' ? 'active' : null ?>">
            <a href="<?= base_url('inventory/returned_items') ?>"><span class="fa fa-reply"></span> <span class="xn-text">Return Items</span></a>                        
        </li>  
        <li class="<?= $this->uri->rsegment(2) == 'performance_log' ? 'active' : null ?>">
            <a href="<?= base_url('users/performance_log') ?>"><span class="fa fa-trophy"></span> <span class="xn-text">Performance Log</span></a>                        
        </li>                
        <li class="<?= $this->uri->rsegment(2) == 'lists' ? 'active' : null ?>">
            <a href="<?= base_url('customers/lists') ?>"><span class="fa fa-users"></span> <span class="xn-text">Customer List</span></a>                        
        </li> 
        
    </ul>
    <!-- END X-NAVIGATION -->
</div>