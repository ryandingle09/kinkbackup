<?php
$menus='';
$ci = & get_instance();

$module 	= $ci->uri->segment(1,0);
$controller = $ci->uri->segment(2);
$action		= $ci->uri->segment(3,0);
if($secondary_menu){
	foreach($secondary_menu as $key=>$smenu){
		$active =($smenu->controller_name==$controller && $controller) ? 'active':'';
		$menus .= '<li class="'.$active.'"><a href="/'.$module.'/'.$smenu->controller_name.'">'.$smenu->display_name.'</a></li><li class="divider"></li>';	
	}
}
echo $menus; 
?>