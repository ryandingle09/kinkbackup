<?php $this->load->view($head_view); ?>

<?php if(isset($nav_view)) $this->load->view($nav_view);?>

<?php $this->load->view($main_view); ?>

<?php if(isset($modal_view)) $this->load->view($modal_view);?>
<?php if(isset($script_view)) $this->load->view($script_view);?>

<?php $this->load->view($foot_view); ?>