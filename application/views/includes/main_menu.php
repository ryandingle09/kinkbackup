<?
$items='';
$ci = & get_instance();

 $module 	= $ci->uri->segment(1,0);
$controller = $ci->uri->segment(2,0);
$action		= $ci->uri->segment(3,0);

 
if($main_menu){

foreach($main_menu as $item)
{
	$active =($item->module_name==$module) ? 'active':'';
	$subactive ='';
	if(isset($sub_menu[$item->modules_id]))
	{
		$items .= ' 
    <li class="'.$active.'">
    	<a class="dropdown-toggle" data-toggle="dropdown" href="#">'.$item->display_name.'<b class="caret"></b></a>
    <div class="dropdown"><ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">';
    	foreach($sub_menu[$item->modules_id] as $key=>$sm){
    		
			$subactive = ($controller === $key)? 'active':'';
			$items .= '<li class="'.$subactive.'" ><a href="/'.strtolower($item->module_name).'/'.$key.'" class="'.strtolower($item->module_name).'">'.$sm.'</a></li>';
		}
		$items .='</ul></div></li>
    ';
	
	}
	else{
	
	$items .= '<li class="'.$active.'"><a href="#" class="'.strtolower($item->module_name).'">'.$item->display_name.'</a></li>';
	}
}
}
echo $items;