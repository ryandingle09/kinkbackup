<?php

class M_modules extends MY_Model {
	function __construct(){
		parent::__construct();	
	}

	public function getActiveModules(){
		try{
			$sql ="	SELECT modules_id, module_name, display_name
					FROM acl_modules
					WHERE acl_modules.status =?
					ORDER BY acl_modules.ordering ASC";
		
			$param=array(1);
				
			$results = $this->select_all($sql,$param,'obj');
				
			if($results){
				return $results;
			}else{
				return 0;
			}
		}catch(Exception $e){
			return 0;
		}
	}
	
	public function getClientActiveModules(){
		try{
			$sql ="	SELECT modules_id, module_name, display_name
					FROM acl_modules
					WHERE acl_modules.display =?
					ORDER BY acl_modules.ordering ASC";
				
			$param=array(1);
			
			$results = $this->select_all($sql,$param,'obj');
			
			if($results){
				return $results;
			}else{
				return 0;
			}
		}catch(Exception $e){
			return 0;
		}
	}
	
	public function getAllModules(){
		$sql ="SELECT * FROM acl_modules";
		$param=array();
		$results = $this->select_all($sql,$param,'obj');
			
		$rs_array= array();
		
		if($results){
			foreach($results as $result){
				$rs_array[$result->modules_id] = $result->module_name;
			}
			
			return $rs_array;
		}
		
		return false;
	}

	public function getModuleId($mod){
		$sql ="SELECT modules_id FROM acl_modules WHERE module_name=?";

		$param = array($mod);

		$result = $this->select($sql,$param,'obj');
		
		if($result){
			return $result->modules_id;
		}else{
			return 0;
		}
	}
	

	public function addModule($POST){
		$time 			= date('Y-m-d G:i:s');
		$userid 		= $this->session->userdata('userid');
		$module_name	= $POST['name'];
		$display_name	= $POST['displayname'];
		$status			= $POST['status'];
		
		$sql ="INSERT INTO acl_modules (module_name, display_name, status, creator, date_created, submitter,date_submitted) 
				VALUES(?,?,?,?,?,?,?) ";
		$param = array($module_name,$display_name,$status,$userid,$time,$userid,$time);
		$status = $this->insert($sql,$param);
	
		return $status;
	}


	public function updateModule($POST){
		$id 	= $POST['mod_id'];
		$name	= $POST['mod_name'];
		$disname= $POST['mod_disname'];
		$status = $POST['mod_status'];
		
		$sql ="UPDATE acl_modules SET module_name=?, display_name=?, status=? WHERE modules_id=?";
		
		$param = array($name,$disname,$status,$id);
		
		$status = $this->query($sql,$param);
		
		return $status;
	}
	
	
	function getActiveSubModules($modules){
		
		foreach($modules as $mod){
			$modid= $mod->modules_id;
			
			$sql = "SELECT * 
					FROM acl_controllers
					LEFT JOIN acl_permissions ON acl_controllers.controllers_id = acl_permissions.controllers_id
					WHERE acl_controllers.modules_id=? 
					AND acl_controllers.status=?
					AND acl_permissions.permissions = 1 ORDER BY acl_controllers.display_name ASC";
			
			$param = array($modid,1);
			
			$results[] = $this->select_all($sql,$param,'obj');
		}
		
		$final =array();
		
		foreach($results as $result){
			if(count($result)> 1){
				foreach($result as $rs){
					$final[$rs->modules_id][$rs->controller_name] = $rs->display_name;
				}
			}
		}
		
		return $final;
	}

	function getActiveStatusCount($module_name){

		$user_id = $this->session->userdata('user_id');

		if($module_name == 'messages'){

			
		 	$sql ="SELECT COUNT(*) cntr FROM messages WHERE to_id=? AND status=?";
		 	$param = array($user_id,0);
		 	$result = $this->select($sql,$param);
		}elseif($module_name == 'quotes'){
			$sql = "SELECT COUNT(*) cntr FROM quotes WHERE status=?";
			$param = array(1);
			$result = $this->select($sql,$param);
		}elseif($module_name == 'jobs'){
			$sql = "SELECT COUNT(*) cntr FROM jobs WHERE status=?";
			$param = array(1);
			$result = $this->select($sql,$param);
		}
		return $result['cntr'];


	}
	
}
