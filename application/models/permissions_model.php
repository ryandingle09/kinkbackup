<?

class Permissions_model extends MY_Model {



	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

			
	}

	public function getPermissionsByGroupID($group_id){
		$sql ="	SELECT 	acl_permissions.permissions_id,
						acl_permissions.permissions,
						acl_modules.modules_id,
					 	acl_modules.module_name,
						acl_modules.display_name AS module_disname,
						acl_controllers.controllers_id,
						acl_controllers.controller_name,
						acl_controllers.display_name AS controller_disname,
						acl_actions.actions_id,
						acl_actions.action_name,
						acl_actions.display_name AS action_disname,
						acl_groups.id AS group_id,
						acl_groups.name as group_name,
						acl_groups.display_name as display_name
				FROM 	acl_permissions, acl_modules, acl_controllers, acl_actions, acl_groups
				WHERE 	acl_permissions.group_id = ?
				AND		acl_permissions.modules_id =  acl_modules.modules_id
				AND		acl_permissions.controllers_id = acl_controllers.controllers_id
				AND		acl_permissions.actions_id = acl_actions.actions_id
				AND		acl_permissions.group_id	= acl_groups.id
				";
		
		$param = array($group_id);
		
		$results = $this->select_all($sql,$param);
		
		return $results;
	}
	
	public function getAllPermissions(){
		$sql ="	SELECT 	acl_permissions.permissions_id,
						acl_modules.modules_id,
					 	acl_modules.module_name,
						acl_modules.display_name AS module_disname,
						acl_controllers.controllers_id,
						acl_controllers.controller_name,
						acl_controllers.display_name AS controller_disname,
						acl_actions.actions_id,
						acl_actions.action_name,
						acl_actions.display_name AS action_disname,
						acl_groups.id AS groups_id,
						acl_groups.name AS group_name,
						acl_groups.display_name AS groups_disname
				FROM 	acl_permissions, acl_modules, acl_controllers, acl_actions, acl_groups
				WHERE 	acl_permissions.modules_id =  acl_modules.modules_id
				AND		acl_permissions.controllers_id = acl_controllers.controllers_id
				AND		acl_permissions.actions_id = acl_actions.actions_id
				AND		acl_permissions.group_id	= acl_groups.id
				";
		
		$results = $this->select_all($sql);
		
		return $results;
	}
	

	public function getGroupPermissionID($POST){
		$time 			= date('Y-m-d G:i:s');
		$userid 		= $this->session->userdata('userid');
		$module_id 		= $POST['module_id'];
		$controller_id 	= $POST['controller_id'];
		$action_id 		= $POST['action_id'];
		$group_id 		= $POST['group_id'];
		
		$sql ="SELECT permissions_id FROM acl_permissions WHERE modules_id =? AND controllers_id=? AND action_id=? AND group_id=?";
		$param = array($module_id, $controller_id, $action_id, $group_id);
		$result = $this->select($sql,$param);
		
		return $result->permissions_id;
	}
	
	public function addGroupPermission($POST){
		$time 			= date('Y-m-d G:i:s');
		$userid 		= $this->session->userdata('userid');
		$module_id 		= $POST['module_id'];
		$controller_id 	= $POST['controller_id'];
		$action_id 		= $POST['action_id'];
		$group_id 		= $POST['group_id'];
		
		$sql ="INSERT INTO acl_permissions (modules_id, controllers_id, actions_id, group_id, permissions, creator, date_created, submitter,date_submitted) 
				VALUES(?,?,?,?,?,?,?,?,?) ";
		$param = array($module_id, $controller_id, $action_id, $group_id, 1 ,$userid, $time, $userid, $time);
		$status = $this->insert($sql,$param);
	
		return $status;
	}
	
	public function deleteGroupPermission($POST){
		$sql ="DELETE FROM acl_permissions WHERE permissions_id=? ";
		$param = array($POST['permission_id']);
		$status = $this->query($sql,$param);
	
		return $status;
	}
	

	public function softDeleteGroupPermission($POST)
	{
		$time 			= date('Y-m-d G:i:s');
		$userid 		= $this->session->userdata('userid');
	
		$sql ="UPDATE acl_permissions SET permissions=?, submitter=?, date_submitted=? WHERE permissions_id=?";
	
		$param = array(0,$userid,$time,$POST['permission_id']);
	
		$status = $this->query($sql,$param);
	
		return $status;
	}
	

	public function updateGroupPermission($POST)
	{
		$time 			= date('Y-m-d G:i:s');
		$userid 		= $this->session->userdata('userid');
	
		$sql ="UPDATE acl_permissions SET permissions=?, submitter=?, date_submitted=? WHERE permissions_id=?";
	
		$param = array(1,$userid,$time,$POST['permission_id']);
	
		$status = $this->query($sql,$param);
	
		return $status;
	}
	
	public function clientSetGroupPermissions($group_id,$permission_arr){
		try{
			$time 			= date('Y-m-d G:i:s');
			$userid 		= $this->session->userdata('userid');
	
			if($permission_arr){
				$sql ="DELETE FROM acl_permissions WHERE group_id=? AND modules_id<>2";
				$param = array($group_id);
				$status = $this->query($sql,$param);
				
				if($status!=0){
					foreach($permission_arr as $acl){
						$sql ="INSERT INTO acl_permissions
							(modules_id, controllers_id, actions_id, group_id, permissions, creator, date_created, submitter,date_submitted)
							VALUES(?,?,?,?,?,?,?,?,?) ";
						$param = array($acl['mod'], $acl['con'], $acl['act'], $group_id, 1 ,$userid, $time, $userid, $time);
						$status = $this->insert($sql,$param);
					}
				}
				return $status;
			}else{
				$sql ="DELETE FROM acl_permissions WHERE group_id=? AND modules_id<>2";
				$param = array($group_id);
				$status = $this->query($sql,$param);
				return $status;
			}
		}catch(Exception $e){
			return 0;
		}
	}
	
	public function checkActionPermission($obj){
		$module = $obj->segment(1,'');
		$con	= $obj->segment(2,'');
		$act	= $obj->segment(3,'index');
		
		if($con==''&&$act=="index"){
			/*fix issue with module and controller have same name*/
			$con = $module;
		}
		$group_id = $this->session->userdata('group_id');
		$sql="SELECT 	acl_permissions.permissions_id,
						acl_permissions.permissions
				FROM 	acl_permissions, acl_modules, acl_controllers, acl_actions, acl_groups
				WHERE 	acl_permissions.group_id = ?
				AND		acl_permissions.modules_id =  acl_modules.modules_id
				AND		acl_permissions.controllers_id = acl_controllers.controllers_id
				AND		acl_permissions.actions_id = acl_actions.actions_id
				AND		acl_permissions.group_id	= acl_groups.id
				AND		acl_modules.module_name=?
				AND		acl_controllers.controller_name=?
				AND		acl_actions.action_name=?";
	
		$param = array($group_id,$module,$con,$act);
	
		$result = $this->select($sql,$param);
	
		if($result){
			$permission= $result->permissions;
			if($permission==0&&strpos($act, 'ajax') === false){
				redirect('/main/index');
			}
		}else{
			if(strpos($act, 'ajax') === false){
				redirect('/main/index');
			}
		}
		return $result;
	}
}