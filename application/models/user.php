<?php

class User extends MY_Model {

	  

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();

			
	}


	function logon($username )
	{


		//$username='ronald.tuibeo@huxxer.com';
		//$password='$2a$08$VmbO.OFwLiizotQS4doYkO15aRsURtHmbzPOO0/nLXlSOksEyz50.';
		echo 	$sql = "SELECT * FROM users WHERE username= ?  ";
		$param = array($username );
		$result =$this->select($sql,$param);

		die;
		return $result;

	}


	public function accessLevel($group_id)
	{


		$sql = "SELECT id,name FROM groups WHERE id=?";
		$param = array($group_id);
		$result = $this->select($sql,$param);

		return $result;



	}

	public function getClients($id)
	{
			
		$sql = "SELECT * FROM users WHERE client_id=?";
		$param = array($id);
		$results = $this->select_all($sql,$param);

		return $results;

	}


	public function getAdminUsers()
	{
			
		$sql = "SELECT * FROM users WHERE group_id !=?";
		$param = array('5');
		$results = $this->select_all($sql,$param);

		return $results;

	}



	public function getAdminUsersIds()
	{
			
		$sql = "SELECT id, concat(first_name,' ',last_name) fullname FROM users WHERE access_level <> ?";
		$param = array(5);
		$results = $this->select_all($sql,$param ,'both' );
			


		$final = array();
		if(is_array($results))
		{
			foreach($results as $result)
			{
				$final[$result['id']]=$result['fullname'];
			}
		}
			
		return $final;

	}



	public function getGroups()
	{
			
		$sql = "SELECT id, group_name FROM lib_groups ";
		$param = array();
		$results = $this->select_all($sql,$param ,'both' );
			


		$final = array();
		if(is_array($results))
		{
			foreach($results as $result)
			{
				$final[$result['id']]=$result['group_name'];
			}
			$final[0] = 'All Groups';
		}
			
		return $final;

	}



	public function getAccessLevel()
	{
			
		$sql = "SELECT id, name FROM acl_groups ";
		$param = array();
		$results = $this->select_all($sql,$param ,'both' );
			


		$final = array();
		if(is_array($results))
		{
			foreach($results as $result)
			{
				$final[$result['id']]=$result['name'];
			}

		}
			
		return $final;

	}

	public function getLibStatus()
	{
			
		$sql = "SELECT id, status_name FROM lib_status ";
		$param = array();
		$results = $this->select_all($sql,$param ,'both' );
			


		$final = array();
		if(is_array($results))
		{
			foreach($results as $result)
			{
				$final[$result['id']]=$result['status_name'];
			}

		}
			
		return $final;

	}


	public function getUser($id)
	{
			
		$sql = "SELECT * FROM users WHERE id=?";
		$param = array($id);
		$results = $this->select($sql,$param);

		return $results;

	}



	public function saveContact($POST)
	{
			
		$firstname = $POST['firstname'];
		$lastname = $POST['lastname'];
		$email = $POST['email'];
		$username = $POST['username'];
		$password = $POST['password'];
		$hashed_password = $POST['hashed_password'];
		$hash = $POST['hash'];
		$client_id = intval($POST['client_id']);
		$current_date_time = date('Y-m-d G:i:s');
		$userid = $this->session->userdata('userid');
		$type = (isset($POST['type']))?$POST['type']:0;
		$group_id ='5';
		$str ='';
		if($type)
		{
			$group_id=$POST['editaccess'];
			$editaccess = $POST['editaccess'];
		}


		$sql = "INSERT INTO users (password,status,email,first_name,last_name,username,real_password,group_id,hash,client_id,creator,date_created,submitter,date_submitted) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$param = array($hashed_password,'1',$email,$firstname,$lastname,$username,$password,$group_id,$hash,$client_id,$userid,$current_date_time,$userid,$current_date_time);

		$results = $this->insert($sql,$param);
		$logs = array('users','insert',$results);
		$log = $this->logs($logs);

		if($type)
		{
				
			if($editaccess=='3')
			{

				foreach($POST['editclient'] as $client)
				{
						
					$str_place = "(?,?,?),";
					$str .= "('$results','$client','$editaccess'),";
				}

			}
			else
			{
				$sql = "SELECT id FROM clients";
				$param=array();
				$clients= $this->select_all($sql,$param);

				foreach($clients as $client)
				{
					$str_place = "(?,?,?),";
					$str .= "('$results','$client','$editaccess'),";
				}
					
					
			}
				
				




			$str_place = substr($str_place,0,-1);
			$str = substr($str,0,-1);

			$sql = "INSERT INTO users_clients (user_id,client_id,access_id)
					VALUES ".$str;
			$param = array( );

			$result = $this->insert($sql,$param);
			$logs = array('users_clients','insert',$result);
			$log = $this->logs($logs);

		}

		return $results;

	}


	public function editUser($POST)
	{
			
			
		$firstname = $POST['first_name'];
		$lastname = $POST['last_name'];
		$email = $POST['email'];
		$username = $POST['username'];
		$group_id = $POST['group_id'];
		$access_level = $POST['access_level'];
		$password = $POST['password'];
		$hashed_password = $POST['hashed_password'];
		$hash = $POST['hash'];
			
		$current_date_time = date('Y-m-d G:i:s');
		$userid = $this->session->userdata('userid');
		$id = intval($POST['uid']);
			
		//get old data
		$sql1 = "SELECT email,first_name,last_name,username,group_id,lib_groups_id,submitter,date_submitted FROM users WHERE id=?";
		$param1=array($id);
		$rs1 = $this->select($sql1,$param1,'assoc');


		if($password == ''){
			$sql1 = "UPDATE users SET    `email`=?, `first_name`=?, `last_name`=?, `username`=?,  `group_id`=?,`lib_groups_id`=?, `submitter`=?, `date_submitted`=?		WHERE id=?";
			$param1 = array( $email,$firstname,$lastname,$username, $access_level,$group_id,$userid,$current_date_time,$id);
			//print_r($param1);

		}else{

			$sql1 = "UPDATE users SET  `password`=?, `email`=?, `first_name`=?, `last_name`=?, `username`=?,   `hash`=?, `group_id`=?, ,`lib_groups_id`=?,`submitter`=?, `date_submitted`=?		WHERE id=?";
			$param1 = array($hashed_password,$email,$firstname,$lastname,$username, $hash,$access_level,$group_id,$userid,$current_date_time,$id);

		}

		$results = $this->query($sql1,$param1);
		if($results){
			$arr = array('users','update',$POST['uid'],$sql1,implode(',',$param1),implode(',',$rs1));
			//$log = $this->Logs_model->datalog('users','update',$POST['uid'],$sql1,implode(',',$param1),implode(',',$rs1));
			$this->logs($arr);
		}
			

		return $results;

	}



	public function getUserClientAccess()
	{

		$sql = "SELECT * FROM access_type WHERE status=?";
		$param = array(1);
		$results = $this->select_all($sql,$param);

		return $results;

	}




	public function delete($post)
	{
		$id = intval($post['id']);
		$sql = "DELETE FROM users WHERE id =?";
		$param=array($id);

		//get old data
		$sql1 = "SELECT * FROM users WHERE id=?";
		$param1=array($id);
		$rs1 = $this->select($sql1,$param1,'assoc');
	 

		$result = $this->query($sql,$param);

		if($result ){
			$arr = array('users','delete',$id,$sql, implode(',',$param) ,implode(',',$rs1));
			//$log = $this->Logs_model->datalog('users','delete',$id,$sql,implode(',',$param),implode(',',$rs1));
			$this->logs($arr);
		}
		return $result;

	}


	public function getUserClients($id)
	{
		$sql = "SELECT client_id FROM users_clients WHERE user_id =?";
		$param=array($id);
			
		$result = $this->select_all($sql,$param,'both');
		return $result;

	}


	public function getCompanyDetailsForInvoice($group)
	{
		$sql = "SELECT * FROM system_company_setup WHERE upper(`prefix`)=upper('$group')";
			
		$param= array();
			
		$result =   $this->select($sql,$param);
		return $result;
			
	}



	public function getAllUsers($access=0,$ids='')
	{

		$where ='';
		$param = array();
		if($access != 0){
			$where = " WHERE access_level=?";
			$param = array($access);	
		}
		
		 
		$sql = "SELECT id,email,concat(first_name,' ',last_name) fullname, first_name,last_name,username,access_level, status,submitter,date_submitted FROM users  " .$where;
		$results = $this->select_all($sql,$param);	
		
		if($ids == ''){
		
			return $results;
			
		}else{
			$final = array();
			if(is_array($results))
			{
				foreach($results as $result)
				{
					$final[$result->id]=$result->fullname ;
				}
	
			}
				
			return $final;
		
		}



	}


	public function saveUser($POST)
	{
			
		$firstname = $POST['first_name'];
		$lastname = $POST['last_name'];
		$email = $POST['email'];
		$username = $POST['username'];
		$group_id = $POST['group_id'];
		$access = $POST['access_level'];
		$password = $POST['password'];
		$hashed_password = $POST['hashed_password'];
		$hash = $POST['hash'];
			
		$current_date_time = date('Y-m-d G:i:s');
		$userid = $this->session->userdata('userid');
			
		$str ='';
			


		$sql = "INSERT INTO users (password,status,email,first_name,last_name,username,group_id,hash,lib_groups_id,creator,date_created,submitter,date_submitted) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$param = array($hashed_password,'1',$email,$firstname,$lastname,$username, $access,$hash,$group_id,$userid, $current_date_time,$userid,$current_date_time);

		$results = $this->insert($sql,$param);

			
		if($results){
			$arr = array('users','insert',$results,$sql,implode(',',$param));
			//$log = $this->Logs_model->datalog('users','insert',$results,$sql,implode(',',$param));
			$this->logs($arr);
		}
			

			

		return $results;

	}

	public function getUserNameList($group=''){
		try{
			$sql = "SELECT id,CONCAT(first_name,' ',last_name) as full_name FROM users WHERE status= 1";
			$param = array();
			$user_list = $this->select_all($sql,$param,'assoc');
			
			if($user_list){
				return $user_list;
			}else{
				return false;
			}
		}catch(Exception $e){
			return false;
		}
		
	}

		
	public function create($post)
	{
			
		$rs = insert_format($post,'users',1);
		  $sql = $rs['sql'];
		$param = $rs['param'];
		 
		 

		$results = $this->insert($sql,$param);

			
		if($results){
			$arr = array('users','insert',$results,$sql,implode(',',$param));
			 
			$this->logs($arr);
		}
		 
		return $results;

	}
	
	 function update($post){
	 
		$id = $post['uid'];
		$password = $post['password'];
		if($password !=''){
			$rs = update_format($post,'users',$id,1,1);
		}else{
			$rs = update_format($post,'users',$id,1 );
		}
		$sql = $rs['sql'];
		$param = $rs['param'];
		
		return $this->query($sql,$param); 
		
		 
	 
	 }



}
