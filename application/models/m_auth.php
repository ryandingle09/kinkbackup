<?php

class M_auth extends MY_Model {
	
	function changePassword($user_id,$new_password){
		try{
			$sql ="UPDATE users SET password=? WHERE user_id=?";
			$param  = array($new_password,$user_id);
			$status = $this->query($sql,$param);
			
			if($status){
				return true;
			}else{
				return false;
			}
		}catch(Exception $e){
			return false;
		}
	}
	
	function logon($username){
		try{
		 	$sql = "SELECT * FROM users WHERE username=?";
			$param = array($username);
			
			$result = $this->select($sql,$param);

			if($result){
				return $result;
			}else{
				return false;
			}
		}catch(Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			return false;
		}
	}
	
	function user_group($user_id){
		try{
			$sql = "SELECT users.group_id,user_group.group_name,user_group.default_home, user_group.page,user_group.class
		 			FROM users
		 			INNER JOIN user_group ON user_group.group_id = users.group_id
		 			WHERE users.user_id=?
		 			AND user_group.status<>0";
			$param = array($user_id);
	
			$result = $this->select($sql,$param);
			if($result){
				return $result;
			}else{
				return false;
			}
		}catch(Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			return false;
		}
	}
	
	function user_department($user_id){
		try{
			$sql = "SELECT departments.id, departments.department_name
		 			FROM  departments_users
		 			LEFT JOIN departments ON departments.id = departments_users.department_id
		 			WHERE departments_users.user_id=?
		 			AND departments.status= 1";
			$param = array($user_id);
	
			$result = $this->select($sql,$param);
			if($result){
				return $result['id'];
			}else{
				return false;
			}
		}catch(Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			return false;
		}
	}
	
	function user_departments($user_id){
		try{
			$sql = "SELECT d.id,d.department_name
		 			FROM  departments_users ds
		 			LEFT JOIN departments d ON d.id = ds.department_id 
		 			WHERE ds.user_id=?
		 			AND d.status=1";

			$param = array($user_id);
		
			$result = $this->select_all($sql,$param);
			if($result){
				return $result;
			}else{
				return false;
			}
		}catch(Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			return false;
		}
	}

	function user_branches($user_id){
		try{
			$sql = "SELECT b.branch_name
		 			FROM  branches b
		 			LEFT JOIN users u ON b.branch_id = u.branch_id 
		 			WHERE u.user_id=?
		 			AND u.status=?";

			$param = array($user_id,1);
		
			$result = $this->select_all($sql,$param);
			if($result){
				return $result;
			}else{
				return false;
			}
		}catch(Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			return false;
		}
	}
	
	public function logIPAddress($ip,$username,$password,$type){
		$current_time = date('Y-m-d G:i:s');
		$sql = "INSERT INTO ip_logs (ip_address,log_time,username,password,type) VALUES(?,?,?,?,?)";
		
		$param = array($ip,$current_time,$username,$password,$type);
		
		$result = $this->insert($sql,$param);
		
		return $result;
	}
	
	function unique_username($username,$user_id){
		try{
			$sql="SELECT count(*) as count FROM users WHERE upper(username)=upper(?) AND user_id <>?";
			$param = array($username,$user_id);
			$result = $this->select($sql,$param);
			if($result){
				if($result['count']==0){
					return true;
				}else{
					return false;
				}
			}
			return false;
		}catch(Exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			return false;
		}
	}

	public function allowedModules($group_id)
	{
		$sql = "SELECT modules.module_name FROM modules INNER JOIN acl_permissions ON modules.module_id = acl_permissions.modules_id
				WHERE acl_permissions.group_id=?";
		$param = array($group_id);

		$result =  $this->select_all($sql,$param );
		$final = array();
		if(!empty($result)){
			foreach ($result as  $value) {
				$final[] = $value['module_name'];
			}
		}
		return $final;
	}
}
