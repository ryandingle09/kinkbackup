var JO = {
    //configuration vars
    base_url	: window.location.protocol+"//"+window.location.host+'/kinkcake/',
    doc      	: $(document),
    //element vars
    add_item            : '.add-item',
    update_item         : '.update-item',
    get_item            : '.get-item',
    get_item_dropdown   : '.get-item-dropdown',
    show_item           : '.show-item',
    delete_item         : '.delete-item',
    selected            : '.selected-item',
    minus_count         : '.minus_count',
    save_print          : '.save-print',
    print_res           : '.print-res',
    live_change         : '.live-change',
    action_select       : '.action_select',
    open_modal          : '.open-modal',
    close_modal         : '.close-modal',
    showRes             : '.showRes',
    get_click           : '.get_click'
};
//computation on cutumized
function custom_computation2()
{
    //final computation
    var delivery_charge     =   parseInt($('.delivery_charge').val());
    var cake_quantity       =   parseInt($('.cake_quantity').val());
    var discount_value      =   parseInt($('.discount_value').val());
    var total_amount        =   parseInt($('.total_amount').val());
    var priority_charge     =   parseInt($('.priority_charge').val());
    var total_price_amount  =   parseInt($('.total_price_amount').val());
    var difficulty          =   parseInt($('.difficulty').val());

    if(isNaN($('.delivery_charge').val()) || $('.delivery_charge').val() == '') delivery_charge = 0;
    if(isNaN($('.discount_value').val()) || $('.discount_value').val() == '') discount_value = 0;
    if(isNaN($('.priority_charge').val()) || $('.priority_charge').val() == '') priority_charge = 0;
    if(isNaN($('.difficulty').val()) || $('.difficulty').val() == '') difficulty = 0;
    var sub_total = total_amount + priority_charge + delivery_charge + difficulty;
    var discount = (discount_value * sub_total / 100);
    var sum = Math.round(sub_total - discount);

    $('.show_total').html(currencyFormat(parseFloat(sum)));
    $('.total_price_amount').val(sum);
    $('.order_total_amount').html(currencyFormat(parseFloat(sum)));
    $('.sub_total').val(parseFloat(sub_total));
    $('.sub_total_amount').html(currencyFormat(parseFloat(sub_total)));
    $('.d_mount').html(currencyFormat(parseFloat(discount)));

    var a = Math.round(parseInt($('.total_price_amount').val())) - Math.round(parseInt($('.amount_to_paid').val()));
    var b = Math.round(parseInt($('.amount_recieved').val())) - Math.round(parseInt($('.amount_to_be_paid').val()));
    var amount_to_paid = parseInt($('.amount_to_paid').val());
    var total_amount = Math.round(parseInt($('.total_amount').val()));
    $('.amount_change').val(currencyFormat(parseFloat(Math.abs(b))));
    if(isNaN(amount_to_paid) || amount_to_paid == ''){
        $('.show_change').html(currencyFormat(parseFloat(0)));
        $('.show_balance').html(currencyFormat(parseFloat(0)));
        $('.amount_paid').val(0.00);
        $('.amount_change').val(0.00);
        $('.amount_balance').val(0.00);
    }
    else if(a >= 0)
    {
        $('.show_change').html(currencyFormat(parseFloat(0)));
        $('.show_balance').html(currencyFormat(parseFloat(a)));
        $('.amount_paid').val(amount_to_paid);
        $('.amount_change').val(0.00);
        $('.amount_balance').val(a);
    }
    else
    { 
        $('.show_change').html(currencyFormat(parseFloat(Math.abs(b))));
        $('.show_balance').html(currencyFormat(parseFloat(0)));
        $('.amount_paid').val(amount_to_paid);
        $('.amount_change').val(parseFloat(Math.abs(b)));
        $('.amount_balance').val(0.00);
    } 
}
function custom_computation(a)
{   var b = a;
    //cus
    var cake_order_set_price = parseInt($('.cake_order_set_price').val());
    var cake_item_price = parseInt($('.cake_item_price').val());
    var cake_design_price = parseInt($('.cake_design_price').val());
    var cake_size_price = parseInt($('.cake_size_price').val());
    var cake_flavor_price = parseInt($('.cake_flavor_price').val());
    var total_addons_price = parseInt($('.total_addons_price').val());
    var total_upgrade_price = parseInt($('.total_upgrade_price').val());
    var quantity = parseInt($('.cake_quantity').val());
    var cospt = cake_order_set_price * quantity;
    
    if(isNaN($('.cake_order_set_price').val()) || $('.cake_order_set_price').val() == '') cake_order_set_price = 0;
    if(isNaN($('.cake_item_price').val()) || $('.cake_item_price').val() == '') cake_item_price = 0;
    if(isNaN($('.cake_design_price').val()) || $('.cake_design_price').val() == '') cake_design_price = 0;
    if(isNaN($('.cake_size_price').val()) || $('.cake_size_price').val() == '') cake_size_price = 0;
    if(isNaN($('.cake_flavor_price').val()) || $('.cake_flavor_price').val() == '') cake_flavor_price = 0;
    if(isNaN($('.total_addons_price').val()) || $('.total_addons_price').val() == '') total_addons_price = 0;
    if(isNaN($('.total_upgrade_price').val()) || $('.total_upgrade_price').val() == '') total_upgrade_price = 0;
    if(isNaN($('.cake_quantity').val()) || $('.cake_quantity').val() == '') quantity = 1;
    //end
    
    //cat
    var product_price = parseInt($('.product_price').val());
    var icing_price = parseInt($('.icing_price').val());
    var flavor_price = parseInt($('.flavor_price').val());
    var size_price = parseInt($('.size_price').val());
    
    if(isNaN($('.product_price').val()) || $('.product_price').val() == '') product_price = 0;
    if(isNaN($('.icing_price').val()) || $('.icing_price').val() == '') icing_price = 0;
    if(isNaN($('.flavor_price').val()) || $('.flavor_price').val() == '') flavor_price = 0;
    if(isNaN($('.size_price').val()) || $('.size_price').val() == '') size_price = 0;
    //end
    
    var total_amount_all_cus = cospt + cake_item_price + cake_design_price + cake_size_price + total_addons_price + cake_flavor_price + total_upgrade_price;
    var total_amount_all_cat = product_price + icing_price + flavor_price + size_price;
    if(b == 'cus')
    {
        $('.total_amount').val(total_amount_all_cus);
        $('.s_amount').html(parseFloat(total_amount_all_cus));
    }
    if(b == 'cat')
    {
        $('.total_amount').val(total_amount_all_cat);
        $('.s_amount').html(parseFloat(total_amount_all_cat));
    }
    //final computation
    var delivery_charge     =   parseInt($('.delivery_charge').val());
    var cake_quantity       =   parseInt($('.cake_quantity').val());
    var discount_value      =   parseInt($('.discount_value').val());
    var total_amount        =   parseInt($('.total_amount').val());
    var priority_charge     =   parseInt($('.priority_charge').val());
    var total_price_amount  =   parseInt($('.total_price_amount').val());
    var difficulty          =   parseInt($('.difficulty').val());

    if(isNaN($('.delivery_charge').val()) || $('.delivery_charge').val() == '') delivery_charge = 0;
    if(isNaN($('.discount_value').val()) || $('.discount_value').val() == '') discount_value = 0;
    if(isNaN($('.priority_charge').val()) || $('.priority_charge').val() == '') priority_charge = 0;
    if(isNaN($('.difficulty').val()) || $('.difficulty').val() == '') difficulty = 0;
    var sub_total = total_amount + priority_charge + delivery_charge + difficulty;
    var discount = (discount_value * sub_total / 100);
    var sum = Math.round(sub_total - discount);

    $('.show_total').html(currencyFormat(parseFloat(sum)));
    $('.total_price_amount').val(sum);
    $('.order_total_amount').html(currencyFormat(parseFloat(sum)));
    $('.sub_total').val(parseFloat(sub_total));
    $('.sub_total_amount').html(currencyFormat(parseFloat(sub_total)));
    $('.d_mount').html(currencyFormat(parseFloat(discount)));

    var a = Math.round(parseInt($('.total_price_amount').val())) - Math.round(parseInt($('.amount_to_paid').val()));
    var b = Math.round(parseInt($('.amount_recieved').val())) - Math.round(parseInt($('.amount_to_be_paid').val()));
    var amount_to_paid = parseInt($('.amount_to_paid').val());
    var total_amount = Math.round(parseInt($('.total_amount').val()));
    $('.amount_change').val(currencyFormat(parseFloat(Math.abs(b))));
    if(isNaN(amount_to_paid) || amount_to_paid == ''){
        $('.show_change').html(currencyFormat(parseFloat(0)));
        $('.show_balance').html(currencyFormat(parseFloat(0)));
        $('.amount_paid').val(0.00);
        $('.amount_change').val(0.00);
        $('.amount_balance').val(0.00);
    }
    else if(a >= 0)
    {
        $('.show_change').html(currencyFormat(parseFloat(0)));
        $('.show_balance').html(currencyFormat(parseFloat(a)));
        $('.amount_paid').val(amount_to_paid);
        $('.amount_change').val(0.00);
        $('.amount_balance').val(a);
    }
    else
    { 
        $('.show_change').html(currencyFormat(parseFloat(Math.abs(b))));
        $('.show_balance').html(currencyFormat(parseFloat(0)));
        $('.amount_paid').val(amount_to_paid);
        $('.amount_change').val(parseFloat(Math.abs(b)));
        $('.amount_balance').val(0.00);
    } 
}

// Calculate the difference of two dates in total days for priority order function
function diffDays(d1, d2)
{
    var ndays;
    var tv1 = d1.valueOf(); // msec since 1970
    var tv2 = d2.valueOf();

    ndays = (tv2 - tv1) / 1000 / 86400;
    ndays = Math.round(ndays - 0.5);
    return ndays;
}

// Start Smart Wizard
var uiSmartWizard2 = function(){

    if($(".wizard1").length > 0){
        //Check count of steps in each wizard
        $(".wizard > ul").each(function(){
            $(this).addClass("steps_"+$(this).children("li").length);
        });//end
        // This par of code used for example
        if($("#wizard-validation").length > 0){
            jQuery.validator.setDefaults({
                debug: true,
                success: "valid"
            });
            var validator = $("#wizard-validation").validate({
                rules: 
                {
                    firstname: {required: true},
                    lastname: {required: true},
                    contact: { required: true},
                    email: {required: true,email: true},
                    address: {required: true},
                    location: {required: true},
                    occasion: {required:true},
                    celebrant: {required:true},
                    occasion_date: {required: true},
                    'addons[]': {required: true,minlength: 1},
                    item: {required: true},
                    category: {required: true},
                    title: {required: true},
                    order_set: {required: true},
                    design: {required: true},
                    size: {required: true},
                    flavor: {required: true},
                    cake_message: {required: true,minlength: 4},
                    cake_upgrade: {required: true},
                    difficulty: {required: true},
                    specification : {required: true},
                    amount_to_be_paid : {required: true},
                    amount_recieved : {required: true},
                    payment_type : {required: true},
                    payment_mode : {required: true},
                    
                    claim_type : {required: true},
                    //delivery
                    delivery_date : {required: true},
                    delivery_time : {required: true},
                    delivery_time_stat : {required: true},
                    delivery_complete_address : {required: true,minlength: 5},
                    delivery_location : {required: true},
                    delivery_contact_no : {required: true},
                    delivery_contact_person : {required: true},
                    //pickup
                    pickup_date : {required: true},
                    pickup_time : {required: true},
                    pickup_time_stat : {required: true},
                    pickup_branch : {required: true},
                },
                messages:
                {
                    difficulty : "Put zero ( 0 ) if no difficulty specified.",
                    'addons[]' : "You must select atleast 1 Addons and specify the quantity",
                    specification : "Type 'None' if no specification needed!",
                    amount_recieved : "Put 0 if Card or Exact Amount Paid by Customer",
                    cake_message : "Type 'None' if no cake message or if item is not a cake.!",
                }
            });
            

        }// End of example

        $(".wizard1").smartWizard({                        
            // This part of code can be removed FROM
            onLeaveStep: function(obj){
                var wizard = obj.parents(".wizard1");
                if(wizard.hasClass("wizard-validation")){
                    var valid = true;
                    $('input,textarea,select',$(obj.attr("href"))).each(function(i,v){
                        valid = validator.element(v) && valid;
                    });
                    if(!valid){
                        wizard.find(".stepContainer").removeAttr("style");
                        validator.focusInvalid();                                
                        return false;
                    }
                }    
                return true;
            },// <-- TO

            //This is important part of wizard init
            onShowStep: function(obj){                        
                var wizard = obj.parents(".wizard1");
                if(wizard.hasClass("show-submit")){
                    var step_num = obj.attr('rel');
                    var step_max = obj.parents(".anchor").find("li").length;
                    if(step_num == step_max){                             
                        obj.parents(".wizard1").find(".actionBar .btn-primary").css("display","block");
                    }                         
                }
                return true;                         
            }//End
        });
    }            

}// End Smart Wizard
uiSmartWizard2();

