;(function(){
    /*var test = {
        testme : function(){
            return this.delegate('body','click',function(event){
                    //alert(Cus.base_url);
            });	
        },
    }*/
    var getCustomerConf = {
        CustomerConf : function(){
            return this.delegate(Cus.get_item,'click',function(){
                var id = $(this).attr('data-id');
                var email = $(this).attr('data-email');
                var contact = $(this).attr('data-contact');
                var fname = $(this).attr('data-fullname');
                $('.c_email').html(email);
                $('.c_contact').html(contact);
                $('.c_fname').html(fname);
                $('.c_id').val(id);
                
                $("#get_customer_data").dataTable({
                    "bDestroy" : true,
                    "bProcessing": false,
                    "bServerSide": false,
                    "deferRender": true,
                    "order": [[ 0, "desc" ]],
                    "oScroller": {
                        "loadingIndicator": true
                    },
                    "sAjaxSource": Cus.base_url+'customers/get_customer_data/'+id
                });
            });	
        }
    };
    
    //configure target variable
    //$.extend(Cus.doc,test);
    $.extend(Cus.doc,getCustomerConf);
    //target
    Cus.doc.CustomerConf();
    //Cus.doc.testme();
}(jQuery,window,document));