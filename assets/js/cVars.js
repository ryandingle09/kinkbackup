var Cus = {
    //configuration vars
    base_url	: window.location.protocol+"//"+window.location.host+'/kinkcake/',
    doc      	: $(document),
    //element vars
    get_item : '.get_item'
};

/****** EXTENDED FUNCTIONS ******/
//money format function
function currencyFormat (num) {
    return "" + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}
$("#customers").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": Cus.base_url+'customers/get_customers'
});
