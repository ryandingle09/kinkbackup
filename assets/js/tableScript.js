//vars
var JO = {
    base_url	: window.location.protocol+"//"+window.location.host+'/kinkcake/',
}


//mars dashboard user tables
$("#mto_cakes").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    dom: 'Rlfrtip',
    colReorder: {
        realtime: true
    },
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'dashboard/get/mto_cakes'
});

$("#decorating").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    dom: 'Rlfrtip',
    colReorder: {
        realtime: true
    },
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'dashboard/get/decorating'
});

$("#finishing").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    dom: 'Rlfrtip',
    colReorder: {
        realtime: true
    },
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'dashboard/get/finishing/'
});

$("#done").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    dom: 'Rlfrtip',
    colReorder: {
        realtime: true
    },
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'dashboard/get/done/'
});
//end