////// EXTENDED FUNCTIONS ////////
//money format function
function currencyFormat (num) {
    return "" + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

$("#customer_select").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    dom: 'Rlfrtip',
    colReorder: {
        realtime: true
    },
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/existing-user'
});
//****JO TRACKER COOKIES
$("#trackercookies").dataTable({//mto cookies
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/trackercookies/cookie'
}); 
///end
//JO TRACKER ETC
$("#StoreSalesjobrequestOnTracker").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/StoreSalesjobrequestOnTracker'
});

$("#StoreSalesonProgessOnTracker").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/StoreSalesonProgessOnTracker'
});

$("#StoreSalesonDoneTracker").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/StoreSalesonDoneTracker'
});

$("#get_fondant_mto_cakes").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/get_fondant_mto_cakes'
});

$("#get_fondant_mto_cakes").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/get_fondant_mto_cakes'
});

$("#get_fondant_mto_decorating").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/get_fondant_mto_decorating'
});

$("#get_fondant_mto_finishing").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/get_fondant_mto_finishing'
});

$("#get_fondant_mto_done").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/get_fondant_mto_done'
});

////
$("#MTO_cakes_uMarsh").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/MTO_cakes_uMarsh'
});

$("#deco_cakes_uMarsh").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/deco_cakes_uMarsh'
});

$("#finishing_cakes_uMarsh").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/finishing_cakes_uMarsh'
});

$("#done_cakes_uMarsh").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/done_cakes_uMarsh'
});
///


//end
$("#mtoClaimatCommisary").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
   
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/mtoClaimatCommisary'
});

$("#jobExternalProductionJobRequest").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/jobExternalProductionJobRequest'
});

$("#jobExternalProductionOnProgress").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/jobExternalProductionOnProgress'
});

$("#mtodispatched").dataTable({
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/mtodispatched'
});

//****internal
//----baking
$("#internalbakingmtocakes").dataTable({//mto cakes
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internalbakingmto/cake/baking'
});

$("#internalbakingmtocookies").dataTable({//mto cookies
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internalbakingmto/cookie/baking'
});
/*
$("#internalbakingavailablecakes").dataTable({///avail cakes
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internalbakingavailable/cake/baking'
});
$("#internalbakingavailablecookies").dataTable({//avail cookies
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internalbakingavailable/cookie/baking'
});
*/
//------------decorating
$("#internaldecomtocakes").dataTable({//mto cakes
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internaldecomto/cake/decorating'
});

$("#internaldecomtocookies").dataTable({//mto cookies
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internaldecomto/cookie/decorating'
});
/*
$("#internaldecoavailablecakes").dataTable({///avail cakes
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internaldecoavailable/cake/decorating'
});
$("#internaldecoavailablecookies").dataTable({//avail cookies
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internaldecoavailable/cookie/decorating'
});
*/
//---- finishing
$("#internalfinishingmtocakes").dataTable({//mto cakes
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internalfinishingmto/cake/finishing'
});

$("#internalfinishingmtocookies").dataTable({//mto cookies
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internalfinishingmto/cookie/finishing'
});

//----- done
$("#internaldonemtocakes").dataTable({//mto cakes
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internaldonemto/cake/done'
});

$("#internaldonemtocookies").dataTable({//mto cookies
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internaldonemto/cookie/done'
});
/*
$("#internaldoneavailablecakes").dataTable({//mto cakes
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internaldonemto/cake/done'
});

$("#internaldoneavailablecookies").dataTable({//mto cookies
    "bDestroy" : true,
    "bProcessing": false,
    "bServerSide": false,
    "deferRender": true,
    "bSort": false,
    //"order": [[ 0, "desc" ]],
    "oScroller": {
        "loadingIndicator": true
    },
    "sAjaxSource": JO.base_url+'jobs/ajax_function/get/internaldonemto/cookie/done'
}); 
 */

