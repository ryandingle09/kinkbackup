;(function(){
    /*var test = {
        testme : function(){
            return this.delegate('body','click',function(event){
                    //alert(JO.base_url);
            });	
        },
    }*/

    var addItemConf = {
        addItem : function()
        {
            return this.delegate(JO.add_item,'submit',function(event){
                event.preventDefault();
                var identity = $(this).attr('data-identity');
                var form = $(this)[0];
                var formData = new FormData(this);
                var redirect = $(this).attr('data-url');
                if($(this).attr('data-identity') == 'customized')
                {
                    $.ajax({
                        url 	: JO.base_url+'jobs/ajax_function/insert',
                        type 	: "POST",
                        cache 	: "false",
                        data 	: formData,
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        beforeSend : function(){
                            $('#message').html('<div style="width: 100%;height: 100%" class="panel-refresh-layer"><img src="img/loaders/default.gif"/></div>');
                        },
                        success : function(data){
                            if(data['status'] == 1){
                                var o_id = data['order_id'];
                                var total_amount = 0; 
                                var subtotal = 0;
                                var amount = 0;
                                var flavor_total = 0;
                                var size_total = 0;
                                var design_total = 0;
                                var cake_design = 0;
                                var cake_type = 0;
                                var add_ons = '';
                                var summary = '';
                                var summary2 = '';
                                var addons = '';
                                var cu_upgrade;
                                var charges = '';
                                var charges_total = 0;
                                var addons_total_q = 0;
                                var addons_total_all = 0;
                                var sub_list = '';
                                $('#modal4').modal('show');
                                $('.showRes').attr('data-id',+o_id);
                                $.getJSON(JO.base_url+'jobs/ajax_function/get/rec/'+o_id+'/get_addons',function(data){
                                    $.each(data, function(key, val){
                                        var a = parseInt(val.a_price);
                                        var b = parseInt(val.quantity);
                                        var ab = a*b;
                                        addons_total_all += ab;
                                        addons += '<tr>';
                                        addons += '<th>'+val.a_name+'</th>';
                                        addons += '<th>'+val.quantity+'</th>';
                                        addons += '<th>'+ab+'</th>';
                                        addons += '</tr>';
                                        $('.addons').html(addons);
                                    });
                                    
                                    $.getJSON(JO.base_url+'jobs/ajax_function/get/rec/'+o_id+'/order_summary2',function(datas){
                                        $.each(datas, function(key, val){

                                            total_amount = flavor_total + size_total + cake_design + cake_type;
                                            $('.total_amount').val(total_amount);

                                            //recipet datas here
                                            //for top summary
                                            summary += '<tr>';
                                            summary += '<th colspan="2">JO#: '+val.JO_no+'</th>';
                                            summary += '<th>'+val.total_product_price+'</th>';
                                            summary += '</tr>';
                                            $('.order_summarys').html(summary);
                                            //for cake product summary
                                            summary2 += '<tr>';
                                            summary2 += '<th>Item</th>';
                                            summary2 += '<th colspan="2">'+val.icing_type+'</th>';
                                            summary2 += '</tr>';
                                            summary2 += '<tr>';
                                            summary2 += '<th>Title :</th>';
                                            summary2 += '<th colspan="2">'+val.title+'</th>';
                                            summary2 += '</tr>';
                                            if(val.order_set != '' )
                                            {   
                                                summary2 += '<tr>';
                                                summary2 += '<th>Order Set</th>';
                                                summary2 += '<th>'+val.order_set+'</th>';
                                                summary2 += '<th></th>';
                                                summary2 += '</tr>';
                                            }
                                            if(val.design != '' )
                                            {
                                                summary2 += '<tr>';
                                                summary2 += '<th>Design :</th>';
                                                summary2 += '<th>'+val.design+'</th>';
                                                summary2 += '<th></th>';
                                                summary2 += '</tr>';
                                            }
                                            if(val.size != '' )
                                            {
                                                summary2 += '<tr>';
                                                summary2 += '<th>Size :</th>';
                                                summary2 += '<th>'+val.size+'</th>';
                                                summary2 += '<th></th>';
                                                summary2 += '</tr>';
                                            }
                                            if(val.flavor != '' )
                                            {
                                                summary2 += '<tr>';
                                                summary2 += '<th>Flavor :</th>';
                                                summary2 += '<th>'+val.flavor+'</th>';
                                                summary2 += '<th></th>';
                                                summary2 += '</tr>';
                                            }
                                            $('.order_summarys2').html(summary2);

                                            //for cake upgrade
                                            if(val.cake_upgrade != 0 )
                                            {
                                                cu_upgrade += '<tr>';
                                                cu_upgrade += '<th>Cake Upgrade</th>';
                                                cu_upgrade += '<th>'+val.cake_upgrade+'</th>';
                                                cu_upgrade += '<th></th>';
                                                cu_upgrade += '</tr>';
                                                $('.cake_upgrade').html(cu_upgrade);
                                            }
                                            //for charges
                                            //delivery
                                            if(val.delivery_charge != 0)
                                            {
                                                charges += '<tr>';
                                                charges += '<th colspan="2">Delivery Charge</th>';
                                                charges += '<th>'+val.delivery_charge+'</th>';
                                                charges += '</tr>';
                                                $('.charges').html(charges);
                                            }

                                            sub_list += '<tr>';
                                            sub_list += '<th colspan="2">Sub Total</th>';
                                            sub_list += '<th>'+val.sub_total+'</th>';
                                            sub_list += '</tr>';
                                            $('.subtotals').html(sub_list);

                                            //for discounts
                                            var dis = (parseInt(val.discount_rate) * val.sub_total / 100);
                                            var ds,d = '';
                                            if(val.discount_type != '')
                                            {	
                                                ds += '<tr>';
                                                ds += '<th colspan="3">Less Discount</th>';
                                                ds += '</tr>';
                                                $('.discounts_name').html(ds);
                                                if(val.discount_type == 'co')
                                                {
                                                    d += '<tr>';
                                                    d += '<th>Others C/O Sir '+val.discount_co+'</th>';
                                                    d += '<th>'+val.discount_rate+' %</th>';
                                                    d += '<th style="text-align:right">'+parseFloat(parseInt(dis))+'</th>';
                                                    d += '</tr>';
                                                }
                                                if(val.discount_type == 'senior-citizen')
                                                {
                                                    d += '<tr>';
                                                    d += '<th>Senior Citizen</th>';
                                                    d += '<th>'+val.discount_rate+' %</th>';
                                                    d += '<th style="text-align:right">'+parseFloat(parseInt(dis))+'</th>';
                                                    d += '</tr>';
                                                }
                                                if(val.discount_promo_code == 'promo')
                                                {
                                                    d += '<tr>';
                                                    d += '<th>Promotion</th>';
                                                    d += '<th>'+val.discount_promo_code+'</th>';
                                                    d += '<th>Free 12pcs cupcakes</th>';
                                                    d += '</tr>';
                                                }

                                                $('.discounts').html(d);
                                            }
                                            //for total price
                                            var ph = '';
                                            ph += '<tr>';
                                            ph += '<th colspan="2">Total Amount</th>';
                                            ph += '<th>'+val.total_price_amount+'</th>';
                                            ph += '</tr>';
                                            $('.tmt').html(ph);
                                        });
                                    });
                                });
                            }
                            else if(data['status'] == 3)
                            {
                                var message = '<div class="alert alert-success"><h1 style="color: #fff">do the magic <span class="fa fa-thumbs-up"></span></h1></div>';
                                $('#message').html(message);
                            }
                            else if(data['status'] == 0){
                                var message = '<div class="alert alert-danger"><strong>PLease complete this fields.</strong><p>'+data['message']+'</p></div>';
                                $('#message').html(message);
                                $('.panel-refresh-layer').hide();
                                $('.page-content').css("height","auto");
                            }
                            else{
                               var message = '<div class="alert alert-danger"><strong>Server Error.</strong><p>Please Contact Software Developer <b></b></p></div>';
                                $('#message').html(message);
                            }	
                        }
                    });
                }
                else if($(this).attr('data-identity') == 'catalogue')
                {
                    $.ajax({
                        url 	: JO.base_url+'jobs/ajax_function/insert',
                        type 	: "POST",
                        cache 	: "false",
                        data 	: formData,
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        dataType : "json",
                        beforeSend : function(){
                            $('#message').html('<div style="width: 100%;height: 100%" class="panel-refresh-layer"><img src="img/loaders/default.gif"/></div>');
                        },
                        success : function(data){
                            var order_id = data['order_id'];
                            if(data['status'] == 1)
                            {
                                $('#modal4').modal('show');
                                $.getJSON(JO.base_url+'jobs/ajax_function/get/rec/'+order_id+'/order_data',function(data){
                                var a,c = '';
                                    $.each(data, function(key, val){
                                        c += '<tr colspan="2"><th>JO#: '+val.branch_code+'-'+val.id+'</th>';
                                        c += '<th>'+val.total_product_price+'</th><tr>';
                                        $('.order_summarys').html(c);
                                        a += '<tr>';
                                        a += '<th>Title Code</th>';
                                        a += '<th colspan="2">'+val.product_code+'</th>';
                                        a += '</tr>';
                                        a += '<tr>';
                                        a += '<th colspan="2">Icing</th>';
                                        a += '<th>'+val.icing_type+'</th>';
                                        a += '</tr>';
                                        a += '<tr>';
                                        a += '<th>Size</th>';
                                        a += '<th colspan="2">'+val.size+'</th>';
                                        a += '</tr>';
                                        a += '<tr>';
                                        a += '<th>Flavor</th>';
                                        a += '<th colspan="2">'+val.flavor+'</th>';
                                        a += '</tr>';
                                        $('#modal4 .modal-body .container_fluid .order_summarys2').html(a);

                                        var b,d,s,ds,ph,p = '';
                                        if(val.delivery_charge != 0)
                                        {
                                            b += '<tr>';
                                            b += '<th colspan="2">Delivery Charge:</th>';
                                            b += '<th>'+val.delivery_charge+'</th>';
                                            b += '</tr>';
                                            $('.charges').html(b);
                                        }

                                        var dis = val.discount_rate * val.sub_total / 100;
                                            s += '<tr>';
                                            s += '<th colspan="2">Sub Total</th>';
                                            s += '<th>'+val.sub_total+'</th>';
                                            s += '</tr>';
                                            $('.subtotals').html(s);
                                        if(val.discount_type != '')
                                        {	
                                            ds += '<tr>';
                                            ds += '<th colspan="3">Less Discount</th>';
                                            ds += '</tr>';
                                            $('.discounts_name').html(ds);
                                            if(val.discount_type == 'co')
                                            {
                                                d += '<tr>';
                                                d += '<th>Others C/O Sir '+val.discount_co+'</th>';
                                                d += '<th>'+val.discount_rate+' %</th>';
                                                d += '<th>'+parseFloat(parseInt(dis))+'</th>';
                                                d += '</tr>';
                                            }
                                            if(val.discount_type == 'senior-citizen')
                                            {
                                                d += '<tr>';
                                                d += '<th>Senior Citizen</th>';
                                                d += '<th>'+val.discount_rate+' %</th>';
                                                d += '<th>'+parseFloat(parseInt(dis))+'</th>';
                                                d += '</tr>';
                                            }
                                            if(val.discount_promo_code == 'promo')
                                            {
                                                d += '<tr>';
                                                d += '<th>Promotion</th>';
                                                d += '<th>'+val.discount_promo_code+'</th>';
                                                d += '<th>Free 12pcs cupcakes</th>';
                                                d += '</tr>';
                                            }
                                            $('.discounts').html(d);
                                        }
                                        ph += '<tr>';
                                        ph += '<th colspan="2">Total Amount</th>';
                                        ph += '<th>'+val.total_price_amount+'</th>';
                                        ph += '</tr>';
                                        $('#modal4 .modal-body .container_fluid .tmt').html(p);

                                        p += '<tr>';
                                        p += '<th colspan="2">Payment Amount</th>';
                                        p += '<th>'+val.amount_paid+'</th>';
                                        p += '</tr>';
                                        p += '<tr>';
                                        p += '<th colspan="2"> Payment Type </th>';
                                        p += '<th>'+val.payment_type+'</th>';
                                        p += '</tr>';
                                        p += '<tr>';
                                        p += '<th colspan="2"> Mode of Payment </th>';
                                        p += '<th>'+val.mode_of_payment+'</th>';
                                        p += '</tr>';
                                        p += '<tr>';
                                        p += '<th colspan="2"> Amount Paid </th>';
                                        p += '<th>'+val.total_price_amount+'</th>';
                                        p += '</tr>';
                                        p += '<tr>';
                                        p += '<th colspan="2">Change</th>';
                                        p += '<th>'+val.amount_change+'</th>';
                                        p += '</tr>';

                                        $('#modal4 .modal-body .container_fluid .ors').html(p);
                                    });
                                });
                            }
                            else if(data['status'] == 3)
                            {
                                var message = '<div class="alert alert-success"><h1 style="color: #fff">do the magic <span class="fa fa-thumbs-up"></span></h1></div>';
                                $('#message').html(message);
                            }
                            else if(data['status'] == 0)
                            {
                                var message = '<div class="alert alert-danger"><strong>PLease complete this fields.</strong><p>'+data['message']+'</p></div>';
                                $('#message').html(message);
                                $('.panel-refresh-layer').hide();
                                $('.page-content').css("height","auto");
                            }
                            else
                            {
                                var message = '<div class="alert alert-danger"><strong>Server Error.</strong><p>Please Contact Software Developer <b></b></p></div>';
                                $('#message').html(message);
                            }	
                        }
                    });
                }
                else
                {
                    console.log('No action required!');
                }
            });	
        }
    };

    //get
    var getItemConf = {
        //click function prototype
        getItem : function(){
            return this.delegate(JO.get_item,'click',function(){
                var identity = $(this).attr('data-identity');
                var id = $(this).attr('data-id');
                if(identity === '')
                {
                        alert('getItemConf test');
                }
                else
                {
                        console.log('No action required!');
                        alert('No action required!');
                }
            });
        },

        //for onchage function prototype
        getItemDropdown : function(){
            return this.delegate(JO.get_item_dropdown,'change',function(){
                var identity = $(this).attr('data-identity');
                var id = $(this).val();
                if($(this).attr('data-push') == 'cake')
                {
                    var input = $(this).attr('data-input');
                    var price = $(this).attr('data-price');
                    var functarget = $(this).attr('data-get');
                    if($(this).attr('data-oc') == "true")
                    {
                        $(input).val(id);
                        if(input == '.cake_category')
                        {
                            $('.s_category').html(id);
                        }
                    }
                    else
                    {
                        $.getJSON(JO.base_url+'jobs/ajax_function/get/'+functarget+'/'+id,function(data){
                            if(data == false)
                            {
                                if(input == '.cake_item') $(input).val('');
                                if(input == '.cake_order_set'){
                                    $(input).val('');
                                    $('.cake_quantity').val('1');
                                }
                                if(input == '.cake_design') $(input).val('');
                                if(input == '.cake_size') $(input).val('');
                                if(input == '.cake_flavor') $(input).val('');
                                $(price).val(val.price);
                                custom_computation('cus');
                            }
                            else
                            {
                                $.each(data, function(key,val){
                                    if(input == '.cake_item')
                                    {
                                        $(input).val(val.type);
                                        $('.s_item').html(val.type);
                                    }
                                    if(input == '.cake_order_set')
                                    {
                                        $(input).val(val.name);
                                        $('.s_order_set').html(val.name);
                                        if(val.name == 'Cookies' || val.name == 'cookies' || val.name == 'cookie')
                                        {
                                            $('.cake_quantity').val(6);
                                        }
                                        else
                                        {
                                            $('.cake_quantity').val(1);
                                        }
                                    }
                                    if(input == '.cake_design')
                                    {
                                        $(input).val(val.name);
                                        $('.s_design').html(val.name);
                                    }
                                    if(input == '.cake_size')
                                    {
                                        $(input).val(val.name);
                                        $('.s_size').html(val.name);
                                    }
                                    if(input == '.cake_flavor')
                                    {
                                        $(input).val(val.name);
                                        $('.s_flavor').html(val.name);
                                    }
                                    $(price).val(val.price);
                                });
                                var all = 0;
                                var item = parseInt($('.cake_item_price').val());
                                var order_set = parseInt($('.cake_order_set_price').val());
                                var design = parseInt($('.cake_design_price').val());
                                var size = parseInt($('.cake_size_price').val());
                                var flavor = parseInt($('.cake_flavor_price').val());
                                
                                if(isNaN(item) || item == 0 || item  == '') item  = 0;
                                if(isNaN(order_set) || order_set == 0 || order_set  == '') order_set  = 0;
                                if(isNaN(design) || design == 0 || design  == '') design  = 0;
                                if(isNaN(size) || size == 0 || size  == '') size  = 0;
                                if(isNaN(flavor) || flavor == 0 || flavor  == '') flavor  = 0;
                                custom_computation('cus');
                             }
                        });
                    }
                }
                else
                {
                    var input = $(this).attr('data-input');
                    $(input).val('');
                }
                if(identity == 'priority_charge')
                {
                    var startDate = new Date(); // current
                    var endDate =   new Date(id); // end to
                    var nDays = diffDays(startDate, endDate);
                    var dep = $('.item').val();
                    if($('.item').length && dep != '')
                    {
                        $.getJSON(JO.base_url+'jobs/ajax_function/get/priority_charge/'+dep,function(data){
                            $.each(data, function(key, val){
                                if(val.days >= nDays)
                                {
                                    $('.priority_charge').val(val.rush_charge);
                                    //final computation
                                    custom_computation2();
                                }else $('.priority_charge').val(0);
                            });
                        });
                    }
                    else if($('.product_icing').val() != '')
                    {
                        $.getJSON(JO.base_url+'jobs/ajax_function/get/priority_charge2/'+$('.product_icing').val(),function(data){
                            $.each(data, function(key, val){
                                if(val.days >= nDays)
                                {
                                    $('.priority_charge').val(val.rush_charge);
                                    custom_computation2();
                                }else $('.priority_charge').val(0);
                            });
                        });
                    }
                    else $('.priority_charge').val(0);
                }
                else if(identity == 'get_flavor')
                {
                    $('.hmm').html('<div style="width: 100%;height: 100%" class="panel-refresh-layer"><img src="img/loaders/default.gif"/></div>');
                    //get flavor
                    $.getJSON(JO.base_url+'jobs/ajax_function/get/flavor/'+id+'/'+$(this).attr('data-caketype'),function(data){
                        $('.hmm').html('&nbsp;');
                        if(data == false)
                        {
                            $('.d').remove();
                            $('.cake_flavor').val('');
                            $('.cake_flavor_price').val('');
                            $('.ssflavor').hide();
                        }
                        else
                        {
                            $('.ssflavor').show();
                            var d = '';
                            d += '<div class="form-group d">';
                            d += '<label>Flavor</label>';
                            d += '<select name="flavor" class="form-control flavor get-item-dropdown" data-price=".cake_flavor_price" data-get="g_flavor" data-push="cake" data-input=".cake_flavor">';
                            d += '<option value="">Select</option>';
                            $.each(data,function(key, val){
                                d += '<option value="'+val.id+'">'+val.name+'</option>';
                            });
                            d +='</select>';
                            d += '</div>';
                            $('.fc').html(d);
                            $('.cake_flavor').val('');
                            $('.cake_flavor_price').val('');
                        }
                    });
                    
                    //get addons
                    $.getJSON(JO.base_url+'jobs/ajax_function/get/addon/'+id,function(data){
                        $('.hmm').html('&nbsp;');
                        if(data == false)
                        {
                            $('.e').remove();
                            $('.ssadn').hide();
                            $('.addons').hide();
                        }
                        else
                        {
                            $('.ssadn').show();
                            $('.addons').show();
                            var e = '';
                            e += '<div class="form-group e">';
                            e += '<label>Add-Ons</label>';
                            e += '<div class="form-horizontal">';
                            e += '<table>';
                            $.each(data,function(key, val){
                                e += '<tr>';
                                e += '<td style="width: 180px"><label><input name="addons[]" data-name="'+val.name+'" data-target="quantity_'+val.id+'" data-id="'+val.id+'" data-identity="addons" data-price="'+val.price+'" value="'+val.id+'" type="checkbox" class="addons_'+val.id+' get_click">&nbsp;'+val.name+'&nbsp;</label></td>';
                                e += '<td>';
                                e += '<div class="form-group">';
                                e += '<input type="text" disabled="disabled" name="quantity_'+val.id+'" class="form-control '+val.id+'_quantity live-change" data-id="'+val.id+'" data-to-show=".adn_price_'+val.id+'" data-identity="addons" data-price="'+val.price+'" placeholder="Quantity"><br>';
                                e += '</div>';
                                e += '</td>';
                                e += '</tr>';
                            });
                            e += '</table>';
                            e += '<input type="text" class="q_count" style="display: none" name="q_count" value="'+data.length+'" />';
                            e += '</div>';
                            e += '</div>';
                            $('.adn').html(e);
                        }
                    });
                    
                }
                else if(identity == 'order_set')
                {
                    if(id != '')
                    {
                        $.getJSON(JO.base_url+'jobs/ajax_function/get/cake_id/'+id,function(data){
                            $.each(data, function(key, val){
                                //get icing_type
                                $('.hmm').html('<div style="width: 100%;height: 100%" class="panel-refresh-layer"><img src="img/loaders/default.gif"/></div>');
                                $('.e,.d').remove();
                                $('.cake_item').val('');
                                $('.cake_item_price').val('');
                                $.getJSON(JO.base_url+'jobs/ajax_function/get/icing_type/'+val.id,function(data){
                                    $('.hmm').html('&nbsp;');
                                    if(data == false)
                                    {
                                        $('.a').remove();
                                        $('.cake_order_set').val(val.name);
                                        $('.ssorderset').hide();
                                    }
                                    else 
                                    {
                                        $('.ssorderset').show();
                                        var a = '';
                                        a += '<div class="form-group a">';
                                        a += '<label>Item</label>';
                                        a += '<select name="item" class="form-control item get-item-dropdown" data-price=".cake_item_price" data-caketype="'+val.id+'" data-get="g_icing_type" data-input=".cake_item" data-push="cake" data-identity="get_flavor">';
                                        a += '<option value="">Select</option>';
                                        $.each(data,function(key, val){
                                            a += '<option value="'+val.id+'">'+val.type+'</option>';
                                        });
                                        a +='</select>';
                                        a += '</div>';
                                        $('.ct').html(a);
                                        $('.cake_order_set').val(val.name);
                                    }
                                });
                                //get cake design
                                $.getJSON(JO.base_url+'jobs/ajax_function/get/design/'+val.id,function(data){
                                    $('.hmm').html('&nbsp;');
                                    if(data == false)
                                    {
                                        $('.b').remove();
                                        $('.cake_design').val('');
                                        $('.cake_design_price').val('');
                                        $('.ssdesign').hide();
                                    }
                                    else
                                    {
                                        $('.ssdesign').show();
                                        var b = '';
                                        b += '<div class="form-group b">';
                                        b += '<label>Design</label>';
                                        b += '<select name="design" class="form-control design get-item-dropdown" data-price=".cake_design_price" data-get="g_design" data-input=".cake_design" data-push="cake">';
                                        b += '<option value="">Select</option>';
                                        $.each(data,function(key, val){
                                            b += '<option value="'+val.id+'">'+val.name+'</option>';
                                        });
                                        b +='</select>';
                                        b += '</div>';
                                        $('.ds').html(b);
                                        $('.cake_design').val('');
                                        $('.cake_design_price').val('');
                                    }
                                });
                                //get sizes
                                $.getJSON(JO.base_url+'jobs/ajax_function/get/size/'+val.id,function(data){
                                    $('.hmm').html('&nbsp;');
                                    if(data == false)
                                    {
                                        $('.c').remove();
                                        $('.cake_size').val('');
                                        $('.cake_size_price').val('');
                                        $('.sssize').hide();
                                    }
                                    else
                                    {
                                        $('.sssize').show();
                                        var c = '';
                                        c += '<div class="form-group c">';
                                        c += '<label>Size</label>';
                                        c += '<select name="size" class="form-control size get-item-dropdown" data-get="g_size" data-price=".cake_size_price" data-push="cake" data-input=".cake_size">';
                                        c += '<option value="">Select</option>';
                                        $.each(data,function(key, val){
                                            c += '<option value="'+val.id+'">'+val.name+'</option>';
                                        });
                                        c += '</select>';
                                        c += '</div>';
                                        $('.sc').html(c);
                                        $('.cake_size').val('');
                                        $('.cake_size_price').val('');
                                    }
                                });
                                
                                //get cake upgrade
                                $.getJSON(JO.base_url+'jobs/ajax_function/get/cake_upgrade/'+val.id,function(data){
                                    $('.hmm').html('&nbsp;');
                                    if(data == false)
                                    {
                                        $('.f').remove();
                                        $('.cake_size').val('');
                                        $('.cake_size_price').val('');
                                        $('.cake_upgrade').hide();
                                    }
                                    else
                                    {
                                        $('.cake_upgrade').show();
                                        var f = '';
                                        f += '<div class="form-group f">';
                                        f += '<br>';
                                        f += '<br>';
                                        f += '<label>Cake Upgrade</label>';
                                        f += '<div class="col-md-12" style="padding-bottom: 20px">';
                                        f += '<div class="form-horizontal">';
                                        $.each(data,function(key, val){
                                            f += '<label class="col-lg-7">&nbsp;<input name="cake_upgrade" value="'+val.name+'" class="cake_upgrade'+val.id+' get_click" data-identity="cake_upgrade" data-price="'+val.price+'" type="radio">&nbsp;&nbsp;'+val.name+'&nbsp;<br></label>';
                                        });
                                        f += '</div>';
                                        f += '</div>';
                                        f += '</div>';
                                        $('.cu').html(f);
                                        $('.cake_size').val('');
                                        $('.cake_size_price').val('');
                                    }
                                });
                            });
                        });
                    }
                    else
                    {
                        
                        custom_computation('cus');
                        var z = '';
                        $('.cake_design').val('');
                        $('.cake_size').val('');
                        $('.cake_flavor').val('');
                        $('.cake_item').val('');
                        $('.cake_order_set').val('');
                        $('.a').html(z);
                        $('.ds').html(z);
                        $('.sc').html(z);
                        $('.fc').html(z);
                        $('.adn').html(z);
                        $('.cu').html(z);
                        console.log('Are you out crazy nigga!');
                    }
                }
                else if(identity == 'celebrant')
                {
                    if(id == 'Birthday'){
                        var a = '<div id="birthday" class="form-group col-md-6">';
                        a += '<label>Celebrant Name</label>';
                        a += '<input type="text" name="celebrant" class="form-control celebrant">';                                 
                        a += '</div>';
                        $(a).insertAfter('.oc');
                    }
                    else
                    {
                        $('#birthday').remove();
                    }
                }
                else if(identity == 'catalogue_products')
                {
                    $.ajax({
                        url : JO.base_url+'jobs/ajax_function/get/'+identity+'/'+id+'',
                        type : 'POST',
                        data : {'id':id,'identity':identity},
                        dataType : 'json',
                        beforeSend : function(){
                            $('.onclass').show().html('<div style="width: 100%;height: 100%" class="panel-refresh-layer"><img src="img/loaders/default.gif"/></div>');
                        },
                        success : function(data){
                            if(data === false)
                            {
                                var table = '<h4>No data for this catalogue design!</h4>';
                                $('.catalogue_design_table_false').show().html(table);
                                $('.catalogue_design_table').hide();
                                $('.onclass').hide();
                            }
                            else
                            {
                                var table= '<thead>';
                                table += '<tr>';
                                table += '<th>Image</th>';
                                table += '<th>Title</th>';
                                table += '<th>Decription</th>';
                                table += '<th>Price</th>';
                                table += '</tr>';
                                table += '</thead>';
                                table += '<tbody>';
                                $.each(data, function(key, val){
                                    table += '<tr>';
                                    table += '<td>';
                                    table += '<a class="gallery-item" href="assets/images/users/avatar.jpg" data-gallery>';
                                    //real image val.image_filename
                                    table += '<img style="width: 150px" class="img-responsive" src="assets/images/users/avatar.jpg"></td>';
                                    table += '</a>';
                                    table += '<td>'+val.product_name+'</td>';
                                    table += '<td><button type="button" class="btn btn-info show-item" data-id="'+val.id+'" data-description="'+val.product_description+'" data-image="'+val.image_filename+'" data-identity="catalogue_description" data-toggle="modal" data-target="#modal1">View</button></td>';
                                    table += '<td><button type="button" class="btn btn-info show-item" data-pname="'+val.product_name+'" data-id="'+val.id+'" data-category="'+val.category_id+'" data-identity="catalogue_price" data-toggle="modal" data-target="#modal2">View</button></td>';
                                    table += '</tr>';
                                });
                                table += '</tbody>';
                                $('.catalogue_design_table').show().html(table);
                                $('.catalogue_design_table_false').hide();
                                 $('.onclass').hide();
                            }
                            $('.page-content').css("height","auto");
                        }
                    });
                }
                else if(identity == 'collection_type')
                {
                    $('.dp').html('<div style="width: 100%;height: 100%" class="panel-refresh-layer"><img src="img/loaders/default.gif"/></div>');
                    if($('.claim_type').val() == 'pickup')
                    {
                        $('.del_charge').hide();
                        $.getJSON(JO.base_url+'jobs/ajax_function/get/branches',function(data){
                            var a = '<div id="pickup" class="panel-body">';
                            a += '<h3>Pickup</h3>';
                            a += '<div class="form-group col-sm-4">';
                            a += '<label>Pickup Date</label>';
                            a += '<input name="pickup_date" type="date" class="form-control pickup_date datepicker get-item-dropdown" data-identity="priority_charge">';
                            a += '</div>';
                            a += '<div class="form-group col-sm-6">';
                            a += '<div class="col-md-6">';
                            a += '<label>Pickup Time</label>';
                            a += '<select name="pickup_time" class="form-control pickup_time col-sm-6">';
                            a += '<option value="">Set Time</option>';
                            for(var i=1;i<=12;i++)
                            {
                                a += '<option value="'+i+'">'+i+':00</option>';
                            }
                            a += '</select>';
                            a += '</div>';
                            a += '<div class="col-md-6">';
                            a += '<label>&nbsp;</label>';
                            a += '<select name="pickup_time_stat" class="form-control pickup_date_stat col-sm-6">';
                            a += '<option value="">Set Time Stat</option>';
                            a += '<option value="AM">AM</option>';
                            a += '<option value="PM">PM</option>';
                            a += '</select>';
                            a += '</div>';
                            a += '</div>';
                            a += '<div class="form-group col-sm-6">';
                            a += '<label>Pickup Branch</label>';
                            a += '<select name="pickup_branch" class="form-control">';
                            a += '<option value="" selected="selected">Select Branch</option>';
                            $.each(data, function(key, val){
                                a += '<option value="'+val.branch_id+'">'+val.branch_name+'</option>';
                            });
                            a += '</select>';
                            a += '</div>';

                            a += '</div>';
                            $('.dp').html(a);
                        });
                    }
                    else if($('.claim_type').val() == 'delivery')
                    {
                        $.getJSON(JO.base_url+'jobs/ajax_function/get/locations',function(data){
                            var  a = '<div id="delivery" class="panel-body">';
                            var now = new Date();
                            a += '<h3>Delivery</h3>';
                            a += '<div class="form-group col-sm-4">';
                            a += '<label>Delivery Date</label>';
                            a += '<input name="delivery_date" type="date" class="form-control delivery_date datepicker get-item-dropdown" data-identity="priority_charge">';
                            a += '</div>';
                            a += '<div class="form-group col-sm-6">';
                            a += '<div class="col-md-6">';
                            a += '<label>Delivery Time</label>';
                            a += '<select name="delivery_time" class="form-control delivery_time col-sm-6">';
                            a += '<option value="">Set Time</option>';
                            for(var i=1;i<=12;i++)
                            {
                                a += '<option value="'+i+'">'+i+':00</option>';
                            }
                            a += '</select>';
                            a += '</div>';
                            a += '<div class="col-md-6">';
                            a += '<label>&nbsp;</label>';
                            a += '<select name="delivery_time_stat" class="form-control col-sm-6 delivery_time_stat">';
                            a += '<option value="">Set Time Stat</option>';
                            a += '<option value="AM">AM</option>';
                            a += '<option value="PM">PM</option>';
                            a += '</select>';
                            a += '</div>';
                            a += '</div>';
                            a += '<div class="form-group col-sm-6">';
                            a += '<label>Location</label>';
                            a += '<select name="delivery_location" class="form-control delivery_location get-item-dropdown" data-identity="collection_charge">';
                            a += '<option value="" selected="selected">Select Location</option>';
                            $.each(data, function(key, val){
                                a += '<option value="'+val.location_name+'">'+val.location_name+'</option>';
                            });
                            a += '</select>';
                            a += '</div>';
                            a += '<div class="form-group col-sm-6">';
                            a += '<label>Complete Address</label>';
                            a += '<textarea name="delivery_complete_address" class="form-control delivery_complete_address"></textarea>';
                            a += '</div>';
                            a += '<div class="form-group col-sm-6">';
                            a += '<label>Contact Person</label>';
                            a += '<input name="delivery_contact_person" type="text" class="form-control delivery_contact_person" />';
                            a += '</div>';
                            a += '<div class="form-group col-sm-6">';
                            a += '<label>Contact No.</label>';
                            a += '<input name="delivery_contact_no" type="text" class="form-control delivery_contact_no" />';
                            a += '</div>';
                            a += '</div>';
                            $('.dp').html(a);
                        });
                    }else{
                        a = '';
                        $('.dp').html(a);
                    }
                }
                else if(identity == 'collection_charge')
                {
                    $.ajax({
                        url : JO.base_url+'jobs/ajax_function/get/'+identity+'/'+id+'',
                        type : 'POST',
                        data : {'id':id,'identity':identity},
                        dataType : 'json',
                        success : function(data){
                            if(data == false)
                            {
                                $('.delivery_charge').val(0);
                                $('.del_charge').hide();
                                $('.d_charge').html(0);
                            }
                            else
                            {
                                $.each(data, function(key, val){
                                $('.delivery_charge').val(val.delivery_charge);
                                $('.del_charge').show();
                                $('.d_charge').html(val.delivery_charge);
                                });
                                //final computation
                                custom_computation2();
                            }
                        }

                    });
                }
                else if(identity == 'discount')
                {
                    if(id == 2)
                    {
                       //$('.discount_value').val($('.co_discount').val());
                       return false;
                    }
                    else
                    {
                        if($('.discount_type').val() == 'co')
                        {
                            var q = '';
                            q +='<tr>';
                            q +='<td>Others: </td>';
                            q +='<td class="c_by"></td>';
                            q +='<td class="d_cm"></td>';
                            q +='<td class="d_mount"></td>';
                            q +='<tr>';
                            $('.dis_class').show().html(q);
                            $('.dis_charge').show();
                            $('.discount_value').val(0);
                            
                            //final computation
                            custom_computation2();
                        }
                        else if($('.discount_type').val() == 'promo')
                        {
                            var o = '';
                            o +='<tr>';
                            o +='<td>Promo: </td>';
                            o +='<td class="p_no"></td>';
                            o +='<td class="p_detail" colspan="2"></td>';
                            o +='<tr>';
                            $('.dis_class').show().html(o);
                            $('.dis_charge').show();
                            $('.discount_value').val(0);
                            
                            //final computation
                            custom_computation2();
                        }
                        else if($('.discount_type').val() == 'senior-citizen')
                        {
                            $.ajax({
                                url : JO.base_url+'jobs/ajax_function/get/'+identity+'/'+id+'',
                                type : 'POST',
                                data : {'id':id,'identity':identity},
                                dataType : 'json',
                                success : function(data){
                                    $.each(data, function(key, val){
                                        $('.discount_value').val(val.percentage);
                                        var diss = '';
                                        diss +='<tr>';
                                        diss +='<td colspan="2">Senior Citizen: </td>';
                                        diss +='<td>'+val.percentage+' %</td>';
                                        diss +='<td class="d_mount"></td>';
                                        diss +='</tr>';
                                        $('.dis_class').show().html(diss);
                                        $('.dis_charge').show();
                                        //final computation
                                        custom_computation2();
                                    });
                                }
                            });
                        }
                        else
                        {
                            $('.dis_class,.dis_charge').hide();
                        }
                        
                    }    
                }
                else
                {
                    console.log('No action required!');
                    return false;
                }
            });
        }
    };

    //update
    var updateItemConf = {
            updateItem : function(){
                return this.delegate(JO.update_item,'click',function(){
                    var identity = $(this).attr('data-identity');
                    var id = $(this).attr('data-id');
                    if(identity === '')
                    {
                        alert('updateItem test');
                    }
                    else
                    {
                        console.log('No action required!');
                        alert('No action required!');
                    }
                });
            }
    };

    //show
    var showItemConf = {
        showItem : function(){
            return this.delegate(JO.show_item,'click',function(){
                var identity = $(this).attr('data-identity');
                var id = $(this).attr('data-id');
                if(identity == 'catalogue_description')
                {
                    var abc = '<div class="row">';
                        abc += '<div class="col-md-4">';
                        //real image $(this).attr('data-image')
                        abc += '<a class="gallery-item" href="assets/images/users/avatar.jpg" data-gallery>';
                        abc += '<img style="300px" clas="img-resposive" src="assets/images/users/avatar.jpg" />';
                        abc += '</a>';
                        abc += '</div>';
                        abc += '<div class="col-md-7 col-md-offset-1">Description : <h2>'+$(this).attr('data-description')+'</h2></div>';
                        abc += '</div>';
                        
                        $('#modal1 .modal-body').html(abc);
                        $('.panel-refresh-layer').hide();
                }
                else if(identity == 'catalogue_price')
                {
                    var product_name = $(this).attr('data-pname');
                    $.ajax({
                        url : JO.base_url+'jobs/ajax_function/get/'+identity+'/'+$(this).attr('data-category')+'',
                        type : 'GET',
                        data : {
                            'category_id': $(this).attr('data-category'),
                            'identity' : identity
                        },
                        dataType : 'json',
                        cache : false,
                        beforeSend : function(){
                            $('#modal1 .modal-body').html('<div style="width: 100%;height: 100%" class="panel-refresh-layer"><img src="img/loaders/default.gif"/></div>');
                        },
                        success : function(data){
                            if(data == false)
                            {
                                var cba = '<h3>No data found!</h3>';
                                $('#modal2 .modal-body').html(cba);
                                $('.panel-refresh-layer').hide();
                            }
                            else
                            {
                                var cba = '<table class="table table-responsive table-bordered">';
                                    cba += '<thead>';
                                    cba += '<tr>';
                                    cba += '<th>Item Category</th>';
                                    cba += '<th>Size</th>';
                                    cba += '<th>Flavor</th>';
                                    cba += '<th>Price</th>';
                                    cba += '<th>Action</th>';
                                    cba += '</tr>';
                                    cba += '</thead>';
                                    cba += '<tbody>';
                                    //cba += '<input type="text" class="product_count" value="'+data.length+'" style="display: none">';
                                $.each(data, function(key, val){
                                var s_count = $('.countme_'+val.product_id+'').length;
                                    cba += '<tr>';
                                    cba += '<th>'+val.c_name+'</th>';
                                    cba += '<th>'+val.s_name+'</th>';
                                    cba += '<th>'+val.f_name+'</th>';
                                    cba += '<th>'+currencyFormat(parseFloat(val.price))+'</th>';
                                    cba += '<th>';
                                    cba += '<button type="button" class="btn btn-default selected-item all-but" data-picing="'+val.i_price+'" data-pflavor="'+val.f_price+'" data-psize="'+val.s_price+'" data-pcode="'+val.title_code+'" data-pname="'+product_name+'" data-id="'+val.product_id+'" data-price="'+val.price+'" data-icing="'+val.i_type+'" data-size="'+val.s_name+'" data-flavor="'+val.f_name+'" ></span> Select </button>';
                                    //cba += '&nbsp;<button type="button" class="btn btn-default ps_count_'+val.product_id+' sp"><b>'+s_count+'</b></button>';
                                    //cba += '&nbsp;<button type="button" class="btn btn-default minus_count sp" data-id="'+val.product_id+'" data-price'+val.product_id+'="'+val.price+'" ><span class="fa fa-refresh"></span> Reset</button>';
                                    //cba += '<input type="text" class="total_price_'+val.product_id+'" style="display: none">';
                                    cba += '</th>';
                                    cba += '</tr>';
                                });
                                cba += '</tbody>';
                                cba += '</table>';
                                cba += '<div class="show_sp">';
                                cba += '</div>';
                                $('#modal2 .modal-body').html(cba);
                                $('.panel-refresh-layer').hide();
                            }
                        }
                    });
                }
                else
                {
                    console.log('No action required!');
                    alert('No action required!');
                }
            });
        }
    };
    //selection product buttons function
    var selectedItemConf = {
        selectedItem : function(){
            return this.delegate(JO.selected,'click',function(){
                var id = $(this).attr('data-id');
                var name = $(this).attr('data-pname');
                var code = $(this).attr('data-pcode');
                var icing = $(this).attr('data-icing');
                var size = $(this).attr('data-size');
                var flavor = $(this).attr('data-flavor');
                var price = $(this).attr('data-price');
                
                var picing = $(this).attr('data-picing');
                var psize = $(this).attr('data-psize');
                var pflavor = $(this).attr('data-pflavor');
                
                $('.total_amount,.total_product_price').val(price);
                $('.s_amount').html(price);
                
                $('.product_id').val(id);
                $('.product_name').val(name);
                $('.product_code').val(code);
                $('.product_icing').val(icing);
                $('.product_size').val(size);
                $('.product_flavor').val(flavor);
                $('.product_price').val(price);
                $('.icing_price').val(picing);
                $('.size_price').val(psize);
                $('.flavor_price').val(pflavor);
                $('.cake_quantity').val(1);
                
                $('.s_code').html(code);
                $('.s_icing').html(icing);
                $('.s_size').html(size);
                $('.s_flavor').html(flavor);
                
                
                var abc = '<div class="row">';
                abc += '<div class="col-md-12">';
                //real image $(this).attr('data-image')
                abc += '<table class="table table-bordered">';
                abc += '<thead>';
                abc += '<tr>';
                abc += '<th>Image</th>';
                abc += '<th>Name</th>';
                abc += '<th>Icing Type</th>';
                abc += '<th>Size</th>';
                abc += '<th>Flavor</th>';
                abc += '<th>Price</th>';
                abc += '</tr>';
                abc += '</thead>';
                abc += '<tbody>';
                abc += '<tr>';
                abc += '<th><a class="gallery-item" href="assets/images/users/avatar.jpg" data-gallery>';
                abc += '<img style="300px" clas="img-responsive" src="assets/images/users/avatar.jpg" />';
                abc += '</a></th>';
                abc += '<th>'+name+'</th>';
                abc += '<th>'+icing+'</th>';
                abc += '<th>'+size+'</th>';
                abc += '<th>'+flavor+'</th>';
                abc += '<th>'+price+'</th>';
                abc += '</thead>';
                abc += '</table>';
                $('.show_sp').html(abc);
                
                var a = '';
                a += '<tr>';
                a += '<th><a class="gallery-item" href="assets/images/users/avatar.jpg" data-gallery>';
                a += '<img style="300px" clas="img-responsive" src="assets/images/users/avatar.jpg" />';
                a += '</a></th>';
                a += '<th>'+name+'</th>';
                a += '<th>'+icing+'</th>';
                a += '<th>'+size+'</th>';
                a += '<th>'+flavor+'</th>';
                a += '<th>'+price+'</th>';
                a += '</tr>';
                $('.p_info').html(a);
                
                custom_computation('cat');
            });	
        },
        
        save_print : function(){
            return this.delegate(JO.save_print,'click',function(){
                if($(this).attr('data-stat') == 0)
                {
                   alert('Kindly print the reciept before exit this window!');
                }
                else
                {
                    window.location.href = ''+$(this).attr('data-url')+'';
                }
            });
        },
        print_res : function(){
            return this.delegate(JO.print_res,'click',function(){
                $(JO.save_print).attr('data-stat',1);
                var divContents = $('#modal4 .modal-body').html();
                var con = $('body').html();
                var printWindow = window.open('', '', 'height=900,width=1000');
                    printWindow.document.write('<html>');
                    printWindow.document.write('<body >');
                    printWindow.document.write(divContents);
                    printWindow.document.write('</body>');
                    printWindow.document.write('</html>');
                    printWindow.document.close();
                    printWindow.print();
             });
        },
        
        get_click : function(){
            return this.delegate(JO.get_click,'click',function(){
                var value = $(this).val();
                var id = $(this).attr('data-id');
                var identity  = $(this).attr('data-identity');
                var quantity  = $(this).attr('data-quantity');
                var name  = $(this).attr('data-name');
                var price = $(this).attr('data-price');
                if(identity == 'addons')
                {
                    if($(this).prop('checked')) 
                    {
                        var addons = '<tr id="a_'+id+'">';
                            addons += '<td>'+name+'</td>';
                            addons += '<td colspan="2" class="adn_quantity_'+id+'"></td>';
                            addons += '<td style="text-align: right" class="adn_price_'+id+'"></td>';
                            addons += '</tr>';
                        $('.ssadn').show();
                        $('.addons').show();
                        $('.addons').prepend(addons);
                        $('.'+id+'_quantity').removeAttr('disabled');
                        $('#a_undefined').remove();
                        
                        var adn_r_data = '<tr id="ab_'+id+'">';
                            adn_r_data += '<td>';
                            adn_r_data += '<input type="int" name="adnrdprice" value="'+price+'" class="adnrdprice_'+id+'">';
                            adn_r_data += '</td>';
                            adn_r_data += '<td>';
                            adn_r_data += '<input type="int" name="adnrdqty" class="adnrdqty_'+id+'">';
                            adn_r_data += '</td>';
                            adn_r_data += '<td>';
                            adn_r_data += '<input type="int" name="adnrdtot" class="adnrealdprice adnrdtot_'+id+'">';
                            adn_r_data += '</td>';
                            adn_r_data += '<td>';
                            adn_r_data += '<input type="int" name="adnrdid" value="'+id+'" class="adnrdid_'+id+'">';
                            adn_r_data += '</td>';
                            adn_r_data += '</tr>';
                        $('.adn_data').append(adn_r_data);
                        $('.claim_type, .discount_type').val('');
                    }
                    else
                    {
                        $('.'+id+'_quantity').attr('disabled','disabled');
                        var t = parseInt($('.'+id+'_quantity').val()) * parseInt($('.'+id+'_quantity').attr('data-price'));
                        var lol = parseInt($('.total_amount').val()) - parseInt(t);
                        $('.total_amount').val(lol);
                        $('.'+id+'_quantity').val('');
                        $('#a_'+id+'').remove();
                        $('#ab_'+id+'').remove();
                        $('#a_undefined').remove();
                        
                        var tots = 0;
                        $('.adnrealdprice').each(function(){
                            tots += parseInt($(this).val());
                        });
                        $('.total_addons_price').val(tots);
                        
                        custom_computation('cus');
                    }
                }
                else if(identity == 'cake_upgrade')
                {
                    $('.s_upgrade').html(value);
                    $('.s_upgrade_price').html(currencyFormat(parseInt(price)));
                    $('.s_cake_upgrades').show();
                    
                    $('.total_upgrade_price').val(price);
                    custom_computation('cus');
                }
                else
                {
                    console.log("Error in getting price or name of this addons or cake upgrade.");
                }
            });
        }
    };
    
    var liveConf = {
        liveChange : function(){
            return this.delegate(JO.live_change,'keyup',function(e){
                var identity = $(this).attr('data-identity');
                var value = $(this).val();
                var id = $(this).attr('data-id');
                if(identity == 'addons')
                {
                    if(value == '' || isNaN(value)) value = 0;
                    var tot_price = parseInt(value) * parseInt($(this).attr('data-price'));
                    $($(this).attr('data-to-show')).html(tot_price);
                    $('.adn_quantity_'+id+'').html(value);
                    $('.adnrdqty_'+id+'').val(value);
                    $('.adnrdtot_'+id+'').val(tot_price);
                    
                    var tots = 0;
                    $('.adnrealdprice').each(function(){
                        tots += parseInt($(this).val());
                    });
                    $('.total_addons_price').val(tots);
                    custom_computation('cus');
                }
                else if($(this).attr('data-push') == 'cake')
                {
                    $($(this).attr('data-input')).val($(this).val());
                    $('.s_title').html($(this).val());
                }
                else if(identity == 'des')
                {
                    $('.must_1').val('');
                    $('.action_select').removeClass('btn-danger');
                    $('.action_select').addClass('btn-info');
                    $('.action_select').attr('data-identity','select-customer');
                    $('.action_select').html('<span class="fa fa-check"></span> Select');
                }
                //pay custom
                else if(identity == 'payment_customized')
                {
                    var a = Math.round(parseInt($('.total_price_amount').val())) - Math.round(parseInt($('.amount_to_paid').val()));
                    var b = Math.round(parseInt($('.amount_recieved').val())) - Math.round(parseInt($('.amount_to_be_paid').val()));
                    var amount_to_paid = parseInt($('.amount_to_paid').val());
                    var total_amount = Math.round(parseInt($('.total_amount').val()));
                    $('.amount_change').val(currencyFormat(parseFloat(Math.abs(b))));
                    $('.show_change').html(currencyFormat(parseFloat(Math.abs(b))));
                    if(isNaN(amount_to_paid) || amount_to_paid == ''){
                        $('.show_change').html(currencyFormat(parseFloat(0)));
                        $('.show_balance').html(currencyFormat(parseFloat(0)));
                        $('.amount_paid').val(0.00);
                        $('.amount_change').val(0.00);
                        $('.amount_balance').val(0.00);
                    }
                    else if(a >= 0)
                    {
                        $('.show_change').html(currencyFormat(parseFloat(0)));
                        $('.show_balance').html(currencyFormat(parseFloat(a)));
                        $('.amount_paid').val(amount_to_paid);
                        $('.amount_change').val(0.00);
                        $('.amount_balance').val(a);
                    }
                    else
                    { 
                        $('.show_change').html(currencyFormat(parseFloat(Math.abs(b))));
                        $('.show_balance').html(currencyFormat(parseFloat(0)));
                        $('.amount_paid').val(amount_to_paid);
                        $('.amount_change').val(parseFloat(Math.abs(b)));
                        $('.amount_balance').val(0.00);
                    } 
                    
                    if( parseInt($(this).val()) < 0 )
                    {
                        alert('Cannot be less than 0 ( Zero )');
                    }
                    else
                    {
                       console.log('wtf is going on nigga!');
                    }
                }
                //pay catalogue
                else if(identity == 'payment_catalogue')
                {
                    var a = Math.round(parseInt($('.total_price_amount').val())) - Math.round(parseInt($('.amount_to_paid').val()));
                    var b = Math.round(parseInt($('.amount_recieved').val())) - Math.round(parseInt($('.amount_to_be_paid').val()));
                    var amount_to_paid = parseInt($('.amount_to_paid').val());
                    var total_amount = Math.round(parseInt($('.total_amount').val()));
                    $('.amount_change').val(currencyFormat(parseFloat(Math.abs(b))));
                    $('.show_change').html(currencyFormat(parseFloat(Math.abs(b))));
                    if(isNaN(amount_to_paid) || amount_to_paid == ''){
                        $('.show_change').html(currencyFormat(parseFloat(0)));
                        $('.show_balance').html(currencyFormat(parseFloat(0)));
                        $('.amount_paid').val(0.00);
                        $('.amount_change').val(0.00);
                        $('.amount_balance').val(0.00);
                    }
                    else if(a >= 0)
                    {
                        $('.show_change').html(currencyFormat(parseFloat(0)));
                        $('.show_balance').html(currencyFormat(parseFloat(a)));
                        $('.amount_paid').val(amount_to_paid);
                        $('.amount_change').val(0.00);
                        $('.amount_balance').val(a);
                    }
                    else
                    { 
                        $('.show_change').html(currencyFormat(parseFloat(Math.abs(b))));
                        $('.show_balance').html(currencyFormat(parseFloat(0)));
                        $('.amount_paid').val(amount_to_paid);
                        $('.amount_change').val(parseFloat(Math.abs(b)));
                        $('.amount_balance').val(0.00);
                    } 
                    
                    if( parseInt($(this).val()) < 0 )
                    {
                        alert('Cannot be less than 0 ( Zero )');
                    }
                    else
                    {
                       console.log('wtf is going on nigga!');
                    }
                }
                else if(identity == 'change')
                {
                    var a = Math.round(parseInt($(this).val())) - Math.round(parseInt($('.amount_to_be_paid').val()));
                    if(isNaN($(this).val()) || $(this).val() == '' || $(this).val() == 0){
                        $('.show_change').html(currencyFormat(parseFloat(0)));
                        $('.amount_change').val(currencyFormat(parseFloat(0)));
                    }
                    else
                    { 
                        $('.show_change').html(currencyFormat(parseFloat(Math.abs(a))));
                        $('.amount_change').val(currencyFormat(parseFloat(Math.abs(a))));
                    } 
                    
                    if( parseInt($(this).val()) < 0 )
                    {
                        alert('Cannot be less than 0 ( Zero )');
                    }
                    else
                    {
                       console.log('wtf is going on nigga!');
                    }
                }
                else if(identity == 'promo')
                {
                    $('.p_no').html('PN#' +$(this).val());
                    $('.p_detail').html($('.promo_detail').val());
                }
                else if(identity == 'co_discount')
                {
                    $('.discount_value').val($(this).val());
                    //final computation
                    custom_computation2();
                    $('.c_by').html('C/O By Sir '+ $('.co_by').val());
                    $('.d_cm').html($(this).val()+ ' %');
                }
                   
                else
                {
                   console.log('No action required!');
                }
            });
        }
    };
    var actionSelectConf = {
        actionSelect : function(){
            return this.delegate(JO.action_select,'click',function(){
                function openWin()
                {
                    myWindow = window.open('','','width=900,height=600');
                    myWindow.document.write("<p>A test Window prepare for printing reciept.<button onclick='javascript:window.print()'>print me test</button></p>");
                    myWindow.focus();
                    //print(myWindow);
                }
                //openWin();
                var id = $(this).attr('data-id');
                var identity = $(this).attr('data-identity');
                if(identity == 'select-customer' && $('.must_1').val() == '')
                {
                    $('.must_1').val(1);
                    $(this).attr('data-identity','deselect-customer');
                    $(this).removeClass('btn-info');
                    $(this).addClass('btn-danger');
                    $(this).html('<span class="fa fa-circle-o"></span> Cancel');
                    $('.selected_customer').val(id);
                    
                    $.getJSON(JO.base_url+'jobs/ajax_function/get/getExsistCustomerData/'+id,function(data){
                        $.each(data, function(key, val){
                            $('.firstname').val(val.firstname);
                            $('.lastname').val(val.lastname);
                            $('.contact').val(val.contact);
                            $('.email').val(val.email);
                            $('.address').val(val.address);
                            $('.location').val(val.location);
                        });
                    });
                }
                else if(identity == 'deselect-customer' && $('.must_1').val() == '1')
                {
                    $('.must_1').val('');
                    $(this).attr('data-identity','select-customer');
                    $(this).removeClass('btn-danger');
                    $(this).addClass('btn-info');
                    $(this).html('<span class="fa fa-check"></span> Select');
                    $('.selected_customer').val(id);
                    $('.firstname').val('');
                    $('.lastname').val('');
                    $('.contact').val('');
                    $('.email').val('');
                    $('.address').val('');
                    $('.location').val('');
                }
                else
                {
                   /*if(confirm("System detected that you already select a customer in this list, would you like to reset selection ?"))
                   {
                        $('.must_1').val(1);
                        $('.action_select').$(this).removeClass('btn-danger');
                        $(this).attr('data-identity','deselect-customer');
                        $(this).removeClass('btn-info');
                        $(this).addClass('btn-danger');
                        $(this).html('<span class="fa fa-circle-o"></span> Cancel');
                        $('.selected_customer').val(id);
                   }
                   else
                   {*/
                    console.log('Lasing kaba brad ?');
                    return false;
                   //}
                }
            });
        },
        //customized reciept
        showRec : function(){
            return this.delegate(JO.showRes,'click',function(){
                var jo_id = $(this).attr('data-id');
                if($('.payment_type').val() == '' || $('.payment_mode').val() == '' || $('.amount_to_paid').val() == '')
                {
                    alert('all fields are required to filled up!');
                }
                else
                {
                   $.ajax({
                        url : JO.base_url+'jobs/ajax_function/insert/setpay/'+jo_id,
                        type : 'POST',
                        data : {
                            'total_amount': $('.total_amount').val(),
                            'total_price_amount': $('.total_price_amount').val(),
                            'amount_paid': $('.amount_to_paid').val(),
                            'amount_change': $('.amount_change').val(),
                            'amount_balance': $('.amount_balance').val(),
                            'payment_type': $("[name=payment_type]").val(),
                            'payment_mode': $("[name=payment_mode]").val(),
                            'sub_total': $('.sub_total').val()
                        },
                        cache : false,
                        dataType: "json",
                        success : function(data){
                            $('#paymentModal').modal('hide');
                            $('#modal4').modal('show');
                            $.getJSON(JO.base_url+'jobs/ajax_function/get/rec/'+jo_id+'/getlastAc',function(datas){
                                var p;
                                $.each(datas, function(key, val){
                                    p += '<tr>';
                                    p += '<th colspan="2">Payment Amount</th>';
                                    p += '<th>'+val.amount_paid+'</th>';
                                    p += '</tr>';
                                    p += '<tr>';
                                    p += '<th colspan="2"> Payment Type </th>';
                                    p += '<th>'+val.payment_type+'</th>';
                                    p += '</tr>';
                                    p += '<tr>';
                                    p += '<th colspan="2"> Mode of Payment </th>';
                                    p += '<th>'+val.mode_of_payment+'</th>';
                                    p += '</tr>';
                                    p += '<tr>';
                                    p += '<th colspan="2"> Amount Paid </th>';
                                    p += '<th>'+val.total_price_amount+'</th>';
                                    p += '</tr>';
                                    p += '<tr>';
                                    p += '<th colspan="2">Change</th>';
                                    p += '<th>'+val.amount_change+'</th>';
                                    p += '</tr>';

                                    $('#modal4 .ors').html(p);
                                });
                            });
                        }
                    });
                }
            });
        }
        
    };
    
    var openModalConf = {
        openModal : function(){
            return this.delegate(JO.open_modal,'click',function(){
                var id = $(this).attr('data-id');
                var identity = $(this).attr('data-identity');
                var modal = $(this).attr('data-modal');
                $(''+modal+'').modal('show');
                if(identity == 'get_customers_from_list')
                {
                    //$('#customer_select').DataTable().ajax.reload();
                    /*$('#customer_select').DataTable().ajax.reload( function ( json ) {
                        $('.ifselected_'+$('.selected_customer').val()+'').removeClass('btn-info');
                        $('.ifselected_'+$('.selected_customer').val()+'').attr('data-identity','deselect-customer');
                        $('.ifselected_'+$('.selected_customer').val()+'').addClass('btn-danger');
                        $('.ifselected_'+$('.selected_customer').val()+'').html('<span class="fa fa-circle-o"></span> Cancel');
                        $('.selected_customer').val($('.ifselected_'+$('.selected_customer').attr('data-id')+'').val());
                    } );*/
                    //if($('.must_1').val() == 1)
                   //{
                        //console.log($('.selected_customer').val());
                        //$('.ifselected_'+$('.selected_customer').val()+'').removeClass('btn-info');
                        //$('.ifselected_'+$('.selected_customer').val()+'').addClass('btn-danger');
                        //('.ifselected_'+$('.selected_customer').val()+'').html('<span class="fa fa-circle-o"></span> Cancel');
                    //}
                }
                else
                {
                   console.log('We di nga!');
                }
            });
        }
    };
    
    var closeModalConf = {
        closeModal : function(){
            return this.delegate(JO.close_modal,'click',function(){
                var id = $(this).attr('data-id');
                var identity = $(this).attr('data-identity');
                var modal = $(this).attr('data-modal');
                $(''+modal+'').modal('hide');
                $(''+modal+' input').val('');
                /*if(identity == 'get_customers_from_list')
                {
                    $('.wizard').bootstrapWizard('next');
                }
                else
                {
                    console.log('hey hey hey heeyyyyyyyy!!');
                }*/
            });
        }
    };
	
    //configure target variable
    //$.extend(JO.doc,test);
    $.extend(JO.doc,addItemConf);
    $.extend(JO.doc,getItemConf);
    $.extend(JO.doc,updateItemConf);
    $.extend(JO.doc,showItemConf);
    $.extend(JO.doc,selectedItemConf);
    $.extend(JO.doc,liveConf);
    $.extend(JO.doc,actionSelectConf);
    $.extend(JO.doc,openModalConf);
    $.extend(JO.doc,closeModalConf);
    
    //set target inside variable
    //JO.doc.testme();
    JO.doc.addItem();
    JO.doc.getItem();
    JO.doc.updateItem();
    JO.doc.showItem();
    JO.doc.getItemDropdown();
    JO.doc.selectedItem();
    JO.doc.save_print();
    JO.doc.print_res();
    JO.doc.liveChange();
    JO.doc.actionSelect();
    JO.doc.openModal();
    JO.doc.closeModal();
    JO.doc.showRec();
    JO.doc.get_click();
}(jQuery,window,document));