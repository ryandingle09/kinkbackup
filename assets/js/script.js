(function(){
	$(document).ready(function(){
		$('a[href="#"]').click(function(e){
			e.preventDefault();
		})
		index = function index(){
			$('input#dp-3').datepicker();

			$('.panel-collapse').click(function(e){
				if($(this).parents('.panel').hasClass('panel-toggled')){
					$(this).parents('.index').find('.panel.target').addClass('panel-toggled');
				}
				else{

				}
			})
		};
		pl = function pl(){
			$('input#dp-3').datepicker();
		};
		calendar = function calendar(){
			$('#calendars').fullCalendar();
		};
		jo = function jo(){
			$('.specialselect').change(function(e){
				$(this).parents('.panel').find('.panel-body.special').addClass('hide');
				$(this).parents('.panel').find('.panel-body#'+$(this).val()).removeClass('hide');
				var h = $('.wizard div'+$('.wizard a.selected').attr('href')+' .panel').height();
				$('.wizard .stepContainer').height(h);
			});
		};
	});
})(jQuery)